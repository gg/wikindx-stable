<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class importAmazonMessages_en
{
public $text;

	public function __construct()
	{
		$this->messages();
	}
// All headings, text hints etc.
	private function messages()
	{
		$this->text = array(
				"importAmazon"	=>	"Amazon Import",
				"heading"	=>	"Amazon Import",
				"url"	=>	"Enter the Amazon URL for the item you wish to import",
				"urlHint" => "https://....",
				"success"	=>	"Successfully added resource",
				"noAccessKey"	=>	"Missing Amazon access key.  Please read modules/importAmazon/README.txt",
				"noSecretAccessKey"	=>	"Missing Amazon secret access key.  Please read modules/importAmazon/README.txt",
				"noInput"	=>	"Missing input",
				"invalidURL1"	=>	"Invalid Amazon URL (unable to strip domain name)",
				"invalidURL2"	=>	"Invalid Amazon URL (unable to strip ISBN)",
				"notBook"	=>	"The resource is not a book",
				"resourceExists"	=>	"That title already exists.",
				"failure"	=>	"Import failed. Amazon reports: ###",
// You can only import from one Amazon region. Change this to match the region you added in config.php $productAdvertisingAPIEndpoint
				"region"	=>	"You can only import from the British Amazon website (amazon.co.uk)",
			);
	}
}
?>