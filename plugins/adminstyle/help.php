<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* ADMINSTYLE class -- help file
*
*
*/
class adminstyle_help
{

	public function __construct()
	{

		GLOBALS::setTplVar('heading', '');
	}
/**
* Print the about page
*/
	public function init()
	{
		$linkSfWikindx = HTML\a("link", "WIKINDX Sourceforge Project", "https://sourceforge.net/projects/wikindx/", "_blank");
		$pString = HTML\p("If you have WIKINDX admin rights, you can create and edit bibliographic styles for on-the-fly formatting when displaying or exporting bibliographic lists.");
		$pString .= HTML\p("These styles are stored as XML files each within its own directory in the styles/bibliography/ directory. This directory <strong>must</strong> be writeable by everyone or at least the web server user (usually user 'nobody'). Additionally, when editing an existing style, the XML style file within its named directory in the styles/bibliography/ directory <strong>must also</strong> be writeable by everyone or the web server user. As new bibliographic styles are created, the WIKINDX team will make these available on the $linkSfWikindx downloads site as plug-ins. Once you have a downloaded file, simply unzip the contents to the styles/bibliography/ directory.");
		$pString .= HTML\p("If you develop new styles yourself, you are strongly encouraged to contact the WIKINDX developers at $linkSfWikindx to make them available to other users.");
		$pString .= HTML\p("You can create a new style based on an existing one by copying the existing style. To remove a style from the list available to your users, disable that style in the Admin|Components menu.");
		$pString .= HTML\p("Please note, to edit a style, you should do it from the same browser window as you use to view a bibliographic list.  This is because, in order to save processing the style definition file each time you list a bibliography, WIKINDX will check to see if the style definition file has been edited and therefore needs reprocessing. This information is stored in a PHP session variable; each browser window has its own separate set of session variables with no cross-interrogation available. If you edit the style definition file from another browser window then you are unlikely to see changes when you refresh your bibliographic list.");

		$pString .= HTML\hr();

		$pString .= HTML\p("Each style has a set of options that define the heading style of titles, how to display numbers and dates etc. and then a separate style definition for each resource type that WIKINDX handles. The 'Short Name' is used by WIKINDX as both the folder and file name and for this reason should not only be a unique name within styles/bibliography/, but should also have no spaces or any other characters that may cause confusion with your operating system (i.e. alphanumeric characters only). The 'Long Name' is the description of the style that is displayed to WIKINDX users.");
		$pString .= HTML\p("The 'Editor switch' requires special attention. Some bibliographic styles require that for books and book chapters, where there exists an editor but no author, the position occupied by the author is taken by the editor. If you select 'Yes' here, you should then supply a replacement editor field. Please note that if the switch occurs, the editor(s) formatting will be inherited from the global settings you supplied for the author. See the examples below.");
		$pString .= HTML\p("The three 'generic style' definitions are required and are used to display any resource type for which there does not yet exist a style definition. This allows you to build up your style definitions bit by bit.  Furthermore, some bibliographic styles provide no formatting guidelines for particular types of resource in which case the generic styles will provide some formatting for those resources according to the general guidelines for that bibliographic style. Each resource for which there is no style definition will fall back to the chosen generic style. The generic styles try their best but if formatting is strange for a particular resource type then you should explicitly define a style definition for that type.");
		$pString .= HTML\p("Each style definition has a range of available fields listed to the right of each input box. These fields are <strong>case-sensitive</strong> and need not all be used. However, with some of the more esoteric styles, the more database fields that have been populated for each resource in the WIKINDX, the more likely it is that the formatting will be correct.");

		$pString .= HTML\hr();

		$pString .= HTML\h("SYNTAX");
		$pString .= HTML\p("The style definition syntax uses a number of rules and special characters:");
		$list = HTML\li("The character '|' separates fields from one another.");
		$list .= HTML\li("If a field does not exist or is blank in the database, none of the definition for that field is printed.");
		$list .= HTML\li(HTML\strong("Field names are case-sensitive") . '&nbsp;' . "and need not all be used.");
		$list .= HTML\li("Within a field, you can add any punctuation characters or phrases you like before and after the field name.");
		$list .= HTML\li("Any word that you wish to be printed and that is the same (even a partial word) as a field name should be enclosed in backticks '`'.");
		$list .= HTML\li("For creator lists (editors, revisers, directors etc.) and pages, alternative singular and plural text can be specified with '^' (e.g. |^p.^pp.^pages| would print the field 'pages' preceded by 'pp.' if there were multiple pages or 'p.' if not).");
		$list .= HTML\li("BBCode [u]..[/u], [i]..[/i], [b]..[/b], [sup]..[/sup] and [sub]..[/sub] can be used to specify underline, italics, bold, superscript and subscript.");
		$list .= HTML\li("The character '%' enclosing any text or punctuation <em>before</em> the field name states that that text or those characters will only be printed if the <em>preceeding</em> field exists or is not blank in the database. The character '%' enclosing any text or punctuation <em>after</em> the field name states that that text or those characters will only be printed if the <em>following</em> field exists or is not blank in the database. It is optional to have a second pair in which case the construct should be read 'if target field exists, then print this, else, if target field does not exist, print that'.  For example, '%: %' will print ': ' if the target field exists else nothing if it doesn't while '%: %. %' will print ': ' if the target field exists else '. ' if it does not.");
		$list .= HTML\li("Characters in fields that do not include a field name should be paired with another set and together enclose a group of fields. If these special fields are not paired unintended results may occur. These are intended to be used for enclosing groups of fields in brackets where <em>at least</em> one of the enclosed fields exists or is not blank in the database.");
		$list .= HTML\li("The above two rules can combine to aid in defining particularly complex bibliographic styles (see examples below). The pair <br><code>|%, %. %|xxxxx|xxxxx|%: %; %|</code><br> states that if at least one of the intervening fields exists, then the comma and colon will be printed; if an intervening field does not exist, then the full stop will be printed <em>only</em> if the <em>preceeding</em> field exists (else nothing will be printed) and the semicolon will be printed <em>only</em> if the <em>following</em> field exists (else nothing will be printed).");
		$list .= HTML\li("If the final set of characters in the style definition is '|.' for example, the '.' is taken as the ultimate punctuation printed at the very end.");
		$list .= HTML\li('Fields can be printed or not dependent upon the existence of preceding or subsequent fields. For example, <code><br>creator. |$shortTitle. $title. $|publicationYear.</code><br> would print the shortTitle field if the creator were populated otherwise it prints the title field.  <code><br>creator. |title. |#ISBN. ##|edition.</code><br> prints the ISBN field if edition exists otherwise it prints nothing.');
		$list .= HTML\li("Newlines may be added with the special string NEWLINE.");
		$pString .= HTML\ol($list);
		$pString .= HTML\p("Tip: In most cases, you will find it easiest to attach punctuation and spacing at the end of the preceding field rather than at the start of the following field. This is especially the case with finite punctuation such as full stops.");

		$pString .= HTML\hr();

		$pString .= HTML\h("EXAMPLES");
		$pString .=HTML\p("<code>" . "author. |publicationYear. |title. |In [i]book[/i], |edited by editor (^ed^eds^). |publisherLocation%:% |publisherName. |edition ed%,%.% |(Originally published originalPublicationYear) |^p.^pp.^pages|." . "</code>" . BR .
			HTML\em("might produce:") . "</code>" . BR . "<code>" . "de Maus, Mickey. 2004. An amusing diversion. In <em>A History of Cartoons</em>, Donald D. A. F. F. Y. Duck, and Bugs Bunny (eds). London: Animatron Publishing. 10th ed, (Originally published 2000) pp.20-9." . "</code>" );
		$pString .= HTML\p(HTML\em("and, if there were no publisher location or edition entered for that resource and only one page number given, it would produce:") . BR . "<code>" . "de Maus, Mickey. 2004. An amusing diversion. In <em>A History of Cartoons</em>, Donald D. A. F. F. Y. Duck, and Bugs Bunny (eds). Animatron Publishing. (Originally published 2000) p.20." . "</code>");
		$pString .= HTML\hr();
		$pString .= HTML\p("<code>" . "author. |[i]title[/i]. |(|publisherLocation%: %|publisherName%, %|publicationYear.|) |ISBN|." . "</code>" . BR .
			HTML\em("might produce:") . "</code>" . BR . "<code>" . "de Maus, Mickey. <em>A big book</em> (London: Animatron Publishing, 1999.) 1234-09876." . "</code>" );
		$pString .= HTML\p(HTML\em("and, if there were no publisher location or publication year entered for that resource, it would produce:") . BR . "<code>" . "de Maus, Mickey. <em>A big book</em>. (Animatron Publishing.) 1234-09876." . "</code>");
		$pString .= HTML\hr();
		$pString .= HTML\p("<code>" . "author. |publicationYear. |[i]title[/i]. |Edited by editor. |edition ed. |publisherLocation%:%.% |publisherName. |Original `edition`, originalPublicationYear|." . "</code>" . BR .
			HTML\em("might produce:") . "</code>" . BR . "<code>" . "Duck, Donald D. A. F. F. Y. 2004. <em>How to Make it Big in Cartoons</em>. Edited by M. de Maus and Goofy. 3rd ed. Selebi Phikwe: Botswana Books. Original edition, 2003." . "</code>" );
		$pString .= HTML\p(HTML\em("and, if there were no author entered for that resource and the replacement editor field were 'editor ^ed.^eds.^ ', it would produce:") . BR . "<code>" . "de Maus, Mickey and Goofy eds. 2004. <em>How to Make it Big in Cartoons</em>. 3rd ed. Selebi Phikwe: Botswana Books. Original edition, 2003." . "</code>");
		$pString .= HTML\hr();
		$pString .= HTML\p("Consider the following (IEEE-type) generic style definition and what it does with a resource type lacking certain fields:" . BR . "<code>" . "creator, |\"title,\"| in [i]collection[/i], |editor, ^Ed.^Eds.^, |edition ed|. publisherLocation: |publisherName, |publicationYear, |pp. pages|." . "</code>" . BR .
			HTML\em("might produce:") . BR . "<code>" . "ed Software, \"Mousin' Around,\". Gaborone: Computer Games 'r' Us, 1876." . "</code>" . BR .
			HTML\em("and, when applied to a resource type with editor and edition fields:") . BR . "<code>" . "Donald D. A. F. F. Y. de Duck, \"How to Make it Big in Cartoons,\"Mickey de Maus and Goofy, Eds., 3rd ed. Selebi Phikwe: Botswana Books, 2003." . "</code>");
		$pString .= HTML\p("Clearly there is a problem here, notably at the end of the resource title. The solution is to use rule no. 10 above:" . BR . "<code>" . "creator, |\"title|%,\" %.\" %|in [i]collection[/i]|%, %editor, ^Ed.^Eds.^|%, %edition ed|%. %|publisherLocation: |publisherName, |publicationYear, |pp. pages|." . "</code>" . BR .
			HTML\em("might produce:") . BR . "<code>" . "ed Software, \"Mousin' Around.\" Gaborone: Computer Games 'r' Us, 1876." . "</code>" . BR .
			HTML\em("and:") . BR . "<code>" . "Donald D. A. F. F. Y. de Duck, \"How to Make it Big in Cartoons,\" Mickey de Maus and Goofy, Eds., 3rd ed. Selebi Phikwe: Botswana Books, 2003." . "</code>");
		$pString .= HTML\p("Bibliographic styles requiring this complexity are few and far between.");

		$pString .= HTML\hr();

		$pString .= HTML\p("If the value entered for the edition of a resource contains non-numeric characters, then, despite having set the global setting for the edition format to ordinal (3rd. etc.), no conversion will take place.");
		$pString .= HTML\p("The formatting of the names, edition and page numbers and the capitalization of the title depends on the global settings provided for your bibliographic style.");

		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSEPOPUP::getInstance();
	}
}
?>