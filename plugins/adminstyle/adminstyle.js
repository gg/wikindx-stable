/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

This program is free software; you can redistribute it and/or modify it under the terms 
of the GNU General Public License as published by the Free Software Foundation; either 
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; 
if not, write to the 
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

The WIKINDX Team 2014
sirfragalot@users.sourceforge.net

**********************************************************************************/

var templatePrefix = 'style_';
var templateSuffix = '';

/**
* adminstyle.js
*/

function previewCite()
{
	templatePrefix = 'cite_';
// Check we have the required input
	if(!A_OBJ[gateway.aobj_index].checkInput(['targetDiv', 'script']))
		return false;
// Set the AJAX object's targetObj property
	A_OBJ[gateway.aobj_index].targetObj = coreGetElementById(A_OBJ[gateway.aobj_index].input.targetDiv);
	var retArray = new Array();
	ajaxReturn = citePreview();
	A_OBJ[gateway.aobj_index].processedScript = A_OBJ[gateway.aobj_index].input.script + ajaxReturn;
// Execute the AJAX
	if(!A_OBJ[gateway.aobj_index].doXmlHttp())
		return false;
	return true;
}

function previewTriggerFromMultiSelect(templateName, footnote = false)
{
	if(footnote && coreGetElementById('footnote_' + templateName + 'Template').value)
	{
		templatePrefix = 'footnote_';
		templateSuffix = 'Template';
	}
	else
	{
		templatePrefix = 'style_';
		templateSuffix = '';
	}
// Check we have the required input
	if(!A_OBJ[gateway.aobj_index].checkInput(['targetDiv', 'triggerField', 'script']))
		return false;
// Set the AJAX object's targetObj property
	A_OBJ[gateway.aobj_index].targetObj = coreGetElementById(A_OBJ[gateway.aobj_index].input.targetDiv);
// Gather the selected OPTIONS of the triggerField select box
	var triggerObj = coreGetElementById(A_OBJ[gateway.aobj_index].input.triggerField);
	var len = triggerObj.options.length;
	var ajaxReturn = '';
	var triggerOptionValues = new Array();
	for(var i = 0; i < len; i++)
	{
		if(!triggerObj.options[i].selected)
			triggerOptionValues.push(triggerObj.options[i].value);
	}
	if(triggerOptionValues.length)
		ajaxReturn = '&ajaxReturn=' + triggerOptionValues.join();
	var retArray = new Array();
	retArray = styleFootnoteCommon(templateName);
    var url = "&style=" + escape(retArray[0]);
	var templateString = coreGetElementById(templatePrefix + templateName + templateSuffix).value; 
	if(!templateString)
	{
		templateName = coreGetElementById('style_' + templateName + '_generic').value;
		var fallbackString = coreGetElementById(templatePrefix + templateName + templateSuffix).value;
	}
	else
		var fallbackString = '';
	ajaxReturn += url + retArray[1] + "&templateName=" + templateName + 
		"&templateString=" + escape(templateString) + "&fallbackString=" + escape(fallbackString);
	A_OBJ[gateway.aobj_index].processedScript = A_OBJ[gateway.aobj_index].input.script + ajaxReturn;
// Execute the AJAX
	if(!A_OBJ[gateway.aobj_index].doXmlHttp())
		return false;
	return true;
}

function transferField(textArea, select)
{
	selectToTextarea(textArea, select);
}

/**
* Transfer elements from source select box to target textarea if selected element's text does not exist in target area
*
* @param string Target object
* @param string Source object
*/
function selectToTextarea(target, source)
{
	var targetObj = coreGetElementById(target);
	var selection;
	var sourceObj = coreGetElementById(source);
	len = sourceObj.options.length;
	for(i = 0; i < len; i++)
	{
		selection = coreEntityDecode(coreUtf8.decode(coreDecode_base64(sourceObj.options[i].value)));
		if(sourceObj.options[i].selected)
			insertTextAtCaret(targetObj, selection);
	}
}

function getSelectionBoundary(el, start)
{
    var property = start ? "selectionStart" : "selectionEnd";
    var originalValue, textInputRange, precedingRange, pos, bookmark;

    if (typeof el[property] == "number")
    {
        return el[property];
    }
    else if (document.selection && document.selection.createRange)
    {
        el.focus();

        var range = document.selection.createRange();
        if (range)
        {
            // Collapse the selected range if the selection is not a caret
            if (document.selection.type == "Text")
            {
                range.collapse(!!start);
            }

            originalValue = el.value;
			if(typeof(el.createTextRange) === 'function')
			{
				textInputRange = el.createTextRange();
				precedingRange = el.createTextRange();
            }
            pos = 0;

            bookmark = range.getBookmark();
            textInputRange.moveToBookmark(bookmark);

            if (originalValue.indexOf("\r\n") > -1)
            {
                // Trickier case where input value contains line breaks

                // Insert a character in the text input range and use that
                //as a marker
                textInputRange.text = " ";
                precedingRange.setEndPoint("EndToStart", textInputRange);
                pos = precedingRange.text.length - 1;

                // Executing an undo command deletes the character inserted
                // and prevents adding to the undo stack.
                document.execCommand("undo");
            }
            else
            {
                // Easier case where input value contains no line breaks
                precedingRange.setEndPoint("EndToStart", textInputRange);
                pos = precedingRange.text.length;
            }
            return pos;
        }
    }
    return 0;
}

function offsetToRangeCharacterMove(el, offset)
{
    return offset - (el.value.slice(0, offset).split("\r\n").length - 1);
}

function setSelection(el, startOffset, endOffset)
{
	if(typeof(el.createTextRange) === 'function')
	    var range = el.createTextRange();
	else
		var range = el.createRange();
    var startCharMove = offsetToRangeCharacterMove(el, startOffset);
    range.collapse(true);
    if (startOffset == endOffset)
    {
        range.move("character", startCharMove);
    }
    else
    {
        range.moveEnd("character", offsetToRangeCharacterMove(el, endOffset));
        range.moveStart("character", startCharMove);
    }
    range.select();
}

function insertTextAtCaret(el, text)
{
    var pos = getSelectionBoundary(el, false);
    var newPos = pos + text.length;
    var val = el.value;
    el.value = val.slice(0, pos) + text + val.slice(pos);
    setSelection(el, newPos, newPos);
}

/*********************************************************************************/


/**
* rewrite creators for style and footnote previews - common code
* 
* @Author Mark Grimshaw with a lot of help from Christian Boulanger
*/
function rewriteCreatorsPreview(templateName, styleArray)
{
	var creatorArray = new Array();
	var currFormField;
	var fieldName;
	var arrayKey;
	var key;
    var a_php = "";
    var total = 0;
    templateName = 'style_' + templateName;
// Add rewrite creator strings for the resource type
	var creators = new Array("creator1", "creator2", "creator3", "creator4", "creator5");
	for (index = 0; index < creators.length; index++)
	{
		arrayKey = creators[index] + "_firstString";
		fieldName = templateName + "_" + creators[index] + "_firstString";
		currFormField = document.forms[0][fieldName];
		if(typeof(currFormField) != 'undefined')
			creatorArray[arrayKey] = currFormField.value; // input and textarea
		
		arrayKey = creators[index] + "_firstString_before";
		fieldName = templateName + "_" + creators[index] + "_firstString_before";
		currFormField = document.forms[0][fieldName];
		if ((typeof(currFormField) != 'undefined') && currFormField.checked) // checkbox
			creatorArray[arrayKey] = "on";
			
		arrayKey = creators[index] + "_remainderString";
		fieldName = templateName + "_" + creators[index] + "_remainderString";
		currFormField = document.forms[0][fieldName];
		if(typeof(currFormField) != 'undefined')
			creatorArray[arrayKey] = currFormField.value; // input and textarea
		
		arrayKey = creators[index] + "_remainderString_before";
		fieldName = templateName + "_" + creators[index] + "_remainderString_before";
		currFormField = document.forms[0][fieldName];
		if ((typeof(currFormField) != 'undefined') && currFormField.checked) // checkbox
			creatorArray[arrayKey] = "on";
			
		arrayKey = creators[index] + "_remainderString_each";
		fieldName = templateName + "_" + creators[index] + "_remainderString_each";
		currFormField = document.forms[0][fieldName];
		if ((typeof(currFormField) != 'undefined') && currFormField.checked) // checkbox
			creatorArray[arrayKey] = "on";
	}
    for (key in creatorArray)
    {
        ++ total;
		escapeKey = escape(String(styleArray[key]).replace(/ /g, '__WIKINDX__SPACE__'));
		escapeKey = escape(String(creatorArray[key]));
		if(escapeKey.match(/%u/)) // unicode character so length changes
		{
			a_php = a_php + "s:" +
				String(key).length + ":\"" + String(key) + "\";s:" +
				escapeKey.length + ":\"" + escapeKey + "\";";
		}
		else
		{
			a_php = a_php + "s:" +
                String(key).length + ":\"" + String(key) + "\";s:" +
                String(creatorArray[key]).length + ":\"" + String(creatorArray[key]) + "\";";
		}
    }
    a_php = "a:" + total + ":{" + a_php + "}";
    return "&rewriteCreator=" + escape(a_php);
}
/**
* pop-up window for style and footnote previews - common code
* 
* @Author Mark Grimshaw with a lot of help from Christian Boulanger
*/
function styleFootnoteCommon(templateName)
{
	var fieldArray = new Array ("style_titleCapitalization", "style_primaryCreatorFirstStyle", 
			"style_primaryCreatorOtherStyle", "style_primaryCreatorInitials", 
			"style_primaryCreatorFirstName", "style_otherCreatorFirstStyle", 
			"style_otherCreatorOtherStyle", "style_otherCreatorInitials", "style_dayFormat", 
			"style_otherCreatorFirstName", "style_primaryCreatorList", "style_otherCreatorList",
			"style_primaryCreatorListAbbreviationItalic", "style_otherCreatorListAbbreviationItalic", 
			"style_monthFormat", "style_editionFormat", "style_primaryCreatorListMore", 
			"style_primaryCreatorListLimit", "style_dateFormat", 
			"style_primaryCreatorListAbbreviation", "style_otherCreatorListMore", 
			"style_runningTimeFormat", "style_primaryCreatorRepeatString", "style_primaryCreatorRepeat", 
			"style_otherCreatorListLimit", "style_otherCreatorListAbbreviation", "style_pageFormat", 
			"style_editorSwitch", "style_editorSwitchIfYes", "style_primaryCreatorUppercase", 
			"style_otherCreatorUppercase", "style_primaryCreatorSepFirstBetween", 
			"style_primaryCreatorSepNextBetween", "style_primaryCreatorSepNextLast", 
			"style_otherCreatorSepFirstBetween", "style_otherCreatorSepNextBetween", 
			"style_otherCreatorSepNextLast", "style_primaryTwoCreatorsSep", "style_otherTwoCreatorsSep", 
			"style_userMonth_1", "style_userMonth_2", "style_userMonth_3", "style_userMonth_4", 
			"style_userMonth_5", "style_userMonth_6", "style_userMonth_7", "style_userMonth_8", 
			"style_userMonth_9", "style_userMonth_10", "style_userMonth_11", "style_userMonth_12", 
			"style_userMonth_13", "style_userMonth_14", "style_userMonth_15", "style_userMonth_16", 
			"style_dateRangeDelimit1", "style_dateRangeDelimit2", "style_dateRangeSameMonth", 
			"style_dateMonthNoDay", "style_dateMonthNoDayString", "style_dayLeadingZero", "style_localisation", 
			"style_titleSubtitleSeparator"
		);
	var fieldName;
	var currFormField;
	var styleArray = new Array ();
	for (index = 0; index < fieldArray.length; index++)
	{
		currFormField = document.forms[0][fieldArray[index]];
		if ((currFormField.type == "checkbox") && currFormField.checked)
			styleArray[fieldArray[index]] = "on"; // checkbox
		else if (currFormField.type != "checkbox")
			styleArray[fieldArray[index]] = currFormField.value; // input and textarea
    }
// rewrite creator array
	var creatorQuery = rewriteCreatorsPreview(templateName, styleArray);
// style definition array
    var a_php = "";
    var total = 0;
	var key;
	var escapeKey;
    for (key in styleArray)
    {
		++ total;
		escapeKey = escape(String(styleArray[key]).replace(/ /g, '__WIKINDX__SPACE__'));
		if(escapeKey.match(/%u/)) // unicode character so length changes
		{
			a_php = a_php + "s:" +
				String(key).length + ":\"" + String(key) + "\";s:" +
				escapeKey.length + ":\"" + escapeKey + "\";";
		}
		else
		{
			a_php = a_php + "s:" +
				String(key).length + ":\"" + String(key) + "\";s:" +
				String(styleArray[key]).length + ":\"" + String(styleArray[key]) + "\";";
		}
    }
    a_php = "a:" + total + ":{" + a_php + "}";
	return new Array(a_php, creatorQuery);
}

/**
* pop-up window for in-text citation previews
* 
* @Author Mark Grimshaw with a lot of help from Christian Boulanger
*/
function citePreview()
{
 var objectReturn = new coreBrowserDimensions();
 var w = objectReturn.browserWidth * 0.95;
 var h = objectReturn.browserHeight * 0.2;
	var fieldArray = new Array (
			"cite_creatorStyle", "cite_creatorOtherStyle", 
			"cite_creatorInitials", "cite_creatorFirstName", 
			"cite_useInitials", "cite_creatorUppercase", "cite_twoCreatorsSep", 
			"cite_creatorSepFirstBetween", "cite_creatorSepNextBetween", "cite_creatorSepNextLast",
			"cite_creatorList", "cite_creatorListMore", 
			"cite_creatorListLimit", "cite_creatorListAbbreviation", "cite_creatorListAbbreviationItalic", 
			"cite_creatorListSubsequent", "cite_creatorListSubsequentMore", 
			"cite_creatorListSubsequentLimit", "cite_creatorListSubsequentAbbreviation", 
			"cite_firstChars", "cite_lastChars", "cite_template", 
			"cite_replaceYear", "cite_followCreatorTemplate", "cite_followCreatorPageSplit", 
			"cite_consecutiveCitationSep", "cite_consecutiveCreatorSep", "cite_consecutiveCreatorTemplate", 
			"cite_subsequentCreatorTemplate", "cite_pageFormat", 
			"cite_yearFormat", "cite_titleCapitalization", "cite_titleSubtitleSeparator", 
			"cite_ambiguous", "cite_ambiguousTemplate", "cite_subsequentCreatorRange", "cite_subsequentFields", 
			"cite_removeTitle"
		);
	var arrayKey;
	var fieldName;
	var currFormField;
	var citeArray = new Array ();
	var creatorArray = new Array ();
	for (index = 0; index < fieldArray.length; index++)
	{
		currFormField = document.forms[0][fieldArray[index]];
		if ((currFormField.type == "checkbox") && currFormField.checked)
			citeArray[fieldArray[index]] = "on"; // checkbox
		else if (currFormField.type != "checkbox")
			citeArray[fieldArray[index]] = currFormField.value; // input and textarea
    }
// rewrite creator array
    var a_php = "";
// style definition array
    a_php = "";
    total = 0;
	var key;
	var escapeKey;
    for (key in citeArray)
    {
		++ total;
		escapeKey = escape(String(citeArray[key]).replace(/ /g, '__WIKINDX__SPACE__'));
		if(escapeKey.match(/%u/)) // unicode character so length changes
		{
			a_php = a_php + "s:" +
				String(key).length + ":\"" + String(key) + "\";s:" +
				escapeKey.length + ":\"" + escapeKey + "\";";
		}
		else
		{
			a_php = a_php + "s:" +
				String(key).length + ":\"" + String(key) + "\";s:" +
				String(citeArray[key]).length + ":\"" + String(citeArray[key]) + "\";";
		}
    }
    a_php = "a:" + total + ":{" + a_php + "}";
	return "&cite=" + escape(a_php);
//	var popUp = window.open(url,'winMeta','height=' + h + 
//		',width=' + w + ',left=10,top=10,status,scrollbars,resizable,dependent');
//		document.write(url);
}