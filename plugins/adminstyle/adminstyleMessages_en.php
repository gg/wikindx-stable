<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
*	ADMINSTYLE plugin -- English messages.
*
*****/
class adminstyleMessages_en
{
public $text;

// Constructor
	public function __construct()
	{
		$this->loadMessages();
	}
	private function loadMessages()
	{
		$this->text = array(
/**
* Menu items
*/
				'pluginSub' =>	'Styles...',
				'addStyle'	=>	'Create Style',
				'copyStyle'	=>	'Copy Style',
				'editStyle'	=>	'Edit Style',
				"shortName"		=>	"Short Name",
				"longName"		=>	"Long Name",
/**
* General
*/
				"successEdit"	=>	"Successfully edited style",
				"successAdd"	=>	"Successfully added style",
/**
* Styles
*/
				"primaryCreatorSep"	=>	"Primary creator delimiters",
				"otherCreatorSep"	=>	"Other creator delimiters",
				"ifOnlyTwoCreators"	=>	"If only two creators",
				"creatorSepBetween"	=>	"between",
				"creatorSepLast"	=>	"before last",
				"sepCreatorsFirst"	=>	"Between first two creators",
				"sepCreatorsNext"	=>	"Between following creators",
				"primaryCreatorStyle" 	=>	"Primary creator style",
				"otherCreatorStyle"	=>	"Other creator styles",
				"creatorFirstStyle" 	=>	"First",
				"creatorOthers"		=>	"Others",
				"creatorInitials"	=>	"Initials",
				"creatorFirstName"	=>	"First name",
				"creatorFirstNameFull"	=>	"Full",
				"creatorFirstNameInitials"	=>	"Initial",
				"primaryCreatorList"	=>	"Primary creator list abbreviation",
				"otherCreatorList"	=>	"Other creator list abbreviation",
				"creatorListFull"	=>	"Full list",
				"creatorListLimit"	=>	"Limit list",
// The next 3 surround form text boxes: "If xx or more creators, list the first xx and abbreviate with xx".  For example: "If 4 or more creators, list the first 1 and abbreviate with ,et. al"
				"creatorListIf"		=>	"If",
				"creatorListOrMore"		=>	"or more creators, list the first",
				"creatorListAbbreviation"	=>	"and abbreviate with",
				"titleCapitalization"	=>	"Title capitalization",
// Title as entered with no changes to capitalization
				"titleAsEntered"	=>	"As entered",
				"availableFields"	=>	"Available fields:",
				"availableFieldsBib"	=>	"Available fields (Bibliography)",
				"availableFieldsFoot"	=>	"Available fields (Footnote)",
				"disableFields"		=>	"Disable fields",
				"editionFormat"		=>	"Edition format",
				"monthFormat"		=>	"Month format",
				"dateFormat"		=>	"Date format",
				"dayFormat"		=>	"Day format",
// Add a leading zero to day if less than 10.
				"dayLeadingZero"	=>	"Add leading zero",
				"pageFormat"		=>	"Page format",
// Length of film, broadcast etc.
				"runningTimeFormat"	=>	"Running time format",
// When displaying a book that has no author but has an editor, do we put the editor in the position occupied by the author?
				"editorSwitchHead"		=>	"Editor switch",
				"editorSwitch"		=>	"For books with no author but an editor, put editor in author position",
				"yes"			=>	"Yes",
				"no"			=>	"No",
				"editorSwitchIfYes"	=>	"If 'Yes', replace editor field in style definitions with",
// Uppercase creator names?
				"uppercaseCreator"	=>	"Uppercase all names",
// For repeated creator names in next bibliographic item
				"repeatCreators"	=>	"For works immediately following by the same creators",
				"repeatCreators1"	=>	"Print the creator list",
				"repeatCreators2"	=>	"Do not print the creator list",
				"repeatCreators3"	=>	"Replace creator list with text below",
// Fallback formatting style when a specific resource type has none defined
				"fallback"		=>	"Fallback style",
				"bibFormat"		=>	"Bibliography Formatting",
				"italics"		=>	"Italics",
// For user specific month naming
				"userMonthSelect"	=>	"Use month names defined below",
				"userMonths"	=>	"User-defined month names (all fields must be completed if selected above)",
				"userSeasons"	=>	"User-defined seasons (all fields must be completed if month format above is set to user-defined)",
// Date ranges for e.g. conferences
				"dateRange"		=>	"Date range",
				"dateRangeDelimit1"	=>	"Delimiter between start and end dates if day and month given",
				"dateRangeDelimit2"	=>	"Delimiter between start and end dates if month only given",
				"dateRangeSameMonth"	=>	"If start and end months are equal",
				"dateRangeSameMonth1"	=>	"Print both months",
				"dateRangeSameMonth2"	=>	"Print start month only",
// Different puncutation may be required if a month is given with no day.
				"dateMonthNoDay"	=>	"If a date has a month but no day",
				"dateMonthNoDay1"	=>	"Use style definition unchanged",
				"dateMonthNoDay2"	=>	"Replace date field in template with:",
// Don't translate 'date'
				"dateMonthNoDayHint"	=>	"(Use 'date' as the field)",
// Which language localization to use for ordinals and months.
				"language"	=>	"Localization",
// Re-write creator(s) portion of templates to handle styles such as DIN 1505.
				"rewriteCreator1"	=>	"Split creator lists and add strings",
				"rewriteCreator2"	=>	"Add string to first name in list",
				"rewriteCreator3"	=>	"Add string to remaining names",
				"rewriteCreator4"	=>	"Before name:",
				"rewriteCreator5"	=>	"To each name:",
				"bibTemplate"	=>	"Bibliography template",
// Don't translate 'pages'
				"footnotePageField"	=> "(Footnote template may also use 'pages')",
// Some general help for using templates displayed in the Admin|Style page
				"templateHelp1"	=>	"1. The three generic bibliography templates are required and will be used if a displayed resource has no bibliographic template.",
				"templateHelp2"	=>	"2. The footnote templates are only required for those styles that use footnotes for citations.  In all cases, the complete bibliography ('works cited') for footnote styles, as well as for endnote and in-text styles, uses the bibliography template.",
// Don't translate 'citation'
				"templateHelp3"	=>	"3. For footnote citations, the 'citation' field above refers to the footnote template or, if that does not exist, to the bibliography template or, if that does not exist, to the fallback style.",
// Don't translate 'pages'
				"templateHelp4"	=>	"4. The 'pages' field in the bibliography template refers to the complete article page range; in the footnote template, it refers to the specific citation page(s).",
				"templateHelp5"	=>	"5. If you define a footnote template for a resource you must also define the bibliography template for that resource.",
				"templateHelp6"	=>	"6. If a resource is missing the first field in the bibliography template you may replace that field with the fields in the partial template (this allows a reordering of the initial fields).",
				"templateHelp7"	=>	"7. In the bibliography template for Book Chapter, the field 'title' refers to the chapter number.",
// For template previewing, allow the use to preview by turning various fields on and off.
				"previewFields"	=>	"Preview with the following fields",
// Characters separating title and subtitle
				"titleSubtitleSeparator"	=>	"Title/subtitle separator",
// See "templateHelp6" above
				"partialTemplate"	=>	"Partial template",
// Use the partial template to replace all of the bibliography template.
				"partialReplace"	=>	"Replace all of original template with partial template",
				"genericBook"		=>	"Generic book-type",
				"genericArticle"	=>	"Generic article-type",
				"genericMisc"		=>	"Generic miscellaneous",
				"previewStyle"	=>	"Preview bibliography",
				"previewCite"	=>	"Preview in-text citation",
				"previewFoot"	=>	"Preview footnote",
				"previewError"	=>	"ERROR",
/**
* Citation
*/
// The displayed hyperlink next to the textarea form input
				"cite"			=>	"Cite",
				"citationFormat"	=>	"Citation Formatting",
// In-text citation style as opposed to endnote style citations.
				"citationFormatInText"	=>	"In-text style",
				"citationFormatEndnote"	=>	"Endnote style",
				"citationFormatFootnote"	=>	"Footnote creators",
				"creatorList"		=>	"Creator list abbreviation",
				"creatorListSubsequent"	=>	"Creator list abbreviation (subsequent appearances)",
				"creatorSep"		=>	"Creator delimiters",
				"creatorStyle" 		=>	"Creator style",
				"lastName"		=>	"Last name only",
// 'Last name only' is a choice in a select box and should not be translated
				"useInitials"		=>	"If 'Last name only', use initials to differentiate between creators with the same surname",
// For consecutive citations by the same creator(s)
				"consecutiveCreator"	=>	"For consecutive citations by the same creator(s) use the following template:",
				"consecutiveCreatorSep"	=>	"and separate citations with:",
// The template is something like '(author|, year)' that the user is asked to enter
				"template"		=>	"Template",
				"consecutiveCitationSep" =>	"Separate consecutive citations with",
// Formatting of years
				"yearFormat"		=>	"Year format",
// Normal, superscript or subscript of citation
				"normal"		=>	"Normal text",
				"superscript"		=>	"Superscript",
				"subscript"		=>	"Subscript",
				"enclosingCharacters"	=>	"Parentheses or other characters enclosing the citation",
// Ambiguous citations
				"ambiguous"		=>	"Ambiguous citations",
				"ambiguousTitle"	=>	"Use the following template",
				"ambiguousYear"		=>	"Add a letter after the year",
				"ambiguousUnchanged"	=>	"Leave citation unchanged",
				"followCreatorTemplate"	=>	"Use template below if a single citation is in the same sentence as the first creator's surname",
// This follows on from sentence above....  Split the pages from the main citation placing the main citation immediately after the creator names in the text and the pages immediately following the quote.  e.g. if the citation is in the form: Grimshaw states:  "WIKINDX is wonderful" [cite]123:25[/cite], the result will be Grimshaw (2005) states:  "WIKINDX is wonderful" (p.25) rather than Grimshaw states:  "WIKINDX is wonderful" (2005, p.25).
				"followCreatorPageSplit" => "and split the citation placing the main citation after the creator names and the page number after the quote:",
// For endnote-style citations
				"footnoteStyleBib"	=>	"Format like bibliography",
				"footnoteStyleInText"	=>	"Format like in-text citations",
				"ibid"		=>	"Replace consecutive citations for the same resource and the same page with this template",
				"idem"		=>	"Replace consecutive citations for the same resource but a different page with this template",
				"opCit"		=>	"Replace previously cited resources with this template",
				"endnoteFormat1" => "Format of the citation in the text",
				"endnoteFormat2" => "Format of the citation in the endnotes",
				"endnoteStyle"	=>	"Endnote/Footnote",
				"endnoteStyle1"	=>	"Endnotes: incrementing",
				"endnoteStyle2"	=>	"Endnotes: same ID for same resource",
				"endnoteStyle3"	=>	"Footnotes: incrementing",
// Ordering of the appended bibliography for in-text citations and endnote-style citations using the same id number for each cited resource
				"orderBib1"	=>	"Bibliography ordering",
				"orderBib2"	=>	"(For in-text citations, endnote-style citations using the same ID number and bibliographies appended to papers using footnotes.)",
				"orderBib3"	=>	"Use this order for endnote-style citations using the same ID number:",
				"order1"	=>	"1st. sort by",
				"order2"	=>	"2nd. sort by",
				"order3"	=>	"3rd. sort by",
				"ascending"	=>	"Ascending",
				"descending"	=>	"Descending",
// For a particular resource type (personal communication for example), replace the in-text citation template with another template
				"typeReplace"	=>	"For in-text citations, replace the citation template with this template",
// Text preceeding and following citations e.g. (see Grimshaw 1999; Boulanger 2004 for example): 'see' is preText and 'for example' is postText
				"preText"	=>	"Preliminary text",
				"postText"	=>	"Following text",
// Formatting of the id number in the endnotes for endnote-style citations
				"endnoteIDEnclose"	=>	"Parentheses or other characters enclosing the ID number",
// For subsequent citations from the same resource
				"subsequentCreator"	=>	"For subsequent citations from the same resource use the following template:",
// This follows on from the text in 'subsequentCreator'
				"subsequentFields"	=>	"only if the sentence containing the citation has the creator surname, title or shortTitle in it:",
// If no year for in-text citations, replace year field
				"replaceYear"	=>	"If no year, replace year field with the following",
// When compiling the appended bibliography for in-text citations, certain resources (e.g. APA personal communication) are not added.
				"notInBibliography"	=>	"Do not add to the bibliography when cited:",
// When using endnote-style citations and defining templates using fields such as 'creator', 'pages', 'year' or 'title'.  Don't translate 'creator', 'pages' or 'citation'.
				"endnoteFieldFormat" => "Fields are formatted as defined in in-text citation formatting above unless using footnotes in which case the 'creator' field is defined below and the 'pages' field format is defined in the footnote template in the bibliography section. If the 'citation' field is used, it should be by itself and it refers to the bibliographic/footnote templates below.",
				"footnoteTemplate"	=>	"Footnote template",
// Set the range of text within which the subsequent creator template (for in-text citations) is used
				"subsequentCreatorRange"	=>	"Range within which subsequent citations are searched for",
				"subsequentCreatorRange1"	=>	"Entire text",
				"subsequentCreatorRange2"	=>	"Paragraph",
				"subsequentCreatorRange3"	=>	"Section",
				"removeTitle"	=>	"Remove title and shortTitle fields from the citation if either of those fields is in the same sentence:",'creator'	=>	"First Creator",
				'title'		=>	"Title",
				'year'		=>	"Publication Year",
/**
* Hints
*/
				"hint_styleShortName"	=>	"(No spaces)",
				"hint_caseSensitive"	=>	"(Fields are case-sensitive)",
		);
	}
}
?>