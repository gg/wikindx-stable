<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class importexportbib_CONFIG
{
	public $wikindxVersion = 3;
}
class importexportbib_EXPORTCONFIG
{
	public $menus = array('plugin1');
	public $authorize = 1;
}
class importexportbib_IMPORTCONFIG
{
	public $menus = array('plugin1');
	public $authorize = 2;
// Path to bibUtils (e.g. '/usr/bin/' for *NIX or
// "D:/wamp/www/wikindx5/bibutils/" for windows).
// If this is FALSE, the plugin's export function
// will assume *NIX and look by default in '/usr/local/bin/'.
// Needs the trailing '/'
	public $bibutilsPath = FALSE;
}
class importexportbib_BIBUTILSCONFIG
{
	public $menus = array('plugin1');
	public $authorize = 1;
// Path to bibUtils (e.g. '/usr/bin/' for *NIX or
// "D:/wamp/www/wikindx5/bibutils/" for windows).
// If this is FALSE, the plugin's export function
// will assume *NIX and look by default in '/usr/local/bin/'.
// Needs the trailing '/'
	public $bibutilsPath = FALSE;
}
?>