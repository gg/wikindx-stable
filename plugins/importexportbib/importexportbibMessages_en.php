<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class importexportbibMessages_en
{
public $text;

	public function __construct()
	{
		$this->messages();
	}
// All headings, text hints etc.
	private function messages()
	{
		$this->text = array(
				"menu"	=>	"Import  & Export...",
				"menuEndnoteImport"	=>	"Import Endnote",
				"headerEndnoteImport"	=>	"Import Endnote XML Bibliography",
				"menuPubMedImport"	=>	"Import PubMed",
				"headerPubMedImport"	=>	"Import PubMed",
				"menuBibutils"	=>	"Bibutils",
				"menuRtfExport"	=>	"Exp. RTF...",
				"menuBibtexExport"	=>	"Exp. BibTeX...",
				"menuHtmlExport"	=>	"Exp. HTML...",
				"menuRisExport"	=>	"Exp. RIS...",
				"menuEndnoteExport"	=>	"Exp. Endnote...",
				"menuExportBasket"	=>  "Basket",
				"menuExportList"	=>  "Last List",
				"menuListFiles"	=>	"Exported Files",
				"menuIdeaExport"	=>	"Export Ideas",
				"headerRtfExport"	=>	"Export Rich Text Format",
				"headerBibtexExport"	=>	"Export BibTeX",
				"headerHtmlExport"	=>	"Export HTML",
				"headerRisExport"	=>	"Export RIS",
				"headerEndnoteExport"	=>	"Export Endnote",
				"headerIdeaExport"	=>	"Export Ideas",
				"headerListFiles"	=>	"Exported Files",
				"headerBibutils"	=>	"Bibutils Conversion",
				"bibutilsinputType"	=>	"Input Type",
				"bibutilsoutputType"	=>	"Output Type",
				"bibutilsinputFile"	=>	"Input File",
				"bibutilsoutputFile"	=>	"Output File",
				"bibutilscredit"	=>	"The Bibutils plugin uses C binaries written by Chris Putnam at ###. Most conversions use the MODS XML
				intermediate format.",
				"bibutilsnoPrograms"	=>	"No Bibutils programs found in ###",
				"bibutilsnoInputType"	=>	"No input type specified",
				"bibutilsnoOutputType"	=>	"No output type specified",
				"bibutilsnoFileInput"	=>	"Missing input file",
				"bibutilsfailedToConvert"	=>	"FAILED to convert (###)",
				"bibutilsxmlOptions"	=>	"Conversion options for intermediate MODS XML",
				'bibutilsignore'	=>	'IGNORE',
				"bibutilsoption1"	=>	"Encode unicode characters directly in the file rather than as XML entities",
				"bibutilsoption2"	=>	"Don't put citation key in the MODS id field",
				"bibutilsoption3"	=>	"Don't split titles into TITLE/SUBTITLE pairs",
				"bibutilsoption4"	=>	"Do not covert latex character combinations",
				"bibutilsoption5"	=>	"Unicode in, unicode out",
				"bibutilsSuccess"	=>	"Successfully converted ###",
				"introEndnoteImport"		=>	"You may import Endnote XML bibliographies (.xml files) here. Large files may take some time so if
					WIKINDX senses that php.ini's 'max_execution_time' variable is about to be exceeded, it will start importing the bibliography in chunks.
					If there is a date field, the format should be either dd/mm/yyyy or yyyy/mm/dd and yyyy will override any year field in the record.
					If you have custom fields in your import file, create custom fields first in the WIKINDX database (the Admin menu) so that you can then map
					the import custom fields.",
				"introRtfExport"		=>	"You may export to Rich Text Format your most recent bibliography list. Large numbers of resources might
					take some time and memory so you might need to adjust php.ini.",
				"category"		=>	"Category",
				"categoryPrompt"	=>	"All WIKINDX resources belong to at least one category which you chose here.  The category(s) a resource belongs to can always be edited later.",
// importDuplicates For file imports, allow duplicates?
				"importDuplicates"	=>	"Import duplicates:",
				"importKeywordIgnore"	=> "Ignore keywords:",
				"storeRawEndnoteImport"		=>	"You may store Endnote fields that WIKINDX does not use so that any resources later exported to Endnote can include this original unchanged data. Store unused fields:",
				"empty"		=>	"File is empty",
				"added"			=>	"No. resources added: ###",
				"discarded"		=>	"No. resources discarded (duplicates or no titles): ###",
// invalidField1 If non-standard import fields are found in the input file, invite the user to map these fields to wikindx fields
				"invalidField1"	=>	"Unknown fields have been found. You may map these fields to WIKINDX fields -- no duplicate mapping is allowed.",
				"invalidField2"	=>	"Where an unknown field is mapped to a WIKINDX field that would normally be automatically mapped to a standard input field, the unknown field mapping takes precedence.",
				"file"			=>	"Import File",
				"tag"			=>	"Tag this import so you can do a mass select or delete later",
// executionTimeExceeded With large imports that would go over php.ini's max_execution time, WIKINDX splits the imports into chunks
				"executionTimeExceeded"	=>	"'max_execution_time' (### seconds) in php.ini was about to be exceeded.  WIKINDX is importing the bibliography in chunks.",
				"addedChunk"			=>	"No. resources added this chunk: ###",
				"fileImport" =>	"You have already imported that file",
				"empty"	=>	"Import bibliography is empty",
				"upload"	=>	"File upload error",
				"noList"	=>	"You must create a list from the Search menu first",
				"noIdeas"	=>	"You have no ideas",
				"allIdeas"	=>	"Export all ideas",
				"selectedIdeas"	=>	"Export selected ideas",
				"exported"	=>	"Data successfully exported",
				"imported"	=>	"Data successfully imported",
				"contents"		=>	"Contents of your temporary folder (newest first):",
				"noContents"	=>	"Directory is empty",
				"warning"		=>	"These files will be available for ### minutes or while you keep your browser open (whichever is the shorter) so download and save them elsewhere immediately",
				"bibliographies"	=>	"Bibliography exports:",
				"bibliography"		=>	"Export bibliographic entries:",
				"fontSize"		=>	"Font size",
// Font type e.g. 'Times New Roman', 'Courier'
				"font"			=>	"Font",
				"abstract"		=>	"Export abstract:",
				"notes"			=>	"Export notes:",
				"quotes"		=>	"Export quotes:",
				"paraphrases"		=>	"Export paraphrases:",
				"musings"		=>	"Export musings:",
				"comments"		=>	"Export comments:",
// Number of indents (tabulation - TAB)
				"indentL"		=>	"Left indent",
				"indentR"		=>	"Right indent",
// 'CR' = carriage return (newlines)
				"crFollowing"		=>	"CR following",
				"crBetween"		=>	"CR between",
// bold, italics, underline
				"textFormat"		=>	"Text format",
// 'tag' = label given to each section (abstract, quotes, quote comments etc.)
				"tag"			=>	"Tag",
// Some text or characters to visually divide resources in the RT output
				"divider"		=>	"Divider between entries",
				"dividerCR"		=>	"CR after entries",
				"isbn"			=>	"Include ID no. (ISBN etc.)",
// Metadata export options
				"metadata"		=>	"If exporting quote and paraphrase comments or musings",
// These two are in a select box and follow on from the string above
				"metadataUser"	=>	"Export only mine",
				"metadataAll"	=>	"Export my data and all public data",
				"metadataFullCite" => "Add resource's primary creator and publication year to metadata",
// Default text for labelling metadata in the RTF export
				"quotesTag"		=>	"QUOTES:",
				"paraphrasesTag"	=>	"PARAPHRASES:",
				"quotesCommentTag"		=>	"COMMENTS:",
				"paraphrasesCommentTag"	=>	"COMMENTS:",
				"musingsTag"		=>	"MUSINGS:",
				"abstractTag"	=>	"ABSTRACT:",
				"notesTag"		=>	"NOTES:",
				"keywords"	=>	"Include resource keywords",
				"exportHyperlink"	=>	"Include a hyperlink to the resource in this WIKINDX:",
				"exportEndnoteFileType"	=>	"Endnote file type",
				"exportEndnoteXml"	=>	"Endnote XML",
				"exportEndnoteTabbed"	=>	"Endnote tabbed",
				"importPubMedIntro"	=>	"Search PubMed: please enter one or more fields.  Multiple items within a field
					should be separated by a space.  Per NCBI policy, a maximum of 100 search results can be obtained.",
				"importPubMedId"	=>	"PubMed ID",
				"importPubMedFields"	=>	"All Fields",
				"importPubMedAuthor"	=>	"Author",
				"importPubMedFirstAuthor"	=>	"First Author",
				"importPubMedLastAuthor"	=>	"Last Author",
				"importPubMedTitle"	=>	"Title",
				"importPubMedAbstract"	=>	"Abstract",
				"importPubMedYear"	=>	"Year",
				"importPubMedJournal"	=>	"Journal",
				"importPubMedVolume"	=>	"Volume",
				"importPubMedIssue"	=>	"Issue",
				"importPubMedLanguage"	=>	"Language",
				"importPubMedLimit"	=>	"Limit search to the last n days",
				"importPubMedMaxResults"	=>	"Max. Results",
				"importPubMedWikindx"	=>	"Import the PubMed import directly into WIKINDX",
				"importPubMedLimitError"	=>	"Please enter an integer up to 100 for Max. Results",
				"importPubMedInputError"	=>	"Please enter one or more fields",
				"importPubMedResult"	=>	"result",
				"importPubMedResults"	=>	"results",
				"importPubMedSuccess"	=>	"Successfully converted ###",
				"importPubMedOutputFile"	=>	"Output File",
				"importPubMedNoResults"	=>	"No Results Found",
				"importPubMedFailConvert"	=>	"FAILED to convert (###)",
				'importEndnoteNotv8'	=>	"XML file is not compatible with Endnote v8 which is required for this import script.",
				"importEndnoteSuccess"	=>	"Successfully imported Endnote file.",
			);
	}
}
?>