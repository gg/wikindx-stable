<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*
* IDEA class
*
* Export ideas
* @author Mark Grimshaw
*/
class IDEA
{
private $db;
private $session;
private $messages;
private $coreMessages;
private $errors;
private $common;
private $parentClass;

// Constructor
	public function __construct($parentClass = FALSE)
	{
		$this->parentClass = $parentClass;
		$this->db = FACTORY_DB::getInstance();
		include_once("core/messages/PLUGINMESSAGES.php");
		$this->messages = new PLUGINMESSAGES('importexportbib', 'importexportbibMessages');
		$this->coreMessages = FACTORY_MESSAGES::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		include_once("plugins/importexportbib/EXPORTCOMMON.php");
		$this->common = new EXPORTCOMMON();
	}
/**
* Display options for exporting
*/
	public function exportOptions()
	{
		$pString = \FORM\formHeader("importexportbib_exportIdea");
		$pString .= \FORM\hidden('method', 'process');
		$selectBox = array(1 => $this->messages->text("allIdeas"), 2 => $this->messages->text("selectedIdeas"));
		$selectBox = \FORM\selectFBoxValue(FALSE, "selectIdea", $selectBox, 2);
		$pString .= \HTML\p($selectBox . '&nbsp;' . \FORM\formSubmit("Proceed"), FALSE, "right");
		$this->ideaList();
		return $pString;
	}
/**
* list available ideas
*/
	private function ideaList()
	{
		$userObj = FACTORY_USER::getInstance();
		$cite = FACTORY_CITE::getInstance();
		$multiUser = $this->session->getVar('setup_MultiUser');
		$ideaList = array();
		$index = 0;
// now get ideas
// Check this user is allowed to read the idea.
		$this->db->formatConditions(array('resourcemetadataMetadataId' => ' IS NULL'));
		if(!$this->common->setIdeasCondition())
			$this->failure(HTML\p($this->messages->text("noIdeas"), 'error'));
		$resultset = $this->db->select('resource_metadata', array('resourcemetadataId', 'resourcemetadataTimestamp', 'resourcemetadataTimestampEdited',
			'resourcemetadataMetadataId', 'resourcemetadataText', 'resourcemetadataAddUserId', 'resourcemetadataPrivate'));
		while($row = $this->db->fetchRow($resultset))
		{
			$ideaList[$index]['metadata'] = $cite->parseCitations(\HTML\dbToHtmlTidy($row['resourcemetadataText']), 'html');
			if($multiUser)
			{
				list($user) = $userObj->displayUserAddEdit($row['resourcemetadataAddUserId'], FALSE, 'idea');
				if($row['resourcemetadataTimestampEdited'] == '0000-00-00 00:00:00')
					$ideaList[$index]['user'] = '&nbsp;' . $this->coreMessages->text('hint', 'addedBy', $user . '&nbsp;' . $row['resourcemetadataTimestamp']);
				else
					$ideaList[$index]['user'] = '&nbsp;' . $this->coreMessages->text('hint', 'addedBy', $user . '&nbsp;' . $row['resourcemetadataTimestamp']) .
					',&nbsp;' . $this->coreMessages->text('hint', 'editedBy', $user . '&nbsp;' . $row['resourcemetadataTimestampEdited']);
			}
			$ideaList[$index]['links'] = array('&nbsp;' . \FORM\checkbox(FALSE, 'checkbox_' . $row['resourcemetadataId'], FALSE));
			++$index;
		}
		if(!$index)
			$this->failure(HTML\p($this->messages->text("noIdeas"), 'error'));
		$ideaList[--$index]['links'][] .= \FORM\formEnd();
		GLOBALS::addTplVar('ideaTemplate', TRUE);
		GLOBALS::addTplVar('ideaList', $ideaList);
	}
	private function failure($error)
	{
		GLOBALS::addTplVar('content', $error);
		FACTORY_CLOSE::getInstance();
	}
}
?>