<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
*	USERWRITECATEGORY plugin -- English messages.
*
*****/
class userwritecategoryMessages_en
{
public $text;

// Constructor
	public function __construct()
	{
		$this->loadMessages();
	}
	private function loadMessages()
	{
		$this->text = array(
/**
* Menu items
*/
				'uwcSub' =>	'Administer Categories...',
				'uwcCategories' => 'Categories',
				'uwcSubcategories' => 'Subcategories',
			);
	}
}
?>