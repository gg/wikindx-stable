**************************************
** 		 userwritecategory  		**
**				v1.6				**
**		  WIKINDX module			**
**************************************

NB. this module is compatible with WIKINDX v5 and up.  Results may be unexpected if used with a lower version.

User administration of categories - non-admin users can add/edit/delete categories (they must be logged in). 

The module registers itself in the 'Edit' menu.

Unzip this file (with directory structure) into wikindx5/plugins/

CHANGELOG:
v1.6
1.	Plugin now uses core WIKINDX code and so is simply a gateway to that.

v1.5
1.  Plugin now compatible with WIKINDX v5.x

v1.4
Plugin compatible only with WIKINDX v4.2.x

v1.3 
Authorization bug fix.

**********

v1.2
Updated for WIKINDX v4

**********

v1.1
Added $this->authorize to control display of menu item for users with at least write access.

Mark Grimshaw 2015.