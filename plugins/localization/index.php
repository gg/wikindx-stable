<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Import initial configuration and initialize the web server
*/
include_once("core/startup/WEBSERVERCONFIG.php");


class localization_MODULE
{
private $db;
private $vars;
private $messages;
private $locMessages;
private $errors;
private $session;
private $badInput;
private $changesNeeded = FALSE;

public $authorize;
public $menus;

// constructor
	public function __construct($menuInit = FALSE)
	{
		include_once("core/messages/PLUGINMESSAGES.php");
		$this->locMessages = new PLUGINMESSAGES('localization', 'localizationMessages');
		include_once("plugins/localization/config.php");
		$config = new localization_CONFIG();
		$this->authorize = $config->authorize;
		if($menuInit) // portion of constructor used for menu initialisation
		{
			$this->makeMenu($config->menus);
			return; // Need do nothing more as this is simply menu initialisation.
		}
		$this->session = FACTORY_SESSION::getInstance();
		$authorize = FACTORY_AUTHORIZE::getInstance();
		if(!$authorize->isPluginExecutionAuthorised($this->authorize)) // not authorised
			FACTORY_CLOSENOMENU::getInstance(); // die
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();


		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->languageSelect = FALSE;
	}
// Make the menus
	private function makeMenu($menuArray)
	{
		$this->menus = array($menuArray[0] => array($this->locMessages->text('header') => 'init'));
	}
// Front page
	public function init($message = FALSE)
	{
		GLOBALS::setTplVar('heading', $this->locMessages->text("header"));
		$pString = $message ? $message : FALSE;
		$pString .= HTML\p($this->locMessages->text('front1'));
		$pString .= HTML\p($this->locMessages->text('front2'));
		$pString .= HTML\p($this->locMessages->text('front10'));
		$pString .= HTML\p($this->locMessages->text('front13'));
		$pString .= HTML\hr();
		if($changes = $this->getChanges())
		{
			$pString .= $changes;
			$pString .= HTML\hr();
		}
		if($string = $this->convertLanguagesLoad())
		{
			$pString .= $string;
			$pString .= HTML\hr();
		}
		if($string = $this->editLanguagesLoad())
		{
			$pString .= $string;
			$pString .= HTML\hr();
		}
		$pString .= $this->createInit();
		GLOBALS::addTplVar('content', $pString);
	}
// Check if files need updating
	private function getChanges()
	{
		$array = array();
		$handle = opendir('languages/en');
		while(FALSE !== ($file = readdir($handle)))
		{
			if(mb_strpos($file, '.') === 0) // a . file or folder
				continue;
			$split = UTF8::mb_explode('.', $file);
			$en[$split[0]] = filectime('languages/en/' . $file);
		}
		$handle = opendir('languages/reference');
		while(FALSE !== ($file = readdir($handle)))
		{
			if(mb_strpos($file, '.') === 0) // a . file or folder
				continue;
			$split = UTF8::mb_explode('.', $file);
			$ref[$split[0]] = filectime('languages/reference/' . $file);
		}
		$handle1 = opendir('languages');
		while(FALSE !== ($dir = readdir($handle1)))
		{
			if(mb_strpos($dir, '.') === 0) // a . file or folder
				continue;
			if($dir == 'index.html') // empty index.html
				continue;
			if(is_dir('languages/' . $dir))
			{
				if(($dir == WIKINDX_LANGUAGE_DEFAULT) || ($dir == 'reference'))
					continue;
				$handle2 = opendir('languages/' . $dir);
				while(FALSE !== ($file = readdir($handle2)))
				{
					if(mb_strpos($file, '.') === 0) // a . file or folder
						continue;
					if(($file == 'description.txt') || ($file == 'CONSTANTS.php'))
						continue;
					$split = UTF8::mb_explode('.', $file);
					if(!array_key_exists($split[0], $en))
						continue;
					if((filectime('languages/' . $dir . DIRECTORY_SEPARATOR . $file) < $en[$split[0]]) &&
						($en[$split[0]] > $ref[$split[0]]))
					{
						$array[] = $dir . DIRECTORY_SEPARATOR;
						break;
					}
				}
			}
		}
		if(!empty($array))
		{
			$this->changesNeeded = TRUE;
			return HTML\p(HTML\color($this->locMessages->text('changes', HTML\strong(join(', ', $array))), 'redText'));
		}
		else
			return HTML\p(HTML\color($this->locMessages->text('noChanges'), 'greenText'));
	}
// Load v3 localizations for conversion
	private function convertLanguagesLoad()
	{
		$langrootdir = 'languages';
		$languages = array();

		if($handle = opendir($langrootdir))
		{
			while(FALSE !== ($dir = readdir($handle)))
			{
				if($dir != '.' && $dir != '..')
				{
					if(
						   file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . '/CONSTANTS.php')
						&& file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . '/ERRORS.php')
						&& file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . '/MESSAGES.php')
						&& file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . '/SUCCESS.php'))
					{
						// read one line
						if($fh = fopen($langrootdir . DIRECTORY_SEPARATOR . $dir . '/description.txt', "r"))
						{
							$string = utf8_encode(fgets($fh));
							if($string)
								$languages[$dir] = $string;

							fclose($fh);
						}
					}
				}
			}

			closedir($handle);
		}
// Remove English!
		unset($languages[WIKINDX_LANGUAGE_DEFAULT]);
		if(empty($languages))
			return FALSE;
		asort($languages);
		$pString = HTML\p(HTML\strong($this->locMessages->text('front14')));
		$pString .= HTML\p($this->locMessages->text('front15'));
		$pString .= FORM\formHeader("localization_convertLocalization");
		$pString .= FORM\selectFBoxValue($this->locMessages->text('front11'), "convertLanguage", $languages, 5);
		$pString .= FORM\formSubmit("Proceed");
		$pString .= FORM\formEnd();
		return $pString;
	}
// Load existing localizations for editing
	private function editLanguagesLoad()
	{
		$language = FACTORY_MESSAGES::getInstance();
		$languages = $language->loadDir();
// Remove English!
		unset($languages[WIKINDX_LANGUAGE_DEFAULT]);
		if(empty($languages))
			return FALSE;
		asort($languages);
		$pString = HTML\p(HTML\strong($this->locMessages->text('front3')));
		$pString .= HTML\p($this->locMessages->text('front4'));

		$pString .= HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= HTML\trStart();
		$pString .= HTML\tdStart();
		$pString .= FORM\formHeader("localization_editLocalization");
		$pString .= HTML\tableStart('left');
		$pString .= HTML\trStart();
		if($this->languageSelect)
			$pString .= HTML\td(FORM\selectedBoxValue($this->locMessages->text('front11'),
			"editLanguage", $languages, $this->languageSelect, 5), 'padding5px');
		else
			$pString .= HTML\td(FORM\selectFBoxValue($this->locMessages->text('front11'), "editLanguage", $languages, 5), 'padding5px');
// Select box for editing a particular file
// PHP 5.3.9 and some suhosin servers introduce max_input_vars which is a limit on form input (1000 by default). MESSAGES is now
// over 1000 so must be split in two for editing (the file is still one file)
		$files = array("HELP", "ERRORS", "MESSAGES_1", "MESSAGES_2", "SUCCESS", "README");
		$pString .= HTML\td(FORM\selectFBox($this->locMessages->text('front5'), "editFile", $files, 5), 'padding5px');
		$pString .= HTML\td(FORM\formSubmit("Proceed"), 'padding5px bottom');
		$pString .= HTML\trEnd();
		$pString .= HTML\tableEnd();
		$pString .= FORM\formEnd();
		$pString .= HTML\tdEnd();

		if($this->changesNeeded)
		{
			$pString .= HTML\tdStart();
			$pString .= HTML\tableStart('left');
			$pString .= HTML\trStart();
			$pString .= HTML\td(FORM\formHeader("localization_moveReference") .
				$this->locMessages->text('moveReference') .
				HTML\p(FORM\formSubmit("Proceed")) . FORM\formEnd(), 'padding5px bottom');
			$pString .= HTML\trEnd();
			$pString .= HTML\tableEnd();
			$pString .= HTML\tdEnd();
		}
		$pString .= HTML\trEnd();
		$pString .= HTML\tableEnd();
		return $pString;
	}
// Update reference files by copying /en files
	function moveReference()
	{
		$handle = opendir('languages/reference');
		while(FALSE !== ($file = readdir($handle)))
		{
			if(mb_strpos($file, '.') === 0) // a . file or folder
				continue;
			unlink('languages/reference/' . $file);
		}
		$handle = opendir('languages/en');
		while(FALSE !== ($file = readdir($handle)))
		{
			if(mb_strpos($file, '.') === 0) // a . file or folder
				continue;
			if(($file == 'description.txt') || ($file == 'description.txt~') || ($file == 'CONSTANTS.php'))
				continue;
			$string = file_get_contents('languages/en/' . $file);
			$split = UTF8::mb_explode('.', $file);
			$classEn = $split[0] . '_en';
			$classRef = $split[0] . '_ref';
			$string = str_replace($classEn, $classRef, $string);
			file_put_contents('languages/reference/' . $file, $string);
		}
		$this->init();
	}
// Create localization form init
	function createInit()
	{
		$default = "WIKINDX " . WIKINDX_VERSION . " [ENTER LOCALIZATION NAME] localization.\n";
		$default .= "\n\nInstallation:\nUnzip with the folder structure into\n\twikindx5/languages/\n";
		$pString = HTML\p(HTML\strong($this->locMessages->text('front6')));
		$pString .= HTML\p($this->locMessages->text('front7'));

		$pString .= FORM\formHeader("localization_createLocalization");
		$pString .= HTML\tableStart('left');
		$pString .= HTML\trStart();
		$pString .= HTML\td(FORM\textInput($this->locMessages->text('front11'), "language"));
		$pString .= HTML\td(FORM\textInput($this->locMessages->text('front8'), "directory", FALSE, 5));
		$pString .= HTML\td(FORM\textAreaInput($this->locMessages->text('front9'), "personalDetails", $default, 60, 5));
		$pString .= HTML\td(FORM\formSubmit("Proceed"), 'bottom');
		$pString .= HTML\trEnd();
		$pString .= HTML\tableEnd();
		$pString .= FORM\formEnd();
		return $pString;
	}
// Convert localization
	public function convertLocalization()
	{
		include_once("plugins/localization/CONVERTLOC.php");
		$convert = new CONVERTLOC();
		$this->init($convert->process());
	}
// Create the initial localization files.
	public function createLocalization()
	{
		include_once("plugins/localization/CREATELOC.php");
		$create = new CREATELOC($this);
		$create->start();
	}
// Display a file for editing
	public function editLocalization()
	{
		include_once("plugins/localization/EDITLOC.php");
		$edit = new EDITLOC($this);
		$edit->display();
	}
// Write an edited file
	public function editFile()
	{
		include_once("plugins/localization/EDITLOC.php");
		$edit = new EDITLOC($this);
		$this->init($edit->edit());
	}
}
?>