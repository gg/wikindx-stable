**************************************
** 		   localization				**
**				v2.7				**
**		  WIKINDX module			**
**************************************

NB. this module is compatible with WIKINDX v5 and up.

Administrator interface for the creation and editing of localization (translated language) files. 

The module registers itself in the 'plugin1' menu and is only for WIKINDX administrators although this can be changed.

Unzip this file (with directory structure) into wikindx5/plugins/

Ensure that you are using the latest WIKINDX version before creating or editing localizations.

You may create new prototype localizations (for later editing) or edit existing localizations through the web interface.  
Messages that have been removed or added in the English files will be automatically removed or added in each file you edit.

v3.8.x localizations can be upgraded to v4.x through the plugin's interface.

UTF8 encoding is automatic.


CHANGELOG:
v2.7
1.  Ensure valid XML is used.

/****************************************************/


CHANGELOG:
v2.6
1.  Dealt with bug concerning conversion of HTML entities
2.  Fix any memory leaks due to an oversight fclose().

/****************************************************/


CHANGELOG:
v2.5
1.  Plugin now compatible with WIKINDX v5.x

/****************************************************/

v2.4
1. PHP 5.3.9 and some suhosin servers introduce max_input_vars which is a limit on form input (1000 by default). 
MESSAGES is now edited in two batches (there is still just the one MESSAGES.xml file).

/****************************************************/

v2.3
1. Plugin compatible only with WIKINDX v4.2.x
2. Added option to update reference files once localization is finished.

v2.2
Focus:  Feature Enhancements

1.  Messages that have been translated but require updating (the original English message has been modified) are now indicated.

/****************************************************/

v2.1
Focus:  Minor bug fixes

1.  PHP variables in the HELP file could not be translated.

/****************************************************/

v2

Focus:  Updated for WIKINDX v4

1/  Non-English files now use XML rather than PHP except for HELP.php and CONSTANTS.php (the latter of which must still be 
edited by hand).

/****************************************************/

v1.2

Focus:  Minor bug fixes

1/ Fixed a security hole giving access to non-administrators.

/****************************************************/

v1.1

Focus:  Minor bug fixes

1/ Fixed bug in displaying original contents of English SUCCESS messages for untranslated messages.

/****************************************************/

v1.0
Initial release.

Mark Grimshaw 2015.