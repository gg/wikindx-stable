<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class EDITLOC
{
private $db;
private $vars;
private $messages;
private $locMessages;
private $errors;
private $session;
private $badInput;
private $referenceNotDefines = array();
private $parentClass;
private $expectedKeys;
private $usedComments = array();
private $redSpot;
private $orangeSpot;

	public function __construct($parentClass)
	{
		$this->parentClass = $parentClass;
		$this->session = FACTORY_SESSION::getInstance();
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();


		$this->badInput = FACTORY_BADINPUT::getInstance();
		include_once("core/messages/PLUGINMESSAGES.php");
		$this->locMessages = new PLUGINMESSAGES('localization', 'localizationMessages');
		GLOBALS::setTplVar('heading', $this->locMessages->text("header"));
		GLOBALS::addTplVar('scripts', "<style>.locRed {background-color:red;} .locOrange {background-color:orange;}</style>");
	}
// Display a file for editing
	public function display($message = FALSE)
	{
		$this->checkEditInput();
		$pString = $message ? $message : FALSE;
		$directory = $this->vars['editLanguage'];
		$editFile = $this->vars['editFile'];
		if($editFile == 'README')
			return $this->editReadme($directory);
		$this->redSpot = 'locRed';
		$this->orangeSpot = 'locOrange';
		if($editFile == 'HELP')
		{
// Check consistency and edit if necessary
			if(!$file = file_get_contents("languages/$directory/HELP.php"))
				$this->badInput->close(HTML\p($this->locMessages->text("noFileRead"), 'error', 'center'),
					$this->parentClass, "init");
			if(mb_strpos($file, "// START__LOCALIZATION__MODULE__EDIT") === FALSE)
				$this->makeConsistent($directory, $editFile);
		}
		if(!$message) // new load
		{
			$this->session->clearArray("localizationModule");
			$this->sessionVars = FALSE;
		}
		else // returning after an error
			$this->sessionVars = $this->session->getArray("localizationModule");
		$pString .= HTML\p($this->locMessages->text('edit1'));
		$pString .= HTML\p($this->locMessages->text('edit2'));
		$pString .= HTML\p(htmlentities($this->locMessages->text('edit3')));
		$pString .= HTML\p($this->locMessages->text('edit4'));

		$pString .= FORM\formHeader("localization_editFile");
		$pString .= FORM\hidden("directory", $directory);
		$pString .= FORM\hidden("editLanguage", $directory);
		$pString .= FORM\hidden("editFile", $editFile);

		$pString .= HTML\tableStart('borderStyleSolid left');

		$pString .= HTML\trStart();
		$pString .= HTML\th('Key');
		$pString .= HTML\th('Reference string');
		$pString .= HTML\th('Translation');
		$pString .= HTML\th('Comment');
		$pString .= HTML\trEnd();

		if($editFile == 'MESSAGES_1')
		{
			$this->openEnglishMessages(1);
			if(filectime('languages/reference/MESSAGES.php') < filectime('languages/en/MESSAGES.php'))
				$this->openReferenceMessages();
			$pString .= $this->printXML($directory, 'MESSAGES');
		}
		if($editFile == 'MESSAGES_2')
		{
			$this->openEnglishMessages(2);
			if(filectime('languages/reference/MESSAGES.php') < filectime('languages/en/MESSAGES.php'))
				$this->openReferenceMessages();
			$pString .= $this->printXML($directory, 'MESSAGES');
		}
		else if($editFile == 'ERRORS')
		{
			$this->openEnglishErrors($editFile);
			if(filectime('languages/reference/ERRORS.php') < filectime('languages/en/ERRORS.php'))
				$this->openReferenceErrors();
			$pString .= $this->printXML($directory, $editFile);
		}
		else if($editFile == 'HELP')
		{
			$this->openEnglishSuccessHelp($editFile);
			if(filectime('languages/reference/HELP.php') < filectime('languages/en/HELP.php'))
				$this->openReferenceSuccessHelp($editFile);
			$pString .= $this->printHelp($directory);
		}
		else if($editFile == 'SUCCESS')
		{
			$this->openEnglishSuccessHelp($editFile);
			if(filectime('languages/reference/SUCCESS.php') < filectime('languages/en/SUCCESS.php'))
				$this->openReferenceSuccessHelp($editFile);
			$pString .= $this->printXML($directory, $editFile);
		}
		$pString .= HTML\tableEnd();

		$pString .= HTML\p(FORM\formSubmit("Save"));
		$pString .= FORM\formEnd();
		$pString = $this->status . $pString;
		GLOBALS::addTplVar('content', $pString);
	}
// Edit the README file
	private function editReadme($directory)
	{
		$file = file_get_contents("languages/$directory/README.txt");
		$version = $this->locMessages->text('version') . "&nbsp;&nbsp;" . WIKINDX_VERSION;
		$pString = FORM\formHeader("localization_editFile");
		$pString .= FORM\hidden("directory", $directory);
		$pString .= FORM\hidden("editFile", 'README');
		$pString .= HTML\p($version . BR . FORM\textAreaInput(FALSE, "input", $file, 80, 10));
		$pString .= HTML\p(FORM\formSubmit("Save"));
		$pString .= FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// Write the edited README file
	private function writeReadme($directory)
	{
		$this->parentClass->languageSelect = $directory;
		if(!$input = trim($this->vars['input']))
			$this->badInput->close(HTML\p($this->locMessages->text("missingInput"),
				"error", "center"), $this->parentClass, 'init');
		if($fh = fopen("languages/$directory/README.txt", "w"))
		{
			if(!fwrite($fh, $input))
				$this->badInput->close(HTML\p($this->locMessages->text("noFileWrite")), $this->parentClass, 'init');

			fclose($fh);
		}
		else
		{
			$this->badInput->close(HTML\p($this->locMessages->text("noFileWrite")), $this->parentClass, 'init');
		}
		include_once("core/display/CLOSE.php");
		new CLOSE($this->parentClass->init(HTML\p($this->locMessages->text("editFile"),
			"success", "center")));
	}
// Gather input for an edited file
	public function edit()
	{
		if(!array_key_exists("directory", $this->vars) || !trim($this->vars['directory']))
			$this->badInput->close(HTML\p($this->locMessages->text("noHidden"), 'error', 'center'),
				$this, 'display');
		else
			$this->vars['language'] = trim($this->vars['directory']); // in case of error so checkEditInput() works
		if(!array_key_exists("editFile", $this->vars) || !trim($this->vars['editFile']))
			$this->badInput->close(HTML\p($this->locMessages->text("noHidden"), 'error', 'center'), $this, 'display');
		else
			$this->vars['file'] = trim($this->vars['editFile']); // in case of error so checkEditInput() works
		$directory = $this->vars['directory'];
		$editFile = $this->vars['editFile'];
		if($editFile == 'README')
			$this->writeReadme($directory); // calls CLOSE()
		if($editFile == 'MESSAGES_1')
			$this->openEnglishMessages(1);
		else if($editFile == 'MESSAGES_2')
			$this->openEnglishMessages(2);
		else if($editFile == 'ERRORS')
			$this->openEnglishErrors($editFile);
		else if($editFile == 'SUCCESS')
			$this->openEnglishSuccessHelp($editFile);
		else if($editFile == 'HELP')
			$this->openEnglishSuccessHelp($editFile);
		$this->checkWriteInput();
// We now have everything we need to start writing
		if(($editFile == 'ERRORS') || ($editFile == 'MESSAGES_1') || ($editFile == 'MESSAGES_2'))
			$this->writeErrorsMessagesFile($editFile, $directory);
		else if($editFile == 'SUCCESS')
			$this->writeSuccessFile($directory);
		else if($editFile == 'HELP')
			$this->writeHelpFile($directory);
		$this->parentClass->languageSelect = $directory;
		return HTML\p($this->locMessages->text("editFile"), "success", "center");
	}
// Write HELP.php file
	private function writeHelpFile($directory)
	{
		$this->sessionVars = $this->session->getArray("localizationModule");
		$pString = '';
		foreach($this->sessionVars as $key => $value)
		{
			if($key == 'file')
				continue;
			$value = html_entity_decode(stripslashes($value), ENT_QUOTES);
// Add slashes to double quotes and '$'
			$value = str_replace('"', '\"', $value);
//			$value = preg_replace('/\\$QUICKSEARCH\\$/u', '\\\\\\\$QUICKSEARCH\\\\\\\$', $value);
			$value = preg_replace('/\\$/u', '\\\\\\\$', $value);
			$pString .= "\t\t\t\t\"$key\"\t=>\t\"$value\",\n\r";
		}
		$pString = "\n$pString";
// Replace everything between '// START__LOCALIZATION__MODULE__EDIT' and '// END__LOCALIZATION__MODULE__EDIT' with new string
		if(!$file = file_get_contents("languages/$directory/HELP.php"))
			$this->badInput->close(HTML\p($this->locMessages->text("noFileRead"), 'error', 'center'), $this, 'display');
		$start = "// START__LOCALIZATION__MODULE__EDIT";
		$end = "// END__LOCALIZATION__MODULE__EDIT";
		$match = "#($start).*($end)#Uus";
		$file = preg_replace($match, "$1" . $pString . "$2", $file);
		if($fh = fopen("languages/$directory/HELP.php", "w"))
		{
			if(!fwrite($fh, $file))
				$this->badInput->close(HTML\p($this->locMessages->text("noFileWrite"), 'error', 'center'), $this, 'display');

			fclose($fh);
		}
		else
		{
			$this->badInput->close(HTML\p($this->locMessages->text("noFileWrite"), 'error', 'center'), $this, 'display');
		}

// update the reference file modification date
		touch('languages/reference/HELP.php');
	}
// Write SUCCESS.xml file
	private function writeSuccessFile($directory)
	{
		$this->sessionVars = $this->session->getArray("localizationModule");

		$this->sessionVars = array_diff_key($this->sessionVars, array('file' => 'file'));

		$this->writeXMLCatalog($directory, "SUCCESS.xml", $this->sessionVars);
// update the reference file modification date
		touch('languages/reference/SUCCESS.php');
	}

// Write ERRORS.xml and MESSAGES.xml files
	private function writeErrorsMessagesFile($editFile, $directory)
	{
		$this->sessionVars = $this->session->getArray("localizationModule");

// Explode keys array(level_sublevel => '', ...) in array('level' => array('sublevel' => '', ...))
		foreach($this->sessionVars as $key => $value)
		{
			$split = UTF8::mb_explode('_', $key, 2);
			if(sizeof($split) != 2)
				continue;
			$array[$split[0]][$split[1]] = preg_replace("/\r|\n|\015|\012/u", "", $value);
		}

// MESSAGES is edited in two batches
        $msgDataArray = array();
		if(($editFile == 'MESSAGES_1') || ($editFile == 'MESSAGES_2'))
		{
			$originalArray = $this->xmlLanguageCatalog2Array('languages/' . $directory . '/MESSAGES.xml');

			$keyCount = -1;
			foreach($originalArray as $key => $subArray)
			{
				++$keyCount;
				if(($editFile == 'MESSAGES_2') && ($keyCount == 15))
					break;
				else if(($editFile == 'MESSAGES_1') && ($keyCount < 15))
					continue;

			    $tMsgArrayData = array();
				$msgDataArray[$key] = $subArray;
			}
		}

		// Compile the catalog in an array
        $xmlDataArray = array();

		if($editFile == 'MESSAGES_2' && count($msgDataArray) > 0)
		{
			$xmlDataArray += $msgDataArray;
		}

		if(count($array) > 0)
		{
            $xmlDataArray += $array;
		}

		if($editFile == 'MESSAGES_1' && count($msgDataArray) > 0)
		{
			$xmlDataArray += $msgDataArray;
		}

		// Write the catalog in an XML file
		$this->writeXMLCatalog($directory, ($editFile == 'MESSAGES_1' || $editFile == 'MESSAGES_2') ? 'MESSAGES.xml' : "$editFile.xml", $xmlDataArray);

// update the reference file modification date
		touch('languages/reference/' . $editFile . '.php');
	}

	private function writeXMLCatalog($directory, $catalogFileName, $stringArray)
	{
        $version = '1.0';
        $encoding = 'UTF-8';
        $standalone = 'yes';
        $rootName = 'language';
        $identString = "\t";

// Start XML
        $xw = new XMLWriter();
        $xw->openMemory();
        $xw->startDocument($version, $encoding, $standalone);

        $xw->startElement($rootName);
        $xw->writeAttributeNs('xml', 'lang', NULL, $directory);

        $xw->setIndentString($identString);
        $xw->setIndent(TRUE);

		foreach($stringArray as $keyLevel1 => $valueLevel1)
		{
            $xw->startElement($keyLevel1);

            if (is_array($valueLevel1))
            {
        		foreach($valueLevel1 as $keyLevel2 => $valueLevel2)
        		{
                    $xw->startElement($keyLevel2);
                    $xw->writeCdata($valueLevel2);
                    $xw->endElement();
        		}
    		}
    		else
    		    $xw->writeCdata($valueLevel1);

            $xw->endElement();
		}

        $xw->endElement();

        $xw->endDocument();

        $pString = $xw->outputMemory();

        if(file_put_contents('languages/' . $directory . "/$catalogFileName", $pString) === FALSE)
            HTML\p($this->locMessages->text("noFileWrite") . ": $catalogFileName", "error", "center");
	}
// Existing localizations may be missing "// START__LOCALIZATION__MODULE__DEFINES" and "// END__LOCALIZATION__MODULE__DEFINES".
// Rewrite the file to make them editable by this module.
	private function makeConsistent($directory, $editFile)
	{
		if(!$lines = file("languages/$directory/$editFile.php"))
			$this->badInput($this->locMessages->text("noFileRead"), "init");
		$pString = '';
		if(($editFile == 'ERRORS') || ($editFile == 'MESSAGES') || ($editFile == 'SUCCESS'))
		{
			foreach($lines as $line)
			{
				if(mb_strpos($line, "return array(") !== FALSE)
					$line .= "// START__LOCALIZATION__MODULE__EDIT". LF;
				else if(mb_strpos($line, ");") != FALSE)
					$line = "// END__LOCALIZATION__MODULE__EDIT". LF . $line;
				$pString .= $line;
			}
		}
		else
		{
			foreach($lines as $line)
			{
				if(mb_strpos($line, "// TRANSLATORS start here") === 0)
					$line .= "// START__LOCALIZATION__MODULE__DEFINES". LF;
				else if(mb_strpos($line, "// TRANSLATORS end here") === 0)
					$line = "// END__LOCALIZATION__MODULE__DEFINES". LF . $line;
				$pString .= $line;
			}
		}
		if($fh = fopen("languages/$directory/$editFile.php", "w"))
		{
			if(!fwrite($fh, $pString))
				$this->badInput($this->locMessages->text("noFileWrite"), "init");

			fclose($fh);
		}
		else
		{
			$this->badInput($this->locMessages->text("noFileWrite"), "init");
		}
		if(!$file = file_get_contents("languages/$directory/$editFile.php"))
			$this->badInput($this->locMessages->text("noFileRead"), "init");
		return $file;
	}
// Open English HELP.php and load arrays
	private function openEnglishSuccessHelp($file)
	{
		include_once("languages/en/$file.php");
		$classname = $file . '_en';
		$class = new $classname();
		if($file == 'HELP')
			$class->translate = TRUE;
		$array = $class->loadArrays();
		foreach($array as $key => $value)
		{
			$this->englishNotDefines[$key] = htmlentities($value);
			$this->expectedKeys[] = $key;
		}
	}
// Open reference MESSAGES.php and load arrays
	private function openReferenceSuccessHelp($file)
	{
		if(is_dir('languages/reference'))
		{
			include_once("languages/reference/$file.php");
			$classname = $file . '_ref';
			$class = new $classname();
			$array = $class->loadArrays();
			foreach($array as $key => $value)
				$this->referenceNotDefines[$key] = htmlentities($value);
		}
	}
// Open English MESSAGES.php and load arrays
// $batch is either 1 or 2 and splits the file in two for editing (NB PHP 5.3.9 max_input_vars)
	private function openEnglishMessages($batch)
	{
		include_once("languages/en/MESSAGES.php");
		$class = new MESSAGES_en();
		$array = $class->loadArrays();
		$keyCount = -1;
		foreach($array as $key => $keyArray)
		{
			++$keyCount;
			if(($batch == 1) && ($keyCount == 15))
				break;
			else if(($batch == 2) && ($keyCount < 15))
				continue;
			foreach($keyArray as $subKey => $string)
			{
				$this->englishNotDefines[$key][$subKey] = htmlentities($string);
				$this->expectedKeys[] = $key . '_' . $subKey;
			}
		}
		unset($class);
	}
// Open reference MESSAGES.php and load arrays
	private function openReferenceMessages()
	{
		if(is_dir('languages/reference'))
		{
			include_once("languages/reference/MESSAGES.php");
			$class = new MESSAGES_ref();
			$array = $class->loadArrays();
			foreach($array as $key => $keyArray)
			{
				foreach($keyArray as $subKey => $string)
					$this->referenceNotDefines[$key][$subKey] = htmlentities($string);
			}
		}
	}
// Open English ERRORS.php and load arrays
	private function openEnglishErrors()
	{
		include_once("languages/en/ERRORS.php");
		$class = new ERRORS_en();
		$array = $class->loadArrays();
		foreach($array as $key => $keyArray)
		{
			foreach($keyArray as $subKey => $string)
			{
				$this->englishNotDefines[$key][$subKey] = htmlentities($string);
				$this->expectedKeys[] = $key . '_' . $subKey;
			}
		}
	}
// Open reference ERRORS.php and load arrays
	private function openReferenceERRORS()
	{
		if(is_dir('languages/reference'))
		{
			include_once("languages/reference/ERRORS.php");
			$class = new ERRORS_ref();
			$array = $class->loadArrays();
			foreach($array as $key => $keyArray)
			{
				foreach($keyArray as $subKey => $string)
					$this->referenceNotDefines[$key][$subKey] = htmlentities($string);
			}
		}
	}
// Grab and display HELP messages
	private function printHelp($directory)
	{
// If returning here after an error, get stored session variable (already htmlentities() encoded)
		if(is_array($this->sessionVars))
		{
			foreach($this->sessionVars as $key => $value)
				$originalArray[$key] = stripslashes($value);
		}
		else
		{
			include_once("languages/$directory/HELP.php");
			$className = 'HELP_' . $directory;
			$class = new $className();
			$class->translate = TRUE;
			$originalArray = $class->loadArrays();
		}
		$pString = '';
		$commentsArray = array();
		$this->notYetTranslated = array();
// Gather any English comments relating to arrays or to individual array entries
		if(!$lines = file("languages/en/HELP.php"))
			$this->badInput->close(HTML\p($this->locMessages->text("noFileRead"), 'error', 'center'),
				$this->parentClass, "init");
		foreach($lines as $line)
		{
			if(mb_strpos($line, "/// ") === 0)
			{
				$split = UTF8::mb_explode(' ', $line);
				array_shift($split); // remove '///'
				$key = array_shift($split);
				$commentsArray[$key] = join(' ', $split);
			}
		}
		$k = 0;
		foreach($this->englishNotDefines as $key => $value)
		{
			$redSpot = $comment = $updatedMessage = FALSE;
			$englishString = $this->englishNotDefines[$key];
			if(array_key_exists($key, $this->referenceNotDefines))
			{
				if($this->referenceNotDefines[$key] != $englishString)
					$updatedMessage = TRUE;
			}
			if(array_key_exists($key, $originalArray))
			{
				if(array_key_exists($key . '_', $commentsArray))
					$comment = BR .
					HTML\span($commentsArray[$key . '_'], "hint");
				if($originalArray[$key])
					$originalString = htmlspecialchars($originalArray[$key], ENT_XML1);
			}
			else
				$originalString = "__WIKINDX__NOT_YET_TRANSLATED__";
// Note any untranslated messages
			if($originalString == "__WIKINDX__NOT_YET_TRANSLATED__")
			{
				$this->notYetTranslated[] = $key;
				$redSpot = $this->redSpot;
			}
			else if($updatedMessage)
			{
				if(array_search($key, $this->notYetTranslated) === FALSE)
					$this->notYetTranslated[] = $key;
				$redSpot = $this->orangeSpot;
			}
			$k++;
			$pString .= $this->tableRow($originalString, $englishString, $comment, $redSpot, $key, $k);
		}
		if(empty($this->notYetTranslated))
			$this->status = HTML\p($this->locMessages->text('edit5'));
		else
		{
			$index = 0;
			$this->status = HTML\p($this->locMessages->text('edit6'));
			$this->status .= HTML\tableStart();
			foreach($this->notYetTranslated as $key)
			{
				if(!$index)
					$this->status .= HTML\trStart();
				$this->status .= HTML\td(HTML\a("link", $key, "#$key"));
				$index++;
				if($index == 5)
				{
					$this->status .= HTML\trEnd();
					$index = 0;
				}
			}
			if($index && ($index != 5))
			{
				while($index < 5)
				{
					$this->status .= HTML\td("&nbsp;");
					$index++;
				}
				$this->status .= HTML\trEnd();
			}
			$this->status .= HTML\tableEnd() . HTML\hr();
		}
		return $pString;
	}
// Grab and display XML messages
	private function printXML($directory, $file)
	{
// If returning here after an error, get stored session variable (already htmlentities() encoded)
		if(is_array($this->sessionVars))
		{
			foreach($this->sessionVars as $key => $value)
				$originalArray[$key] = $value;
		}
		else
		{
			$fileXML = 'languages/' . $directory . "/" . $file . '.xml';
			$originalArray = $this->xmlLanguageCatalog2Array($fileXML);
		}
		$pString = '';
		$commentsArray = array();
		$this->notYetTranslated = array();
// Gather any English comments relating to arrays or to individual array entries
		if(!$lines = file("languages/en/$file.php"))
			$this->badInput->close(HTML\p($this->locMessages->text("noFileRead"), 'error', 'center'),
				$this->parentClass, "init");
		foreach($lines as $line)
		{
			if(mb_strpos($line, "/// ") === 0)
			{
				$split = UTF8::mb_explode(' ', $line);
				array_shift($split); // remove '///'
				$key = array_shift($split);
				$commentsArray[$key] = join(' ', $split);
			}
		}
		$k = 0;
		foreach($this->englishNotDefines as $key => $value)
		{
		    $k++;
			if($file == 'SUCCESS')
				$pString .= $this->grabSuccessElement($key, $value, $originalArray, $commentsArray, $k);
			else
			{
				foreach($value as $subKey => $subValue)
					$pString .= $this->grabXMLElement($key, $subKey, $subValue, $originalArray, $commentsArray, $k);
			}
		}
		if(empty($this->notYetTranslated))
			$this->status = HTML\p($this->locMessages->text('edit5'));
		else
		{
			$index = 0;
			$links = array();
			$this->status = HTML\tableStart();
			foreach($this->notYetTranslated as $key)
			{
				$status = '';
				if(!$index)
					$status .= HTML\trStart();
				$split = UTF8::mb_explode('_', $key, 2);
				if(sizeof($split) == 2)
				{
					if(!array_key_exists($split[0], $links))
					{
						$links[$split[0]] = FALSE;
						$status .= HTML\td(HTML\a("link", $split[0], "#$key"));
					}
					else
						$index--;
				}
				else
					$status .= HTML\td(HTML\a("link", $key, "#$key"));
				$index++;
				if($index == 5)
				{
					$status .= HTML\trEnd();
					$index = 0;
				}
				$this->status .= $status;
			}
			if($index && ($index != 5))
			{
				while($index < 5)
				{
					$this->status .= HTML\td("&nbsp;");
					$index++;
				}
				$this->status .= HTML\trEnd();
			}
			$this->status .= HTML\tableEnd() . HTML\hr();
			if(empty($links))
				$this->status = HTML\p($this->locMessages->text('edit6')) . $this->status;
			else
				$this->status = HTML\p($this->locMessages->text('edit7')) . $this->status;
		}
		return $pString;
	}
// grab element from SUCCESS.xml
	private function grabSuccessElement($key, $englishString, $originalArray, $commentsArray, $idx)
	{
		$redSpot = $comment = $elementFound = $updatedMessage = FALSE;
		if(array_key_exists($key, $this->referenceNotDefines))
		{
			if($this->referenceNotDefines[$key] != $englishString)
				$updatedMessage = TRUE;
		}
		if(array_key_exists($key, $originalArray) && (($element = $originalArray[$key]) != '__WIKINDX__NOT_YET_TRANSLATED__'))
		{
			if(array_key_exists($key . '_', $commentsArray))
				$comment = BR . HTML\span($commentsArray[$key . '_'], "hint");
			$originalString = $element;
			$elementFound = TRUE;
		}
		if(!$elementFound)
		{
			$originalString = "__WIKINDX__NOT_YET_TRANSLATED__";
			$this->notYetTranslated[] = $key;
			$redSpot = $this->redSpot;
		}
		else if($updatedMessage)
		{
			if(array_search($key, $this->notYetTranslated) === FALSE)
				$this->notYetTranslated[] = $key;
			$redSpot = $this->orangeSpot;
		}
		return $this->tableRow($originalString, $englishString, $comment, $redSpot, $key, $idx);
	}
// grab element from MESSAGES.xml or ERRORS.xml
	private function grabXMLElement($key, $subKey, $englishString, $originalArray, $commentsArray, $idx)
	{
		$redSpot = $comment = $elementFound = $updatedMessage = FALSE;
		$realKey = $key . '_' . $subKey;
		if(array_key_exists($key, $this->referenceNotDefines) && array_key_exists($subKey, $this->referenceNotDefines[$key]))
		{
			if($this->referenceNotDefines[$key][$subKey] != $englishString)
				$updatedMessage = TRUE;
		}
		if(array_key_exists($key, $originalArray))
		{
			$array = $originalArray[$key];
			if(array_key_exists($subKey, $array) && (($element = $array[$subKey]) != '__WIKINDX__NOT_YET_TRANSLATED__'))
			{
				if(array_key_exists($realKey, $commentsArray) && !array_key_exists($realKey, $this->usedComments))
				{
					$comment = BR . HTML\span($commentsArray[$realKey], "hint");
					$this->usedComments[$realKey] = FALSE;
				}
				if(array_key_exists($key . '_', $commentsArray) && !array_key_exists($key . '_', $this->usedComments))
				{
					$comment = BR . HTML\span($commentsArray[$key . '_'], "hint");
					$this->usedComments[$key . '_'] = FALSE;
				}
				$originalString = $element;
				$elementFound = TRUE;
			}
		}
		if(!$elementFound)
		{
			$originalString = "__WIKINDX__NOT_YET_TRANSLATED__";
			if(array_search($realKey, $this->notYetTranslated) === FALSE)
				$this->notYetTranslated[] = $realKey;
			$redSpot = $this->redSpot;
		}
		else if($updatedMessage)
		{
			if(array_search($realKey, $this->notYetTranslated) === FALSE)
				$this->notYetTranslated[] = $realKey;
			$redSpot = $this->orangeSpot;
		}
		return $this->tableRow($originalString, $englishString, $comment, $redSpot, $realKey, $idx);
	}
// produce table row
	private function tableRow($originalString, $englishString, $comment, $redSpot, $key, $idx)
	{
		$pString = HTML\trStart($idx % 2 ? 'alternate1' : 'alternate2');
		$pString .= HTML\td($key, $redSpot);

		$strlen = mb_strlen($originalString);

		$height = 1;
		if($strlen < 100)
			$height = 2;
		else if($strlen < 300)
			$height = 3;
		else if($strlen < 500)
			$height = 4;
		else if($strlen < 1000)
			$height = 8;
		else
			$height = 16;

		$pString .= HTML\td($englishString);
		$pString .= HTML\td(FORM\textAreaInput(FALSE, $key, $originalString, 60, $height));
		$pString .= HTML\td($comment);

		$pString .= HTML\trEnd();
		return $pString;
	}
// Check display edit input is valid
	private function checkEditInput()
	{
		if(!array_key_exists("editLanguage", $this->vars))
			$this->badInput->close(HTML\p($this->locMessages->text("noLanguageInput"), 'error', 'center'),
				 $this, 'display');
		if(!array_key_exists("editFile", $this->vars))
			$this->badInput->close(HTML\p($this->locMessages->text("noDirectoryInput"), 'error', 'center'),
				 $this, 'display');
	}
// Check write input is valid
	private function checkWriteInput()
	{
		foreach($this->expectedKeys as $keyInput)
		{
			if(array_key_exists($keyInput, $this->vars) && ($input = trim($this->vars[$keyInput])))
			{
				$this->input[$keyInput] = $input;
				$sessionVars[$keyInput] = $input;
			}
		}
		$this->session->writeArray($sessionVars, "localizationModule");
	}
// convert XML language catalog to array
	private function xmlLanguageCatalog2Array($xmlCatalogFile)
	{
		$sxi = new SimpleXmlIterator($xmlCatalogFile, NULL, TRUE);
		return $this->sxiToArray($sxi);
	}
// convert XML to array
	private function sxiToArray($sxi)
	{
	    $catArray = array();
		for($sxi->rewind(); $sxi->valid(); $sxi->next())
		{
			if($sxi->hasChildren())
				$catArray[$sxi->key()] = $this->sxiToArray($sxi->current());
			else
				$catArray[$sxi->key()] = htmlspecialchars(strval($sxi->current()), ENT_XML1);
		}
		return $catArray;
	}
}
?>