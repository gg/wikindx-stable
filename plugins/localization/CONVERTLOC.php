<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class CONVERTLOC
{
private $locMessages;
private $vars;

// constructor
	public function __construct()
	{
		include_once("core/messages/PLUGINMESSAGES.php");
		$this->locMessages = new PLUGINMESSAGES('localization', 'localizationMessages');
		$this->vars = GLOBALS::getVars();
	}
// Start the conversion
	public function process()
	{
// convert new English Help file
		if($message = $this->convertHelp())
			return $message;
		$files = array("ERRORS", "MESSAGES", "SUCCESS");
		foreach($files as $file)
		{
			if(!is_dir('languages/' . $this->vars['convertLanguage']))
				return HTML\p($this->locMessages->text('directoryExists') . ' languages/' . $this->vars['convertLanguage'], "error", "center");
			elseif((!file_exists('languages/' . $this->vars['convertLanguage'] . DIRECTORY_SEPARATOR . $file . '.php') &&
				!file_exists('languages/' . $this->vars['convertLanguage'] . DIRECTORY_SEPARATOR . $file . '.xml')))
				return HTML\p($this->locMessages->text('noFile') . ' ' . $file . '.php', "error", "center");
		}
		foreach($files as $file)
		{
			if(file_exists('languages/' . $this->vars['convertLanguage'] . DIRECTORY_SEPARATOR . $file . '.xml')) // already converted
				continue;
			include_once('languages/' . $this->vars['convertLanguage'] . "/$file.php");
			$className = $file . "_" .$this->vars['convertLanguage'];
			$class = new $className;
			$this->convert($class, $file);
		}
// remove old files
		@unlink('languages/' . $this->vars['convertLanguage'] . '/ABOUT.php');
		@unlink('languages/' . $this->vars['convertLanguage'] . '/HELPCONFIG.php');
		@unlink('languages/' . $this->vars['convertLanguage'] . '/HELPIMPORT.php');
		@unlink('languages/' . $this->vars['convertLanguage'] . '/HELPSTYLE.php');
		@unlink('languages/' . $this->vars['convertLanguage'] . '/USINGWIKINDX.php');
		@unlink('languages/' . $this->vars['convertLanguage'] . '/SUCCESS.php');
		@unlink('languages/' . $this->vars['convertLanguage'] . '/MESSAGES.php');
		@unlink('languages/' . $this->vars['convertLanguage'] . '/ERRORS.php');
		return HTML\p($this->locMessages->text('convertLocalization'), "success", "center");
	}
// convert HELP.php -- copy and change 'class HELP_en' to 'class HELP_xx' where 'xx' is the current language directory
	private function convertHelp()
	{
		if(!is_dir('languages/en'))
			return HTML\p($this->locMessages->text('directoryExists') . ' languages/en', "error", "center");
		if(!file_exists('languages/en/HELP.php'))
			return HTML\p($this->locMessages->text('noFile') . ' languages/en/HELP.php', "error", "center");
		if(!$file = file_get_contents("languages/en/HELP.php"))
			return HTML\p($this->locMessages->text("noFileRead") . ' languages/en/HELP.php', "error", "center");
// remove existing HELP.php
		@unlink('languages/' . $this->vars['convertLanguage'] . '/HELP.php');
		$file = preg_replace('/class HELP_en/Uus', 'class HELP_' . $this->vars['convertLanguage'], $file);

		$fw = FALSE;

		if($fh = fopen('languages/' . $this->vars['convertLanguage'] . '/HELP.php', "w"))
		{
			$fw = fwrite($fh, $file);

			fclose($fh);
		}

		if ($fh === FALSE || $fw === FALSE)
			return HTML\p($this->locMessages->text("noFileWrite") . 'HELP.php', "error", "center");
		else
			return FALSE; // success!
	}
// Convert each file
	private function convert($class, $file)
	{
		$arrays = $class->loadArrays();
// Start XML
		$fileString = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
		$fileString .= '<language xml:lang="en">';
		foreach($arrays as $arrayName => $array)
		{
			$fileString .= "<$arrayName>";
			if($file == 'SUCCESS') // only one array
				$fileString .= "<![CDATA[" . preg_replace("/\r|\n|\015|\012/u", "", $array) . "]]>";
			else
			{
				foreach($array as $key => $value)
					$fileString .= "<$key><![CDATA[" . preg_replace("/\r|\n|\015|\012/u", "", $value) . "]]></$key>";
			}
			$fileString .= "</$arrayName>";
		}
		$fileString .= "</language>";
		$xml = new SimpleXMLElement(html_entity_decode($fileString, ENT_NOQUOTES, "UTF-8"));
		$fileString = $xml->asXML();

        $xw = new XMLWriter();
        $xw->openMemory();
        $xw->startDocument("1.0");

        $xw->startElement("language");

		foreach($arrays as $arrayName => $array)
		{
		    $xw->startElement($arrayName);

			if($file == 'SUCCESS') // only one array
			    $xw->text($array);
			else
			{
                foreach($array as $key => $value)
                {
                    $xw->startElement($key);
                    $xw->text($value);
                    $xw->endElement();
                }
			}

			$xw->endElement();
		}

        $xw->endElement();

        $xw->endDocument();

        $bla = $xw->outputMemory();

        file_put_contents('languages/' . $this->vars['convertLanguage'] . "/_" . $file . ".xml", $bla);



		$fw = FALSE;

		if($fh = fopen('languages/' . $this->vars['convertLanguage'] . "/$file.xml", "w"))
		{
			$fw = fputs($fh, $fileString);

			fclose($fh);
		}

		if ($fh === FALSE || $fw === FALSE)
			HTML\p($this->locMessages->text("noFileWrite") . ": $file.xml", "error", "center");
	}
}
?>