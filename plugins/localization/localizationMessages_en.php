<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class localizationMessages_en
{
public $text;

	public function __construct()
	{

		$this->messages();
	}
// All headings, text hints etc.
	private function messages()
	{
		$this->text = array(
				"header"	=>	"Localization",
				"front1"	=>	"This module is intended to aid the creation and editing of WIKINDX localization files. All
				new localizations are stored in a separate directory within the languages/ directory. To remove a
				localization, either permanently remove that folder from the languages/ directory or temporarily disable it
				from the Admin|Administer Addons menu. The languages/ directory and, if
				you are editing localizations, any localization subdirectories and the files they contain, should be writeable by the web user.",
				"front2"	=>	"When creating a localization, you should remember that, although CONSTANTS.php will be initialized with English language
				constants, you will need to manually edit that file in an external text editor (and save it in UTF-8 format).
				This is because a small amount of PHP scripting is required, the content of which depends upon the language being used.",
				"front3"	=>	"Edit Localization",
				"front4"	=>	"Select the localization and file you wish to edit:",
				"front5"	=>	"Edit file",
				"front6"	=>	"Create Localization",
				"front7"	=>	"Enter the language name and a directory name (English alphabetic characters only) for
				this localization (usually based on the two-letter country code found at " . HTML\a("link", "wikipedia",
				"https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2", "_blank") . ") and, if you wish, your personal details
				for a README file:",
				"front8"	=>	"Directory",
				"front9"	=>	"Personal details",
				"front10"	=>	"<strong>NB</strong> -- You should be using the <strong>latest WIKINDX version</strong> before
				doing this (in order to have the most up-to-date English localization files) and should ensure that you are using
				the latest version of this plug-in.  In this case, and when upgrading a localization from a previous
				WIKINDX version, the module will
				automatically remove deleted English messages from your files and add additional English messages.  These additional
				messages will be marked as needing attention when you open a file for editing as will messages requiring updating.
				On this front page, changes to English messages between
				WIKINDX versions are assessed by comparing the file modification dates of the files in languages/xx/ with those
				of the files in languages/en/. However, as soon as you open a file and save it, the modification date of
				that file will be updated, regardless of whether you have dealt with the modified messages or not, and changes to
				messages in the corresponding languages/en/ file will no longer be noted on the front page.",
				"front11"	=>	"Localization",
				"front13"	=>	"Once you have completed a localization, please consider contacting the " . HTML\a("link",
				"WIKINDX&nbsp;project&nbsp;administrator", "https://sourceforge.net/projects/wikindx", "_blank") . "
				so that it may be made available to others.",
				"front14"	=>	"Convert v3 Localization",
				"front15"	=>	"Select the v3 language you wish to convert to v4.  After the old files have been converted to
				the new XML structure, they will be deleted (leaving README.txt, description.txt, HELP.php and CONSTANTS.php) so
				ensure they are backed up.",
				"edit1"	=>	"Words preceded by a '$' or enclosed with '#' are PHP variables that will be substituted in the core WIKINDX
				code.  Do not translate these.  NB -- they are case-sensitive.",
				"edit2"	=>	"3 consecutive #s appearing anywhere in a message will be replaced by text supplied
				by the core WIKINDX code. Do not remove it. You may reposition it though.",
				"edit3"	=>	"HTML codes such as '<br>' or '<em>....</em>' should
				be copied without change.  Text enclosed in such codes may be translated unless otherwise stated.",
				"edit4"	=>	"All fields must be filled in but you can always save then return to this file and continue editing another day.",
				"edit5"	=>	"This file
				appears to match the English equivalent and so is probably up-to-date.",
				"edit6"	=>	"The following messages require translation or updating:",
				"edit7"	=>	"The following groups contain messages (either added or updated) requiring translation:",
				"version"	=>	"The WIKINDX version is:",
				"noLanguageInput"	=>	"Missing language name",
				"noDirectoryInput"	=>	"Missing directory name",
				"badDirectoryInput"	=>	"Use only English
				alphabetic characters for the directory name",
				"directoryExists"	=>	"That directory name already exists",
				"noDirectoryCreate"	=>	"Unable to create directory",
				"noFileCreate"	=>	"Unable to create or open the file for writing",
				"noFileWrite"	=>	"Unable to write to file",
				"noFileRead"	=>	"Unable to read file",
				"noFile"	=>	"File does not exist:",
				"noHidden"	=>	"Missing hidden form input",
				"noKeyInput"	=>	"Missing text input",
				"corruptFile"	=>	"File is corrupted.  Please ensure that all messages are on one line with no
				linebreaks or newlines",
				"missingInput"	=>	"Missing input",
				"createLocalization"	=>	"Successfully created new prototype localization",
				"convertLocalization"	=>	"Successfully converted localization.  You may now edit it",
				"editFile"	=>	"Successfully edited file",
				"changes" => "Edits appear to be required for the following localization directories: ###",
				"noChanges"	=>	"All localizations appear to be up to date (but read the notice above).",
				"moveReference"	=>	"If you have completed your localization, update the reference files so that the plugin knows
				your localization is up-to-date.",
		);
	}
}
?>