<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class CREATELOC
{
private $locMessages;
private $vars;
private $badInput;
private $parentClass;

	public function __construct($parentClass)
	{
		$this->parentClass = $parentClass;
		$this->vars = GLOBALS::getVars();
		$this->badInput = FACTORY_BADINPUT::getInstance();

		include_once("core/messages/PLUGINMESSAGES.php");
		$this->locMessages = new PLUGINMESSAGES('localization', 'localizationMessages');
	}
// Create the initial localization files.
	public function start()
	{
		$this->checkCreateInput();
		$languageArray = UTF8::mb_explode(' ', mb_strtolower(trim($this->vars['language'])));
		foreach($languageArray as $key => $value)
			$languageArray[$key] = ucfirst($value);
		$language = join(' ', $languageArray);
		$directory = mb_strtolower(trim($this->vars['directory']));
		$this->createDirectory($directory);
		$personalDetails = trim($this->vars['personalDetails']);
		if($personalDetails)
			$this->createFile($personalDetails, "README.txt", $directory);
		$this->createFile($language, "description.txt", $directory);
		$licence = $this->createLicence();
		$files = array("HELP", "ERRORS", "MESSAGES", "SUCCESS", "CONSTANTS");
		foreach($files as $file)
		{
			if($file == 'MESSAGES' || $file == 'ERRORS' || $file == 'SUCCESS')
			{
        		include_once("languages/en/$file.php");
        		$className = $file . '_en';
        		$class = new $className();
        		$catArray = $class->loadArrays();

        	    $this->createXMLCatalog($directory, "$file.xml", $catArray);
			}
			else if($file == 'HELP')
			{
				$pString = $this->printHeader($language, $file, $directory, $licence);
				$pString .= $this->createHelp($directory);
				$this->createFile($pString, "HELP.php", $directory);
			}
			else if($file == 'CONSTANTS')
			{
				$pString = $this->printHeader($language, $file, $directory, $licence);
				$pString .= $this->createConstants($directory);
				$this->createFile($pString, "CONSTANTS.php", $directory);
			}
		}
		include_once("core/display/CLOSE.php");
		new CLOSE($this->parentClass->init(HTML\p($this->locMessages->text("createLocalization"),
			"success", "center")));
	}
// Create the CONSTANTS.php file.  This is the only one that must be edited by hand so copy from the English file and modify the CONSTANTS_en string
	private function createConstants($directory)
	{
		if(!$lines = file("languages/en/CONSTANTS.php"))
			$this->badInput(HTML\p($this->locMessages->text("noFileRead"), 'error', 'center'), $this->parentClass, "init");
// Discard everything up to appearance of '// START__LOCALIZATION__MODULE__COPY'
		$startCopy = FALSE;
		$pString = '';
		foreach($lines as $line)
		{
			if(!$startCopy && mb_strpos($line, "// START__LOCALIZATION__MODULE__COPY") === FALSE)
				continue;
			else
				$startCopy = TRUE;
			if(mb_strpos($line, "CONSTANTS_en") !== FALSE)
				$pString .= str_replace("CONSTANTS_en", "CONSTANTS_" . $directory, $line);
			else
				$pString .= $line;
		}
		return $pString;
	}

	private function createXMLCatalog($directory, $catalogFileName, $stringArray)
	{
        $version = '1.0';
        $encoding = 'UTF-8';
        $standalone = 'yes';
        $rootName = 'language';
        $identString = "\t";

// Start XML
        $xw = new XMLWriter();
        $xw->openMemory();
        $xw->startDocument($version, $encoding, $standalone);

        $xw->startElement($rootName);
        $xw->writeAttributeNs('xml', 'lang', NULL, $directory);

        $xw->setIndentString($identString);
        $xw->setIndent(TRUE);

		foreach($stringArray as $keyLevel1 => $valueLevel1)
		{
            $xw->startElement($keyLevel1);

            if (is_array($valueLevel1))
            {
        		foreach($valueLevel1 as $keyLevel2 => $valueLevel2)
        		{
                    $xw->startElement($keyLevel2);
                    $xw->writeCdata("__WIKINDX__NOT_YET_TRANSLATED__");
                    $xw->endElement();
        		}
    		}
    		else
    		    $xw->writeCdata("__WIKINDX__NOT_YET_TRANSLATED__");

            $xw->endElement();
		}

        $xw->endElement();

        $xw->endDocument();

        $pString = $xw->outputMemory();

        file_put_contents('languages/' . $directory . "/$catalogFileName", $pString);
	}

// Create prototype HELP.php
	private function createHelp($directory)
	{
		$pString = <<< END
class HELP_$directory
{
	public function __construct()
	{
	}
	public function loadArrays()
	{
		return array(
// START__LOCALIZATION__MODULE__EDIT
END;
// Get all messages from English  HELP.php
		include_once("languages/en/HELP.php");
		$class = new HELP_en();
		$array = $class->loadArrays();
		foreach($array as $key => $null)
			$pString .= "\n\t\t\t\"$key\"\t=>\t\"__WIKINDX__NOT_YET_TRANSLATED__\",";
		$pString .= "\n// END__LOCALIZATION__MODULE__EDIT\n\t\t);\n\t}\n}\n?>";
		return $pString;
	}
// Create and write file.
	private function createFile($input, $file, $directory)
	{
		if($fp = fopen("languages/$directory/$file", "w"))
		{
			if(!fputs($fp, $input))
				$this->badInput->close(HTML\p($this->locMessages->text('noFileWrite'), 'error', 'center'), $this->parentClass, "init");

			fclose($fp);
		}
		else
		{
			$this->badInput->close(HTML\p($this->locMessages->text('noFileCreate'), 'error', 'center'), $this->parentClass, "init");
		}
	}
// Create directory - return error if exists or unable to create
	private function createDirectory($directory)
	{
		if(is_dir("languages/$directory"))
			$this->badInput->close(HTML\p($this->locMessages->text('directoryExists'), 'error', 'center'), $this->parentClass, "init");
		if(!mkdir("languages/$directory"))
			$this->badInput->close(HTML\p($this->locMessages->text('noDirectoryCreate'), 'error', 'center'), $this->parentClass, "init");
	}
// Print header for each .php file
	private function printHeader($language, $file, $directory, $licence)
	{
		$pString = "<?php\n/**********************************************************************************";
		$pString .= $licence;
		$pString .= "\n\n" . $file . "_" . "$directory WIKINDX localization class -- $language";
		$pString .= "\n**********************************************************************************/";
		$pString .= "\n\n";
		return $pString;
	}
// Licence info string for .php files
	private function createLicence()
	{
		$date = getDate();
		$year = $date['year'];
		return <<< END
\nWIKINDX: Bibliographic Management system.
Copyright (C)

This work is licensed under a Creative Commons
Attribution-NonCommercial-ShareAlike 2.0 License.
For information, see docs/LICENCE.txt.

\nThe WIKINDX Team $year\nsirfragalot@users.sourceforge.net
END;
	}
// Check create input is valid
	private function checkCreateInput()
	{
		if(!array_key_exists("language", $this->vars) || !trim($this->vars['language']))
			$this->badInput->close(HTML\p($this->locMessages->text("noLanguageInput"), 'error', 'center'), $this->parentClass, "init");
		if(!array_key_exists("directory", $this->vars) || !trim($this->vars['directory']))
			$this->badInput->close(HTML\p($this->locMessages->text("noDirectoryInput"), 'error', 'center'), $this->parentClass, "init");
		if(preg_match("/[^a-zA-Z]/u", trim($this->vars['directory'])))
			$this->badInput->close(HTML\p($this->locMessages->text("badDirectoryInput"), 'error', 'center'), $this->parentClass, "init");
	}
}
?>