**************************************
** 		   Localized front page		**
**			description		  		**
**				v1.3				**
**		  WIKINDX module			**
**************************************

NB. this module is compatible with WIKINDX v5 and up.  Results may be unexpected if used with a lower version.

Store and make available localized versions of the front page description depending on the current language 
localization the user is using.

The module registers itself in the 'Admin' menu.


Unzip this file (with directory structure) into wikindx5/plugins/


CHANGELOG:
v1.3
1. Added a check in case there are no languages installed other than English

v1.2
1.  Plugin now compatible with WIKINDX v5.x

v1.1 ~ 20th May 2013
1.  Initial release

Mark Grimshaw 2018.