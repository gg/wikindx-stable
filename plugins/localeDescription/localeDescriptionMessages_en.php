<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class localeDescriptionMessages_en
{
public $text;

	public function __construct()
	{
		$this->messages();
	}
// All headings, text hints etc.
	private function messages()
	{
		$this->text = array(
				"menu"	=>	"Localize Front Page",
				"heading"	=>	"Localize Front Page Description",
				"text1"	=>	"Here, you can set an alternate front page description for each localization language you have installed.
							 Alternate descriptions are stored in the config_ database as configDescription_xx where 'xx' is the localization
							 folder name. The original description, set in Admin|Configure, is displayed whenever the user changes to a localization
							language for which there is no alternate front page description.",
				"text2"	=>	"If you empty the description and submit, you are removing the database field.",
				"choose"	=>	"Choose localization",
				"original"	=>	"Original Admin|Configure description",
				"missingLanguage"	=>	"Missing language",
				"success"		=>	"Successfully edited ###",
				"onlyEnglish"	=>	"Only English is available on this WIKINDX – set the front page description via the Admin|Configure interface",
			);
	}
}
?>