**************************************
** 		   Choose Language			**
**				v1.2				**
**		  WIKINDX module			**
**************************************

NB. this module is compatible with WIKINDX v5 and up.

A small select box for users to quickly and efficiently change language 
localization without the need to edit their preferences.  Any change 
is not permanent and will be lost when the user logs out.

If the WIKINDX does not have two or more languages available to users, the 
plugin will not display.

The module registers itself as an inline plugin.

Unzip this file (with directory structure) into wikindx5/plugins/


CHANGELOG:

v1.2
1.  Plugin now compatible with WIKINDX v5.x

v1.1
Plugin compatible only with WIKINDX v4.2.x

v1.0
Initial release.

Mark Grimshaw 2015.