**************************************
** 		 wordProcessor 		 		**
**				v1.5				**
**		  WIKINDX module			**
**************************************

NB. this module is compatible with WIKINDX v5 and up.  Results may be unexpected (at least) if used with a 
lower version.

Logged in users can use a WYSIWYG word processor for the writing of articles.  WIKINDX resources can be cited,  
bibliographic and citation styles can be applied to the article and the article exported in Rich Text Format 
(.rtf) which can be opened in most common word processors including OpenOffice.org and Word. 

The module registers itself in the 'plugin1' menu but this can be changed from the WIKINDX Admin|Plugins 
interface.

Unzip this file (with directory structure) into wikindx5/plugins/

Papers can be imported from earlier versions by using the 'Import paper' function.

Mark Grimshaw 2017.

CHANGELOG
v1.5
1.  Plugin now compatible with WIKINDX v5.x
2.  Some minor debugging for listing/opening/deleting files.
3.  Imagemagick no longer used.

v1.4
On some server environments, filepaths were incorrect for some tinyMCE functionality (core wikindx must be updated to at least v4.2.2).

v1.3
Plugin compatible only with WIKINDX v4.2.x

v1.2
Added a check on startup for deleted wikindx users -- if found, delete entries from db table and papers/ folder.

Mark Grimshaw 2015.