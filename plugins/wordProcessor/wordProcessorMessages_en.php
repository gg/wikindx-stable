<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
*	WORDPROCESSOR plugin -- messages.
*
*****/
class wordProcessorMessages_en
{
public $text;

// Constructor
	public function __construct()
	{
		$this->loadMessages();
	}
	public function loadMessages()
	{
		$this->text = array(
/**
* Menu items
*/
				"wpSub"		=>		"Word Processor...",
				"wpOpen"	=>		"Open Paper",
				"wpList"		=>		"List Papers",
				"wpDelete"		=>		"Delete Paper",
				"wpImport"		=>		"Import Paper",
				"wpNew"		=>		"New Paper",
/**
* Other messages
*/
				'headingPaper'	=>	'Word Processor',
				'headingImport'	=>	'Import Paper',
				'headingOpen'	=>	'Open Paper',
				'headingDelete'	=>	'Delete Paper',
				'headingListPapers'	=>	'List Papers',
				"headingSavePaper"	=>	'Save Paper',
				"headingExportPaper"	=>	'Export & Save Paper',
				"headingAddFootnote"	=>	"Add Footnote",
				"headingAddTable"	=>	"Add Table",
				"headingAddImage"	=>	"Add Image",
				"headingAddLink"	=>	"Add Link",
				"headingAddCitation"	=>	"Add Citation",
				"paperTitle"	=>	"Title of Paper",
				"revert"	=>		"Revert to saved version",
				"revertConfirm"	=>	"Are you sure you wish to revert to the saved version?",
				"new"		=>		"New paper",
				"open"		=>		"Open paper",
				"delete"	=>		"Delete paper",
				"deleteConfirm"		=>	"Delete paper(s): ",
				"deletePaper"	=>	"Successfully deleted paper(s)",
				"list"		=>		"List papers",
				"backup"	=>		"You can download and backup papers if you wish. The newest files are displayed first.",
				"noPapers"	=>		"You have no papers",
				"import"	=>		"Import paper",
				"import2"	=>		"You can import a paper here which should be a plain file as backed up from WIKINDX",
				"import3"	=>	"Filename",
/// browserTry When WIKINDX detects what is probably an incompatible browser for the SUWP, issue a warning and give the option to give it a try anyway
				"browserTry"	=>	"Try to use the SUWP anyway",
/// noAutoSave Warning that there is no autosave or prompt to save when user accesses another menu item without saving first!
				"noAutosave"	=>	"There is no autosave or save prompt",
/// menuRestore The SUWP menu is hidden for safety reasons when an edit has been made to the paper.  This is a hyperlink that restores it.
				"menuRestore"	=>	"Restore menu",
				"appendPaper"	=>	"Paper has now been loaded ready for appending",
/// addSection Add a section break before appending the paper
				"addSection"	=>	"Add section break before appended paper:",
/// citeTagOnly When searching and inserting metadata in the SUWP, insert only the cite tag (and any pages)
				"citeTagOnly"	=>	"Insert only the cite tag",
				"paperExists"	=>	"The paper you are trying to import already exists in this wikindx",
				"savePaper"	=>	"Successfully saved paper",
				"deletePaper"	=>	"Successfully deleted paper(s)",
				"importPaper"	=>	"Successfully imported paper",
/// loadAppendPaper Load a paper ready for appending one paper to another in the WP
				"loadAppendPaper"	=>	"Load paper",
				"alreadyDeleted"	=>	"You have already deleted that paper",
				"savedStatus"	=>	"Paper saved",
				"notSavedStatus"	=>	"Paper not yet saved",
				"saveFailure"	=>	"Paper not saved: write error",
				"invalidTitle"	=>	"Paper not saved: invalid title",
				"saveAsNewVersion"	=>	"Save new version",
/// Export functions
				"export"	=>	"Export paper to",
				"exportRtf"	=>	"Rich Text Format (RTF)",
/// lineSpacePaper Line spacing for paper exporting to RTF etc.
				"lineSpacePaper"	=>	"Paper line space",
				"lineSpaceBib"	=>	"Bibliography line space",
				"singleSpace"	=>	"Single",
				"oneHalfSpace"	=>	"1.5 lines",
				"doubleSpace"	=>	"Double",
/// indentBib Bibliography indentation for exporting to RTF etc.
				"indentBib"		=>	"Bibliography indentation",
				"indentNone"	=>	"None",
				"indentAll"		=>	"All",
				"indentFL"		=>	"First line",
				"indentNotFL"	=>	"All but first line",
/// indentFt Spacing, font size and indentation for footnotes
				"indentFt"		=>	"Footnote indentation",
				"lineSpaceFt"	=>	"Footnote line space",
				"fontSizeFt"	=>	"Footnote font size",
/// pageNumber Page numbering for RTF exports
				"pageNumber"	=>	"Page numbering",
				"pageNumberFooter"	=>	"Footer",
				"pageNumberHeader"	=>	"Header",
				"pageNumberNone"	=>	"None",
/// pageNumberalign Page number alignment for RTF exports
				"pageNumberAlign"	=>	"Page number alignment",
				"pageNumberAlignCentre"	=>	"Centre",
				"pageNumberAlignLeft"	=>	"Left",
				"pageNumberAlignRight"	=>	"Right",
/// indentQuoteWords Indentation options for large quotations
				"indentQuoteWords"	=>	"Indent quotations with at least this number of words",
				"lineSpaceIndentQ"	=>	"Quotation line space",
				"indentQuoteFontSize"	=>	"Font size",
				"indentQuoteMarks"	=>	"Keep quotation marks",
				"exportAndSave"	=>	"Export and Save",
/// sectionFtRestart If a section has been inserted in the paper, any footnotes are renumbered from 1 at each section
				"sectionFtRestart" => "Restart footnotes at each section",
/// paperSize Size of exported paper (letter, A4 etc.)
				"paperSize"	=>	"Paper size",
				"tableColumns"	=>	"Columns",
				"tableRows"	=>	"Rows",
				"imagePath"	=>	"Image URL",
				"linkPath"	=>	"URL",
			);
	}
}
?>