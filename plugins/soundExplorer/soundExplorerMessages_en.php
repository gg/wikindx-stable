<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
*	SOUNDEXPLORER plugin -- English messages.
*
*****/
class soundExplorerMessages_en
{
public $text;

// Constructor
	public function __construct()
	{
		$this->loadMessages();
	}
	private function loadMessages()
	{
		$this->text = array(
/**
* Menu items
*/
				"se"	=>		"Sound Explorer",
				"seExplain"	=>	"Sound Explorer will store searches then, at any time in the future, play a sound if a resource matching
					a past search is found in a current list operation. This works on the principle of a serendipitous conjunction of past searches
					with a current search result, hopefully allowing you to make new creative connections by prodding at the boundaries of your
					current conceptual space.  But it is also useful in a multi-user
					WIKINDX where it functions like a 'tripwire' signal -- the sound will play if a new resource or metadata has been added by another
					user that matches a stored search (if the resource is being displayed in a list). It is currently an experimental
					prototype and has been tested in the Firefox, Safari, and Chrome web browsers. Other browsers are untested.",
				"seOn"	=>		"Sound Explorer On",
				"seOff"	=>		"Sound Explorer Off",
				"seLabel"	=>	"Search Label",
				"seSound"	=>	"Sound",
				"seSearchStatus"	=>	"Status",
				"seEnabled"	=>	"Enabled",
				"seDisabled"	=>	"Disabled",
				"seNewSearch"	=>	"New search",
				"seSearchDelete"	=>	"Delete",
				"seSearchNote"	=>	"Notes",
				"seMatchedSearches"	=>	"Matched searches for current list",
				"seToggleSuccess"	=> "Sound Explorer successfully configured",
				"seSearchSubmit"	=>	"Store the Sound Explorer search",
				"seStoreSuccess"	=> "Sound Explorer search successfully stored",
				"seDeleteSuccess"	=> "Sound Explorer search successfully deleted",
			);
	}
}
?>