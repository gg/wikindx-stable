**************************************
** 		   Sound Explorer			**
**				v2.1				**
**		  WIKINDX module			**
**************************************

NB. this module is compatible with WIKINDX v5 and up.

Sound Explorer will store searches then, at any time in the future, play a sound if a resource matching 
a past search is found in a current list operation. This works on the principle of a serendipitous conjunction of past searches 
with a current search result, hopefully allowing you to make new creative connections by prodding at the boundaries of your 
current conceptual space.  But it is also useful in a multi-user 
WIKINDX where it functions like a 'tripwire' signal -- the sound will play if a new resource or metadata has been added by another 
user that matches a stored search (if the resource is being displayed in a list). It is currently an experimental 
prototype and hjas been tested in the Firefox, Safari, and Chrome web browsers. Other browsers are untested.

The module registers itself as an inline plugin and is only for registered WIKINDX users although this can be changed in the plugin's index.php.

Unzip this file (with directory structure) into wikindx5/plugins/


CHANGELOG:
v2.1
1.  Plugin reconfigured to use HTML5 audio.

v1.2
1.  Plugin now compatible with WIKINDX v5.x

v1.1
Plugin compatible only with WIKINDX v4.2.x

v1.0
Initial release.

Mark Grimshaw 2017.