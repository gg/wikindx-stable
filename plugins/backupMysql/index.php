<?php
/*****
*	backupMysql class.
*
*	v1.3 2017
*
*	Backup the database
*
*       Support MySQL db engine
*
*	Database dumps are written into plugins/backupMysql/dumps/ which should be writeable by the webserver user
*
*****/

/**
* Import initial configuration and initialize the web server
*/
include_once("core/startup/WEBSERVERCONFIG.php");


class backupMysql_MODULE
{
const DUMP_DIRECTORY = 'plugins/backupMysql/dumps';

private $messages;
private $coreMessages;
private $errors;
private $config;
private $session;
private $vars;

public $authorize;
public $menus;

// Constructor.
// $menuInit is TRUE if called from MENU.php
	public function __construct($menuInit = FALSE)
	{
		include_once("core/messages/PLUGINMESSAGES.php");
		$this->messages = new PLUGINMESSAGES('backupMysql', 'backupMysqlMessages');
		$this->coreMessages = FACTORY_MESSAGES::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		include_once("plugins/backupMysql/config.php");
		$this->config = new backupMysql_CONFIG();
		$this->session = FACTORY_SESSION::getInstance();
		$this->authorize = $this->config->authorize;
		if($menuInit)
		{
			$this->makeMenu($this->config->menus);
			return; // Need do nothing more as this is simply menu initialisation.
		}

		$authorize = FACTORY_AUTHORIZE::getInstance();
		if(!$authorize->isPluginExecutionAuthorised($this->authorize)) // not authorised
			FACTORY_CLOSENOMENU::getInstance(); // die

		$this->vars = GLOBALS::getVars();
		GLOBALS::setTplVar('heading', $this->messages->text('heading'));
}
// Make the menus
	private function makeMenu($menuArray)
	{
		$this->menus = array($menuArray[0] => array($this->messages->text('menu') => "init"));
	}
// This is the initial method called from the menu item.
	public function init()
	{
		return $this->display();
	}
// display
	public function display($message = FALSE)
	{
		if($message)
			$pString = $message;
		else
			$pString = '';
		$pString .= HTML\tableStart('generalTable borderStyleSolid');
		$pString .= HTML\trStart();
		$td = FORM\formHeader("backupMysql_backup");
		$td .= HTML\p(FORM\formSubmit($this->messages->text('heading')));
		$td .= FORM\formEnd();
		$pString .= HTML\td($td);
		$files = $this->listFiles();
		if(!empty($files))
		{
			$td = '';
			foreach($files as $fileName => $tStamp)
			{
				$td .= HTML\a("link", $fileName, "index.php?action=backupMysql_downloadFile" .
				htmlentities("&filename=" . $fileName), "_blank") . BR . LF;
			}
			$pString .= HTML\td($td);
		}
		if(!empty($files))
		{
			$td = HTML\td($this->deleteList(array_keys($files)));
			$pString .= HTML\td($td);
		}
		$pString .= HTML\trEnd();
		$pString .= HTML\tableEnd();
		GLOBALS::addTplVar('content', $pString);
	}
	public function downloadFile($message = FALSE, $errorMethod = FALSE)
	{
		$dirName = 'plugins/backupMysql/dumps/';
		$filename = $this->vars['filename'];
		if(file_exists($dirName . "/" . $filename) === FALSE)
		{
			$this->badInput->closeType = 'closePopup';
			$this->badInput->close($this->errors->text("file", "missing"));
			die;
		}
		else
		{
		    $type = 'application/x-sql+gzip';
		    $size = filesize($dirName . "/" . $filename);
		    $lastmodified = date(DateTime::RFC1123, filemtime($dirName . "/" . $filename));
		}
		FILE\setHeaders($type, $size, $filename, $lastmodified);
		FILE\readfile_chunked($dirName . "/" . $filename);
		die;
	}
/**
* List all dumps for deleting
*/
	private function deleteList($files)
	{
		foreach($files as $file)
			$fileArray[$file] = $file;
		$td = FORM\formHeader("backupMysql_delete");
		$td .= FORM\selectFBoxValueMultiple(FALSE, 'files', $fileArray, 10) .
			BR . HTML\span($this->coreMessages->text("hint", "multiples"), 'hint');
		$td .= HTML\p(FORM\formSubmit('Delete'));
		$td .= FORM\formEnd();
		return $td;
	}
/**
* Delete chosen files
*/
	public function delete()
	{
		if(!array_key_exists('files', $this->vars))
		{
			$this->display($this->errors->text("inputError", "missing"));
			return;
		}
		foreach($this->vars['files'] as $file)
			@unlink('plugins/backupMysql/dumps/' . $file);
		$this->display(HTML\p($this->messages->text("deleted"), 'success'));
	}
/**
* list all files in dump/ directory
*
* @return array $fileArray
*/
	private function listFiles()
	{
		$fileArray = array();

		if(file_exists(self::DUMP_DIRECTORY))
		{
    		$dh = opendir(self::DUMP_DIRECTORY);

    		while(FALSE !== ($file = readdir($dh)))
    		{
    			if(($file == ".") || ($file == "..") || ($file == 'index.html'))
    				continue;
    			if(($file == 'CVS') || (mb_strpos($file, '.') === 0))
    				continue;
    			$fileArray[$file] = filemtime('plugins/backupMysql/dumps/' . $file);
    		}

    		asort($fileArray, SORT_NUMERIC);
    		$fileArray = array_reverse($fileArray);

    		closedir($dh);
		}

		return $fileArray;
	}
// backup the database
	public function backup()
	{
	// Save memory limit configuration
	$memory_limit = ini_get ('memory_limit');

	// Use an unlimited memmory temporarily,
	// because the recordset can be really huge
	ini_set('memory_limit', '-1');

/*
	|--------------------------------------------------|
	|    Example MySQL Backup File                      |
	|                                                   |
	|    Written by: Justin Keller <kobenews@cox.net>   |
	|   Released under GNU Public license.             |
	|                                                  |
	|    Only use with MySQL database backup class,     |
	|    version 1.0.0 written by Vagharshak Tozalakyan |
	|    <vagh@armdex.com>.                             |
	|--------------------------------------------------|
*/
// Check dumps/ is writable
		if(!is_writable(self::DUMP_DIRECTORY))
		{
			$this->display(HTML\p($this->messages->text('noWrite', mb_substr(sprintf('%o', fileperms(self::DUMP_DIRECTORY)), -4)), 'error'));
			return;
		}
		require_once 'mysql_backup.class.php';
		$backup_obj = new MySQL_Backup();

//----------------------- EDIT - REQUIRED SETUP VARIABLES -----------------------


//Tables you wish to backup. All tables in the database will be backed up if this array is null.
		$backup_obj->tables = array();

//------------------------ END - REQUIRED SETUP VARIABLES -----------------------

//-------------------- OPTIONAL PREFERENCE VARIABLES ---------------------

//Add DROP TABLE IF EXISTS queries before CREATE TABLE in backup file.
		$backup_obj->drop_tables = TRUE;

//Only structure of the tables will be backed up if true.
		$backup_obj->struct_only = FALSE;

//Include comments in backup file if true.
		$backup_obj->comments = TRUE;

//Directory on the server where the backup file will be placed. Used only if task parameter equals MSB_SAVE.
		$backup_obj->backup_dir = 'plugins/backupMysql/dumps/';

//Default file name format.
		$backup_obj->fname_format = 'd_m_y__H_i_s';

//--------------------- END - OPTIONAL PREFERENCE VARIABLES ---------------------

//---------------------- EDIT - REQUIRED EXECUTE VARIABLES ----------------------

/*
	Task:
		MSB_STRING - Return SQL commands as a single output string.
		MSB_SAVE - Create the backup file on the server.
		MSB_DOWNLOAD - Download backup file to the user's computer.
*/
		$task = MSB_SAVE;

//Optional name of backup file if using 'MSB_SAVE' or 'MSB_DOWNLOAD'. If nothing is passed, the default file name format will be used.
		$filename = '';

//Use GZip compression if using 'MSB_SAVE' or 'MSB_DOWNLOAD'?
		$use_gzip = TRUE;

//--------------------- END - REQUIRED EXECUTE VARIABLES ----------------------

//-------------------- NO NEED TO ANYTHING BELOW THIS LINE --------------------

		if (!$backup_obj->Execute($task, $filename, $use_gzip))
			$output = HTML\p($backup_obj->error, 'error');
		else
			$output = HTML\p('Operation Completed Successfully At: <strong>' . date('g:i:s A') . '</strong><em> ( Local Server Time )</em>', 'success');
		$this->display($output);

	// Restore memory limit configuration
	ini_set('memory_limit', $memory_limit);
	}
}
?>