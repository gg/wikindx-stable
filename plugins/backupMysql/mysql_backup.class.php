<?php

/*
  MySQL database backup class, version 1.0.0
  Written by Vagharshak Tozalakyan <vagh@armdex.com>
  Released under GNU Public license

  Adaptations for mysqli, to handle NULLs etc. by Mark Grimshaw April 2013
  Change mysql drivers form mysql to mysqli (PHP >= 5.5.x compliance) by S. Aulery 2016

  NB (8 june 2016): It seems that this class is no longer maintained by the original author
*/


define('MSB_VERSION', '1.4.0');

define('MSB_NL', "\r\n");

define('MSB_STRING', 0);
define('MSB_DOWNLOAD', 1);
define('MSB_SAVE', 2);
define('PORT_DEFAULT', 3306);

class MySQL_Backup
{
	var $server = 'localhost';
	var $port = PORT_DEFAULT;
	var $username = 'root';
	var $password = '';
	var $database = '';
	var $link_id = -1;
	var $connected = false;
	var $tables = array();
	var $drop_tables = true;
	var $struct_only = false;
	var $comments = true;
	var $backup_dir = '';
	var $fname_format = 'd_m_y__H_i_s';
	var $error = '';
	var $errno = 0;

	private $db;
	private $vars;
	private $prefix;


	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$config = FACTORY_CONFIG::getInstance();
		$this->server = $config->WIKINDX_DB_HOST;
		$this->username = $config->WIKINDX_DB_USER;
		$this->password = $config->WIKINDX_DB_PASSWORD;
		$this->database = $config->WIKINDX_DB;
		$this->prefix = $config->WIKINDX_DB_TABLEPREFIX;
	}


	function Execute($task = MSB_STRING, $fname = '', $compress = false)
	{
		if (!($sql = $this->_Retrieve()))
		{
			return false;
		}

		if ($task == MSB_SAVE)
		{
			if (empty($fname))
			{
				$fname = $this->backup_dir;
				$fname .= date($this->fname_format);
				$fname .= ($compress ? '.sql.gz' : '.sql');
			}

			return $this->_SaveToFile($fname, $sql, $compress);
		}
		elseif ($task == MSB_DOWNLOAD)
		{
			if (empty($fname))
			{
				$fname = date($this->fname_format);
				$fname .= ($compress ? '.sql.gz' : '.sql');
			}

			return $this->_DownloadFile($fname, $sql, $compress);
		}
		else
		{
			return $sql;
		}
	}


	function _Query($sql)
	{
		$result = $this->db->query($sql);

		$this->error = $this->db->error;
		$this->errno = $this->db->errno;

		return $result;
	}


	function _GetTables()
	{
		$value = array();
		$tables = $this->db->listTables(TRUE);

		foreach($tables as $table)
		{
			if (empty($this->tables) || in_array($table, $this->tables))
			{
			    // Process only tables of Wikindx
			    if (substr($table, 0, strlen($this->prefix)) == $this->prefix)
			        $value[] = $table;
			}
		}

		if (!sizeof($value))
		{
			$this->error = 'No tables found in database.';
			$this->errno = $this->db->errno;
			return false;
		}

		return $value;
	}


	function _DumpTable($table, $nulls)
	{
		$value = '';

		if ($this->comments)
		{
			$value .= '#' . MSB_NL;
			$value .= '# Table structure for table `' . $table . '`' . MSB_NL;
			$value .= '#' . MSB_NL . MSB_NL;
		}

		if ($this->drop_tables)
		{
			$value .= 'DROP TABLE IF EXISTS `' . $table . '`;' . MSB_NL;
		}

		// Lock the table
		// Generetable a CREATE script for the table
		// Unlock the table
		$sql = "SHOW CREATE TABLE `$table`;";

		// Try to generate the CREATE script
		if (!($result = $this->_Query($sql)))
			return false;

		$row = $this->db->fetchRow($result);
		$value .= str_replace("\n", MSB_NL, stripslashes($row['Create Table'])) . ';';
		$value .= MSB_NL . MSB_NL;

		if (!$this->struct_only)
		{
			if ($this->comments)
			{
				$value .= '#' . MSB_NL;
				$value .= '# Dumping data for table `' . $table . '`' . MSB_NL;
				$value .= '#' . MSB_NL . MSB_NL;
			}

			$value .= $this->_GetInserts($table, $nulls);
		}

		$value .= MSB_NL . MSB_NL;

		return $value;
	}


	function _GetInserts($table, $nulls)
	{
		$value = '';
		$prefix = $this->prefix;

		// Lock the table
		$this->_Query("LOCK TABLES `$table` WRITE;");

		// Select all rows of the table
		$sql = $this->db->selectNoExecute(preg_replace("/$prefix/ui", '', $table), '*') . ";\n";

		if (($result = $this->db->query($sql)))
		{
			$row = $this->db->fetchRow($result);

			if (is_array($row))
			{
				$fields = array();
				foreach ($row as $field => $data)
				{
					$fields[] = "`" . $field . "`";
				}

				$listOfFields = '(' . join(',', $fields) . ')';
				$listValues = '';

				do {
					$array = array();
					foreach ($row as $field => $data)
					{
						if(!$data && (array_search($field, $nulls) !== FALSE))
							$array[] = 'NULL';
						elseif(ctype_digit($data))
							$array[] = $data;
						else
							$array[] = "'" . $this->db->escapeString($data) . "'";
					}

					$listValues .= MSB_NL . '(' . join(',', $array) . '),';
				} while ($row = $this->db->fetchRow($result));

				$listValues = mb_substr($listValues, 0, -1);

				$value = "INSERT INTO `$table` $listOfFields VALUES $listValues;" . MSB_NL;
			}
		}

		// Unlock the table
		$this->_Query("UNLOCK TABLES;");

		return $value;
	}


	function _Retrieve()
	{
		$value = '';

		if ($this->comments)
		{
			$value .= '#' . MSB_NL;
			$value .= '# Database dump' . MSB_NL;
			$value .= '# Created by MySQL_Backup class, ver. ' . MSB_VERSION . MSB_NL;
			$value .= '#' . MSB_NL;
			$value .= '# Host: ' . $this->server . MSB_NL;
			$value .= '# Generated: ' . date('M j, Y') . ' at ' . date('H:i') . MSB_NL;
			$value .= '# MySQL version: ' . $this->db->getStringEngineVersion() . MSB_NL;
			$value .= '# PHP version: ' . phpversion() . MSB_NL;
			$value .= '#' . MSB_NL;
			$value .= '# Database: `' . $this->database . '`' . MSB_NL;

			$value .= '#' . MSB_NL . MSB_NL . MSB_NL;
		}

		if (!($tables = $this->_GetTables()))
		{
			return false;
		}

		foreach ($tables as $table)
		{
			// For ANSI behavior (MySQL, PG at least)
    		// We must always use TABLE_SCHEMA in the WHERE clause
    		// and the raw value of TABLE_SCHEMA otherwise MySQL scans
    		// the disk for db names and slow down the server
    		// https://dev.mysql.com/doc/refman/5.7/en/information-schema-optimization.html
			$sql = "
			    SELECT COLUMN_NAME, IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS
			    WHERE
			        TABLE_NAME = '$table'
			        AND TABLE_SCHEMA = '" . $this->database . "';";

			$result = $this->_Query($sql);

			$nulls = array();
			while($row = $this->db->fetchRow($result))
			{
				if($row['IS_NULLABLE'] == 'YES')
					$nulls[] = $row['COLUMN_NAME'];
			}

			if (!($table_dump = $this->_DumpTable($table, $nulls)))
			{
				$this->error = $this->db->error;
				$this->errno = $this->db->errno;
				return false;
			}

			$value .= $table_dump;
		}

		return $value;
	}


	function _SaveToFile($fname, $sql, $compress)
	{
		if ($compress)
		{
			if (!($zf = gzopen($fname, 'w9')))
			{
				$e = error_get_last();
				$this->error = $e['message'];
				$this->errno = $e['type'];
				return false;
			}
			else
			{
				gzwrite($zf, $sql);
				gzclose($zf);
			}
		}
		else
		{
			if ($f = fopen($fname, 'w'))
			{
				fwrite($f, $sql);
				fclose($f);
			}
			else
			{
				$e = error_get_last();
				$this->error = $e['message'];
				$this->errno = $e['type'];
				return false;
			}
		}

		return true;
	}


	function _DownloadFile($fname, $sql, $compress)
	{
		header('Content-disposition: filename="' . $fname . '";');
		header('Content-type: application/octetstream');
		header('Pragma: no-cache');
		header('Expires: 0');
		echo ($compress ? gzencode($sql) : $sql);
		return true;
	}
}

?>