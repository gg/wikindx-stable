<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

class backupMysqlMessages_en
{
public $text;

	public function __construct()
	{
		$this->messages();
	}
// All headings, text hints etc.
	private function messages()
	{
		$this->text = array(
				"menu"		=>	"Backup Database",
				"heading"	=>	"Backup Database",
				"backup"	=>	"Backup",
				"noWrite"	=>	"plugins/backupMysql/dumps/ is not writeable by the web server user.  It currently has the permissions: ###",
				"deleted"	=>	"Files deleted",
			);
	}
}
?>