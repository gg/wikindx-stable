                         --o READMEFIRST o--

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---

///////////////////
If you are upgrading from a previous installation of WIKINDX, read docs/UPGRADE.txt.
///////////////////

NB
///////////////////
INSTALLATION OF WIKINDX 5.x

1.  If you are installing a fresh WIKINDX with a blank database, read
docs/INSTALL.txt

2.  If you are upgrading a previous installation of WIKINDX 4.x to 5.x,
simply overwrite the existing installation.  $WIKINDX_WIKINDX_PATH has been
added to config.php.dist and should be copied to config.php and the first
running of WIKINDX will upgrade the database in a single step.  If upgrading a
v4.x installation below v4.2, you should also  download and reinstall all your
plugins as, from v4.2, WIKINDX introduced a plugin compatibility check.

3.  If you are  upgrading from a previous installation  of WIKINDX below v4.x,
read docs/UPGRADE.txt

4.  WIKINDX uses caching in the database _cache table for lists of creators, keywords etc.
For large WIKINDX databases, if you receive messages such as 'MySQL server has gone away',
try increasing max_allowed_packet in the MySQL server.

5. If your database is over 1500 resources and you expect to export (with the importexport plugin)
lists of resources of at least this length, then you should set
public $WIKINDX_MEMORY_LIMIT = "64M";
or higher in config.php in order to avoid memory allocation errors.
///////////////////


///////////////////
A test database (wikindx5_1_testDatabase.sql) is provided in docs/. Read 
docs/README_TESTDATABASE.txt.
///////////////////

Other README files can be found in docs/

Developed under the Creative Commons CC-BY-NC-SA 2.0 license, the project homepage
can be found at: https://sourceforge.net/projects/wikindx/ and the required
files/updates and a variety of enhancements (language locailzations and
plug-ins) are freely available there.

WIKINDX Requirements (and has been tested on):

 * PHP >= v5.6.x and <= 7.3.x
 * MySQL >= v5.7.5 or MariaDB >= 10.2 (mysqli driver)
 * Apache >= 2.x or nginx >= 1.11 (or any web Server able to run PHP scripts).

The  client  accesses  the  database via  a  standard  (graphical)  web
browser. WIKINDX  makes use of the tinyMCE editor  (v2.0.4) and this
limits the web browsers that can be used.

A list of compatible browsers can be found here:

    http://www.tinymce.com/wiki.php/Browser_compatiblity.

Generally, though, if you use the following, you should be OK:

 * Internet Explorer >= v9
 * Edge >= v10
 * Firefox >= v12
 * Chrome >= v4
 * Safari >= v10
 * Opera >= v12
 * WebKit-based navigators (https://en.wikipedia.org/wiki/List_of_web_browsers#WebKit-based)

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---


--
Mark Grimshaw-Aagaard
The WIKINDX Team 2019
sirfragalot@users.sourceforge.net
