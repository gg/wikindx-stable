-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Add three fields to handle GDPR and improve auth security
-- 

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%users ADD `usersChangePasswordTimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%users ADD `usersGDPR` VARCHAR(1) NOT NULL DEFAULT 'N';
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%users ADD `usersBlock` VARCHAR(1) NOT NULL DEFAULT 'N';

-- Update all current users usersChangePasswordTimestamp fields to their current usersTimestamp fields
UPDATE %%WIKINDX_DB_TABLEPREFIX%%users
SET usersChangePasswordTimestamp = usersTimestamp;
