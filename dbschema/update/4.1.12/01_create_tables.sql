-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Create tables user_bibliography_resource and user_groups_users
-- 

CREATE TABLE IF NOT EXISTS `%%WIKINDX_DB_TABLEPREFIX%%user_bibliography_resource` (
	`userbibliographyresourceId` int(11) NOT NULL AUTO_INCREMENT,
	`userbibliographyresourceBibliographyId` int(11) DEFAULT NULL,
	`userbibliographyresourceResourceId` int(11) DEFAULT NULL,
	PRIMARY KEY (`userbibliographyresourceId`),
	INDEX (`userbibliographyresourceResourceId`)
) ENGINE=MyISAM CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `%%WIKINDX_DB_TABLEPREFIX%%user_groups_users` (
	`usergroupsusersId` int(11) NOT NULL AUTO_INCREMENT,
	`usergroupsusersGroupId` int(11) DEFAULT NULL,
	`usergroupsusersUserId` int(11) DEFAULT NULL,
	PRIMARY KEY (`usergroupsusersId`)
) ENGINE=MyISAM CHARSET=utf8;
