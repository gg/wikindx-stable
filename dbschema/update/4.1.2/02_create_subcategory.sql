-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Create new table subcategory
-- 

CREATE TABLE IF NOT EXISTS `%%WIKINDX_DB_TABLEPREFIX%%subcategory` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`categoryId` int(11),
	`subcategory` varchar(255),
	PRIMARY KEY (`id`)
) ENGINE=MyISAM CHARSET=utf8;
