-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Add temporary indices to speed up the upgrade
-- 

SET NAMES latin1;
SET CHARACTER SET latin1;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%collection ADD INDEX `collectionTitle` (collectionTitle);
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%collection ADD INDEX `collectionTitleShort` (collectionTitleShort);
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%collection ADD INDEX `collectionType` (collectionType);

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%creator    ADD INDEX `firstname` (firstname);
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%creator    ADD INDEX `surname` (surname);
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%creator    ADD INDEX `initials` (initials);
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%creator    ADD INDEX `creator` (creator);
