-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2019 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Add columns
-- 

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_attachments ADD COLUMN `resourceattachmentsDescription` TEXT DEFAULT NULL;