-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Remove unused ressource tables
-- 

DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%resource_quote;
DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%resource_quote_text;
DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%resource_quote_comment;
DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%resource_paraphrase;
DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%resource_paraphrase_text;
DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%resource_paraphrase_comment;
DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%resource_musing;
DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%resource_musing_text;
