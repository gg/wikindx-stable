-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Remove fields resourcekeywordQuoteId, resourcekeywordParaphraseId, and resourcekeywordMusingId
-- 

ALTER TABLE  %%WIKINDX_DB_TABLEPREFIX%%resource_keyword DROP COLUMN resourcekeywordQuoteId;
ALTER TABLE  %%WIKINDX_DB_TABLEPREFIX%%resource_keyword DROP COLUMN resourcekeywordParaphraseId;
ALTER TABLE  %%WIKINDX_DB_TABLEPREFIX%%resource_keyword DROP COLUMN resourcekeywordMusingId;
