-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Drop columns some fields of config table and notify table
-- 

SET NAMES latin1;
SET CHARACTER SET latin1;

DROP TABLE IF EXISTS %%WIKINDX_DB_TABLEPREFIX%%notify;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%config DROP COLUMN kwBibliography;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%users  DROP COLUMN kwBibliography;