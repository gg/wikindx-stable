-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2019 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Ensure DB schema of converted databases matches that in createSQL.xml
-- 

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%news MODIFY COLUMN `newsTimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_attachments MODIFY COLUMN `resourceattachmentsTimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_attachments MODIFY COLUMN `resourceattachmentsEmbargoUntil` DATETIME DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_metadata MODIFY COLUMN `resourcemetadataTimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_metadata MODIFY COLUMN `resourcemetadataTimestampEdited` DATETIME DEFAULT NULL;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_timestamp MODIFY COLUMN `resourcetimestampTimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_timestamp MODIFY COLUMN `resourcetimestampTimestampAdd` DATETIME DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%users MODIFY COLUMN `usersTimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%users MODIFY COLUMN `usersNotifyTimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%users MODIFY COLUMN `usersChangePasswordTimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%user_register MODIFY COLUMN `userregisterTimestamp` DATETIME DEFAULT CURRENT_TIMESTAMP;
