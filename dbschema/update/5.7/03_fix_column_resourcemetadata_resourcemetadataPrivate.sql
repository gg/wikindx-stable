-- 
-- WIKINDX : Bibliographic Management system.
-- @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
-- @author The WIKINDX Team
-- @copyright 2018 Stéphane Aulery <lkppo@users.sourceforge.net>
-- @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
-- 
-- Fix a mistake on the length of resourcemetadataPrivate field
-- 

ALTER TABLE %%WIKINDX_DB_TABLEPREFIX%%resource_metadata MODIFY COLUMN `resourcemetadataPrivate` varchar(1) DEFAULT 'N';
