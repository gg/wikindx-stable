<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* COOKIE
*
* Common cookie functions
*
* @version	1
*
*	@package wikindx5\core\session
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class COOKIE
{
/** string */
private $cookieName;
/** array */
private $cookieVars;

/**
* COOKIE
*/
	public function __construct()
	{
// whatever array cookie variables are in, we grab 'em.
		if(isset($_COOKIE))
			$this->cookieVars = &$_COOKIE;
		$config = FACTORY_CONFIG::getInstance();
		$this->cookieName = "wikindx_" . $config->WIKINDX_DB;
	}
/**
* Set a cookie if user requests through mywikindx 'remember me' checkbox
*
* store only username and password so user does not have to go through initial authentication
* @param string $username
*/
	public function storeCookie($username)
	{
		$valueArray = array('username' => $username);
		$value = base64_encode(serialize($valueArray));
		$expire = 60*60*24*100; // 100 days
		$path = "/";
// we don't fail if this cookie can't be set.
		setcookie($this->cookieName, $value, time() + $expire, $path);
	}
/**
* remove cookie
*/
	public function deleteCookie()
	{
		setcookie($this->cookieName, "", time() - 3600, "/");
	}
/**
* Get cookie
*
* @return boolean TRUE if cookie set
*/
	public function grabCookie()
	{
		if(!array_key_exists($this->cookieName, $this->cookieVars))
			return FALSE; // no cookie set

		$cookieArray = unserialize(base64_decode($this->cookieVars[$this->cookieName]));

		if(!array_key_exists('username', $cookieArray))
			return FALSE; // invalid cookie

		if(!$cookieArray['username'])
			return FALSE; // invalid cookie

// Cookie set so check for valid username
		$db = FACTORY_DB::getInstance();
		$recordSet = $db->select('users', array('usersUsername', 'usersId', 'usersAdmin'), array('usersUsername' => $cookieArray['username']));
		if(!$db->numRows($recordSet))
			return FALSE;

		$user = FACTORY_USER::getInstance();
		$user->environment($db->fetchRow($recordSet));
		return TRUE;
	}
}
?>