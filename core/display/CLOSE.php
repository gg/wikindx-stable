<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017–2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @copyright 2019 Stéphane Aulery <lkppo@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Close WIKINDX tidily and print footer.
*
* @version 2
*
* @package wikindx5\core\display
*
*/
class CLOSE
{
/** object */
protected $db;
/** object */
protected $template;
/** object */
protected $config;
/** object */
protected $messages;
/** object */
protected $session;


/**
* CLOSE
*
* @param boolean $displayHeader default TRUE
* @param boolean $displayFooter default TRUE
* @param boolean $displayMenu default TRUE
* @param boolean $displayPopUp default FALSE
*/
	public function __construct($displayHeader = TRUE, $displayFooter = TRUE, $displayMenu = TRUE, $displayPopUp = FALSE)
	{
		$this->config = FACTORY_CONFIG::getInstance();
		$this->db = FACTORY_DB::getInstance();
		$this->session = FACTORY_SESSION::getInstance();

		$this->messages = FACTORY_MESSAGES::getInstance();
		$styles = \LOADSTYLE\loadDir();

		// Preparation of values
		$numberOfResources = $this->db->selectFirstField("database_summary", "databasesummaryTotalResources");

		// if this is a logged in user, display the username after the heading
		if($userId = $this->session->getVar('setup_UserId'))
		{
			$this->db->formatConditions(array('usersId' => $userId));
			$username = \HTML\dbToHtmlTidy($this->db->selectFirstField('users', 'usersUsername'));
		}
		else
			$username = '--';

		// During setup, there are no default style configured in session
		// So, we take the first of those installed
		$styleName = $styles[$this->session->getVar("setup_Style", key($styles))];

		if($useBib = $this->session->getVar("mywikindx_Bibliography_use"))
		{
			$this->db->formatConditions(array('userbibliographyId' => $useBib));
			$bib = \HTML\dbToHtmlTidy($this->db->selectFirstField('user_bibliography', 'userbibliographyTitle'));
		}
		else
			$bib = $this->messages->text("user", "masterBib");

		$footer['wikindxVersion'] = WIKINDX_VERSION . "&nbsp;&copy;" . WIKINDX_COPYRIGHT_YEAR;
        $footer['numResources'] = $this->messages->text("footer", "resources") . "&nbsp;" . $numberOfResources;
		$footer['username'] = $this->messages->text("user", "username") . ":&nbsp;" . $username;
		$footer['bibliography'] = $this->messages->text("footer", "bib") . "&nbsp;" . $bib;
		$footer['style'] = $this->messages->text("footer", "style") . "&nbsp;" . $styleName;
		$footer['numQueries'] = $this->messages->text("footer", "queries") . "&nbsp;" . GLOBALS::getDbQueries();
		$footer['dbTime'] = $this->messages->text("footer", "dbtime") . "&nbsp;" . '%%DBTIMER%%' . "&nbsp;secs";
		$footer['scriptTime'] = $this->messages->text("footer", "execution") . "&nbsp;" . '%%SCRTIMER%%' . "&nbsp;secs";

		// Assigning values to the template and rendering
		$this->template = FACTORY_TEMPLATE::getInstance();
		$this->template->loadTemplate();
		// Extract and fix url separator for HTML rendering
		$tplPath = DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $this->template->getDirectory());

		GLOBALS::addTplVar('displayPopUp', $displayPopUp);

		GLOBALS::addTplVar('tplPath', $this->config->WIKINDX_BASE_URL . $tplPath);
        GLOBALS::addTplVar('lang', $this->session->getVar("setup_Language"));
        if(property_exists($this->config, 'WIKINDX_RSS_ALLOW'))
        {
			GLOBALS::addTplVar('displayRss', $this->config->WIKINDX_RSS_ALLOW);
			GLOBALS::addTplVar('rssTitle', $this->config->WIKINDX_RSS_TITLE);
			GLOBALS::addTplVar('rssFeed', $this->config->WIKINDX_BASE_URL . WIKINDX_RSS_PAGE);
		}
		else
			GLOBALS::addTplVar('displayRss', FALSE);
		// HEADERS
		GLOBALS::addTplVar('displayHeader', $displayHeader);

		// Check if this parameter exists because throws an error at install stage
		// TODO (lkppo): loading process could be changed to load configuration separately of AUTHORIZE class
		if (property_exists($this->config, 'WIKINDX_TITLE'))
			$title = \HTML\dbToHtmlTidy($this->config->WIKINDX_TITLE);
		else
			$title = WIKINDX_NAME;

    	GLOBALS::addTplVar('title', \HTML\stripHtml($title)); // Admins can add HTML formatting in the configure interface.
    	GLOBALS::addTplVar('headTitle', $title);

		// Mandatory script for Ajax and core functions
		GLOBALS::addTplVar('scripts', '<script src="' . $this->config->WIKINDX_BASE_URL . '/core/javascript/coreJavascript.js"></script>');
		GLOBALS::addTplVar('scripts', '<script src="' . $this->config->WIKINDX_BASE_URL . '/core/javascript/progressbar.js"></script>');
		GLOBALS::addTplVar('scripts', '<script src="' . $this->config->WIKINDX_BASE_URL . '/core/ajax/json2.js"></script>');
		GLOBALS::addTplVar('scripts', '<script src="' . $this->config->WIKINDX_BASE_URL . '/core/ajax/ajax.js"></script>');
		GLOBALS::addTplVar('scripts', '<script src="' . $this->config->WIKINDX_BASE_URL . $tplPath . '/template.js"></script>');

		// MENU
		GLOBALS::addTplVar('displayMenu', $displayMenu);
		// If the menu is hidden, we can avoid to build it
		if ($displayMenu)
		{
			include_once("core/navigation/MENU.php");
			$menu = new MENU();
			$menu->menus();
		}

		$this->tidySession();

		// FOOTERS
		GLOBALS::addTplVar('displayFooter', $displayFooter);
		// Although the footer is hidden, we can't avoid to assign its variables
		// because someone can use them at an other place of his custom template
		GLOBALS::addTplVar("footerInfo", $footer);
		GLOBALS::addTplVar('wkx_link', WIKINDX_URL);
		GLOBALS::addTplVar('wkx_title', mb_strtolower(WIKINDX_NAME));
		GLOBALS::addTplVar('wkx_mimetype_rss', WIKINDX_MIMETYPE_RSS);

		// Get the time elapsed before template rendering
		GLOBALS::setPageEndingTime(microtime());
		$scriptExecutionTimeBeforeRendering = GLOBALS::getPageElapsedTime();

		// RENDER PAGE

		// Get the list of template variables defined in the global store
		$tplKeys = GLOBALS::getTplVarKeys();

		// Merge the list with some mandatories template variable names
		// We need absolutely these variables defined in the template for the placement of plugins.
		// This is one more thing for template designers.
		array_push($tplKeys, 'heading');
		array_push($tplKeys, 'scripts');
		array_push($tplKeys, 'menu');
		array_push($tplKeys, 'help');
		array_push($tplKeys, 'content');
		array_push($tplKeys, 'inline1');
		array_push($tplKeys, 'inline2');
		array_push($tplKeys, 'inline3');
		array_push($tplKeys, 'inline4');

		$debugLogSQLString = '';

		$tplKeys = array_unique($tplKeys);
		// Extract data of all template variables form the global store and give them to the template system
		foreach ($tplKeys as $k)
		{
		    $tplVars = GLOBALS::getTplVar($k);
//print_r($tplVars); print '<p>';
		    $s = '';
		    $t = NULL; // Type of the variable to give to the template

		    foreach ($tplVars as $v)
		    {
		        // We have to assimile NULL to string because sometimes
		        // this value can be inserted unintentionally if a variable is empty.
		        // The same error happend with a mixture of string
		        // and others type but this is obviously an error
		        // and we raise an error in that case.
		        if ($t == NULL)
		            $t = (is_string($v) || $v == NULL) ? 's' : 'm';

		        if ($t == ((is_string($v) || $v == NULL) ? 's' : 'm'))
		        {
		            if ($t == 's')
    		        {
    		            // Concat all strings data
    		            if ($k == 'scripts') $v .= LF;
    		            $s .= $v;
    		        }
    		        else
    		        {
    		            // If multiple no-string data are defined,
    		            // only the last defined will be assigned
    		            // In this case there should be only one,
    		            // but we have to protected againt a mistake
    		            // to no break rendering arbitrarily
    		            $s = $v;
    		        }
		        }
		        else
		        {
		            $errorMessage = "Mixed data type in '$k' template variable";

            		if ($this->config->WIKINDX_DEBUG_ERRORS)
            			trigger_error($errorMessage, E_USER_ERROR);
            		else
            			$s = $errorMessage;

            		break;
		        }
		    }

		    // logsql is a specific case : this variable is injected in all template,
		    // just before the body end tag when the debug sql mode is ON.
		    // We don't want to provide a way to template designer to disable it.
		    if ($k != 'logsql')
		        $this->template->tpl->assign($k, $s);
		    else
		        $debugLogSQLString = $s;

		    GLOBALS::clearTplVar($k);
		}

		$this->template->tpl->display('display.tpl');

		// Get the time elapsed after template rendering
		// which is also the total time elapsed
		GLOBALS::setPageEndingTime(microtime());
		$scriptExecutionTimeAfterRendering = GLOBALS::getPageElapsedTime();

		// Time elapsed in db connection
		$dbConnectionTime = GLOBALS::getDbConnectionTimeElapsed();

		// Time elapsed in db queries
		$dbExecutionTime = GLOBALS::getDbTimeElapsed();

		// Time elapsed in data processing and internal logic (no template rendering and db query)
		$scriptElapsedTime = $scriptExecutionTimeBeforeRendering - $dbExecutionTime;

		// Time elapsed in template rendering
		$templateElapsedTime = $scriptExecutionTimeAfterRendering - $scriptExecutionTimeBeforeRendering;

		// Retrieve page code after rendering and insert public timers
		$outputString = ob_get_clean();
		$outputString = str_replace('%%DBTIMER%%', sprintf('%0.5f', $dbConnectionTime + $dbExecutionTime), $outputString);
		$outputString = str_replace('%%SCRTIMER%%', sprintf('%0.5f', $scriptExecutionTimeAfterRendering), $outputString);

		// Insert debug info only if we are on the main page
		if (mb_strripos('plugins/', $_SERVER['SCRIPT_NAME']) === FALSE)
		{
    	    $debugString = '';
    		// Insert SQL log
    		if ($this->config->WIKINDX_DEBUG_SQL && in_array('logsql', $tplKeys))
    		{
    		    $debugString .= $debugLogSQLString;
    		}
    		// Insert debug timers
    		if ( property_exists($this->config, 'WIKINDX_DEBUG_ERRORS') && $this->config->WIKINDX_DEBUG_ERRORS)
    		{
    			$lineEnding = BR;
    			$debugString .= "<p style='font-family: monospace; font-size: 8pt; text-align: right;'>". LF;
    			$debugString .= "PHP execution time: " . sprintf('%0.5f', $scriptElapsedTime) . " s$lineEnding";
    			$debugString .= "SQL connection time: " . sprintf('%0.5f', $dbConnectionTime) . " s$lineEnding";
    			$debugString .= "SQL execution time: " . sprintf('%0.5f', $dbExecutionTime) . " s$lineEnding";
    			$debugString .= "TPL rendering time: " . sprintf('%0.5f', $templateElapsedTime) . " s$lineEnding";
    			$debugString .= "Total elapsed time: " . sprintf('%0.5f', $scriptExecutionTimeAfterRendering) . " s$lineEnding";
    			$debugString .= "Peak memory usage: " . sprintf('%0.4f', memory_get_peak_usage() / 1048576) . " MB$lineEnding";
    			$debugString .= "Memory at close: " . sprintf('%0.4f', memory_get_usage() / 1048576) . " MB";
    			$debugString .= "\n</p>\n</body>";

    		}

    		$outputString = str_replace('</body>', $debugString, $outputString);
		}

		// Send page code to browser
		print $outputString;

		// If this function have been called directly (not inherited), we must die
		if (__CLASS__ == get_called_class()) die;
	}
/**
* tidySession
*
* @author Mark Grimshaw <sirfragalot@users.sourceforge.net>
* @version 1
*
* A convenient place to clear certain session values which we definitely don't want the next time around
*/
	protected function tidySession()
	{
		$session = FACTORY_SESSION::getInstance();
// This if TRUE is the last operation made use of LISTCOMMON::display()
		$session->delVar("list_On");
	}
}
/**
* Close WIKINDX tidily (no menu - used for initial logon screen).
*
* @version 2
*
* @package wikindx5\core\display
* @author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class CLOSENOMENU extends CLOSE
{
/**
* CLOSENOMENU
*
* @param boolean $displayHeader default TRUE
* @param boolean $displayFooter default TRUE
* @param boolean $displayMenu default FALSE
* @param boolean $displayPopUp default FALSE
*/
	public function __construct($displayHeader = TRUE, $displayFooter = TRUE, $displayMenu = FALSE, $displayPopUp = FALSE)
	{
		parent::__construct($displayHeader, $displayFooter, $displayMenu, $displayPopUp);
		die;
	}
}
/**
* Close WIKINDX tidily.  Used for javascript pop-ups such as citation that don't require header, images, menus etc.
*
* @version 2
*
* @package wikindx5\core\display
* @author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class CLOSEPOPUP extends CLOSE
{
/**
* CLOSEPOPUP
*
* @param boolean $displayHeader default TRUE
* @param boolean $displayFooter default FALSE
* @param boolean $displayMenu default FALSE
* @param boolean $displayPopUp default TRUE
*/
	public function __construct($displayHeader = TRUE, $displayFooter = FALSE, $displayMenu = FALSE, $displayPopUp = TRUE)
	{
		parent::__construct($displayHeader, $displayFooter, $displayMenu, $displayPopUp);
		die;
	}
}
/**
* Close WIKINDX by simply printing GLOBALS::buildTplVarString('content') without any more content.  Typically used with AJAX to print
* strings to a DIV within the WIKINDX page
*
* @version 2
*
* @package wikindx5\core\display
* @author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class CLOSERAW extends CLOSE
{
/**
* CLOSERAW
*/
	public function __construct()
	{
		// In this mode, we don't use template engine
		$this->tidySession();

		print GLOBALS::buildTplVarString('content');

		if(ob_get_length() !== FALSE)
			ob_end_flush();
		die; // In this we always die to not had other content by error
	}
}
?>