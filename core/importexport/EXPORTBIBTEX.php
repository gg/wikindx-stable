<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* EXPORTBIBTEX
*/

require_once("core/importexport/EXPORTER.php");

/**
* Export BibTeX
*
* @version	1
*
*	@package wikindx5\core\importexport
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class EXPORTBIBTEX extends EXPORTER
{
/** object */
private $db;
/** object */
private $map;

/**
* Extends EXPORTER
*/
	function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->map = FACTORY_BIBTEXMAP::getInstance();
	}

/**
 * display bibtex data for one resource (from resource lists)
 *
 * @param array $row
 * @param array $creators
 * @return string
 */
	public function export($row,$creators)
	{
		$this->creators = $creators;
        $this->entry = $this->authors = array();
		$authors = $entry = FALSE;
		$type = $this->map->types[$row['resourceType']];

		$this->getData($row);

        if(!empty($this->entry))
            $entry = $this->convertEntry();
        if(!empty($this->authors))
            $authors = $this->convertEntryAuthors();

		$ret = '@'.mb_strtoupper($type)."{" . $row['resourceBibtexKey'] . "\n";
		$ret .= $authors;
		$ret .= $entry;
		$ret .= "\n}\n";
		return $ret;
	}

/**
* Convert raw array of data to bibtex format
*
* @return string
*/
    protected function convertEntry()
    {
        $array = array();
        array_map(array($this, "uEncode"), $this->entry);
        foreach($this->entry as $key => $value)
            $array[] = "$key = $value";
        return join(LF, $array);
	}
/**
* call back for convertEntry()
*
* @param string $element
* @return string
*/
	protected function uEncode($element)
	{
		return $element;
	}
/**
* create author string
*
* @return string
*/
    private function convertEntryAuthors()
    {
        return 'author = ' . join(" and ", $this->authors) . "\n";
    }
}
?>