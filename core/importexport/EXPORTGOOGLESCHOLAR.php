<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Extends EXPORTER
*/
require_once("core/importexport/EXPORTER.php");

/**
* Make WIKINDX resources available to Google Scholar (see config.php)
*
* @version	1
*
*	@package wikindx5\core\importexport
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class EXPORTGOOGLESCHOLAR extends EXPORTER
{
/** object */
protected $db;
/** object */
private $session;
/** object */
protected $map;
/** object */
private $config;
/** boolean */
private $noGs = FALSE;

/**
*	EXPORTGOOGLESCHOLAR
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
		include_once("core/importexport/GOOGLESCHOLARMAP.php");
		$this->map = new GOOGLESCHOLARMAP();
	}
/**
* Display googlescholar data for one resource
*
* @param array $row
* @param array $creators
* @return string
*/
	public function export($row, $creators)
	{
		$this->entry = $this->authors = array();
		$entry = FALSE;
		global $_SERVER;
		if($attach = $this->attachedFiles($row['resourceId']))
			$entry .= '<meta name="citation_pdf_url" content="' . $this->config->WIKINDX_BASE_URL . '/' . $attach . '">';
		if($this->noGs)
			return FALSE;
		$this->creators = $creators;
		$this->getData($row);
		if(!empty($this->entry))
			$entry .= $this->convertEntry();
		if(!empty($this->authors))
			$entry .= $this->convertEntryAuthors();
		if($publisher = $this->publisher($row))
			$entry .= '<meta name="citation_publisher" content="' . $this->uEncode($publisher) . '">';
		if(array_key_exists('resourceDoi', $row) && $row['resourceDoi'])
			$entry .= '<meta name="citation_doi" content="' . $this->uEncode(str_replace('doi:', '', $row['resourceDoi'])) . '">';
		$entry .= $this->keywords($row);
		$entry .= $this->languages($row);
		return $entry;
	}
/**
* Make attached files available
*
* @param int $resourceId
* @return string
*/
	private function attachedFiles($resourceId)
	{
// Are only logged on users allowed to view this file?
		if($this->session->getVar("setup_FileViewLoggedOnOnly"))
			return FALSE;
		$attach = FACTORY_ATTACHMENT::getInstance();
		$this->db->formatConditions(array('resourceattachmentsResourceId' => $resourceId));
		$this->db->orderBy('resourceattachmentsFilename');
		$recordset = $this->db->select('resource_attachments',
			array('resourceattachmentsId', 'resourceattachmentsHashFilename', 'resourceattachmentsFileName',
			'resourceattachmentsPrimary', 'resourceattachmentsEmbargo'));
		if(!$this->db->numRows($recordset))
		{
			if($this->config->WIKINDX_GS_ATTACHMENT)
				$this->noGs = TRUE;
			return FALSE;
		}
		$multiple = $this->db->numRows($recordset) > 1 ? TRUE : FALSE;
		$primary = FALSE;
		while($row = $this->db->fetchRow($recordset))
		{
			if(!$this->session->getVar("setup_Superadmin") && ($row['resourceattachmentsEmbargo'] == 'Y'))
			{
				continue;
			}
			if($multiple && ($row['resourceattachmentsPrimary'] == 'Y'))
			{
				$primary = $attach->makeLink($row, $multiple, FALSE, FALSE);
				break;
			}
			else if(!$multiple)
				$primary = $attach->makeLink($row, $multiple, FALSE, FALSE);
		}
		return $primary;
	}
/**
* Get resource publisher
*
* @param array $row
* @return string
*/
	private function publisher($row)
	{
		if(!$row['publisherName'] && !$row['publisherLocation'])
			return FALSE;
		return $row['publisherLocation'] ? $row['publisherName'] .
			': ' . $row['publisherLocation'] : $row['publisherName'];
	}
/**
* Get resource keywords
*
* @param array $row
* @return string
*/
	private function keywords($row)
	{
		$rId = $row['resourceId'];
		$this->db->formatConditions(array('resourcekeywordResourceId' => $rId));
		$this->db->leftJoin('keyword', 'keywordId', 'resourcekeywordKeywordId');
		$this->db->orderBy('keywordKeyword');
		$resultset = $this->db->select('resource_keyword', array('resourcekeywordKeywordId', 'keywordKeyword'));
		while($row = $this->db->fetchRow($resultset))
			$array[] = '<meta name="citation_keyword" content="' . $this->uEncode($row['keywordKeyword']) . '">';
		if(!isset($array))
			return FALSE;
		return join('', $array);
	}
/**
* get resource languages
*
* @param array $row
* @return string
*/
	private function languages($row)
	{
		$rId = $row['resourceId'];
		$this->db->formatConditions(array('resourcelanguageResourceId' => $rId));
		$this->db->leftJoin('language', 'languageId', 'resourcelanguageLanguageId');
		$this->db->orderBy('languageLanguage');
		$resultset = $this->db->select('resource_language', array('resourcelanguageLanguageId', 'languageLanguage'));
		while($row = $this->db->fetchRow($resultset))
			$array[] = '<meta name="citation_language" content="' . $this->uEncode($row['languageLanguage']) . '">';
		if(!isset($array))
			return FALSE;
		return join('', $array);
	}

/**
* Convert raw array of data to appropriate google scholar format
*
* @return string
*/
	protected function convertEntry()
	{
		$array = array();
		foreach($this->entry as $key => $value)
		{
			if($key == 'date')
			{
				$array[] = '<meta name="citation_publication_date" content="' . str_replace('-', '/', $value) . '">';
				continue;
			}
			$array[] = '<meta name="' . $key . '" content="' . $value . '">';
		}
		return join('', $array);
	}
/**
* Convert raw array of authors to appropriate google scholar format
*
* @return string
*/
	protected function convertEntryAuthors()
	{
		$array = array();
		foreach($this->authors as $value)
			$array[] = '<meta name="citation_author" content="' . $this->uEncode($value) . '">';
		return join('', $array);
	}
/**
* URL encode
*
* @param string $element
* @return string
*/
	protected function uEncode($element)
	{
		return htmlentities($element, ENT_QUOTES, "UTF-8");
	}
}
?>