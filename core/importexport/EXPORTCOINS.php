<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
*	EXPORTCOINS
*/
require_once("core/importexport/EXPORTER.php");

/**
* Export COINS
*
* @version	1
*
*	@package wikindx5\core\importexport
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class EXPORTCOINS extends EXPORTER
{
/**
* Extends EXPORTER
*/
	public function __construct()
	{
		$this->config = FACTORY_CONFIG::getInstance();
		$this->db = FACTORY_DB::getInstance();
		include_once("core/importexport/COINSMAP.php");
		$this->map = new COINSMAP();
	}
/**
* Display coins data for one resource (from resource lists)
*
* @param array $row
* @param array $creators
* @return string
*/
	public function export($row, $creators)
	{
		$this->creators = $creators;
		$this->entry = $this->authors = array();
		$genre = $authors = $entry = $url = $doi = $abstract = FALSE;
		$this->getData($row);
		$type = $this->map->types[$row['resourceType']];
		if($this->map->genres[$row['resourceType']])
			$genre = "&amp;rft.genre=" . urlencode($this->map->genres[$row['resourceType']]);
		if(!empty($this->entry))
			$entry = $this->convertEntry();
		if(!empty($this->authors))
			$authors = $this->convertEntryAuthors();
		if(array_key_exists('resourcetextUrls', $row) && $row['resourcetextUrls'])
		{
			$urls = unserialize(base64_decode($row['resourcetextUrls']));
			$url = '&amp;rft_id=' . urlencode(array_shift($urls));
		}
		if(array_key_exists('resourceDoi', $row) && $row['resourceDoi'])
			$doi = '&amp;rft_id=info:doi/' . $this->uEncode($row['resourceDoi']);
		if(array_key_exists('resourcetextAbstract', $row) && $row['resourcetextAbstract'])
			$abstract = '&amp;rft_id=info:abstract/' . $this->uEncode($row['resourcetextAbstract']);
		$sid ="&amp;rfr_sid=info:sid/" . $this->config->WIKINDX_BASE_URL . $_SERVER['SCRIPT_NAME'];
		$return = "<span " . $this->map->coinsBase . "$type" . "$genre" .
			$entry . $authors . $url . $doi . $sid . $abstract . "\"></span>";
		return $return;
	}

/**
* Convert raw array of data to appropriate coins format
*
* @return string
*/
	protected function convertEntry()
	{
		$array = array();
		array_map(array($this, "uEncode"), $this->entry);
		foreach($this->entry as $key => $value)
			$array[] = "rft." . $key . "=" . $value;
		return "&amp;" . join("&amp;", $array);
	}
/**
* Convert raw array of authors to appropriate coins format*
* @return string
*/
	protected function convertEntryAuthors()
	{
		$array = array();
		array_map(array($this, "uEncode"), $this->authors);
		foreach($this->authors as $value)
			$array[] = "rft.au=" . $value;
		return "&amp;" . join("&amp;", $array);
	}
/**
* Callback for convertEntry()
*
* @param string $element
* @return string
*/
	protected function uEncode($element)
	{
		return urlencode($element);
	}
}
?>