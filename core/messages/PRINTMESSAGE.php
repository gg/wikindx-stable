<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @copyright 2018-2019 Stéphane Aulery <lkppo@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* PRINTMESSAGES
*
* Handle localizations
*
* @version	1
*
*	@package wikindx5\core\messages
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class PRINTMESSAGE
{
/** string */
private $messageFile;
/** object */
private $session;
/** array */
protected $temp = array();
/** array */
private $langList = FALSE;

/**
* PRINTMESSAGES
*
* @param string $file
* @param string $language Language directory in languages/ Default = FALSE
*/
	public function __construct($file, $language = FALSE)
	{
		$this->messageFile = $file;
		$this->session = FACTORY_SESSION::getInstance();
		if(is_string($language))
			$languageDir = $language;
		elseif(!$languageDir = $this->session->getVar("setup_Language"))
			$languageDir = WIKINDX_LANGUAGE_DEFAULT;

// language may have been disabled by admin so set to default English
		if(!file_exists('languages/' . $languageDir . '/description.txt'))
		{
			$languageDir = WIKINDX_LANGUAGE_DEFAULT;
			if(!$this->session->getVar('setup_ReadOnly'))
				$this->session->setVar("setup_Language", $languageDir);
		}

		$this->languageArray = $this->loadArray($languageDir);
	}
/**
* Grab the message
*
* @param string $arrayName
* @param string $indexName
* @return string
*/
	protected function text($arrayName, $indexName)
	{
		if(!array_key_exists($arrayName, $this->languageArray))
			$this->loadEnglish($arrayName);
/**
* For ease of use, copy $languageArray[$arrayname] to $temp
*/
		if(array_key_exists($arrayName, $this->languageArray))
			$this->temp = $this->languageArray[$arrayName];
/**
* if message missing from language pack, use default English
*/
		if(!array_key_exists($indexName, $this->temp))
			$this->loadEnglish($arrayName);
		else if ($this->temp[$indexName] == "__WIKINDX__NOT_YET_TRANSLATED__")
			$this->loadEnglish($arrayName);
	}
/**
* Load English message arrays in case a localized message is missing
*
* @param string $arrayName
*/
	private function loadEnglish($arrayName)
	{
		include_once("languages/en/" . $this->messageFile . ".php");
		$class = $this->messageFile . "_" . WIKINDX_LANGUAGE_DEFAULT;
		$messages = new $class();
		$arrayEnglish = $messages->loadArrays();
		$this->temp = $arrayEnglish[$arrayName];
	}
/**
* load language array
*
* @param string $languageDir
* @return array
*/
	private function loadArray($languageDir)
	{
		if(($languageDir == WIKINDX_LANGUAGE_DEFAULT) || ($this->messageFile == 'HELP'))
		{
			include_once("languages/$languageDir/" . $this->messageFile . '.php');
			$className = $this->messageFile . "_" . $languageDir;
			$languageClass = new $className;
			return $languageClass->loadArrays();
		}
		else
		{
			$languageArray = array();
			$file = 'languages/' . $languageDir . "/" . $this->messageFile . '.xml';
			$array = $this->xmlLanguageCatalog2Array($file);

			if($this->messageFile == 'SUCCESS')
			{
				foreach($array as $key => $subArray)
					$languageArray[$key] = $subArray;
			}
			else
			{
			    foreach($array as $topKey => $topArray)
			    {
			        if (is_array($topArray))
			        {
    			        foreach($topArray as $subKey => $subArray)
    			        {
    			            $languageArray[$topKey][$subKey] = $subArray;
    			        }
			        }
			    }
			}
		}
		return $languageArray;
	}
/**
* convert XML language catalog to array
*
* @param string $xmlCatalogFile
* @return array
*/
	private function xmlLanguageCatalog2Array($xmlCatalogFile)
	{
		$sxi = new SimpleXmlIterator($xmlCatalogFile, NULL, TRUE);
		return $this->sxiToArray($sxi);
	}
/**
* convert XML to array
*
* @param xmlCatalogFile $sxi
* @return array
*/
	private function sxiToArray($sxi)
	{
	    $catArray = array();
		for($sxi->rewind(); $sxi->valid(); $sxi->next())
		{
			if($sxi->hasChildren())
				$catArray[$sxi->key()] = $this->sxiToArray($sxi->current());
			else
				$catArray[$sxi->key()] = htmlspecialchars(strval($sxi->current()), ENT_XML1);
		}
		return $catArray;
	}

/**
* read and return languages/ directory for language preferences and check we have a sane environment
*
* @return array
*/
	public function loadDir()
	{
		$langrootdir = 'languages';

		// Use an internal array to "memoize" the list of installed languages
		// This function is called multiple times while its result is almost static but expensive due to disk access.
		if ($this->langList === FALSE)
		{
			$array = array();

			if($handle = opendir($langrootdir))
			{
				while(FALSE !== ($dir = readdir($handle)))
				{
					if($dir != '.' && $dir != '..' && is_dir($langrootdir . DIRECTORY_SEPARATOR . $dir))
					{
						// English uses .php for the language files
						if ($dir == WIKINDX_LANGUAGE_DEFAULT)
							$catalogextension = 'php';
						else
							$catalogextension = 'xml';

						if(
							   file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'CONSTANTS.php')
							&& file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'HELP.php')
							&& file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'MESSAGES.' . $catalogextension)
							&& file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'ERRORS.' . $catalogextension)
							&& file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'SUCCESS.' . $catalogextension))
						{
    						if(file_exists($langrootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt'))
    						{
							    // read one line
    							$fh = fopen($langrootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt', "r");
								if($string = fgets($fh)) $array[$dir] = $string;
								fclose($fh);
							}
						}
					}
				}

				closedir($handle);

				// sort alphabetically on the key
				ksort($array);
			}

			$this->langList = $array;
		}

		return $this->langList;
	}
}

/**
* MESSAGES extends PRINTMESSAGES
*
* Handle general messages
*
* @version	1
*
*	@package wikindx5\core\messages
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class MESSAGES extends PRINTMESSAGE
{
/**
*  MESSAGES
*
* @param string $language Language directory in languages/ Default = FALSE
*/
	public function __construct($language = FALSE)
	{
		parent::__construct("MESSAGES", $language);
	}
/**
* Grab the message
*
* @param string $arrayName
* @param string $indexName
* @param string $extra Optional string that replaces '###' in the array element value string. Default is FALSE
* @param boolean $html Optional boolean for HTML printing (TRUE/default) or plain text (FALSE)
* @return string
*/
	public function text($arrayName, $indexName, $extra = FALSE, $html = TRUE)
	{
		parent::text($arrayName, $indexName);
// e.g. some submit buttons may have text from elsewhere and not from the MESSAGES::submit array
		if(!array_key_exists($indexName, $this->temp))
			$text = $indexName;
		else
			$text = $this->temp["$indexName"];
		$pString = $extra !== FALSE ?
			preg_replace("/###/u", trim($extra), $text) :
			preg_replace("/###/u", '', $text);
		$pString = stripslashes($pString);
		if($html)
			return UTF8::html_uentity_decode(trim($pString));
		else
			return trim($pString);
	}
}

/**
* ERRORS extends PRINTMESSAGES
*
* Handle error messages
*
* @version	1
*
*	@package wikindx5\core\messages
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class ERRORS extends PRINTMESSAGE
{
/**
* ERRORS
*/
	public function __construct()
	{
		parent::__construct("ERRORS");
	}
/**
* Grab the message
*
* @param string $arrayName
* @param string $indexName
* @param string $extra Optional string that replaces '###' in the array element value string. Default is FALSE
* @param boolean $html Optional boolean for HTML printing (TRUE/default) or plain text (FALSE)
* @return string
*/
	public function text($arrayName, $indexName, $extra = FALSE, $html = TRUE)
	{
		parent::text($arrayName, $indexName);
		$pString = $extra !== FALSE ?
			preg_replace("/###/u", trim($extra), $this->temp["$indexName"]) :
			preg_replace("/###/u", '', $this->temp["$indexName"]);
		$pString = stripslashes($pString);
		if($html)
			return \HTML\p(UTF8::html_uentity_decode(trim($pString)), "error", "center");
		else
			return trim($pString);
	}
}

/**
* SUCCESS extends PRINTMESSAGES
*
* Handle success messages
*
* @version	1
*
*	@package wikindx5\core\messages
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class SUCCESS extends PRINTMESSAGE
{
/**
* SUCCESS
*/
	public function __construct()
	{
		parent::__construct("SUCCESS");
	}
/**
* Grab the message
*
*
* Override class function as /languages/xx/SUCCESS.xml has only one array
* @param string $indexName
* @param string $extra Optional string that replaces '###' in the array element value string. Default is FALSE
* @param boolean $html Optional boolean for HTML printing (TRUE/default) or plain text (FALSE)
* @return string
*/
	public function text($indexName, $extra = FALSE, $html = TRUE)
	{
// if message missing from language pack, use default English
        $temp = "__WIKINDX__NOT_YET_TRANSLATED__";
		if(array_key_exists($indexName, $this->languageArray))
            $temp = $this->languageArray[$indexName];

        if ($temp == "__WIKINDX__NOT_YET_TRANSLATED__")
		{
			include_once("languages/en/SUCCESS.php");
			$class = "SUCCESS_en";
			$messages = new $class();
			$arrayEnglish = $messages->loadArrays();

		    if(array_key_exists($indexName, $this->arrayEnglish))
			    $temp = $this->arrayEnglish[$indexName];
		    else
			    $temp = "";
		}

		$pString = $extra !== FALSE ?
			preg_replace("/###/u", trim($extra), $temp) :
			preg_replace("/###/u", '', $temp);
		$pString = stripslashes($pString);
		if($html)
			return \HTML\p(UTF8::html_uentity_decode(trim($pString)), "success", "center");
		else
			return trim($pString);
	}
}

/**
* HELP extends PRINTMESSAGES
*
* Handle help messages
*
* @version	1
*
*	@package wikindx5\core\messages
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class HELP extends PRINTMESSAGE
{
/** object */
private $messages;
/** array */
private $arrayEnglish;
/**
* HELP
*/
	public function __construct()
	{
		parent::__construct("HELP");
		include_once("languages/en/HELP.php");
		$class = "HELP_en";
		$this->messages = new $class();
		$this->arrayEnglish = $this->messages->loadArrays();
	}
/**
* Grab the message
*
*
* Override class function as /languages/xx/HELP.php has only one array
* @param string $indexName
* @param string $extra Optional string that replaces '###' in the array element value string. Default is FALSE
* @param boolean $html Optional boolean for HTML printing (TRUE/default) or plain text (FALSE)
* @return string
*/
	public function text($indexName, $extra = FALSE, $html = TRUE)
	{
// if message missing from language pack, use default English
        $temp = "__WIKINDX__NOT_YET_TRANSLATED__";
		if(array_key_exists($indexName, $this->languageArray))
            $temp = $this->languageArray[$indexName];

        if ($temp == "__WIKINDX__NOT_YET_TRANSLATED__")
		{
		    if(array_key_exists($indexName, $this->arrayEnglish))
			    $temp = $this->arrayEnglish[$indexName];
		    else
			    $temp = "";
		}

// replace #xxx# variables
		$temp = preg_replace_callback('/#([^#]*)#/Uu', array($this, 'textCallback'), $temp);
		$pString = $extra !== FALSE ?
			preg_replace("/###/u", trim($extra), $temp) :
			preg_replace("/###/u", '', $temp);
		$pString = stripslashes($pString);
		if($html)
			return UTF8::html_uentity_decode(trim($pString));
		else
			return trim($pString);
	}
/**
* Callback function for preg_replace_callback() in text()
*
* @param array $matches
* @return string
*/
	private function textCallback($matches)
	{
		$var = 'public' . $matches[1];
		return $this->messages->{$var};
	}
}
?>