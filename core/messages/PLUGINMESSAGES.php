<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* PLUGINMESSAGES
*
* Handle plugin localizations
*
* @version	1
*
*	@package wikindx5\core\messages
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class PLUGINMESSAGES
{
/** string */
private $pluginDir;
/** string */
private $pluginFile;
/** string */
private $language;
/** object */
private $session;
/** array */
private $messages = array();
/** array */
private $englishMessages = array();

/**
 * PLUGINMESSAGES
 *
 * @param string $pluginDir
 * @param string $pluginFile
 */
	public function __construct($pluginDir, $pluginFile)
	{
		$this->pluginFile = $pluginFile;
		$this->pluginDir = $pluginDir;
		$this->session = FACTORY_SESSION::getInstance();
		if(!$this->language = $this->session->getVar("setup_Language"))
			$this->language = "en";
		$this->loadLanguage();
		$this->loadEnglish();
	}
/**
* Grab the localized message
*
* @param string $indexName
* @param string $extra Optional string that replaces '###' in the array element value string. Default is FALSE
* @return string
*/
	public function text($indexName, $extra = FALSE)
	{
		if(!array_key_exists($indexName, $this->messages))
		{
			if(!array_key_exists($indexName, $this->englishMessages))
				die("<p>Message <strong>$indexName</strong> not found in English language pack.</p>");
			else
				$message = $this->englishMessages["$indexName"];
		}
		else
			$message = $this->messages["$indexName"];
		$pString = $extra !== FALSE ?
			preg_replace("/###/u", trim($extra), $message)
			:
			preg_replace("/###/u", '', $message);
		return UTF8::html_uentity_decode(trim($pString));
	}
/**
* Load English message arrays in case of missing localized messages
*/
	private function loadEnglish()
	{
		$file = "plugins/" . $this->pluginDir . "/" . $this->pluginFile . '_en.php';
		if(!file_exists($file))
			return;
		include_once($file);
		$class = $this->pluginFile . "_en";
		$languageClass= new $class();
		$this->englishMessages = $languageClass->text;
	}
/**
Load localized language array
*/
	private function loadLanguage()
	{
		$file = "plugins/" . $this->pluginDir . "/" . $this->pluginFile . '_' . $this->language . '.php';
		if(!file_exists($file))
			return;
		include_once($file);
		$className = $this->pluginFile . "_" . $this->language;
		$languageClass = new $className;
		$this->messages = $languageClass->text;
	}
}