<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Parse the bibliographic style's XML
*
* Conversion to use with PHP's simpleXML by Ritesh Agrawal and Mark Grimshaw 2007/2008
*
* @version	1
*
*	@package wikindx5\core\bibcitation
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class PARSEXML
{
/** object */
private $session;
/** string */
private $setupStyle;
/** array */
public $info = array();
/** array */
public $citation = array();
/** array */
public $footnote = array();
/** array */
public $common = array();
/** array */
public $types = array();

/**
* PARSEXML
*/
	public function __construct()
	{
		$this->session = FACTORY_SESSION::getInstance();
	}
/**
* Read the chosen bibliographic style and create arrays based on resource type.
*
* @author	Mark Grimshaw
* @version	1
*
* @param string $output 'html', plain', 'rtf'. Default is 'html'
* @param	boolean $export		The requested bibliographic output style.
* @param	string $stylePath	The path where the styles are.
* @return	boolean
*/
	public function loadStyle($output, $export, $stylePath)
	{
		$this->getStyle($output, $export);
//05/05/2005 G.GARDEY: add a last "/" to $stylePath if not present.
		$stylePath = trim($stylePath);
		if(mb_substr($stylePath, mb_strlen($stylePath) - 1, 1) != DIRECTORY_SEPARATOR){
			$stylePath .= DIRECTORY_SEPARATOR;
		}
		$uc = $stylePath . mb_strtolower($this->setupStyle) . "/" . mb_strtoupper($this->setupStyle) . ".xml";
		$lc = $stylePath . mb_strtolower($this->setupStyle) . "/" . mb_strtolower($this->setupStyle) . ".xml";
		$styleFilePath = file_exists($uc) ? $uc : $lc;
// If the file cannot be opened, it may have just now been disabled by the admin so load the default
// style instead and reset the user's session
		if(!file_exists($styleFilePath))
		{
// style file is stored uppercase in the DB
		    $co = FACTORY_CONFIGDBSTRUCTURE::getInstance();
		    $this->setupStyle = $co->getOne('configStyle');

			if (empty($this->setupStyle))
			    $this->setupStyle = WIKINDX_STYLE_DEFAULT;

			$styleFilePath = $stylePath . mb_strtolower($this->setupStyle) . "/" . mb_strtoupper($this->setupStyle) . '.xml';

			if($fh = fopen($styleFilePath, "r"))
			{
				if(!$this->session->getVar('setup_ReadOnly'))
					$this->session->setVar("setup_Style", $this->setupStyle);

				fclose($fh);
			}
			else
				return FALSE;
		}
		if(!$this->loadCache($styleFilePath))
		{
			$this->extractEntries($styleFilePath);
			$this->createCache();
		}
		return TRUE;
	}
/**
* Load style cache file if available
*
* @param string $styleFilePath Absolute path of the style file
* @return boolean
*/
	private function loadCache($styleFilePath)
	{
        $styleCacheFilePath = 'styles/CACHE/' . $this->setupStyle;

        // If the cache file is missing, abort loading
		if(!file_exists($styleCacheFilePath))
			return FALSE;
        // If the cache file is expired, delete it and abort loading
        if (filemtime($styleFilePath) >= filemtime($styleCacheFilePath))
        {
            unlink($styleCacheFilePath);
            return FALSE;
        }
        // Load cache file, if readable
		if (FALSE !== ($fh = fopen($styleCacheFilePath, "r")))
		{
			$this->info = unserialize(fgets($fh));
			$this->citation = unserialize(fgets($fh));
			$this->footnote = unserialize(fgets($fh));
			$this->common = unserialize(fgets($fh));
			$this->types = unserialize(fgets($fh));

			fclose($fh);

			return TRUE;
		}

		// Fallback case: Cache not loaded!
		return FALSE;
	}
/**
* Create style cache
*/
	private function createCache()
	{
		if(FALSE !== ($fh = fopen('styles/CACHE/' . $this->setupStyle, "w")))
		{
			// Serialize each array and write as one line to cache file
			fputs($fh, serialize($this->info) . "\n");
			fputs($fh, serialize($this->citation) . "\n");
			fputs($fh, serialize($this->footnote) . "\n");
			fputs($fh, serialize($this->common) . "\n");
			fputs($fh, serialize($this->types) . "\n");

			fclose($fh);
		}

		// Fallback case: do nothing -- i.e. continue to read directly from XML files
	}
/**
* Get the bibliographic style file
*
* @param string $output 'html', plain', 'rtf'. Default is 'html'
* @param	boolean $export		The requested bibliographic output style.
*/
	private function getStyle($output, $export)
	{
		if($output == 'rtf')
		{
			if($export)
				$this->setupStyle = $this->session->getVar("exportPaper_Style");
			else
				$this->setupStyle = $this->session->getVar("wp_ExportStyle");
		}
		if(!$this->setupStyle)
			$this->setupStyle = $this->session->getVar("setup_Style");
	}
/**
* Extract entries from file
*
* @param string $file - Location of StyleFile
*/
	public function extractEntries($file)
	{
		$xmlString = simplexml_load_file($file);
		$this->info = $this->XMLToArray($xmlString->info);
		$this->getStyleTypes($xmlString);
		$this->getFootnotes($xmlString);
		$this->common = $this->XMLToArray($xmlString->bibliography->common);
		$this->citation = $this->XMLToArray($xmlString->citation);
//		$this->footnote = $this->XMLToArray($xmlString->footnote);
		unset($xmlString);
	}
/**
* Convert XML to array
*
* code borrowed from http://php.net
* @param string $xml
* @return mixed
*/
	private function XMLToArray($xml)
	{
		if ($xml instanceof SimpleXMLElement)
		{
			$children = $xml->children();
			$return = null;
		}
		foreach ($children as $element => $value)
		{
			if ($value instanceof SimpleXMLElement)
			{
				$values = (array)$value->children();
				if (count($values) > 0)
					$return[$element] = $this->XMLToArray($value);
				else
				{
					if (!isset($return[$element]))
						$return[$element] = (string)$value;
					else
					{
						if (!is_array($return[$element]))
							$return[$element] = array($return[$element], (string)$value);
						else
							$return[$element][] = (string)$value;
					}
				}
			}
		}
		if (is_array($return))
			return $return;
		else
			return FALSE;
	}
/**
* Cycle through XML
*
* @param string $xmlString
*/
	private function getFootnotes($xmlString)
	{
		foreach($xmlString->footnote->resource as $value)
		{
			$name = null;
			foreach($value->attributes() as $id => $v)
			{
				if($id == "name")
					$name = trim($v);
			}
			$this->footnote[$name] = $this->XMLToArray($value);
		}
	}
/**
* Cycle through XML
*
* @param string $xmlString
*/
	private function getStyleTypes($xmlString)
	{
		foreach($xmlString->bibliography->resource as $value)
		{
			$name = null;
			$creatorRewriteArray = array();
			foreach($value->attributes() as $id => $v)
			{
				if($id == "name")
					$name = trim($v);
				else
					$creatorRewriteArray[$id] = (string)$v;
			}
			$this->types[$name] = $this->XMLToArray($value) + $creatorRewriteArray;
			if(isset($value->fallbackstyle))
			{
				$this->types['fallback'][$name] = trim((string)($value->fallbackstyle));
			}
		}
	}
}
?>