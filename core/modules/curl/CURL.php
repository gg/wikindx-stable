<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Generalized CURL function.
*
* @version	1
*
*	@package wikindx5
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class CURL
{
	public function __construct()
	{
	}
/**
* mass cache of attachments
*/
	public function attachmentCache()
	{
// HTTP charset (HTTP specification doesn't permit to declare Content-type separately)
		header('Content-type: ' . WIKINDX_MIMETYPE_TXT . '; charset=' . WIKINDX_CHARSET);
// Define a custom header that point to the hash of the parsed file
// This is mandatory because the output of PdfToText could be altered at byte level
		header('resourceattachmentsHashFilename: ' . $_GET['id']);
		include_once("core/modules/list/FILETOTEXT.php");
		$ftt = new FILETOTEXT();
		print $ftt->convertToText($_GET['file'], $_GET['fileType']);
		FACTORY_CLOSERAW::getInstance(); // die
	}
}
?>