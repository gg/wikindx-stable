<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
*	ADMINCOMPONENTS class.
*
*	Administration of components including plugins, bibliography styles, languages, and templates.
*****/
class ADMINCOMPONENTS
{
private $db;
private $vars;
private $session;
private $errors;
private $messages;
private $success;
private $gatekeep;
private $possibleMenus;
private $possibleContainers;
private $co;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->session = FACTORY_SESSION::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();
		$this->co = FACTORY_CONFIGDBSTRUCTURE::getInstance();
		$this->gatekeep = FACTORY_GATEKEEP::getInstance();

		if(!array_key_exists('method', $this->vars) ||
			(($this->vars['method'] != 'initConfigMenu') && ($this->vars['method'] != 'initConfigInline')))
			\AJAX\loadJavascript($this->config->WIKINDX_BASE_URL . '/core/modules/admin/adminComponents.js');
		$this->gatekeep->requireSuper = TRUE;
		$this->gatekeep->init();
		$this->possibleMenus = array(
			'wikindx' => 'Wikindx',
			'res' => $this->messages->text('menu', 'res'),
			'search' => $this->messages->text('menu', 'search'),
			'text' => $this->messages->text('menu', 'text'),
			'admin' => $this->messages->text('menu', 'admin'),
			'plugin1' => $this->messages->text('menu', 'plugin1'),
			'plugin2' => $this->messages->text('menu', 'plugin2'),
			'plugin3' => $this->messages->text('menu', 'plugin3')
		);
		$this->possibleContainers = array('inline1' => 'inline1', 'inline2' => 'inline2',
			'inline3' => 'inline3', 'inline4' => 'inline4');
		include_once("core/modules/help/HELPMESSAGES.php");
		$help = new HELPMESSAGES();
		GLOBALS::setTplVar('help', $help->createLink('plugins'));
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "adminComponents"));
	}
// Display options.
	public function init($message = FALSE)
	{
		include_once("core/modules/LOADEXTERNALMODULES.php");
		$loadmodules = new LOADEXTERNALMODULES();

		foreach (FILE\dirInDirToArray($loadmodules->moduleDirectory) as $dir)
		{
			$index = $loadmodules->moduleDirectory . $dir . DIRECTORY_SEPARATOR . 'index.php';
			$config = $loadmodules->moduleDirectory . $dir . DIRECTORY_SEPARATOR . 'config.php';
			$type = $loadmodules->moduleDirectory . $dir . DIRECTORY_SEPARATOR . 'plugintype.txt';

			if(file_exists($index) && file_exists($config) && file_exists($type))
			{
				if(!$loadmodules->checkVersion($loadmodules->moduleDirectory, $dir))
				{
					$message .= \HTML\hr();
					$message .= $this->errors->text('warning', 'pluginVersion', WIKINDX_PLUGIN_VERSION);
					break;
				}
			}
		}

		$pString = '';
		if($message)
			$pString .= \HTML\p($message, "error", "center");
		$pString .= $this->displayEnableDisable();
		$pString .= \HTML\hr();
		$pString .= $this->displayConfig();
		GLOBALS::addTplVar('content', $pString);
	}
// Display the config settings for the plugin
	private function displayConfig()
	{
		list($enabledMenu, $enabledInline) = $this->configurablePlugins();
		if(empty($enabledMenu) && empty($enabledInline))
			return FALSE;
		$pString = '';
		if(!empty($enabledMenu))
		{
			$pString .= \FORM\formHeader("admin_ADMINCOMPONENTS_CORE");
			$pString .= \FORM\hidden("method", "writeConfigMenu");
			$jsonArray = array();
			$jScript = 'index.php?action=admin_ADMINCOMPONENTS_CORE&method=initConfigMenu';
			$jsonArray[] = array(
				'startFunction' => 'triggerFromMultiSelect',
				'script' => "$jScript",
				'triggerField' => 'configFileMenu',
				'targetDiv' => 'divMenu',
				);
			$js = \AJAX\jActionForm('onchange', $jsonArray);
			$pString .= \HTML\tableStart('');
			$pString .= \HTML\trStart();
			$pString .= \HTML\td(\FORM\selectFBoxValue($this->messages->text('misc',
				'pluginsConfigureMenu'), 'configFileMenu', $enabledMenu, 5, FALSE, $js));
			$pString .= \HTML\td(\HTML\div('divMenu', $this->getConfigDetailsMenu($enabledMenu)), 'left top width80percent');
			$pString .= \HTML\trEnd();
			$pString .= \HTML\tableEnd();
			$pString .= \HTML\p(\FORM\formSubmit('Proceed'));
			$pString .= \FORM\formEnd();
		}
		if(!empty($enabledInline))
		{
			if(!empty($enabledMenu))
				$pString .= \HTML\hr();
			$pString .= \FORM\formHeader("admin_ADMINCOMPONENTS_CORE");
			$pString .= \FORM\hidden("method", "writeConfigInline");
			$jsonArray = array();
			$jScript = 'index.php?action=admin_ADMINCOMPONENTS_CORE&method=initConfigInline';
			$jsonArray[] = array(
				'startFunction' => 'triggerFromMultiSelect',
				'script' => "$jScript",
				'triggerField' => 'configFileInline',
				'targetDiv' => 'divInline',
				);
			$js = \AJAX\jActionForm('onchange', $jsonArray);
			$pString .= \HTML\tableStart();
			$pString .= \HTML\trStart();
			$pString .= \HTML\td(\FORM\selectFBoxValue($this->messages->text('misc',
				'pluginsConfigureInline'), 'configFileInline', $enabledInline, 5, FALSE, $js));
			$pString .= \HTML\td(\HTML\div('divInline', $this->getConfigDetailsInline($enabledInline)), 'left top width80percent');
			$pString .= \HTML\trEnd();
			$pString .= \HTML\tableEnd();
			$pString .= \HTML\p(\FORM\formSubmit('Proceed'));
			$pString .= \FORM\formEnd();
		}
		return $pString;
	}
/**
* return list of plugins that are configurable.
*/
	private function configurablePlugins()
	{
		$enabledMenu = $enabledInline = array();

		$rootdir = 'plugins';

		foreach (FILE\dirInDirToArray($rootdir) as $dir)
		{
    		$index = $rootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'index.php';
    		$config = $rootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'config.php';
    		$type = $rootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'plugintype.txt';
    		$description = $rootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt';

    		if(file_exists($index) && file_exists($config))
    		{
    			$string = FALSE;
    // read one line
    			if($fh = fopen($type, "r"))
    			{
    				$type = fgets($fh);
    				fclose($fh);
    			}
    // read one line
    			if(file_exists($description) && $fh = fopen($description, "r"))
    			{
    				$string = fgets($fh);
    				fclose($fh);
    			}

    			if ($type === FALSE)
    				$type = '';

    			if ($string === FALSE)
    				$string = $dir;
    			if($type == 'menu')
    				$enabledMenu[$dir] = $string;
    			else if($type == 'inline')
    				$enabledInline[$dir] = $string;
    		}
		}
		return array($enabledMenu, $enabledInline);
	}
/**
* Get config details for menu plugins and put into form elements
*/
	private function getConfigDetailsMenu($enabled)
	{
		if(array_key_exists('ajaxReturn', $this->vars))
			$file = $this->vars['ajaxReturn'];
		else // grab the first of the list
		{
			foreach($enabled as $file => $null)
				break;
		}

		$pString = '';

		if(file_exists('plugins/' . $file . '/README.txt') || file_exists('plugins/' . $file . '/README'))
		{
			$js = "onClick=\"coreOpenPopup('index.php?action=admin_ADMINCOMPONENTS_CORE&method=readMe&file=$file'); return false\"";
			$pString .= \HTML\div("divReadMeMenu", \HTML\aBrowse('green', '1em',
				$this->messages->text('misc', 'openReadme'), '#', '', '', $js), 'floatRight');
		}
		else
			$pString .= \HTML\div("divReadMeMenu", $this->messages->text('misc', 'noReadme'), 'floatRight');

		if($fh = fopen('plugins/' . $file . '/config.php', "r"))
		{
			$string = '';
			while(!feof($fh))
				$string .= fgets($fh);
			fclose($fh);
			$pString .= \FORM\textareaInput($this->messages->text('misc',
				'pluginConfig'), 'configConfig', $string, 100, 8);
		}

		return $pString;
	}
/**
* Display README file
*/
	public function readMe()
	{
		$fh = FALSE;

		if(file_exists('plugins/' . $this->vars['file'] . '/README.txt'))
			$fh = fopen('plugins/' . $this->vars['file'] . '/README.txt', "r");

		if($fh === FALSE)
			if(file_exists('plugins/' . $this->vars['file'] . '/README'))
				$fh = fopen('plugins/' . $this->vars['file'] . '/README', "r");

		if($fh !== FALSE)
		{
			$pString = '';

			while(!feof($fh))
				$pString .= fgets($fh) . BR;

			fclose($fh);
		}
		else
		{
			$pString = $this->errors->text('file', 'read');
		}

		GLOBALS::addTplVar('content', $pString);
		FACTORY_CLOSEPOPUP::getInstance();
	}
/**
* Get config details for inline plugins and put into form elements
*/
	private function getConfigDetailsInline($enabled)
	{
		$arrayIndex = 0;
		$usedContainers = $conflictContainers = array();
		foreach($enabled as $file => $null)
		{
			include_once("plugins/$file/config.php");
			$configClass = $file . '_CONFIG';
			if(!$arrayIndex)
			{
				$firstConfigClass = $configClass;
				$firstFile = $null;
				$arrayIndex++;
			}
			$config = new $configClass();
			if(($index = array_search($config->container, $usedContainers)) === FALSE)
				$usedContainers[$file] = $config->container;
			else
			{
				$conflictContainers[$index] = $usedContainers[$index];
				$conflictContainers[$file] = $config->container;
			}
		}
		if(array_key_exists('ajaxReturn', $this->vars))
		{
			$file = $this->vars['ajaxReturn'];
			include_once("plugins/$file/config.php");
			$configClass = $file . '_CONFIG';
		}
		else // grab the first of the list
		{
			$configClass = $firstConfigClass;
			$file = $firstFile;
		}
		$config = new $configClass();

		$pString = '';

		if(file_exists('plugins/' . $file . '/README.txt') || file_exists('plugins/' . $file . '/README'))
		{
			$js = "onClick=\"coreOpenPopup('index.php?action=admin_ADMINCOMPONENTS_CORE&method=readMe&file=$file'); return false\"";
			$pString .= \HTML\div("divReadMeInline", \HTML\aBrowse('green', '1em',
				$this->messages->text('misc', 'openReadme'), '#', '', '', $js), 'floatRight');
		}
		else
			$pString .= \HTML\div("divReadMeInline", $this->messages->text('misc', 'noReadme'), 'floatRight');

		$pString .= \FORM\selectedBoxValue($this->messages->text('misc',
			'pluginsContainers'), 'configContainers', $this->possibleContainers, $config->container, 4);

		if(!empty($conflictContainers))
		{
			foreach($conflictContainers as $plugin => $container)
				$array[] = $plugin . ':&nbsp;' . $container;

			$pString .= $this->errors->text('warning', 'pluginConflict', join(', ', $array));
		}

		return $pString;
	}
/**
* AJAX-based DIV content creator for configuration of plugin menus
*/
	public function initConfigMenu()
	{
		list($enabledMenu) = $this->configurablePlugins();
		$div = \HTML\div('divMenu', $this->getConfigDetailsMenu($enabledMenu));
		GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('innerHTML' => $div)));
		FACTORY_CLOSERAW::getInstance();
	}
/**
* AJAX-based DIV content creator for configuration of inline plugins
*/
	public function initConfigInline()
	{
		list(, $enabledInline) = $this->configurablePlugins();
		$div = \HTML\div('divInline', $this->getConfigDetailsInline($enabledInline));
		GLOBALS::addTplVar('content', \AJAX\encode_jArray(array('innerHTML' => $div)));
		FACTORY_CLOSERAW::getInstance();
	}
// Display enable and disable select boxes for plugins etc.
	private function displayEnableDisable()
	{
		$pString = \FORM\formHeader("admin_ADMINCOMPONENTS_CORE", "onsubmit=\"selectAll();return true;\"");
		$pString .= \FORM\hidden("method", "enableDisable");
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
// plugins
			list($enabled, $disabled) = $this->listPlugins();


			$pString .= \HTML\tdStart('left middle width50percent');
			$pString .= \HTML\tableStart('generalTable borderStyleNone');
			$pString .= \HTML\trStart();
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text('misc',
				'pluginsEnabled'), 'pluginsEnabled', $enabled, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'));

			if(empty($enabled) && empty($disabled))
				$pString .= \HTML\td('&nbsp;');
			else
			{
				$jsonArray = array();
				$jsonArray[] = array('startFunction' => 'disablePlugin');
				$toRightImage = \AJAX\jActionIcon('toRight', 'onclick', $jsonArray);
				$jsonArray = array();
				$jsonArray[] = array('startFunction' => 'enablePlugin');
				$toLeftImage = \AJAX\jActionIcon('toLeft', 'onclick', $jsonArray);
				$pString .= \HTML\td(\HTML\p($toRightImage) . \HTML\p($toLeftImage));
			}

			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text('misc',
				'pluginsDisabled'), 'pluginsDisabled', $disabled, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'), 'left middle width50percent');
			$pString .= \HTML\trEnd();
			$pString .= \HTML\tableEnd();
			$pString .= \HTML\tdEnd();


// styles
			list($enabled, $disabled) = $this->listStyles();

			$pString .= \HTML\tdStart('left middle width50percent');
			$pString .= \HTML\tableStart('generalTable borderStyleNone');
			$pString .= \HTML\trStart();
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text('misc',
				'stylesEnabled'), 'stylesEnabled', $enabled, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'));

			if((sizeof($enabled) + sizeof($disabled)) < 2)
				$pString .= \HTML\td('&nbsp;');
			else
			{
				$jsonArray = array();
				$jsonArray[] = array('startFunction' => 'disableStyle');
				$toRightImage = \AJAX\jActionIcon('toRight', 'onclick', $jsonArray);
				$jsonArray = array();
				$jsonArray[] = array('startFunction' => 'enableStyle');
				$toLeftImage = \AJAX\jActionIcon('toLeft', 'onclick', $jsonArray);
				$pString .= \HTML\td(\HTML\p($toRightImage) . \HTML\p($toLeftImage));
			}

			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text('misc',
				'stylesDisabled'), 'stylesDisabled', $disabled, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'), 'left middle width50percent');
			$pString .= \HTML\trEnd();
			$pString .= \HTML\tableEnd();
			$pString .= \HTML\tdEnd();

		$pString .= \HTML\trEnd();


// templates
		$pString .= \HTML\trStart();

			list($enabled, $disabled) = $this->listTemplates();

			$pString .= \HTML\tdStart('left middle width50percent');
			$pString .= \HTML\tableStart('generalTable borderStyleNone');
			$pString .= \HTML\trStart();
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text('misc',
				'templatesEnabled'), 'templatesEnabled', $enabled, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'));

			if((sizeof($enabled) + sizeof($disabled)) < 2)
				$pString .= \HTML\td('&nbsp;');
			else
			{
				$jsonArray = array();
				$jsonArray[] = array('startFunction' => 'disableTemplate');
				$toRightImage = \AJAX\jActionIcon('toRight', 'onclick', $jsonArray);
				$jsonArray = array();
				$jsonArray[] = array('startFunction' => 'enableTemplate');
				$toLeftImage = \AJAX\jActionIcon('toLeft', 'onclick', $jsonArray);
				$pString .= \HTML\td(\HTML\p($toRightImage) . \HTML\p($toLeftImage));
			}

			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text('misc',
				'templatesDisabled'), 'templatesDisabled', $disabled, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'), 'left middle width50percent');
			$pString .= \HTML\trEnd();
			$pString .= \HTML\tableEnd();
			$pString .= \HTML\tdEnd();


// languages
			list($enabled, $disabled) = $this->listLanguages();

			$pString .= \HTML\tdStart('left middle width50percent');
			$pString .= \HTML\tableStart('generalTable borderStyleNone');
			$pString .= \HTML\trStart();
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text('misc',
				'languagesEnabled'), 'languagesEnabled', $enabled, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'));

			if((sizeof($enabled) + sizeof($disabled)) < 2)
				$pString .= \HTML\td('&nbsp;');
			else
			{
				$jsonArray = array();
				$jsonArray[] = array('startFunction' => 'disableLanguage');
				$toRightImage = \AJAX\jActionIcon('toRight', 'onclick', $jsonArray);
				$jsonArray = array();
				$jsonArray[] = array('startFunction' => 'enableLanguage');
				$toLeftImage = \AJAX\jActionIcon('toLeft', 'onclick', $jsonArray);
				$pString .= \HTML\td(\HTML\p($toRightImage) . \HTML\p($toLeftImage));
			}

			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text('misc',
				'languagesDisabled'), 'languagesDisabled', $disabled, 5) . BR .
				\HTML\span($this->messages->text("hint", "multiples"), 'hint'), 'left middle width50percent');
			$pString .= \HTML\trEnd();
			$pString .= \HTML\tableEnd();
			$pString .= \HTML\tdEnd();

		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();

		$pString .= \HTML\p(\FORM\formSubmit('Proceed'));

		$pString .= \FORM\formEnd();
		return $pString;
	}
// List plugins
	private function listPlugins()
	{
		$enabled = $disabled = array();

		$rootDir = 'plugins';

		foreach (FILE\dirInDirToArray($rootDir) as $dir)
		{
    		$filePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'index.php';
    		$descFilePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt';

    		if(file_exists($filePath))
    		{
    			if(file_exists($descFilePath))
    			{
    			    $fh = fopen($descFilePath, "r");
    				$string = fgets($fh);
    				$enabled[$dir] = $string;
    				fclose($fh);
    			}
    			else
    				$enabled[$dir] = $dir;
    		}
    		else if(file_exists($filePath . '~'))
    		{
    			if(file_exists($descFilePath))
    			{
    			    $fh = fopen($descFilePath, "r");
    				$string = fgets($fh);
    				$disabled[$dir] = $string;
    				fclose($fh);
    			}
    			else
    				$disabled[$dir] = $dir;
    		}
		}

		return array($enabled, $disabled);
	}
// List styles
	private function listStyles()
	{
		$enabled = $disabled = array();

		$rootDir = 'styles/bibliography';

		foreach (FILE\dirInDirToArray($rootDir) as $dir)
		{
			$filePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml";

			if(file_exists($filePath))
			{
			    $fh = fopen($filePath, "r");
				while(!feof($fh))
				{
					$line = stream_get_line($fh, 1000000, "". LF);
					if(preg_match("/<description>(.*)<\\/description>/Uui", $line, $matches))
					{
						$array[mb_strtoupper($dir)] = $matches[1];
						$enabled[$dir] = $matches[1];
						break;
					}
				}
				fclose($fh);
			}
			else if(file_exists($filePath . "~"))
			{
			    $fh = fopen($filePath . "~", "r");
				while(!feof($fh))
				{
					$line = stream_get_line($fh, 1000000, "". LF);
					if(preg_match("/<description>(.*)<\\/description>/Uui", $line, $matches))
					{
						$array[mb_strtoupper($dir)] = $matches[1];
						$disabled[$dir] = $matches[1];
						break;
					}
				}
				fclose($fh);
			}
		}

		return array($enabled, $disabled);
	}
// List templates
	private function listTemplates()
	{
		$enabled = $disabled = array();

		$rootDir = 'templates';

		foreach (FILE\dirInDirToArray($rootDir) as $dir)
		{
    		$filePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt';

    		if(file_exists($filePath))
    		{
    		    $fh = fopen($filePath, "r");
    			$enabled[$dir] = fgets($fh);
    			fclose($fh);
    		}
    		else if(file_exists($filePath . '~'))
    		{
    		    $fh = fopen($filePath . '~', "r");
    			$disabled[$dir] = fgets($fh);
    			fclose($fh);
    		}
		}

		return array($enabled, $disabled);
	}
// List languages
	private function listLanguages()
	{
		$enabled = $disabled = array();

		$rootDir = 'languages';

		foreach (FILE\dirInDirToArray($rootDir) as $dir)
		{
    		$filePath = $rootDir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt';

    		if(file_exists($filePath))
    		{
    		    $fh = fopen($filePath, "r");
    			$enabled[$dir] = fgets($fh);
    			fclose($fh);
    		}
    		else if(file_exists($filePath . '~'))
    		{
    		    $fh = fopen($filePath . '~', "r");
    			$disabled[$dir] = fgets($fh);
    			fclose($fh);
    		}
		}

		return array($enabled, $disabled);
	}
/**
* Actually enable/disable plugins
*/
	public function enableDisable()
	{
		$bool = FALSE;

		$stylesDir = 'styles' . DIRECTORY_SEPARATOR . 'bibliography' . DIRECTORY_SEPARATOR;

		if(array_key_exists('pluginsEnabled', $this->vars))
		{
			foreach($this->vars['pluginsEnabled'] as $dir)
			{
				if(file_exists('plugins/' . $dir . '/index.php~'))
					$bool = @rename('plugins/' . $dir . '/index.php~', 'plugins/' . $dir . '/index.php');
			}
		}
		if(array_key_exists('pluginsDisabled', $this->vars))
		{
			foreach($this->vars['pluginsDisabled'] as $dir)
			{
				if(file_exists('plugins/' . $dir . '/index.php'))
					$bool = @rename('plugins/' . $dir . '/index.php', 'plugins/' . $dir . '/index.php~');
			}
		}
// styles
		$enabled = array();
		if(array_key_exists('stylesEnabled', $this->vars))
		{
			foreach($this->vars['stylesEnabled'] as $dir)
			{
				if(file_exists($stylesDir . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml~"))
					$bool = @rename($stylesDir . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml~", $stylesDir . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml");
				$enabled[] = mb_strtoupper($dir);
			}
		}
		if(array_key_exists('stylesDisabled', $this->vars))
		{
			$disabled = array();
			foreach($this->vars['stylesDisabled'] as $dir)
			{
				if((sizeof($enabled) > 0) && file_exists($stylesDir . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml"))
					$bool = @rename($stylesDir . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml", $stylesDir . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml~");
			}
			$this->dbStyles();
		}
// templates
		$enabled = array();
		if(array_key_exists('templatesEnabled', $this->vars))
		{
			foreach($this->vars['templatesEnabled'] as $dir)
			{
				if(file_exists('templates/' . $dir . '/description.txt~'))
					$bool = @rename('templates/' . $dir . '/description.txt~', 'templates/' . $dir . '/description.txt');
				$enabled[] = mb_strtoupper($dir);
			}
		}
		if(array_key_exists('templatesDisabled', $this->vars))
		{
			$disabled = array();
			foreach($this->vars['templatesDisabled'] as $dir)
			{
				if((sizeof($enabled) > 0) && file_exists('templates/' . $dir . '/description.txt'))
					$bool = @rename('templates/' . $dir . '/description.txt', 'templates/' . $dir . '/description.txt~');
			}
			$this->dbTemplates();
		}
// languages
		$enabled = array();
		if(array_key_exists('languagesEnabled', $this->vars))
		{
			foreach($this->vars['languagesEnabled'] as $dir)
			{
				if(file_exists('languages/' . $dir . '/description.txt~'))
					$bool = @rename('languages/' . $dir . '/description.txt~', 'languages/' . $dir . '/description.txt');
				$enabled[] = mb_strtoupper($dir);
			}
		}
		if(array_key_exists('languagesDisabled', $this->vars))
		{
			$disabled = array();
			foreach($this->vars['languagesDisabled'] as $dir)
			{
				if((sizeof($enabled) > 0) && file_exists('languages/' . $dir . '/description.txt'))
					$bool = @rename('languages/' . $dir . '/description.txt', 'languages/' . $dir . '/description.txt~');
			}
			$this->dbLanguages();
		}
		if(!$bool)
			return $this->init($this->errors->text('file', 'write'));
		else
		    return $this->init($this->success->text("plugins"));
	}
/**
Change menu plugin configuration settings
*/
	public function writeConfigMenu()
	{
		$bool = FALSE;
		if($fh = @fopen('plugins/' . $this->vars['configFileMenu'] . '/config.php', "w"))
		{
			$bool = @fwrite($fh, stripslashes($this->vars['configConfig']));
			fclose($fh);
		}
		if($bool === FALSE)
			return $this->init($this->errors->text('file', 'write'));
		else
		    return $this->init($this->success->text("plugins"));
	}
/**
Change inline plugin configuration settings
*/
	public function writeConfigInline()
	{
		$bool = FALSE;
		$configFile = 'plugins' . DIRECTORY_SEPARATOR . $this->vars['configFileInline'] . DIRECTORY_SEPARATOR. 'config.php';
		include_once($configFile);
		$class = $this->vars['configFileInline'] . '_CONFIG';
		$config = new $class();
		$vars = get_class_vars($class);
		$varString = '';
		foreach($vars as $var => $value)
		{
			if($var == 'container')
				$varString .= "". LF . 'public $' . $var . " = '" . $this->vars['configContainers'] . "';";
			else
			{
				if(is_string($value))
					$value = "'" . $value . "'";
				$varString .= "". LF . 'public $' . $var . ' = ' . $value . ";";
			}
		}
		if($fh = @fopen($configFile, "w"))
		{
			$string = "<?php\nclass $class {";
			$string .= $varString;
			$string .= "\n}\n?>";
			$bool = @fwrite($fh, $string);
			fclose($fh);
		}
		if($bool === FALSE)
			return $this->init($this->errors->text('file', 'write'));
		else
		    return $this->init($this->success->text("plugins"));
	}
/**
* If styles have been disabled, check they no longer exist in user preferences.
* If they do, replace with the first style available
*/
	private function dbStyles()
	{
		$disabled = array();
		$defaultStyle = FALSE;

		$rootdir = 'styles' . DIRECTORY_SEPARATOR . 'bibliography' . DIRECTORY_SEPARATOR;

		foreach (FILE\dirInDirToArray($rootdir) as $dir)
		{
    		if(file_exists($rootdir . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml~"))
    			$disabled[] = mb_strtoupper($dir);
    		if(file_exists($rootdir . $dir . DIRECTORY_SEPARATOR . mb_strtoupper($dir) . ".xml") && !$defaultStyle)
    			$defaultStyle = mb_strtoupper($dir);
		}

		$this->db->formatConditionsOneField($disabled, 'usersStyle');
		$recordset = $this->db->select('users', 'usersId');
		while($row = $this->db->fetchRow($recordset))
		{
			$this->db->formatConditions(array('usersId' => $row['usersId']));
			$this->db->update('users', array('usersStyle' => $defaultStyle));
// reset session for this user
			if($row['usersId'] == $this->session->getVar("setup_UserId"))
				$this->session->setVar("setup_Style", $defaultStyle);
		}
		$style = $this->co->getOne('configStyle');
		if(array_search($style, $disabled) !== FALSE)
			$this->co->updateOne('configStyle', $defaultStyle);
// delete style cache files
		foreach($disabled as $dir)
			@unlink('styles/CACHE/' . $dir);
	}
/**
* If templates have been disabled, check they no longer exist in user preferences.
* If they do, replace with the first template available
*/
	private function dbTemplates()
	{
		$disabled = array();
		$default = FALSE;

		$rootdir = 'templates';

		foreach (FILE\dirInDirToArray($rootdir) as $dir)
		{
    		if(file_exists($rootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt~'))
    			$disabled[] = $dir;
    		if(file_exists($rootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt') && !$default)
    			$default = $dir;
		}

		$this->db->formatConditionsOneField($disabled, 'usersTemplate');
		$recordset = $this->db->select('users', 'usersId');
		while($row = $this->db->fetchRow($recordset))
		{
			$this->db->formatConditions(array('usersId' => $row['usersId']));
			$this->db->update('users', array('usersTemplate' => $default));
// reset session for this user
			if($row['usersId'] == $this->session->getVar("setup_UserId"))
				$this->session->setVar("setup_Template", $default);
		}
		$style = $this->co->getOne('configTemplate');
		if(array_search($style, $disabled) !== FALSE)
			$this->co->updateOne('configTemplate', $default);
	}
/**
* If languages have been disabled, check they no longer exist in user preferences.
* If they do, replace with the first language available
*/
	private function dbLanguages()
	{
		$disabled = array();
		$default = FALSE;

		$rootdir = 'languages';

		foreach (FILE\dirInDirToArray($rootdir) as $dir)
		{
    		if(file_exists($rootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt~'))
    			$disabled[] = $dir;
    		if(file_exists($rootdir . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt') && !$default)
    			$default = $dir;
		}

		$this->db->formatConditionsOneField($disabled, 'usersLanguage');
		$recordset = $this->db->select('users', 'usersId');
		while($row = $this->db->fetchRow($recordset))
		{
			$this->db->formatConditions(array('usersId' => $row['usersId']));
			$this->db->update('users', array('usersLanguage' => $default));
// reset session for this user
			if($row['usersId'] == $this->session->getVar("setup_UserId"))
				$this->session->setVar("setup_Language", $default);
		}
		$style = $this->co->getOne('configLanguage');
		if(array_search($style, $disabled) !== FALSE)
			$this->co->updateOne('configLanguage', $default);
	}
}
?>