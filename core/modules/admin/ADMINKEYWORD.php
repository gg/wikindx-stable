<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* ADMINKEYWORD class
*
*/
class ADMINKEYWORD
{
private $db;
private $vars;
private $errors;
private $messages;
private $success;
private $session;
private $keyword;
private $gatekeep;
private $badInput;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();
		$this->session = FACTORY_SESSION::getInstance();

		$this->keyword = FACTORY_KEYWORD::getInstance();

		$this->gatekeep = FACTORY_GATEKEEP::getInstance();
		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->gatekeep->init();
		$this->session->clearArray('edit');
	}
	public function editInit($message = FALSE)
	{
// Edit operations use functions from core/modules/edit/EDITKEYWORD.php
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "edit", " (" . $this->messages->text("resources", "keyword") . ")"));
		include_once('core/modules/edit/EDITKEYWORD.php');
		$keyword = new EDITKEYWORD();
		$keyword->init();
	}
	public function mergeInit($message = FALSE)
	{
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "adminKeywords"));
		$keywords = $this->keyword->grabAll();
		$pString = \HTML\p($this->messages->text("misc", "keywordMerge"));
		if($message)
			$pString .= \HTML\p($message);
		if(is_array($keywords) && !empty($keywords))
		{
			$pString .= \FORM\formHeader('admin_ADMINKEYWORD_CORE');
			$pString .= \FORM\hidden("method", "merge");
			$pString .= \HTML\tableStart('left');
			$pString .= \HTML\trStart();
			$td = \FORM\selectFBoxValueMultiple(FALSE, "keywordIds", $keywords, 20) .
				BR . \HTML\span($this->messages->text("hint", "multiples"), 'hint');
			$pString .= \HTML\td($td);
			$td = \FORM\textInput($this->messages->text("misc", "keywordMergeTarget"),
				"keywordText", FALSE, 50);
			$td .= BR;
			$td .= BR;
			$td .= \FORM\formSubmit("Proceed");
			$pString .= \HTML\td($td);
			$pString .= \HTML\trEnd();
			$pString .= \HTML\tableEnd();
			$pString .= \FORM\formEnd();
		}
		else
			$pString .= \HTML\p($this->messages->text("misc", "noKeywords"));
		GLOBALS::addTplVar('content', $pString);
	}
// start merging process
	public function merge()
	{
		if(!array_key_exists("keywordIds", $this->vars))
			$this->badInput->close($this->errors->text("inputError", "missing"), $this, 'mergeInit');
		if(!array_key_exists("keywordText", $this->vars) || !trim($this->vars['keywordText']))
			$this->badInput->close($this->errors->text("inputError", "missing"), $this, 'mergeInit');
		if(array_key_exists("glossaries", $this->vars))
			$keywordIds = unserialize(base64_decode($this->vars['keywordIds']));
		else
			$keywordIds = $this->vars['keywordIds'];
		$newKeyword = trim($this->vars['keywordText']);
		$newKeywordId = $this->insertKeyword($newKeyword);
		if(($index = array_search($newKeywordId, $keywordIds)) !== FALSE)
			unset($keywordIds[$index]);
		if(empty($keywordIds)) // basically, we're renaming the keyword and that's all
		{
			$this->db->formatConditions(array('keywordId' => $newKeywordId));
			$this->db->update('keyword', array('keywordKeyword' => $newKeyword));
		}
		else
		{
// Check for glossary entries
			if(!array_key_exists("glossaries", $this->vars))
			{
				$this->db->formatConditionsOneField($keywordIds, 'keywordId');
				$resultset = $this->db->select('keyword', array('keywordId', 'keywordKeyword', 'keywordGlossary'));
				$glossaryString = '';
				while($row = $this->db->fetchRow($resultset))
					$glossaryString .= \HTML\p(\HTML\strong($row['keywordKeyword']) . ":&nbsp;&nbsp;" . $row['keywordGlossary']);
				if($glossaryString)
				{
					$pString = \HTML\p($this->messages->text("resources", "glossaryMerge"));
					$pString .= \FORM\formHeader('admin_ADMINKEYWORD_CORE');
					$pString .= \FORM\hidden("method", "merge");
					$pString .= \FORM\hidden("glossaries", TRUE);
					$pString .= \FORM\hidden("keywordIds", base64_encode(serialize($keywordIds)));
					$pString .= \FORM\hidden("keywordText", $newKeyword);
					$pString .= \HTML\p($this->messages->text('resources', 'glossary') . BR .
						\FORM\textareaInput(FALSE, "glossary", FALSE, 50, 10));
					$pString .= \FORM\formSubmit("Proceed");
					$pString .= \FORM\formEnd();
					$pString .= $glossaryString;
					GLOBALS::setTplVar('heading', $this->messages->text("heading", "adminKeywords"));
					GLOBALS::addTplVar('content', $pString);
					return; // break out here
				}
			}
// Remove old keywords
			$this->db->formatConditionsOneField($keywordIds, 'keywordId');
			$this->db->delete('keyword');
// Add or edit glossary
			if(array_key_exists("glossary", $this->vars))
			{
				$glossary = trim($this->vars['glossary']);
				$this->db->formatConditions(array('keywordId' => $newKeywordId));
				if($glossary)
					$this->db->update('keyword', array('keywordGlossary' => $glossary));
				else
					$this->db->updateNull('keyword', 'keywordGlossary');
			}
// update references to keyword ID
			$this->db->formatConditionsOneField($keywordIds, 'resourcekeywordKeywordId');
			$this->db->update('resource_keyword', array('resourcekeywordKeywordId' => $newKeywordId));
// If we are merging, say, 2 keywords that a particular resource has, need to ensure we do not duplicate keyword entries in
// resource_keyword for that resource
			$deleteIds = $rIds = array();
			$resultset = $this->db->select('resource_keyword', array('resourcekeywordId', 'resourcekeywordResourceId',
				'resourcekeywordMetadataId', 'resourcekeywordKeywordId'));
			while($row = $this->db->fetchRow($resultset))
			{
				if(!array_key_exists($row['resourcekeywordId'], $deleteIds) &&
					$row['resourcekeywordResourceId'] && array_key_exists($row['resourcekeywordResourceId'], $rIds)
					&& ($rIds[$row['resourcekeywordResourceId']] == $row['resourcekeywordKeywordId']))
					$deleteIds[] = $row['resourcekeywordId'];
				else if($row['resourcekeywordResourceId'])
					$rIds[$row['resourcekeywordResourceId']] = $row['resourcekeywordKeywordId'];
				else if(!array_key_exists($row['resourcekeywordId'], $deleteIds) &&
					$row['resourcekeywordMetadataId'] && array_key_exists($row['resourcekeywordMetadataId'], $rIds)
					&& ($rIds[$row['resourcekeywordMetadataId']] == $row['resourcekeywordKeywordId']))
					$deleteIds[] = $row['resourcekeywordId'];
				else if($row['resourcekeywordMetadataId'])
					$rIds[$row['resourcekeywordMetadataId']] = $row['resourcekeywordKeywordId'];
			}
			if(!empty($deleteIds))
			{
				$this->db->formatConditionsOneField($deleteIds, 'resourcekeywordId');
				$this->db->delete('resource_keyword');
			}
		}
// remove cache files for keywords
		$this->db->deleteCache('cacheKeywords');
		$this->db->deleteCache('cacheResourceKeywords');
		$this->db->deleteCache('cacheMetadataKeywords');
		$this->db->deleteCache('cacheQuoteKeywords');
		$this->db->deleteCache('cacheParaphraseKeywords');
		$this->db->deleteCache('cacheMusingKeywords');
		$this->mergeInit($this->success->text("keywordMerge"));
	}
// Insert new keyword or return ID if already exists
	private function insertKeyword($keyword)
	{
		$this->keywordExists = TRUE;
		if($id = $this->keyword->checkExists($keyword))
			return $id;
		$this->keywordExists = FALSE;
		$fields[] = "keywordKeyword";
		$values[0] = $keyword;
// given keyword doesn't exist so now write to db
		$this->db->insert('keyword', $fields, $values);
		return $this->db->lastAutoId();
	}
	public function deleteInit($message = FALSE)
	{
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "delete2", " (" .
			$this->messages->text("resources", "keyword") . ")"));
		$keywords = $this->keyword->grabAll();
		if(!$keywords)
		{
			GLOBALS::addTplVar('content', $this->messages->text('misc', 'noKeywords'));
			return;
		}
		$pString = $message;
		$pString .= \HTML\tableStart('left');
		$pString .= \HTML\trStart();
		$td = \FORM\formHeader('admin_ADMINKEYWORD_CORE');
		$td .= \FORM\hidden("method", "deleteConfirm");
		$td .= \FORM\selectFBoxValueMultiple(FALSE, "delete_KeywordId", $keywords, 20) .
			BR . \HTML\span($this->messages->text("hint", "multiples"), 'hint');
		$td .= \HTML\p(\FORM\formSubmit("Proceed"));
		$td .= \FORM\formEnd();
		$pString .= \HTML\td($td);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		GLOBALS::addTplVar('content', $pString);
	}
/**
* Confirm deletes.
*/
	public function deleteConfirm()
	{
		$this->session->delVar('editLock');
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "delete2", " (" .
			$this->messages->text("resources", "keyword") . ")"));
		$input = array_values($this->vars['delete_KeywordId']);
		$keywords = $this->keyword->grabAll();
		$keywordInput = "'" . implode("', '", array_keys(array_intersect(array_flip($keywords), $input))) . "'";
		$keywordInput = html_entity_decode($keywordInput);
		$pString = \HTML\p($this->messages->text("resources", "deleteConfirmKeywords") . ":&nbsp;&nbsp;$keywordInput");
		$pString .= \FORM\formHeader("admin_ADMINKEYWORD_CORE");
		$pString .= \FORM\hidden("delete_KeywordId", base64_encode(serialize($this->vars['delete_KeywordId'])));
		$pString .= \FORM\hidden("method", 'delete');
		$pString .= \HTML\p(\FORM\formSubmit("Proceed"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// write to the database.
	public function delete()
	{
		if($this->session->getVar('editLock'))
			$this->badInput->close($this->errors->text("done", "keywordDelete"), $this, 'deleteInit');
		if(!array_key_exists('delete_KeywordId', $this->vars) || !$this->vars['delete_KeywordId'])
			$this->badInput->close($this->errors->text("inputError", "missing"), $this, 'deleteInit');
		foreach(unserialize(base64_decode($this->vars['delete_KeywordId'])) as $deleteId)
		{
// Delete old keyword
			$this->db->formatConditions(array('keywordId' => $deleteId));
			$this->db->delete('keyword');
// remove cache files for keywords
			$this->db->deleteCache('cacheKeywords');
			$this->db->deleteCache('cacheResourceKeywords');
			$this->db->deleteCache('cacheMetadataKeywords');
			$this->db->deleteCache('cacheQuoteKeywords');
			$this->db->deleteCache('cacheParaphraseKeywords');
			$this->db->deleteCache('cacheMusingKeywords');
// Select all resources and metadata referencing this old keyword and remove keyword from list
			$this->db->formatConditions(array('resourcekeywordKeywordId' => $deleteId));
			$this->db->delete('resource_keyword');
		}
// lock reload
		$this->session->setVar('editLock', TRUE);
// Clear session
		$this->session->clearArray("edit");
// send back to deleteDisplay with success message
		$this->deleteInit($this->success->text("keywordDelete"));
	}
}
?>