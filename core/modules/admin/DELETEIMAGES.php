<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* DELETEREIMAGES class
*
* Delete images
*
*/
class DELETEIMAGES
{
private $db;
private $vars;
private $messages;
private $errors;
private $success;
private $session;
private $badInput;
private $gatekeep;
private $usedImages = array();
private $unusedImages = array();
private $numUsedImages = 0;
private $numUnusedImages = 0;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->gatekeep = FACTORY_GATEKEEP::getInstance();
	}
// check we are allowed to delete and load appropriate method
	public function init($message = FALSE)
	{
		$this->gatekeep->requireSuper = TRUE; // only admins can delete images if set to TRUE
		$this->gatekeep->init();
		if(array_key_exists('function', $this->vars))
		{
			$function = $this->vars['function'];
			$this->{$function}();
		}
		else
			$this->display();
	}
// display select box of images to delete
	private function display($message = FALSE)
	{
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "adminImages"));
		$pString = $message ? $message : FALSE;
		if(!$this->grabImages())
		{
			$pString .= $this->messages->text("misc", "noImages");
		}
		else
		{
    		$pString .= \HTML\tableStart();
    		$pString .= \HTML\trStart();
    		if(!empty($this->usedImages))
    		{
    			$td = \FORM\formHeader('admin_DELETEIMAGES_CORE');
    			$td .= \FORM\hidden('function', 'process');
    			$size = $this->numUsedImages > 20 ? 20 : $this->numUsedImages;
    			$td .= \FORM\selectFBoxValueMultiple($this->messages->text("misc", "usedImages"), "image_ids", $this->usedImages, $size, 80) .
    				BR . \HTML\span($this->messages->text("hint", "multiples"), 'hint') . BR .
    				BR . \FORM\formSubmit('Delete');
    			$td .= \FORM\formEnd();
    			$pString .= \HTML\td($td);
    		}
    		if(!empty($this->unusedImages))
    		{
    			$td = \FORM\formHeader('admin_DELETEIMAGES_CORE');
    			$td .= \FORM\hidden('function', 'process');
    			$size = $this->numUnusedImages > 20 ? 20 : $this->numUnusedImages;
    			$td .= \FORM\selectFBoxValueMultiple($this->messages->text("misc", "unusedImages"), "image_ids", $this->unusedImages, $size, 80) .
    				BR . \HTML\span($this->messages->text("hint", "multiples"), 'hint') . BR .
    				BR . \FORM\formSubmit('Delete');
    			$td .= \FORM\formEnd();
    			$pString .= \HTML\td($td);
    		}
    		$pString .= \HTML\trEnd();
    		$pString .= \HTML\tableEnd();
		}

		GLOBALS::addTplVar('content', $pString);
	}
// Grab all images in array.
// key is the file name, value is the display
	private function grabImages()
	{
		include_once("core/file/images.php");
		$encodeExplorer = new EncodeExplorer();
		$encodeExplorer->init();
		$location = new Location();
		$fileManager = new FileManager();
		$fileManager->run($location);
		$files = $encodeExplorer->run($location, TRUE);
		foreach($files as $file)
		{
			$stmts = array();
			$this->db->formatConditions($this->db->formatFields('resourcemetadataText') . $this->db->like('%', $file->getName(), '%'));
			$stmts[] = $this->db->selectNoExecute('resource_metadata', array(array('resourcemetadataId' => 'id')), TRUE, TRUE, TRUE);
			$this->db->formatConditions($this->db->formatFields('resourcetextAbstract') . $this->db->like('%', $file->getName(), '%'));
			$stmts[] = $this->db->selectNoExecute('resource_text', array(array('resourcetextId' => 'id')), TRUE, TRUE, TRUE);
			$this->db->formatConditions($this->db->formatFields('resourcetextNote') . $this->db->like('%', $file->getName(), '%'));
			$stmts[] = $this->db->selectNoExecute('resource_text', array(array('resourcetextId' => 'id')), TRUE, TRUE, TRUE);
			$this->db->formatConditions(array('configName' => 'configDescription'));
			$this->db->formatConditions($this->db->formatFields('configText') . $this->db->like('%', $file->getName(), '%'));
			$stmts[] = $this->db->selectNoExecute('config', array(array('configId' => 'id')), TRUE, TRUE, TRUE);
			$resultSet = $this->db->query($this->db->union($stmts));
			if($this->db->numRows($resultSet))
			{
				$this->usedImages[$file->getName()] = EncodeExplorer::getFilename($file) . ' (' . EncodeExplorer::formatSize($file->getSize()) . ')';
				++$this->numUsedImages;
			}
			else
			{
				$this->unusedImages[$file->getName()] = EncodeExplorer::getFilename($file) . ' (' . EncodeExplorer::formatSize($file->getSize()) . ')';
				++$this->numUnusedImages;
			}
		}
		return ($this->numUsedImages || $this->numUnusedImages);
	}
	private function process()
	{
		if(!$this->validateInput())
		{
			$this->display($this->errors->text("inputError", "missing"));
			FACTORY_CLOSE::getInstance();
		}
		foreach($this->vars['image_ids'] as $image)
			@unlink('images/' . $image);
		$pString = $this->success->text("imageDelete");
		$this->display($pString);
	}
// validate input
	private function validateInput()
	{
	    return array_key_exists('image_ids', $this->vars);
	}
}
?>