/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

Creative Commons
Creative Commons Legal Code
Attribution-NonCommercial-ShareAlike 2.0
THE WORK IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE”) — docs/LICENSE.txt. 
THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER 
THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.

BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. 
THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.

The WIKINDX Team 2018
sirfragalot@users.sourceforge.net

**********************************************************************************/

/**
* Javascript functions for core/modules/admin/ADMINUSER.php
*
* @version 1
* @date April 2018
* @author Mark Grimshaw
*/

/**
* Transfer an option from the authorized users selectbox to the blocked users selectbox
*/
function blockUser()
{
	var target = 'blockedUsers';
	var source = 'authorizedUsers';
	coreSelectToSelect(target, source);
}
/**
* Transfer an option from the blocked users selectbox to the authorized users selectbox
*/
function authUser()
{
	var target = 'authorizedUsers';
	var source = 'blockedUsers';
	coreSelectToSelect(target, source);
}
/**
* On submit, select all options in select boxes -- this allows PHP to pick up those options
*/
function selectAll()
{
	selectAllProcess('authorizedUsers');
	selectAllProcess('blockedUsers');
}
/**
* Select selected options
*/
function selectAllProcess(box)
{
	var element = box;
	var obj = coreGetElementById(element);
	if(obj == null)
		return;
	for(i = obj.options.length - 1; i >= 0; i--)
		obj.options[i].selected = true;
}