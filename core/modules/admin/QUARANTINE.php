<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
*	Quarantine and approve resources
*
*/
class QUARANTINE
{
private $db;
private $vars;
private $gatekeep;
private $badInput;
private $errors;
private $navigate;
private $success;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();
		$this->navigate = FACTORY_NAVIGATE::getInstance();
		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->gatekeep = FACTORY_GATEKEEP::getInstance();
		$this->gatekeep->requireSuper = TRUE;
	}
// Approve resource
	public function approve()
	{
		$this->gatekeep->init();
		if(!array_key_exists('resourceId', $this->vars))
			$this->badInput->close($this->errors->text("inputError", "missing"));
		$this->db->formatConditions(array('resourcemiscId' => $this->vars['resourceId']));
		$updateArray['resourcemiscQuarantine'] = 'N';
		$this->db->update('resource_misc', $updateArray);
		$this->navigate->resource($this->vars['resourceId'], $this->success->text("quarantineApprove"));
	}
// Quarantine resource
	public function putInQuarantine()
	{
		$this->gatekeep->init();
		if(!array_key_exists('resourceId', $this->vars))
			$this->badInput->close($this->errors->text("inputError", "missing"));
		$this->db->formatConditions(array('resourcemiscId' => $this->vars['resourceId']));
		$updateArray['resourcemiscQuarantine'] = 'Y';
		$this->db->update('resource_misc', $updateArray);
		$this->navigate->resource($this->vars['resourceId'], $this->success->text("quarantined"));
	}
/**
* Quickly check if there are any quarantined resources (used in MENU)
*/
	public function checkQuarantine()
	{
		$this->db->formatConditions(array('resourcemiscQuarantine' => 'Y'));
		$resultset = $this->db->select('resource_misc', 'resourcemiscId');
		$nbQuarantined = $this->db->numRows($resultset);
		return ($nbQuarantined > 0);
	}
}
?>