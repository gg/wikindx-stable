<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Convert DOC, DOCX and PDF files to plain text ready for searching.
*
* @version	1
*
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*	Code adapted from:
*		PHP DOC DOCX PDF to Text by Aditya Sarkar at www.phpclasses.org/package/8908-PHP-Convert-DOCX-DOC-PDF-to-plain-text.html
*		and
*		https://coderwall.com/p/x_n4tq/how-to-read-doc-using-php
*
*/
class FILETOTEXT
{
/** string */
private $fileName;
/** int */
var $multibyte = 4; // Use setUnicode(TRUE|FALSE)
/** string */
var $convertquotes = ENT_QUOTES; // ENT_COMPAT (double-quotes), ENT_QUOTES (Both), ENT_NOQUOTES (None)
/** boolean */
var $showprogress = TRUE; // TRUE if you have problems with time-out
/** string */
var $decodedtext = '';
/** array */
var $readFiles = array();

public function __construct()
{
	include_once('core/modules/list/PdfToText/PdfToText.phpclass');
}

// Check files in attachments/ have been cached (only PDF, DOC, DOCX)
public function checkCache()
{
	$db = FACTORY_DB::getInstance();
	$vars = GLOBALS::getVars();
	$session = FACTORY_SESSION::getInstance();
	$attachDir = \FACTORY_CONFIG::getInstance()->WIKINDX_ATTACHMENTS_DIR;
	$cacheDir = \FACTORY_CONFIG::getInstance()->WIKINDX_ATTACHMENTSCACHE_DIR;
	$mem = ini_get('memory_limit');
	$maxExecTime = ini_get('max_execution_time');
	ini_set('memory_limit', '-1'); // NB not always possible to set
	ini_set('max_execution_time', '-1'); // NB not always possible to set
// Turn error display off so that errors from PdfToText don't get written to screen (still written to the cache files)
	$errorDisplay = ini_get('display_errors');
	error_reporting(0);
	ini_set('display_errors', 'Off');
	if(array_key_exists('cacheCurl', $vars) && ($vars['cacheCurl'] == 'on'))
	{
		$session->setVar('cache_Curl', TRUE);
		if(function_exists('curl_multi_exec'))
		{
			$ch = array();
			$mh = curl_multi_init();
			$script = $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'];
			$curlExists = TRUE;
		}
		else
			$curlExists = FALSE;
	}
	else
	{
		$session->delVar('cache_Curl');
		$curlExists = FALSE;
	}
// Attempting to avoid timeouts if max execution time cannot be set. This is done on a trial and error basis.
	if(ini_get('memory_limit') == -1) // unlimited
	{
		$maxCount = FALSE;
		$maxSize = FALSE;
	}
	else if(ini_get('memory_limit') >= 129)
	{
		$maxCount = 30;
		$maxSize = 30000000; // 30MB
	}
	else if(ini_get('memory_limit') >= 65)
	{
		$maxCount = 20;
		$maxSize = 15000000; // 15MB
	}
	else
	{
		$maxCount = 10;
		$maxSize = 5000000; // 5MB
	}
	$input = FALSE;
	if(array_key_exists('cacheLimit', $vars))
	{
		$input = trim($vars['cacheLimit']);
		if(is_numeric($input) && is_int($input + 0)) // include cast to number
		{
			$maxCount = $input;
			$session->setVar('cache_Limit', $input);
		}
	}
	if(!$input)
		$session->delVar('cache_Limit');
	$count = 0;
	$size = 0;
	$mimeTypes = array(WIKINDX_MIMETYPE_PDF, WIKINDX_MIMETYPE_DOCX, WIKINDX_MIMETYPE_DOC);
	$db->formatConditionsOneField($mimeTypes, 'resourceattachmentsFileType');
	$resultset = $db->select('resource_attachments',
		array('resourceattachmentsResourceId', 'resourceattachmentsHashFilename', 'resourceattachmentsFileType', 'resourceattachmentsFileSize'));
	while($row = $db->fetchRow($resultset))
	{
		$f = $row['resourceattachmentsHashFilename'];
		$fileName = $attachDir . DIRECTORY_SEPARATOR . $f;
		$fileNameCache = $cacheDir . DIRECTORY_SEPARATOR . $f;
		if(!file_exists($fileName) || (file_exists($fileNameCache) && filemtime($fileNameCache) > filemtime($fileName)))
			continue; // already cached
		else if($curlExists)
		{
			$curlTarget = $script . '?' .
				'action=curl_CURL_CORE' .
				'&method=attachmentCache' .
				'&file=' . urlencode($fileName) .
				'&fileType=' . urlencode($row['resourceattachmentsFileType']) .
				'&id=' . $f;
			$ch_x = curl_init($curlTarget);
			$ch[$row['resourceattachmentsHashFilename']] = $ch_x;
			curl_setopt($ch_x, CURLOPT_RETURNTRANSFER, true);
			// Get the headers too
			curl_setopt($ch_x, CURLOPT_HEADER, true);
			curl_setopt($ch_x, CURLOPT_TIMEOUT, ini_get('max_execution_time'));
			curl_multi_add_handle($mh, $ch_x);
		}
		else
		{
			try
			{
				file_put_contents($fileNameCache, $this->convertToText($fileName, $row['resourceattachmentsFileType']));
			}
			catch (Exception $e)
			{
				file_put_contents($fileNameCache, '');
			}
		}
		++$count;
		$size += $row['resourceattachmentsFileSize'];
		if($maxCount)
		{
			if($count >= $maxCount)
				break;
		}
		if($maxSize)
		{
			if($size >= $maxSize)
				break;
		}
	}
	if($curlExists)
	{
		$running = null;
		do
		{
			curl_multi_exec($mh, $running);
		}
		while($running);
		foreach($ch as $ch_x)
		{
			$return = curl_multi_getcontent($ch_x);
			curl_multi_remove_handle($mh, $ch_x);
			curl_close($ch_x);

			// Identify the file parsed with its custom header 'resourceattachmentsHashFilename'
			// This is mandatory because the output of PdfToText could be altered at byte level
			$split = UTF8::mb_explode("\r\n\r\n", $return, 2);
			if(sizeof($split) == 2)
			{
				$headers = $split[0];
				$body = $split[1];

				// Split headers / body
				$headers = UTF8::mb_explode("\r\n", $headers);
				foreach ($headers as $h)
				{
					// Split each header in key / value
					$h = UTF8::mb_explode(":", $h);
					if(sizeof($split) == 2)
					{
						// Identify the file parsed
						if($h[0] == 'resourceattachmentsHashFilename')
						{
							$texts[trim($h[1])] = trim($body);
							try
							{
								file_put_contents($cacheDir . DIRECTORY_SEPARATOR . trim($h[1]), $texts[trim($h[1])]);
							}
							catch (Exception $e)
							{
								file_put_contents($cacheDir . DIRECTORY_SEPARATOR . trim($h[1]), '');
							}
						}
					}
				}
			}
		}
		curl_multi_close($mh);
	}
	$cacheDirFiles = scanDir($cacheDir);
	foreach($cacheDirFiles AS $key => $value)
	{
		if(($value == 'index.html') || ($value == 'ATTACHMENTS_CACHE') || (strpos($value, '.') === 0))
			unset($cacheDirFiles[$key]);
	}
	$session->setVar('cache_Attachments', sizeOf($cacheDirFiles));
	ini_set('display_errors', $errorDisplay);
	if(\FACTORY_CONFIG::getInstance()->WIKINDX_DEBUG_ERRORS)
		error_reporting(E_ALL);
	ini_set('memory_limit', $mem);
	ini_set('max_execution_time', $maxExecTime);
	include_once("core/startup/HOUSEKEEPING.php");
	$hk = new HOUSEKEEPING(FALSE);
}

public function convertToText($filename, $mimeType)
{
	$this->fileName = $filename;
	if(isset($this->fileName) && !file_exists($this->fileName))
		return FALSE;
	if(array_key_exists($this->fileName, $this->readFiles))
		return $this->readFiles[$this->fileName];
	if($mimeType == WIKINDX_MIMETYPE_DOC)
		$text = $this->readWord();
	else if($mimeType == WIKINDX_MIMETYPE_DOCX)
		$text = $this->read_docx();
    else if($mimeType == WIKINDX_MIMETYPE_PDF)
    {
		$importPDF = new PdfToText($this->fileName, PdfToText::PDFOPT_NO_HYPHENATED_WORDS);
		if($importPDF->Text)
		{
			$this->readFiles[$this->fileName] = $importPDF->Text;
			return $importPDF->Text;
		}
    }
    if($text)
    {
		$this->readFiles[$this->fileName] = $text;
    	return $text;
    }
    else
		return FALSE;
}
private function readWord()
{
  if(file_exists($this->fileName))
  {
      if(($fh = fopen($this->fileName, 'r')) !== false )
      {
         $headers = fread($fh, 0xA00);

         // 1 = (ord(n)*1) ; Document has from 0 to 255 characters
         $n1 = ( ord($headers[0x21C]) - 1 );

         // 1 = ((ord(n)-8)*256) ; Document has from 256 to 63743 characters
         $n2 = ( ( ord($headers[0x21D]) - 8 ) * 256 );

         // 1 = ((ord(n)*256)*256) ; Document has from 63744 to 16775423 characters
         $n3 = ( ( ord($headers[0x21E]) * 256 ) * 256 );

         // 1 = (((ord(n)*256)*256)*256) ; Document has from 16775424 to 4294965504 characters
         $n4 = ( ( ( ord($headers[0x21F]) * 256 ) * 256 ) * 256 );

         // Total length of text in the document
         $textLength = ($n1 + $n2 + $n3 + $n4);
		 if($textLength <= 0)
		 	return FALSE;
         $extracted_plaintext = fread($fh, $textLength);
		fclose($fh);
         return utf8_encode($extracted_plaintext);
      } else {
        return false;
      }
  } else {
    return false;
  }
}
private function read_docx()
{
	$striped_content = '';
	$content = '';
	$zip = zip_open($this->fileName);
	if (!$zip || is_numeric($zip))
		return false;
	while ($zip_entry = zip_read($zip))
	{
		if (zip_entry_open($zip, $zip_entry) == FALSE)
			continue;
		if (zip_entry_name($zip_entry) != "word/document.xml")
			continue;
		$content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
		zip_entry_close($zip_entry);
	}
	zip_close($zip);
	unset($zip);
	$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
	$content = str_replace('</w:r></w:p>', CR.LF, $content);
	$striped_content = strip_tags($content);
	return $striped_content;
}

private function setFilename($filename)
{
	$this->decodedtext = '';
	$this->fileName = $filename;
}

private function output($echo = false)
{
	if($echo)
		echo $this->decodedtext;
	else
		return $this->decodedtext;
}

private function setUnicode($input)
{
	if($input == true)
		$this->multibyte = 4;
	else
		$this->multibyte = 2;
}

private function decodePDF()
{
	$infile = @file_get_contents($this->fileName, FILE_BINARY);
	if (empty($infile))
		return "";
	$transformations = array();
	$texts = array();
	preg_match_all("#obj[\n|\r](.*)endobj[\n|\r]#ismuU", $infile . "endobj" . CR, $objects);
	$objects = @$objects[1];
	for ($i = 0; $i < count($objects); $i++)
	{
		$currentObject = $objects[$i];
		@set_time_limit ();
		if($this->showprogress)
		{
			flush();
			ob_flush();
		}
		if (preg_match("#stream[\n|\r](.*)endstream[\n|\r]#ismuU", $currentObject . "endstream" . CR, $stream ))
		{
			$stream = ltrim($stream[1]);
			$options = $this->getObjectOptions($currentObject);
			if (!(empty($options["Length1"]) && empty($options["Type"]) && empty($options["Subtype"])) )
				continue;
			unset($options["Length"]);
			$data = $this->getDecodedStream($stream, $options);
			if (mb_strlen($data))
			{
				if (preg_match_all("#BT[\n|\r](.*)ET[\n|\r]#ismuU", $data . "ET" . CR, $textContainers))
				{
					$textContainers = @$textContainers[1];
					$this->getDirtyTexts($texts, $textContainers);
				}
				else
					$this->getCharTransformations($transformations, $data);
			}
		}
	}
	$this->decodedtext = $this->getTextUsingTransformations($texts, $transformations);
}

private function decodeAsciiHex($input)
{
	$output = "";
	$isOdd = true;
	$isComment = false;
	for($i = 0, $codeHigh = -1; $i < mb_strlen($input) && $input[$i] != '>'; $i++)
	{
		$c = $input[$i];
		if($isComment)
		{
			if ($c == '\r' || $c == '\n')
				$isComment = false;
			continue;
		}
		switch($c)
		{
			case '\0': case '\t': case '\r': case '\f': case '\n': case ' ': break;
			case '%':
				$isComment = true;
			break;
			default:
				$code = hexdec($c);
				if($code === 0 && $c != '0')
					return "";
				if($isOdd)
					$codeHigh = $code;
				else
					$output .= chr($codeHigh * 16 + $code);
				$isOdd = !$isOdd;
			break;
		}
	}
	if($input[$i] != '>')
		return "";
	if($isOdd)
		$output .= chr($codeHigh * 16);
	return $output;
}

private function decodeAscii85($input)
{
	$output = "";
	$isComment = false;
	$ords = array();
	for($i = 0, $state = 0; $i < mb_strlen($input) && $input[$i] != '~'; $i++)
	{
		$c = $input[$i];
		if($isComment)
		{
			if ($c == '\r' || $c == '\n')
				$isComment = false;
			continue;
		}
		if (($c == '\0') || ($c == '\t') || ($c == '\r') || ($c == '\f') || ($c == '\n') || ($c == ' '))
			continue;
		if ($c == '%')
		{
			$isComment = true;
			continue;
		}
		if ($c == 'z' && $state === 0)
		{
			$output .= str_repeat(chr(0), 4);
			continue;
		}
		if ($c < '!' || $c > 'u')
			return "";
		$code = ord($input[$i]) & 0xff;
		$ords[$state++] = $code - ord('!');
		if ($state == 5)
		{
			$state = 0;
			for ($sum = 0, $j = 0; $j < 5; $j++)
				$sum = $sum * 85 + $ords[$j];
			for ($j = 3; $j >= 0; $j--)
				$output .= chr($sum >> ($j * 8));
		}
	}
	if ($state === 1)
		return "";
	elseif ($state > 1)
	{
		for ($i = 0, $sum = 0; $i < $state; $i++)
			$sum += ($ords[$i] + ($i == $state - 1)) * pow(85, 4 - $i);
		for ($i = 0; $i < $state - 1; $i++)
		{
			try
			{
				if(false == ($o = chr($sum >> ((3 - $i) * 8))))
					throw new Exception('Error');
				$output .= $o;
			}
			catch (Exception $e)
				{ /*Dont do anything*/ }
		}
	}
	return $output;
}

private function decodeFlate($data)
{
	return @gzuncompress($data);
}

private function getObjectOptions($object)
{
	$options = array();
	if (preg_match("#<<(.*)>>#ismuU", $object, $options))
	{
		$options = UTF8::mb_explode("/", $options[1]);
		@array_shift($options);
		$o = array();
		for($j = 0; $j < @count($options); $j++)
		{
			$options[$j] = preg_replace("#\s+#u", " ", trim($options[$j]));
			if(mb_strpos($options[$j], " ") !== false)
			{
				$parts = UTF8::mb_explode(" ", $options[$j]);
				$o[$parts[0]] = $parts[1];
			}
			else
				$o[$options[$j]] = true;
		}
		$options = $o;
		unset($o);
	}
	return $options;
}

private function getDecodedStream($stream, $options)
{
	$data = "";
	if (empty($options["Filter"]))
		$data = $stream;
	else
	{
		$length = !empty($options["Length"]) ? $options["Length"] : mb_strlen($stream);
		$_stream = mb_substr($stream, 0, $length);

		foreach ($options as $key => $value)
		{
			if ($key == "ASCIIHexDecode")
				$_stream = $this->decodeAsciiHex($_stream);
			elseif ($key == "ASCII85Decode")
				$_stream = $this->decodeAscii85($_stream);
			elseif ($key == "FlateDecode")
				$_stream = $this->decodeFlate($_stream);
			elseif ($key == "Crypt")
			{ // TO DO
			}
		}
		$data = $_stream;
	}
	return $data;
}

private function getDirtyTexts(&$texts, $textContainers)
{
	for ($j = 0; $j < count($textContainers); $j++)
	{
		if (preg_match_all("#\[(.*)\]\s*TJ[\n|\r]#ismuU", $textContainers[$j], $parts))
			$texts = array_merge($texts, array(@implode('', $parts[1])));
		elseif (preg_match_all("#T[d|w|m|f]\s*(\(.*\))\s*Tj[\n|\r]#ismuU", $textContainers[$j], $parts))
			$texts = array_merge($texts, array(@implode('', $parts[1])));
		elseif (preg_match_all("#T[d|w|m|f]\s*(\[.*\])\s*Tj[\n|\r]#ismuU", $textContainers[$j], $parts))
			$texts = array_merge($texts, array(@implode('', $parts[1])));
	}
}

private function getCharTransformations(&$transformations, $stream)
{
	preg_match_all("#([0-9]+)\s+beginbfchar(.*)endbfchar#ismuU", $stream, $chars, PREG_SET_ORDER);
	preg_match_all("#([0-9]+)\s+beginbfrange(.*)endbfrange#ismuU", $stream, $ranges, PREG_SET_ORDER);
	for ($j = 0; $j < count($chars); $j++)
	{
		$count = $chars[$j][1];
		$current = UTF8::mb_explode("\n", trim($chars[$j][2]));
		for ($k = 0; $k < $count && $k < count($current); $k++)
		{
			if (preg_match("#<([0-9a-f]{2,4})>\s+<([0-9a-f]{4,512})>#uis", trim($current[$k]), $map))
				$transformations[UTF8::mb_str_pad($map[1], 4, "0")] = $map[2];
		}
	}
	for ($j = 0; $j < count($ranges); $j++)
	{
		$count = $ranges[$j][1];
		$current = UTF8::mb_explode("\n", trim($ranges[$j][2]));
		for ($k = 0; $k < $count && $k < count($current); $k++)
		{
			if (preg_match("#<([0-9a-f]{4})>\s+<([0-9a-f]{4})>\s+<([0-9a-f]{4})>#uis", trim($current[$k]), $map))
			{
				$from = hexdec($map[1]);
				$to = hexdec($map[2]);
				$_from = hexdec($map[3]);
				for ($m = $from, $n = 0; $m <= $to; $m++, $n++)
					$transformations[sprintf("%04X", $m)] = sprintf("%04X", $_from + $n);
			}
			elseif (preg_match("#<([0-9a-f]{4})>\s+<([0-9a-f]{4})>\s+\[(.*)\]#ismuU", trim($current[$k]), $map))
			{
				$from = hexdec($map[1]);
				$to = hexdec($map[2]);
				$parts = preg_split("#\s+#u", trim($map[3]));

				for ($m = $from, $n = 0; $m <= $to && $n < count($parts); $m++, $n++)
					$transformations[sprintf("%04X", $m)] = sprintf("%04X", hexdec($parts[$n]));
			}
		}
	}
}

private function getTextUsingTransformations($texts, $transformations)
{
	$document = "";
	for ($i = 0; $i < count($texts); $i++)
	{
		$isHex = false;
		$isPlain = false;
		$hex = "";
		$plain = "";
		for ($j = 0; $j < mb_strlen($texts[$i]); $j++)
		{
			$c = $texts[$i][$j];
			switch($c)
			{
				case "<":
					$hex = "";
					$isHex = true;
					$isPlain = false;
				break;
				case ">":
					$hexs = str_split($hex, $this->multibyte); // 2 or 4 (UTF8 or ISO)
					for ($k = 0; $k < count($hexs); $k++)
					{

						$chex = UTF8::mb_str_pad($hexs[$k], 4, "0"); // Add tailing zero
						if (isset($transformations[$chex]))
							$chex = $transformations[$chex];
						$document .= html_entity_decode("&#x".$chex.";");
					}
					$isHex = false;
				break;
				case "(":
					$plain = "";
					$isPlain = true;
					$isHex = false;
				break;
				case ")":
					$document .= $plain;
					$isPlain = false;
				break;
				case "\\":
					$c2 = $texts[$i][$j + 1];
					if (in_array($c2, array("\\", "(", ")")))
						$plain .= $c2;
					elseif ($c2 == "n")
						$plain .= '\n';
					elseif ($c2 == "r")
						$plain .= '\r';
					elseif ($c2 == "t")
						$plain .= '\t';
					elseif ($c2 == "b")
						$plain .= '\b';
					elseif ($c2 == "f")
						$plain .= '\f';
					elseif ($c2 >= '0' && $c2 <= '9')
					{
						$oct = preg_replace("#[^0-9]#u", "", mb_substr($texts[$i], $j + 1, 3));
						$j += mb_strlen($oct) - 1;
						$plain .= html_entity_decode("&#".octdec($oct).";", $this->convertquotes);
					}
					$j++;
				break;
				default:
					if ($isHex)
						$hex .= $c;
					elseif ($isPlain)
						$plain .= $c;
				break;
			}
		}
		$document .= "\n";
	}
	return $document;
}
}