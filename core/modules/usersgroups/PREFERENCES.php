<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* PREFERENCES class
*
* User preferences
*/
class PREFERENCES
{
private $errors;
private $messages;
private $success;
private $session;
private $vars;
private $badInput;

	public function __construct()
	{
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->badInput = FACTORY_BADINPUT::getInstance();


		GLOBALS::setTplVar('heading', $this->messages->text("heading", "configure"));
	}
// display options
	public function init($error = FALSE)
	{
		include_once("core/modules/help/HELPMESSAGES.php");
		$help = new HELPMESSAGES();
		GLOBALS::setTplVar('help', $help->createLink('preferences'));
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "configure"));
		$pString = \FORM\formHeader('usersgroups_PREFERENCES_CORE');
		$pString .= \FORM\hidden("method", "write");
		if($error)
		{
			$pString .= $error;
// As language may have changed, we create a new MESSAGES and SUCCESS object.
			$this->messages = new MESSAGES();
			GLOBALS::setTplVar('heading', $this->messages->text("heading", "configure"));
		}
		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("config", "paging"), "Paging",
			$this->session->getVar("setup_Paging"), 5) . " " . \HTML\span('*', 'required') . BR .
			\HTML\span($this->messages->text("hint", "pagingLimit"), 'hint'));
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("config", "maxPaging"), "PagingMaxLinks",
			$this->session->getVar("setup_PagingMaxLinks"), 5) . " " . \HTML\span('*', 'required'));
		if(!$this->session->getVar("setup_PagingTagCloud"))
			$this->session->setVar("setup_PagingTagCloud", 100);
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("config", "pagingTagCloud"), "PagingTagCloud",
			$this->session->getVar("setup_PagingTagCloud"), 5) . " " . \HTML\span('*', 'required') .
			BR . \HTML\span($this->messages->text("hint", "pagingLimit"), 'hint'));
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("config", "stringLimit"), "StringLimit",
			$this->session->getVar("setup_StringLimit"), 5) . " " . \HTML\span('*', 'required') . BR .
			\HTML\span($this->messages->text("hint", "pagingLimit"), 'hint'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();

		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();
		$input = $this->session->getVar("setup_PagingStyle") == 'A' ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "pagingStyle"),
			"PagingStyle", $input));
		$input = $this->session->getVar("setup_UseWikindxKey") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "useWikindxKey"),
			"UseWikindxKey", $input));
		$input = $this->session->getVar("setup_UseBibtexKey") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "useBibtexKey"),
			"UseBibtexKey", $input));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();

		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();
		$input = $this->session->getVar("setup_DisplayBibtexLink") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "displayBibtexLink"),
			"DisplayBibtexLink", $input));
		$input = $this->session->getVar("setup_DisplayCmsLink") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "displayCmsLink"),
			"DisplayCmsLink", $input));
		$input = $this->session->getVar("setup_Listlink") ? "CHECKED" : FALSE;
		$pString .= \HTML\td(\FORM\checkbox($this->messages->text("config", "listlink"), "Listlink", $input));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();

		$pString .= \HTML\tableStart('generalTable borderStyleSolid left');
		$pString .= \HTML\trStart();

		$templates = FACTORY_TEMPLATE::getInstance()->loadDir();
		$subTd = \HTML\tableStart();
		$subTd .= \HTML\trStart();
		if($this->session->getVar("setup_Template") && array_key_exists($this->session->getVar("setup_Template"), $templates))
			$subTd .= \HTML\td(\FORM\selectedBoxValue($this->messages->text("config", "template"),
				"Template", $templates, $this->session->getVar("setup_Template"), 4)
				 . " " . \HTML\span('*', 'required'));
		else
			$subTd .= \HTML\td(\FORM\selectFBoxValue($this->messages->text("config", "template"),
				"Template", $templates, 4)

				 . " " . \HTML\span('*', 'required'));
		$menus[0] = $this->messages->text("config", "templateMenu1");
		$menus[1] = $this->messages->text("config", "templateMenu2");
		$menus[2] = $this->messages->text("config", "templateMenu3");
		$subTd .= \HTML\td(\FORM\selectedBoxValue($this->messages->text("config", "templateMenu"),
			"TemplateMenu", $menus, $this->session->getVar("setup_TemplateMenu"), 3)
			 . " " . \HTML\span('*', 'required'));
		$subTd .= \HTML\trEnd();
		$subTd .= \HTML\tableEnd();
		$pString .= \HTML\td($subTd);
		$language = FACTORY_MESSAGES::getInstance();
		$languages = $language->loadDir();
		if($this->session->getVar("setup_Language") && array_key_exists($this->session->getVar("setup_Language"), $languages))
			$pString .= \HTML\td(\FORM\selectedBoxValue($this->messages->text("config", "language"),
				"Language", $languages, $this->session->getVar("setup_Language"))
				 . " " . \HTML\span('*', 'required'));
		else
			$pString .= \HTML\td(\FORM\selectFBoxValue($this->messages->text("config", "language"),
				"Language", $languages) . " " . \HTML\span('*', 'required'));

		$styles = \LOADSTYLE\loadDir();
		if($this->session->getVar("setup_Style") && array_key_exists($this->session->getVar("setup_Style"), $styles))
			$pString .= \HTML\td(\FORM\selectedBoxValue($this->messages->text("config", "style") , "Style",
				$styles, $this->session->getVar("setup_Style"), 4) . " " . \HTML\span('*', 'required'));
		else
			$pString .= \HTML\td(\FORM\selectFBoxValue($this->messages->text("config", "style") , "Style",
				$styles, 4) . " " . \HTML\span('*', 'required'));

		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();

		$pString .= \HTML\p(\FORM\formSubmit());
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// write preferences
	function write()
	{
// checkInput writes the session
		$this->checkInput();
// If this is a logged on user, write preferences to WKX_user_preferences
		if($userId = $this->session->getVar("setup_UserId"))
		{
			$user = FACTORY_USER::getInstance();
			$user->writePreferences($userId);
		}
// As language may have changed, we destroy the existing one then create new objects
		FACTORY_MESSAGES::getFreshInstance();
		FACTORY_ERRORS::getFreshInstance();
		FACTORY_HELP::getFreshInstance();
		$this->success = FACTORY_SUCCESS::getFreshInstance();
		$this->init($this->success->text("config"));
	}
// Check input
	function checkInput()
	{
		$required = array("Language", "Template", "Style", "Paging", "PagingMaxLinks", "StringLimit", "PagingTagCloud");
		foreach($required as $value)
		{
			if(!array_key_exists($value, $this->vars) || !$this->vars[$value])
				$this->badInput->close($this->errors->text("inputError", "missing", " ($value) "), $this, 'init');
			else
			{
				if(($value == 'PagingTagCloud') || ($value == 'Paging') || ($value == 'StringLimit'))
				{
					if($this->vars[$value] < -1)
						$this->vars[$value] = -1;
				}
				$array[$value] = $this->vars[$value];
			}
		}
		if(!array_key_exists("TemplateMenu", $this->vars))
			$this->badInput->close($this->errors->text("inputError", "missing", " (TemplateMenu) "), $this, 'init');
		else
			$array['TemplateMenu'] = $this->vars['TemplateMenu'];
		if(!is_numeric($this->vars['Paging']))
			$this->badInput->close($this->errors->text("inputError", "nan", " (paging) "), $this, 'init');
		if(!is_numeric($this->vars['PagingTagCloud']))
			$this->badInput->close($this->errors->text("inputError", "nan", " (paging) "), $this, 'init');
		if(!is_numeric($this->vars['PagingMaxLinks']))
			$this->badInput->close($this->errors->text("inputError", "nan", " (pagingMaxLinks) "), $this, 'init');
		if(!is_numeric($this->vars['StringLimit']))
			$this->badInput->close($this->errors->text("inputError", "nan", " (stringLimit) "), $this, 'init');
// All input good - write to session
		$this->session->writeArray($array, "setup");
		if(array_key_exists("PagingStyle", $this->vars))
			$this->session->setVar("setup_PagingStyle", 'A');
		else
			$this->session->delVar("setup_PagingStyle");
		$this->session->delVar("sql_LastMulti"); // always reset in case of paging changes
		$this->session->delVar("sql_LastIdeaSearch"); // always reset in case of paging changes
		if(array_key_exists("UseWikindxKey", $this->vars))
			$this->session->setVar("setup_UseWikindxKey", TRUE);
		else
			$this->session->delVar("setup_UseWikindxKey");
		if(array_key_exists("UseBibtexKey", $this->vars))
			$this->session->setVar("setup_UseBibtexKey", TRUE);
		else
			$this->session->delVar("setup_UseBibtexKey");
		if(array_key_exists("DisplayBibtexLink", $this->vars))
			$this->session->setVar("setup_DisplayBibtexLink", TRUE);
		else
			$this->session->delVar("setup_DisplayBibtexLink");
		if(array_key_exists("DisplayCmsLink", $this->vars))
			$this->session->setVar("setup_DisplayCmsLink", TRUE);
		else
			$this->session->delVar("setup_DisplayCmsLink");
		if(array_key_exists("Listlink", $this->vars))
			$this->session->setVar("setup_Listlink", TRUE);
		else
			$this->session->delVar("setup_Listlink");
	}
}
?>