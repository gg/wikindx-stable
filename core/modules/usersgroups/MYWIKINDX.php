<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */


/**
* MYWIKINDX class
*/
class MYWIKINDX
{
private $db;
private $vars;
private $errors;
private $messages;
private $success;
private $session;
private $gatekeep;
private $config;
private $badInput;
private $user;
private $bib;
private $groupBib = FALSE;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();
		$this->session = FACTORY_SESSION::getInstance();


		$this->gatekeep = FACTORY_GATEKEEP::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->user = FACTORY_USER::getInstance();
		$this->bib = FACTORY_BIBLIOGRAPHYCOMMON::getInstance();
		$this->gatekeep->init();
	}
	public function init($message = FALSE)
	{
		include_once("core/modules/help/HELPMESSAGES.php");
		$help = new HELPMESSAGES();
		GLOBALS::setTplVar('help', $help->createLink('myWikindx'));
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx"));
		include_once('core/modules/usersgroups/FORGET.php');
		$forget = new FORGET();
		$pString = $message ? $message : '';
		$this->loadSession();
/**
* For a test user (see config.php)
*/
		if($this->session->getVar('setup_UserId') != $this->config->WIKINDX_RESTRICT_USERID)
		{
			$pString .= $this->user->displayUserDetails('usersgroups_MYWIKINDX_CORE', 'editUser');
			$pString .= BR . \HTML\hr() . BR;
			if($this->session->issetVar("mywikindx_Email") && $this->config->WIKINDX_MAIL_SERVER)
			{
				$pString .= $forget->forgetSet();
				$pString .= BR . \HTML\hr() . BR;
			}
// in case superadmin has not entered email yet or notification is disabled
			if($this->session->issetVar("mywikindx_Email") && $this->session->issetVar("setup_Notify") &&
				$this->config->WIKINDX_MAIL_SERVER)
			{
				$pString .= $this->displayNotify();
				$pString .= BR . \HTML\hr() . BR;
			}
		}
		$pString .= $this->listUserGroups();
		$pString .= BR . \HTML\hr() . BR;
		$pString .= $this->listBibliographies();
		$pString .= BR . \HTML\hr() . BR;
		$pString .= $this->listUserTags();
		GLOBALS::addTplVar('content', $pString);
	}
// Select box of user bibliographies
	private function listBibliographies()
	{
// Get this user's bibliographies
		if($this->session->getVar('mywikindx_Bibliographies'))
		{
			$bibsRaw = unserialize($this->session->getVar("mywikindx_Bibliographies"));
			foreach($bibsRaw as $key => $value)
				$bibsU[$key] = \HTML\dbToFormTidy($value);
		}
// Get this user's user group bibliographies
		if($this->session->getVar('mywikindx_Groupbibliographies'))
		{
			$bibsRaw = unserialize($this->session->getVar("mywikindx_Groupbibliographies"));
			foreach($bibsRaw as $key => $value)
				$bibsUG[$key] = \HTML\dbToFormTidy($value);
		}
		$bibsU = $this->bib->getUserBibs();
		$bibsUG = $this->bib->getGroupBibs();
		if(!empty($bibsU))
		{
			$bibsArray[-1] = $this->messages->text('user', 'userBibs');
			foreach($bibsU as $key => $value)
				$bibsArray[$key] = $value;
		}
		if(!empty($bibsUG))
		{
			$bibsArray[-2] = $this->messages->text('user', 'userGroupBibs');
			foreach($bibsUG as $key => $value)
				$bibsArray[$key] = $value;
		}
		$pString = \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \HTML\p(\HTML\strong($this->messages->text("user", 'bib')));
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		if(!isset($bibsArray))
		{
			$pString .=  \HTML\td($this->messages->text("user", "noBibs"));
			$radios = \HTML\p(\FORM\radioButton(FALSE, "method", "createBibInit", TRUE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "createBib"), FALSE, "left");
		}
		else
		{
			$pString .= \HTML\td(\FORM\selectFBoxValue(FALSE, "bibId", $bibsArray, 5));
			$radios = \HTML\p(\FORM\radioButton(FALSE, "method", "createBibInit", TRUE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "createBib"), FALSE, "left");
			$radios .= \HTML\p(\FORM\radioButton(FALSE, "method", "createGroupBibInit", FALSE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "createGroupBib"), FALSE, "left");
			$radios .= \HTML\p(\FORM\radioButton(FALSE, "method", "editBibInit", FALSE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "editBib"), FALSE, "left");
			$radios .= \HTML\p(\FORM\radioButton(FALSE, "method", "deleteBibInit", FALSE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "deleteBib"), FALSE, "left");
		}
		$pString .= \HTML\td($radios);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \HTML\p(\FORM\formSubmit("Proceed"));
		$pString .= \FORM\formEnd();
		return $pString;
	}
// Select box of user tags
	private function listUserTags()
	{
// Get this user's user tags
		$userTagsObject = FACTORY_USERTAGS::getInstance();
		$userTags = $userTagsObject->grabAll();
		$pString = \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \HTML\p(\HTML\strong($this->messages->text("user", 'userTags')));
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		if(empty($userTags))
		{
			$pString .=  \HTML\td($this->messages->text("user", "noUserTags"));
			$radios = \HTML\p(\FORM\radioButton(FALSE, "method", "createUserTagInit", TRUE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "createUserTag"), FALSE, "left");
		}
		else
		{
			$pString .= \HTML\td(\FORM\selectFBoxValue(FALSE, "userTagId", $userTags, 5));
			$radios = \HTML\p(\FORM\radioButton(FALSE, "method", "createUserTagInit", TRUE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "createUserTag"), FALSE, "left");
			$radios .= \HTML\p(\FORM\radioButton(FALSE, "method", "editUserTagInit", FALSE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "editUserTag"), FALSE, "left");
			$radios .= \HTML\p(\FORM\radioButton(FALSE, "method", "deleteUserTagInit", FALSE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "deleteUserTag"), FALSE, "left");
		}
		$pString .= \HTML\td($radios);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \HTML\p(\FORM\formSubmit("Proceed"));
		$pString .= \FORM\formEnd();
		return $pString;
	}
// Edit user details
	public function editUser()
	{
		if(!trim($this->vars['password']) || !trim($this->vars['passwordConfirm']))
			$this->badInput->close($this->init($this->errors->text("inputError", "missing")));
		if(trim($this->vars['password']) != trim($this->vars['passwordConfirm']))
			$this->badInput->close($this->init($this->errors->text("inputError", "invalid")));
		if((!array_key_exists('email', $this->vars) || !trim($this->vars['email'])))
			$this->badInput->close($this->init($this->errors->text("inputError", "missing")));
		$this->user->writeUser(FALSE); // FALSE = editing user
		include_once("core/modules/email/EMAIL.php");
		$emailClass = new EMAIL();
		if(!$emailClass->userEdit())
			$this->badInput->close($this->errors->text("inputError", "mail", GLOBALS::getError()));
		$this->init($this->success->text("userEdit"));
		FACTORY_CLOSE::getInstance();
	}
// List usergroups
	private function listUserGroups()
	{
		$pString = \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \HTML\p(\HTML\strong($this->messages->text("user", "groups")));
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		if(!$groups = $this->user->listUserGroups())
		{
			$pString .=  \HTML\td($this->messages->text("user", "noGroups"));
			$radios = \HTML\p(\FORM\radioButton(FALSE, "method", "createUserGroupInit", TRUE) . "&nbsp;&nbsp;" .
				$this->messages->text("user", "createGroup"), FALSE, "left");
		}
		else
		{
			$pString .= \HTML\td(\FORM\selectFBoxValue(FALSE, "groupId", $groups, 5));
			if(!$this->session->getVar('mywikindx_group_radio'))
				$checked = TRUE;
			else
				$checked = $this->session->getVar('mywikindx_group_radio') == 'create' ? TRUE : FALSE;
			$radios = \HTML\p(\FORM\radioButton(FALSE, "method", "createUserGroupInit", $checked) .
				"&nbsp;&nbsp;" . $this->messages->text("user", "createGroup"), FALSE, "left");
			$checked = $this->session->getVar('mywikindx_group_radio') == 'edit' ? TRUE : FALSE;
			$radios .= \HTML\p(\FORM\radioButton(FALSE, "method", "editUserGroupInit", $checked) .
				"&nbsp;&nbsp;" . $this->messages->text("user", "editGroup"), FALSE, "left");
			$checked = $this->session->getVar('mywikindx_group_radio') == 'delete' ? TRUE : FALSE;
			$radios .= \HTML\p(\FORM\radioButton(FALSE, "method", "deleteUserGroupInit", $checked) .
				"&nbsp;&nbsp;" . $this->messages->text("user", "deleteGroup"), FALSE, "left");
		}
		$pString .= \HTML\td($radios);
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \HTML\p(\FORM\formSubmit("Proceed"));
		$pString .= \FORM\formEnd();
		return $pString;
	}
/**
* Display options for adding a user group
*/
	public function createUserGroupInit($error = FALSE)
	{
		$tinymce = FACTORY_LOADTINYMCE::getInstance();
		$this->session->delVar('mywikindx_group_add');
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "createGroup")));
		$this->session->delVar("mywikindx_groupLock");
		$pString = $error ? \HTML\p($error, "error", "center") : '';
		$pString .= \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "createUserGroup");
		$pString .= $tinymce->loadBasicTextarea();
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$sessVar = $this->session->issetVar("mywikindx_groupTitle") ?
			\HTML\dbToFormTidy($this->session->getVar("mywikindx_groupTitle")) : FALSE;
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "groupTitle"),
			"title", $sessVar, 50, 255) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= BR . "&nbsp;" . BR;
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$sessVar = $this->session->issetVar("mywikindx_groupDescription") ?
			\HTML\dbToFormTidy($this->session->getVar("mywikindx_groupDescription")) : FALSE;
		$pString .= \HTML\td(\FORM\textAreaInput($this->messages->text("user", "groupDescription"),
			"description", $sessVar, 80, 10));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$users = $this->user->grabAll(TRUE);
// add 0 => IGNORE to $array
		$temp[0] = $this->messages->text("misc", "ignore");
		foreach($users as $key => $value)
		{
			if($key == $this->session->getVar('setup_UserId'))
				continue;
			$temp[$key] = $value;
		}
		$pString .= \HTML\p(\FORM\selectFBoxValueMultiple($this->messages->text("user", "groupUserAdd"),
			"users", $temp, 10) . BR . $this->messages->text("hint", "multiples"));
		$pString .= \HTML\p(\FORM\formSubmit("Add"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
/**
* create a user group
*/
	public function createUserGroup()
	{
		if(array_key_exists('description', $this->vars) && trim($this->vars['description']))
		{
			$this->session->setVar('mywikindx_groupDescription', $this->vars['description']);
			$fields[] = 'usergroupsDescription';
			$values[] = $this->vars['description'];
		}
		if(!$title = trim($this->vars['title']))
			$this->badInput->close($this->createUserGroupInit($this->errors->text("inputError", "missing")));
		$this->session->setVar('mywikindx_groupTitle', $title);
		if($this->session->getVar("mywikindx_groupLock"))
			$this->badInput->close($this->init($this->errors->text("done", "group")));
		$this->checkUserGroupExists($title);
		$userId = $this->session->getVar('setup_UserId');
		$fields[] = 'usergroupsTitle';
		$values[] = $title;
		$fields[] = 'usergroupsAdminId';
		$values[] = $userId;
		$this->db->insert('user_groups', $fields, $values);
		$groupId = $this->db->lastAutoId();
		$userIds[] = $userId;
		foreach($this->vars['users'] as $userId)
		{
			if(!$userId) // IGNORE
				continue;
			$userIds[] = $userId;
		}
// Insert new users
		foreach($userIds as $id)
			$this->db->insert('user_groups_users', array('usergroupsusersUserId', 'usergroupsusersGroupId'),
				array($id, $groupId));
		$this->session->setVar("mywikindx_groupLock", TRUE);
		$this->session->delVar('mywikindx_groupDescription');
		$this->session->delVar('mywikindx_groupTitle');
		$pString = $this->success->text("groupAdd");
		$this->init($pString);
		FACTORY_CLOSE::getInstance();
	}
// Display options for editing a user group
	public function editUserGroupInit($error = FALSE)
	{
		$tinymce = FACTORY_LOADTINYMCE::getInstance();
		$this->session->delVar('mywikindx_group_add');
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "editGroup")));
		$this->session->delVar("mywikindx_groupLock");
		if(!array_key_exists('groupId', $this->vars) || !trim($this->vars['groupId']))
			$this->badInput->close($this->init($this->errors->text("inputError", "missing")));
		$this->checkValidUserGroup();
		$pString = '';
		$groupUsers = array();
		if($error)
			$pString .= \HTML\p($error, "error", "center");
		$pString .= \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "editUserGroup");
		$pString .= \FORM\hidden("groupId", $this->vars['groupId']);
		$pString .= $tinymce->loadBasicTextarea();
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		if($this->session->issetVar("mywikindx_groupTitle"))
		{
			$title = \HTML\dbToFormTidy($this->session->getVar("mywikindx_groupTitle"));
			$description = \HTML\dbToFormTidy($this->session->getVar("mywikindx_groupDescription"));
		}
		else
		{
			$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
			$recordset = $this->db->select('user_groups', array('usergroupsTitle', 'usergroupsDescription'));
			$row = $this->db->fetchRow($recordset);
			$title = \HTML\dbToFormTidy($row['usergroupsTitle']);
			$description = \HTML\dbToFormTidy($row['usergroupsDescription']);
			$this->db->formatConditions(array('usergroupsusersGroupId' => $this->vars['groupId']));
			$this->db->leftJoin('users', 'usersId', 'usergroupsusersUserId');
			$recordset = $this->db->select('user_groups_users',
				array('usergroupsusersUserId', 'usersUsername', 'usersFullname', 'usersAdmin'));
// add 0 => IGNORE to $array
			$groupUsers[0] = $this->messages->text("misc", "ignore");
			while($row = $this->db->fetchRow($recordset))
			{
				if(!$row['usergroupsusersUserId'])
					continue;
				if($row['usergroupsusersUserId'] == $this->session->getVar('setup_UserId'))
					continue;
				$groupUsers[$row['usergroupsusersUserId']] = \HTML\dbToFormTidy($row['usersUsername']);
				if($row['usersFullname'])
					$groupUsers[$row['usergroupsusersUserId']] .= " (" . \HTML\dbToFormTidy($row['usersFullname']) . ")";
				if($row['usersAdmin'] == 'Y')
					$groupUsers[$row['usergroupsusersUserId']] .= " ADMIN";
			}
		}
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "groupTitle"), "title",
			$title, 50, 255) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= BR . "&nbsp;" . BR;
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\textAreaInput($this->messages->text("user", "groupDescription"),
			"description", $description, 80, 10));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$users = $this->user->grabAll(TRUE);
// add 0 => IGNORE to $array
		$temp[0] = $this->messages->text("misc", "ignore");
		foreach($users as $key => $value)
		{
			if($key == $this->session->getVar('setup_UserId'))
				continue;
			if(array_key_exists($key, $groupUsers))
				continue;
			$temp[$key] = $value;
		}
		$pString .= BR . "&nbsp;" . BR;
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		if(sizeof($groupUsers) > 1)
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("user",
			"groupUserDelete"), "groupUsers", $groupUsers, 10) . BR .
			$this->messages->text("hint", "multiples"));
		if(sizeof($temp) > 1)
			$pString .= \HTML\td(\FORM\selectFBoxValueMultiple($this->messages->text("user", "groupUserAdd"),
				"users", $temp, 10) . BR . $this->messages->text("hint", "multiples"));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \HTML\p(\FORM\formSubmit("Edit"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// edit a user group
	public function editUserGroup()
	{
		if($this->session->getVar("mywikindx_groupLock"))
			$this->badInput->close($this->init($this->errors->text("done", "group")));
		if(!array_key_exists('groupId', $this->vars) || !$this->vars['groupId'])
			$this->badInput->close($this->init($this->errors->text("inputError", "missing")));
		if(!$title = trim($this->vars['title']))
			$this->badInput->close($this->EditUserGroupInit($this->errors->text("inputError", "missing")));
		$this->checkValidUserGroup();
		$this->checkUserGroupExists($title, $this->vars['groupId']);
		if(trim($this->vars['description']))
		{
			$this->session->setVar('mywikindx_groupDescription', $this->vars['description']);
			$updateArray['usergroupsDescription'] = $this->vars['description'];
		}
		else
		{
			$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
			$this->db->updateNull('user_groups', 'usergroupsDescription');
		}
		$updateArray['usergroupsTitle'] = $title;
		$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
		$this->db->update('user_groups', $updateArray);
		$addUsers = array();
		if(array_key_exists('users', $this->vars))
		{
			foreach($this->vars['users'] as $id) // to be added
			{
				if(!$id) // IGNORE
					continue;
				$addUsers[] = $id;
			}
		}
// delete any users
		if(array_key_exists('groupUsers', $this->vars))
		{
			foreach($this->vars['groupUsers'] as $id) // to be deleted
			{
				if(!$id) // 'IGNORE'
					continue;
				$this->db->formatConditions(array('usergroupsusersGroupId' => $this->vars['groupId']));
				$this->db->formatConditions(array('usergroupsusersUserId' => $id));
				$this->db->delete('user_groups_users');
			}
		}
// Insert new users
		foreach($addUsers as $id)
			$this->db->insert('user_groups_users', array('usergroupsusersUserId', 'usergroupsusersGroupId'),
				array($id, $this->vars['groupId']));
		$this->session->setVar("mywikindx_groupLock", TRUE);
		$this->session->delVar('mywikindx_groupDescription');
		$this->session->delVar('mywikindx_groupTitle');
		$pString = $this->success->text("groupEdit");
		$this->init($pString);
		FACTORY_CLOSE::getInstance();
	}
// Ask for confirmation of delete user group
	function deleteUserGroupInit()
	{
		if(!array_key_exists('groupId', $this->vars) || !$this->vars['groupId'])
			$this->badInput->close($this->init($this->errors->text("inputError", "invalid")));
		$this->checkValidUserGroup();
		$this->session->delVar('mywikindx_group_add');
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "deleteGroup")));
		$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
		$title = $this->db->selectFirstField('user_groups', 'usergroupsTitle');
		$pString = \HTML\p(\HTML\strong($this->messages->text("user", "deleteConfirmGroup") . ": ") .
			\HTML\dbToHtmlTidy($title));
		$pString .= \HTML\p($this->messages->text("user", "deleteGroup2"));
		$pString .= \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "deleteUserGroup");
		$pString .= \FORM\hidden("groupId", $this->vars['groupId']);
		$pString .= BR . \FORM\formSubmit('Confirm');
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// Delete user group
	function deleteUserGroup()
	{
		if(!array_key_exists('groupId', $this->vars) || !$this->vars['groupId'])
			$this->badInput->close($this->init($this->errors->text("inputError", "invalid")));
		$this->checkValidUserGroup();
// Get any bibliographyIds and delete those bibliographies
		$this->db->formatConditions(array('userbibliographyUserGroupId' => $this->vars['groupId']));
		$recordset = $this->db->select('user_bibliography', 'userbibliographyId');
		while($row = $this->db->fetchRow($recordset))
			$bibIds[] = $row['userbibliographyId'];
		if(isset($bibIds))
		{
			$this->db->formatConditionsOneField($bibIds, 'userbibliographyresourceBibliographyId');
			$this->db->delete('user_bibliography_resource');
		}
		$this->db->formatConditions(array('userbibliographyUserGroupId' => $this->vars['groupId']));
		$this->db->delete('user_bibliography');
// delete users from usergroup
		$this->db->formatConditions(array('usergroupsusersGroupId' => $this->vars['groupId']));
		$this->db->delete('user_groups_users');
// Delete usergroup
		$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
		$this->db->delete('user_groups');
		$pString = $this->success->text("groupDelete");
		$this->init($pString);
		FACTORY_CLOSE::getInstance();
	}
// display email notification for adding/editing resources
	private function displayNotify()
	{
		$pString = \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "setNotify");
		$pString .= \HTML\p(\HTML\strong($this->messages->text("user", "notification")));
		if(!$this->session->getVar("mywikindx_Notify"))
		{
			$this->db->formatConditions(array('usersId' => $this->session->getVar('setup_UserId')));
			$recordset = $this->db->select('users', array('usersNotify', 'usersNotifyAddEdit',
				'usersNotifyThreshold', 'usersNotifyTimestamp', 'usersNotifyDigestThreshold'));
			$row = $this->db->fetchRow($recordset);
			$this->session->setVar("mywikindx_Notify", $row['usersNotify']);
			if($row['usersNotifyAddEdit'] == 'A')
			{
				$this->session->setVar("mywikindx_NotifyAdd", TRUE);
				$this->session->setVar("mywikindx_NotifyEdit", TRUE);
			}
			else if($row['usersNotifyAddEdit'] == 'N')
			{
				$this->session->setVar("mywikindx_NotifyAdd", TRUE);
				$this->session->delVar("mywikindx_NotifyEdit");
			}
			else if($row['usersNotifyAddEdit'] == 'E')
			{
				$this->session->setVar("mywikindx_NotifyEdit", TRUE);
				$this->session->delVar("mywikindx_NotifyAdd");
			}
			$this->session->setVar("mywikindx_NotifyThreshold", $row['usersNotifyThreshold']);
			$this->session->setVar("mywikindx_NotifyTimestamp", $row['usersNotifyTimestamp']);
			$this->session->setVar("mywikindx_NotifyDigestThreshold", $row['usersNotifyDigestThreshold']);
		}
		$checked = $this->session->getVar("mywikindx_Notify") == 'N' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "Notify", "N", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "notifyNone"));
		$checked = $this->session->getVar("mywikindx_Notify") == 'A' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "Notify", "A", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "notifyAll"));
		$checked = $this->session->getVar("mywikindx_Notify") == 'M' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "Notify", "M", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "notifyMyBib"));
		$checked = $this->session->getVar("mywikindx_Notify") == 'C' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "Notify", "C", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "notifyMyCreator"));
		$add = $this->session->issetVar("mywikindx_NotifyAdd") ? 'CHECKED' : FALSE;
		$edit = $this->session->issetVar("mywikindx_NotifyEdit") ? 'CHECKED' : FALSE;
		$pString .= \HTML\p($this->messages->text("user", "notifyAdd") . ":&nbsp;&nbsp;" .
			\FORM\checkbox(FALSE, "NotifyAdd", $add) . BR .
			$this->messages->text("user", "notifyEdit") . ":&nbsp;&nbsp;" .
			\FORM\checkbox(FALSE, "NotifyEdit", $edit));
		$array = array(0 => $this->messages->text("user", "notifyImmediate"), 1 => 1, 7 => 7, 14 => 14, 28 => 28);
		$selected = $this->session->getVar('mywikindx_NotifyThreshold');
		if($selected)
			$pString .= \HTML\p(\FORM\selectedBoxValue($this->messages->text("user", "notifyThreshold"),
			"NotifyThreshold", $array, $selected, 5));
		else
			$pString .= \HTML\p(\FORM\selectFBoxValue($this->messages->text("user", "notifyThreshold"),
			"NotifyThreshold", $array, 5));
		$pString .= \HTML\p(\FORM\textInput($this->messages->text("user", "notifyDigestThreshold"),
			"DigestThreshold", $this->session->getVar('mywikindx_NotifyDigestThreshold'), 5, 255));
		$pString .= \HTML\p(\FORM\formSubmit("Edit"));
		$pString .= \FORM\formEnd();
		return $pString;
	}
// Set email notification
	public function setNotify()
	{
		if(!array_key_exists('Notify', $this->vars) || !$this->vars['Notify'])
			$this->badInput->close($this->init($this->errors->text("inputError", "missing")));
		$updateArray = array('usersNotify' => $this->vars['Notify']);
		if(array_key_exists('NotifyAdd', $this->vars) && array_key_exists('NotifyEdit', $this->vars))
		{
			$this->session->setVar("mywikindx_NotifyAdd", TRUE);
			$this->session->setVar("mywikindx_NotifyEdit", TRUE);
			$updateArray['usersNotifyAddEdit'] = 'A';
		}
		else if(array_key_exists('NotifyAdd', $this->vars))
		{
			$this->session->setVar("mywikindx_NotifyAdd", TRUE);
			$this->session->delVar("mywikindx_NotifyEdit");
			$updateArray['usersNotifyAddEdit'] = 'N';
		}
		else if(array_key_exists('NotifyEdit', $this->vars))
		{
			$this->session->setVar("mywikindx_NotifyEdit", TRUE);
			$this->session->delVar("mywikindx_NotifyAdd");
			$updateArray['usersNotifyAddEdit'] = 'E';
		}
		else
		{
			$this->session->setVar("mywikindx_NotifyAdd", TRUE);
			$this->session->setVar("mywikindx_NotifyEdit", TRUE);
			$updateArray['usersNotifyAddEdit'] = 'A';
		}
		if(array_key_exists('NotifyThreshold', $this->vars))
		{
			$this->session->setVar("mywikindx_NotifyThreshold", $this->vars['NotifyThreshold']);
			$updateArray['usersNotifyThreshold'] = $this->vars['NotifyThreshold'];
		}
		if(array_key_exists('DigestThreshold', $this->vars) AND is_numeric(trim($this->vars['DigestThreshold'])))
		{
			$this->session->setVar("mywikindx_NotifyDigestThreshold", trim($this->vars['DigestThreshold']));
			$updateArray['usersNotifyDigestThreshold'] = trim($this->vars['DigestThreshold']);
		}
		else
		{
			$this->session->setVar("mywikindx_NotifyDigestThreshold", 100);
			$updateArray['usersNotifyDigestThreshold'] = 100;
		}
		$this->db->formatConditions(array('usersId' => $this->session->getVar('setup_UserId')));
		$this->db->update('users', $updateArray);
		$this->session->setVar("mywikindx_Notify", $this->vars['Notify']);
		$this->init($this->success->text("notify"));
		FACTORY_CLOSE::getInstance();
	}
	public function createGroupBibInit()
	{
		$this->groupBib = TRUE;
		$this->createBibInit();
	}
// Display options for adding a user or group bibliography
	public function createBibInit($error = FALSE)
	{
		$tinymce = FACTORY_LOADTINYMCE::getInstance();
// If creating a group bibliography, this user must own groups
		if($this->groupBib)
		{
			GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
				": " . $this->messages->text("user", "createGroupBib")));
			$groupString = \FORM\hidden('groupBib', TRUE);
			$this->db->formatConditions(array('usergroupsAdminId' => $this->session->getVar('setup_UserId')));
			$this->db->orderBy('usergroupsTitle');
			$recordset = $this->db->select('user_groups', array('usergroupsId', 'usergroupsTitle'));
			if(!$this->db->numRows($recordset))
				$this->badInput->close($this->init($this->errors->text("inputError", "userHasNoGroups")));
			while($row = $this->db->fetchRow($recordset))
				$groups[$row['usergroupsId']] = \HTML\dbToFormTidy($row['usergroupsTitle']);
			$groupString .= \HTML\p(\FORM\selectFBoxValue($this->messages->text("user", "addGroupsToBib"),
				"groupId", $groups, 10));
		}
		else
		{
			$groupString = FALSE;
			GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
				": " . $this->messages->text("user", "createBib")));
		}
		$this->session->delVar('mywikindx_Bibliography_add');
		$this->session->delVar('mywikindx_PagingStart');
		$this->session->delVar('mywikindx_PagingStartAlpha');
		$this->session->delVar("bibliography_lock");
		$pString = $error ? \HTML\p($error, "error", "center") : '';
		$pString .= \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "createBib");
		$pString .= $tinymce->loadBasicTextarea();
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$sessVar = $this->session->issetVar("mywikindx_Title") ?
			\HTML\dbToFormTidy($this->session->getVar("mywikindx_Title")) : FALSE;
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "bibTitle"),
			"title", $sessVar, 50, 255) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= BR . "&nbsp;" . BR;
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$sessVar = $this->session->issetVar("mywikindx_Description") ?
			\HTML\dbToFormTidy($this->session->getVar("mywikindx_Description")) : FALSE;
		$pString .= \HTML\td(\FORM\textAreaInput($this->messages->text("user", "bibDescription"),
			"description", $sessVar, 80, 10));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= $groupString;
		$pString .= \HTML\p(\FORM\formSubmit("Add"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// create a bibliography
	public function createBib()
	{
		if($this->session->getVar("bibliography_lock"))
			$this->badInput->close($this->init($this->errors->text("done", "bibliography")));
		if(array_key_exists('description', $this->vars) && trim($this->vars['description']))
		{
			$this->session->setVar('mywikindx_Description', $this->vars['description']);
			$fields[] = 'userbibliographyDescription';
			$values[] = $this->vars['description'];
		}
		if(!$title = trim($this->vars['title']))
			$this->badInput->close($this->createBibInit($this->errors->text("inputError", "missing")));
		$this->session->setVar('mywikindx_Title', $title);
		$this->checkBibliographyExists($title);
		$fields[] = 'userbibliographyTitle';
		$values[] = $title;
		$fields[] = 'userbibliographyUserId';
		$values[] = $this->session->getVar('setup_UserId');
		if(array_key_exists('groupId', $this->vars) && $this->vars['groupId'])
		{
			$fields[] = 'userbibliographyUserGroupId';
			$values[] = $this->vars['groupId'];
		}
		$this->db->insert('user_bibliography', $fields, $values);
		$bibDbId = $this->db->lastAutoId();
		if($this->groupBib)
		{
			if(array_key_exists('groupId', $this->vars) && $this->vars['groupId']) // valid id -- not 'IGNORE'
			{
				$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
				$rowBibIds = $this->db->selectFirstField('user_groups', 'usergroupsBibliographyIds');
				$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
				if(!$rowBibIds)
					$this->db->updateSingle('user_groups', 'usergroupsBibliographyIds' . "=" . $bibDbId);
				else
				{
					$bibs = UTF8::mb_explode(',', $rowBibIds);
					if(array_search($bibDbId, $bibs) === FALSE)
					{
						$bibs[] = $bibDbId;
						$this->db->updateSingle('user_groups', 'usergoupsBibliographyIds' . "=" . join(',', $bibs));
					}
				}
			}
		}
		$this->session->setVar("bibliography_lock", TRUE);
		$this->session->delVar('mywikindx_Description');
		$this->session->delVar('mywikindx_Title');
		$this->session->setVar("setup_Bibliographies", TRUE);
		$this->init($this->success->text("bibliographyAdd"));
		FACTORY_CLOSE::getInstance();
	}
// Display options for editing a bibliography
	public function editBibInit($error = FALSE)
	{
		$tinymce = FACTORY_LOADTINYMCE::getInstance();
		$this->session->delVar('mywikindx_Bibliography_add');
		$this->session->delVar('mywikindx_PagingStart');
		$this->session->delVar('mywikindx_PagingStartAlpha');
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "editBib")));
		$this->session->delVar("bibliography_lock");
		if(!array_key_exists('bibId', $this->vars) || ($this->vars['bibId'] < 0) || !trim($this->vars['bibId']))
			$this->badInput->close($this->init($this->errors->text('inputError', 'missing')));
		$userGroupId = FALSE;
		$pString = $error ? \HTML\p($error, "error", "center") : '';
		$pString .= \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "editBib");
		$pString .= \FORM\hidden("bibId", $this->vars['bibId']);
		$pString .= $tinymce->loadBasicTextarea();
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		if($this->session->issetVar("mywikindx_Title"))
		{
			$title = \HTML\dbToFormTidy($this->session->getVar("mywikindx_Title"));
			$description = \HTML\dbToFormTidy($this->session->getVar("mywikindx_Description"));
		}
		else
		{
			$this->db->formatConditions(array('userbibliographyUserId' => $this->session->getVar('setup_UserId')));
			$this->db->formatConditions(array('userbibliographyId' => $this->vars['bibId']));
			$recordset = $this->db->select('user_bibliography',
				array('userbibliographyTitle', 'userbibliographyDescription', 'userbibliographyUserGroupId'));
			$row = $this->db->fetchRow($recordset);
			$description = \HTML\dbToFormTidy($row['userbibliographyDescription']);
			$title = \HTML\dbToFormTidy($row['userbibliographyTitle']);
			$userGroupId = $row['userbibliographyUserGroupId'];
		}
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "bibTitle"), "title",
			$title, 50, 255) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= BR . "&nbsp;" . BR;
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td(\FORM\textAreaInput($this->messages->text("user", "bibDescription"),
			"description", $description, 80, 10));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		if($userGroupId) // This is a user group bibliography
		{
			$pString .= \FORM\hidden('groupBib', TRUE);
			$this->db->formatConditions(array('usergroupsAdminId' => $this->session->getVar('setup_UserId')));
			$this->db->orderBy('usergroupsTitle');
			$recordset = $this->db->select('user_groups', array('usergroupsId', 'usergroupsTitle'));
			while($row = $this->db->fetchRow($recordset))
				$groups[$row['usergroupsId']] = \HTML\dbToFormTidy($row['usergroupsTitle']);
			$pString .= \HTML\p(\FORM\selectedBoxValue($this->messages->text("user", "addGroupsToBib"),
				"groupId", $groups, $userGroupId, 10));
		}
		$pString .= \HTML\p(\FORM\formSubmit("Edit"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// edit a bibliography
	public function editBib()
	{
		if($this->session->getVar("bibliography_lock"))
			$this->badInput->close($this->init($this->errors->text("done", "bibliography")));
		if(!array_key_exists('bibId', $this->vars) || !$this->vars['bibId'] || (!$title = trim($this->vars['title'])))
			$this->badInput->close($this->editBibInit($this->errors->text('inputError', 'missing')));
		$this->checkBibliographyExists($title, $this->vars['bibId']);
		if(trim($this->vars['description']))
		{
			$this->session->setVar('mywikindx_Description', $this->vars['description']);
			$updateArray['userbibliographyDescription'] = $this->vars['description'];
		}
		else
		{
			$this->db->formatConditions(array('userbibliographyId' => $this->vars['bibId']));
			$this->db->formatConditions(array('userbibliographyUserId' => $this->session->getVar('setup_UserId')));
			$this->db->updateNull('user_bibliography', 'userbibliographyDescription');
		}
		$updateArray['userbibliographyTitle'] = $title;
		if(array_key_exists('groupId', $this->vars)) // user group bibliography
			$updateArray['userbibliographyUserGroupId'] = $this->vars['groupId'];
		$this->db->formatConditions(array('userbibliographyUserId' => $this->session->getVar('setup_UserId')));
		$this->db->formatConditions(array('userbibliographyId' => $this->vars['bibId']));
		$this->db->update('user_bibliography', $updateArray);
		$this->session->setVar("bibliography_lock", TRUE);
		$this->session->delVar('mywikindx_Description');
		$this->session->delVar('mywikindx_Title');
		$this->init($this->success->text("bibliographyEdit"));
		FACTORY_CLOSE::getInstance();
	}
// Ask for confirmation of delete bibliography
	public function deleteBibInit()
	{
		if(!array_key_exists('bibId', $this->vars) || !$this->vars['bibId'])
			$this->badInput->close($this->init($this->errors->text('inputError', 'invalid')));
		$this->db->formatConditions(array('userbibliographyUserId' => $this->session->getVar('setup_UserId')));
		$this->db->formatConditions(array('userbibliographyId' => $this->vars['bibId']));
		$recordset = $this->db->select('user_bibliography', array('userbibliographyTitle', 'userbibliographyUserGroupId'));
		if(!$this->db->numRows($recordset))
			$this->badInput->close($this->init($this->errors->text('inputError', 'invalid')));
		$this->session->delVar("bibliography_lock");
		$row = $this->db->fetchRow($recordset);
		if($row['userbibliographyUserGroupId'])
			$pString = \FORM\hidden("groupBib", TRUE);
		else
			$pString = '';
		$title = $row['userbibliographyTitle'];
		$this->session->delVar('mywikindx_Bibliography_add');
		$this->session->delVar('mywikindx_PagingStart');
		$this->session->delVar('mywikindx_PagingStartAlpha');
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "deleteBib")));
		$pString .= \HTML\p(\HTML\strong($this->messages->text("user", "deleteConfirmBib") . ": ") .
			\HTML\dbToHtmlTidy($title) . BR . $this->messages->text("hint", "deleteConfirmBib"));
		$pString .= \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "deleteBib");
		$pString .= \FORM\hidden("bibId", $this->vars['bibId']);
		$pString .= \HTML\p(\FORM\formSubmit("Confirm"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// Delete bibliography
	public function deleteBib()
	{
		if($this->session->getVar("bibliography_lock"))
			$this->badInput->close($this->init($this->errors->text("done", "bibliography")));
		if(!array_key_exists('bibId', $this->vars) || !$this->vars['bibId'])
			$this->badInput->close($this->init($this->errors->text('inputError', 'invalid')));
/*		$this->db->formatConditions(array('userbibliographyUserId' => $this->session->getVar('setup_UserId')));
		$this->db->formatConditions(array('userbibliographyId' => $this->vars['bibId']));
		$userbibliographyUserGroupId = $this->db->selectFirstField('user_bibliography', 'userbibliographyUserGroupId');
		if(!$userbibliographyUserGroupId)
			$this->badInput->close($this->init($this->errors->text('inputError', 'invalid')));
*/		$this->db->formatConditions(array('userbibliographyUserId' => $this->session->getVar('setup_UserId')));
		$this->db->formatConditions(array('userbibliographyId' => $this->vars['bibId']));
		$this->db->delete('user_bibliography');
// Get any bibliographyIds and delete those bibliographies
		$this->db->formatConditions(array('userbibliographyresourceBibliographyId' => $this->vars['bibId']));
		$this->db->delete('user_bibliography_resource');
		$this->session->setVar("bibliography_lock", TRUE);
		if($this->vars['bibId'] == $this->session->getVar('mywikindx_Bibliography_use'))
			$this->session->delVar('mywikindx_Bibliography_use');
		$bibsU = $this->bib->getUserBibs();
		$bibsUG = $this->bib->getGroupBibs();
		if(empty($bibsU) && empty($bibsUG))
			$this->session->delVar("setup_Bibliographies");
		$this->init($this->success->text("bibliographyDelete"));
		FACTORY_CLOSE::getInstance();
	}
// Display options for adding a user tag
	public function createUserTagInit($error = FALSE)
	{
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "createUserTag")));
		$this->session->delVar('mywikindx_UserTag_add');
		$this->session->delVar("bibliography_lock");
		$pString = $error ? \HTML\p($error, "error", "center") : '';
		$pString .= \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "createUserTag");
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		$sessVar = $this->session->issetVar("mywikindx_Title") ?
			\HTML\dbToFormTidy($this->session->getVar("mywikindx_Title")) : FALSE;
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "bibTitle"),
			"title", $sessVar, 50, 255) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \HTML\p(\FORM\formSubmit("Add"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// create a user tag
	public function createUserTag()
	{
		if($this->session->getVar("bibliography_lock"))
			$this->badInput->close($this->init($this->errors->text("done", "userTag")));
		if(!$title = trim($this->vars['title']))
			$this->badInput->close($this->createUserTagInit($this->errors->text("inputError", "missing")));
		$this->session->setVar('mywikindx_Title', $title);
		$userTagsObject = FACTORY_USERTAGS::getInstance();
		if($userTagsObject->checkExists($title))
			$this->badInput->close($this->createUserTagInit($this->errors->text('inputError', 'userTagExists')));
		$fields[] = 'usertagsTag';
		$values[] = $title;
		$fields[] = 'usertagsUserId';
		$values[] = $this->session->getVar('setup_UserId');
		$this->db->insert('user_tags', $fields, $values);
		$this->session->setVar("bibliography_lock", TRUE);
		$this->session->delVar('mywikindx_Title');
		$this->init($this->success->text("usertagAdd"));
		FACTORY_CLOSE::getInstance();
	}
// Display options for editing a user tag
	public function editUserTagInit($error = FALSE)
	{
		$this->session->delVar('mywikindx_Bibliography_add');
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "editUserTag")));
		$this->session->delVar("bibliography_lock");
		if(!array_key_exists('userTagId', $this->vars) || ($this->vars['userTagId'] < 0))
			$this->badInput->close($this->init($this->errors->text('inputError', 'missing')));
		$pString = $error ? \HTML\p($error, "error", "center") : '';
		$pString .= \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "editUserTag");
		$pString .= \FORM\hidden("userTagId", $this->vars['userTagId']);
		$pString .= \HTML\tableStart();
		$pString .= \HTML\trStart();
		if($this->session->issetVar("mywikindx_Title"))
			$title = \HTML\dbToFormTidy($this->session->getVar("mywikindx_Title"));
		else
		{
			$this->db->formatConditions(array('usertagsId' => $this->vars['userTagId']));
			$title = \HTML\dbToFormTidy($this->db->selectFirstField('user_tags', 'usertagsTag'));
		}
		$pString .= \HTML\td(\FORM\textInput($this->messages->text("user", "bibTitle"), "title",
			$title, 50, 255) . " " . \HTML\span('*', 'required'));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \HTML\p(\FORM\formSubmit("Edit"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// edit a user tag
	public function editUserTag()
	{
		if($this->session->getVar("bibliography_lock"))
			$this->badInput->close($this->init($this->errors->text("done", "bibliography")));
		if(!array_key_exists('userTagId', $this->vars) || !$this->vars['userTagId'] || (!$title = trim($this->vars['title'])))
			$this->badInput->close($this->editUserTagInit($this->errors->text('inputError', 'missing')));
		$userTagsObject = FACTORY_USERTAGS::getInstance();
		if($userTagsObject->checkExists($title))
			$this->badInput->close($this->editUserTagInit($this->errors->text('inputError', 'userTagExists')));
		$updateArray['usertagsTag'] = $title;
		$this->db->formatConditions(array('usertagsId' => $this->vars['userTagId']));
		$this->db->update('user_tags', $updateArray);
		$this->session->setVar("bibliography_lock", TRUE);
		$this->session->delVar('mywikindx_Title');
		$this->init($this->success->text("usertagEdit"));
		FACTORY_CLOSE::getInstance();
	}
// Ask for confirmation of delete a user tag
	public function deleteUserTagInit()
	{
		if(!array_key_exists('userTagId', $this->vars) || !$this->vars['userTagId'])
			$this->badInput->close($this->init($this->errors->text('inputError', 'invalid')));
		$this->session->delVar("bibliography_lock");
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "myWikindx",
			": " . $this->messages->text("user", "deleteUserTag")));
		$this->db->formatConditions(array('usertagsId' => $this->vars['userTagId']));
		$title = \HTML\dbToHtmlTidy($this->db->selectFirstField('user_tags', 'usertagsTag'));
		$pString = \HTML\p(\HTML\strong($this->messages->text("user", "deleteConfirmUserTag") . ": ") . $title);
		$pString .= \FORM\formHeader('usersgroups_MYWIKINDX_CORE');
		$pString .= \FORM\hidden("method", "deleteUserTag");
		$pString .= \FORM\hidden("userTagId", $this->vars['userTagId']);
		$pString .= \HTML\p(\FORM\formSubmit("Confirm"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// Delete a user tag
	public function deleteUserTag()
	{
		if($this->session->getVar("bibliography_lock"))
			$this->badInput->close($this->init($this->errors->text("done", "userTag")));
		if(!array_key_exists('userTagId', $this->vars) || !$this->vars['userTagId'])
			$this->badInput->close($this->init($this->errors->text('inputError', 'invalid')));
		$this->db->formatConditions(array('usertagsId' => $this->vars['userTagId']));
		$this->db->delete('user_tags');
// Remove user_tag ids from resource_user_tags.TagIds
// Grab info from WKX_resource_user_tags table and store those resources that require updating.....
		$this->db->formatConditions(array('resourceusertagsTagId' => $this->vars['userTagId']));
		$this->db->delete('resource_user_tags');
		$this->session->setVar("bibliography_lock", TRUE);
		$this->init($this->success->text("usertagDelete"));
		FACTORY_CLOSE::getInstance();
	}
// Load user details into session mywikindx_ array
	private function loadSession($id = FALSE)
	{
		if(!$id)
			$id = $this->session->getVar("setup_UserId");
		$userArray = array(array('usersUsername' => 'Username'), array('usersPassword' => 'Password'),
			array('usersEmail' => 'Email'), array('usersFullname' => 'Fullname'),
			array('usersAdmin' => 'Admin'), array('usersCookie' => 'Cookie'));
		for($i = 1; $i < 4; $i++)
			$userArray[] = "usersPasswordQuestion$i";
		$this->db->formatConditions(array('usersId' => $id));
		$recordset = $this->db->select('users', $userArray);
		if(!$this->db->numRows($recordset))
			$this->badInput->close($this->errors->text("dbError", "read"));
		$row = $this->db->fetchRow($recordset);
		foreach($userArray as $key)
		{
			if(is_array($key))
				$key = array_shift($key);
			if(($key == 'usersAdmin') || ($key == 'usersCookie'))
			{
				if($row[$key] == 'Y')
					$this->session->setVar("mywikindx_" . $key, TRUE);
				else
					$this->session->delVar("mywikindx_" . $key);
				continue;
			}
			if($row[$key])
				$this->session->setVar("mywikindx_" . $key, $row[$key]);
		}
	}
// check this user can edit, delete and deleteFrom from user groups
	private function checkValidUserGroup()
	{
		$this->db->formatConditions(array('usergroupsId' => $this->vars['groupId']));
		$adminId = $this->db->selectFirstField('user_groups', 'usergroupsAdminId');
		if($this->session->getVar('setup_UserId') != $adminId)
			$this->badInput->close($this->init($this->errors->text("inputError", "invalid")));
	}
/**
* Does user group already exist?
* If $groupId, we're editing an existing user group.
*/
	private function checkUserGroupExists($title, $groupId = FALSE)
	{
		if($groupId)
			$this->db->formatConditions(array('usergroupsId' => $groupId), TRUE);
		$recordset = $this->db->select('user_groups', array('usergroupsTitle', 'usergroupsId'));
		while($row = $this->db->fetchRow($recordset))
		{
			if($title == $row['usergroupsTitle'])
			{
				if($groupId)
					$this->badInput->close($this->editUserGroupInit($this->errors->text("inputError", "groupExists")));
				else
					$this->badInput->close($this->createUserGroupInit($this->errors->text("inputError", "groupExists")));
			}
		}
	}
/**
* Does bibliography already exist?
* If $bibId, we're editing an existing user group.
*/
	private function checkBibliographyExists($title, $bibId = FALSE)
	{
		if($bibId)
			$this->db->formatConditions(array('userbibliographyId' => $bibId), TRUE);
		$recordset = $this->db->select('user_bibliography', array('userbibliographyTitle', 'userbibliographyId'));
		while($row = $this->db->fetchRow($recordset))
		{
			if($title == $row['userbibliographyTitle'])
			{
				if($bibId)
					$this->badInput->close($this->editBibInit($this->errors->text("inputError", "bibExists")));
				else
					$this->badInput->close($this->createBibInit($this->errors->text("inputError", "bibExists")));
			}
		}
	}
}
?>