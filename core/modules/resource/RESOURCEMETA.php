<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* RESOURCEMETA class
*
* Deal with resource's quotes, paraphrases and musings
*/
class RESOURCEMETA
{
private $db;
private $vars;
private $session;
private $messages;
private $user;
private $icons;
private $common;
private $cite;
private $quote = array();
private $paraphrase = array();
private $musing = array();
private $userId;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->session = FACTORY_SESSION::getInstance();

		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->user = FACTORY_USER::getInstance();
		$this->icons = FACTORY_LOADICONS::getInstance();
		$this->common = FACTORY_RESOURCECOMMON::getInstance();
		$this->cite = FACTORY_CITE::getInstance();
		$this->userId = $this->session->getVar('setup_UserId');
	}
// Display resource's quotes
	public function viewQuotes($row)
	{
		$write = $this->session->getVar('setup_Write') ? TRUE : FALSE;
		$this->db->formatConditions(array('resourcemetadataResourceId' => $row['resourceId']));
		$this->db->formatConditions($this->db->formatFields('resourcemetadataType') . $this->db->equal . $this->db->tidyInput('q'));
		$this->db->orderBy($this->db->tidyInputClause('resourcemetadataPageStart') . '+0', FALSE, FALSE);
		$recordset = $this->db->select('resource_metadata',
			array('resourcemetadataId', 'resourcemetadataPageStart', 'resourcemetadataPageEnd',
			'resourcemetadataParagraph', 'resourcemetadataSection', 'resourcemetadataChapter',
			'resourcemetadataText', 'resourcemetadataAddUserId'));
		$numRows = $this->db->numRows($recordset);
		if(!$numRows && !$write)
			return array();
		if($write)
		{
			$this->quote['editLink'] = \HTML\a($this->icons->addLink, $this->icons->add,
				"index.php?action=resource_RESOURCEQUOTE_CORE&method=quoteEdit" . htmlentities("&resourceId=" . $row['resourceId']));
		}
		$this->view($row['resourceId'], $recordset, 'quote');
		return $this->quote;
	}
// Display resource's paraphrases
	public function viewParaphrases($row)
	{
		$write = $this->session->getVar('setup_Write') ? TRUE : FALSE;
		$this->db->formatConditions(array('resourcemetadataResourceId' => $row['resourceId']));
		$this->db->formatConditions($this->db->formatFields('resourcemetadataType') . $this->db->equal . $this->db->tidyInput('p'));
		$this->db->orderBy($this->db->tidyInputClause('resourcemetadataPageStart') . '+0', FALSE, FALSE);
		$recordset = $this->db->select('resource_metadata',
			array('resourcemetadataId', 'resourcemetadataPageStart', 'resourcemetadataPageEnd',
			'resourcemetadataParagraph', 'resourcemetadataSection', 'resourcemetadataChapter',
			'resourcemetadataText', 'resourcemetadataAddUserId'));
		$numRows = $this->db->numRows($recordset);
		if(!$numRows && !$write)
			return array();
		if($write)
			$this->paraphrase['editLink'] = \HTML\a($this->icons->addLink, $this->icons->add,
				"index.php?action=resource_RESOURCEPARAPHRASE_CORE&method=paraphraseEdit" . htmlentities("&resourceId=" . $row['resourceId']));
		$this->view($row['resourceId'], $recordset, 'paraphrase');
		return $this->paraphrase;
	}
// Display resource's musings
	public function viewMusings($row)
	{
		$resourceId = $row['resourceId'];
		$write = $this->session->getVar('setup_Write') ? TRUE : FALSE;
		$this->db->formatConditions(array('resourcemetadataResourceId' => $row['resourceId']));
		$this->db->formatConditions($this->db->formatFields('resourcemetadataType') . $this->db->equal . $this->db->tidyInput('m'));
		$this->db->orderBy($this->db->tidyInputClause('resourcemetadataPageStart') . '+0', FALSE, FALSE);
		$recordset = $this->db->select('resource_metadata',
			array('resourcemetadataId', 'resourcemetadataPageStart', 'resourcemetadataPageEnd',
			'resourcemetadataParagraph', 'resourcemetadataSection', 'resourcemetadataChapter', 'resourcemetadataPrivate',
			'resourcemetadataText', 'resourcemetadataAddUserId', 'resourcemetadataTimestamp'));
		$numRows = $this->db->numRows($recordset);
		if(!$numRows && !$write)
			return array();
		if($write)
			$this->musing['editLink'] = \HTML\a($this->icons->addLink, $this->icons->add,
				"index.php?action=resource_RESOURCEMUSING_CORE&method=musingEdit" . htmlentities("&resourceId=" . $row['resourceId']));
		$patterns = FALSE;		$write = $this->session->getVar('setup_Write') ? TRUE : FALSE;
		$index = 0;
		if(array_key_exists("search", $this->vars) && $this->vars["search"] = 'highlight')
		{
			$searchTerms = UTF8::mb_explode(",", $this->session->getVar('search_Highlight'));
			foreach($searchTerms as $term)
				$patterns[] = "/($term)(?!\S*\" \S*>)/i";
		}
		$index = 0;
		while($row = $this->db->fetchRow($recordset))
		{
			if(($row['resourcemetadataPrivate'] == 'Y') && ($this->userId != $row['resourcemetadataAddUserId']))
				continue;
// If numeric, this comment may be viewed by members of user groups of which $row[$addUserId] is a member
			else if(is_numeric($row['resourcemetadataPrivate']))
			{
				$this->db->formatConditions(array('usergroupsusersUserId' => $this->userId));
				$this->db->formatConditions(array('usergroupsusersGroupId' => $row['resourcemetadataPrivate']));
				$recordset2 = $this->db->select('user_groups_users', 'usergroupsusersId');
				if(!$this->db->numRows($recordset2))
					continue;
			}
			$this->musing[$index]['details'] = $this->getDetails($row);
			$text = $this->cite->parseCitations(\HTML\dbToHtmlTidy($row['resourcemetadataText']), 'html');
			$this->musing[$index]['musing'] = $this->common->doHighlight($text);
			$this->musing[$index]['timestamp'] = $row['resourcemetadataTimestamp'];
			$this->db->formatConditions(array('resourcekeywordMetadataId' => $row['resourcemetadataId']));
			$this->db->leftJoin('keyword', 'keywordId', 'resourcekeywordKeywordId');
			$recordset2 = $this->db->select('resource_keyword', array('keywordId', 'keywordKeyword', 'keywordGlossary'));
			while($row2 = $this->db->fetchRow($recordset2))
			{
				$this->musing[$index]['keywordTitle'] = $this->messages->text("resources", "keywords");
				$this->musing[$index]['keywords'][] = \HTML\a("link",
					\HTML\dbToHtmlTidy($row2['keywordKeyword']), "index.php?action=list_LISTSOMERESOURCES_CORE" .
					'&method=metaKeywordProcess' . htmlentities("&id=" . $row2['keywordId']), "", \HTML\dbToHtmlPopupTidy($row2['keywordGlossary']));
			}
			$users = $this->user->displayUserAddEdit($row['resourcemetadataAddUserId'], TRUE, 'musing');
			$this->musing[$index]['userAdd'] = $users[0];
			if($write && ($row['resourcemetadataAddUserId'] == $this->userId))
				$this->musing[$index]['editLink'] = \HTML\a($this->icons->editLink, $this->icons->edit,
				"index.php?action=resource_RESOURCEMUSING_CORE&method=musingEdit" . htmlentities("&resourceId=" . $resourceId .
				'&resourcemetadataId=' . $row['resourcemetadataId']));
			$index++;
		}
		if(!empty($this->musing))
			$this->musing['title'] = $this->messages->text("viewResource", "musings");
		return $this->musing;
	}
// Display quotes, paraphrases etc.
	private function view($resourceId, $recordset, $type)
	{
		if($type == 'quote')
			$this->quote['title'] = $this->messages->text("viewResource", "quotes");
		else // 'paraphrase'
			$this->paraphrase['title'] = $this->messages->text("viewResource", "paraphrases");
		$patterns = FALSE;
		$action = $type == 'quote' ? 'quoteEdit' : 'paraphraseEdit';
		$phpFile = $type == 'quote' ? 'resource_RESOURCEQUOTE_CORE' : 'resource_RESOURCEPARAPHRASE_CORE';
		$write = $this->session->getVar('setup_Write') ? TRUE : FALSE;
		$index = 0;
		$thisUserId = $this->session->getVar('setup_UserId');
		if(array_key_exists("search", $this->vars) && $this->vars["search"] = 'highlight')
		{
			$searchTerms = UTF8::mb_explode(",", $this->session->getVar('search_Highlight'));
			foreach($searchTerms as $term)
				$patterns[] = "/($term)(?!\S*\" \S*>)/i";
		}
		$index = 0;
		while($row = $this->db->fetchRow($recordset))
		{
			$this->{$type}[$index]['metaId'] = $row['resourcemetadataId'];
			$this->{$type}[$index]['details'] = $this->getDetails($row);
			$this->{$type}[$index]['commentTitle'] = $this->messages->text("resources", "comment");
			$text = $this->cite->parseCitations(\HTML\dbToHtmlTidy($row['resourcemetadataText']), 'html');
			$this->{$type}[$index][$type] = $this->common->doHighlight($text);
			$this->db->formatConditions(array('resourcekeywordMetadataId' => $row['resourcemetadataId']));
			$this->db->leftJoin('keyword', 'keywordId', 'resourcekeywordKeywordId');
			$recordset2 = $this->db->select('resource_keyword', array('keywordId', 'keywordKeyword', 'keywordGlossary'));
			while($row2 = $this->db->fetchRow($recordset2))
			{
				$this->{$type}[$index]['keywordTitle'] = $this->messages->text("resources", "keywords");
				$this->{$type}[$index]['keywords'][] = \HTML\a("link",
					\HTML\dbToHtmlTidy($row2['keywordKeyword']), "index.php?action=list_LISTSOMERESOURCES_CORE" .
					'&method=metaKeywordProcess' . htmlentities("&id=" . $row2['keywordId']), "", \HTML\dbToHtmlPopupTidy($row2['keywordGlossary']));
			}
			$users = $this->user->displayUserAddEdit($row['resourcemetadataAddUserId'], TRUE, $type);
			$this->{$type}[$index]['userAdd'] = $users[0];
// check for comments
			$this->db->formatConditions(array('resourcemetadataMetadataId' => $row['resourcemetadataId']));
			if($type == 'quote')
				$this->db->formatConditions(array('resourcemetadataType' => 'qc'));
			else
				$this->db->formatConditions(array('resourcemetadataType' => 'pc'));
			$this->db->orderBy('resourcemetadataTimestamp', TRUE, FALSE);
			$recordset2 = $this->db->select('resource_metadata', array('resourcemetadataText', 'resourcemetadataTimestamp',
				'resourcemetadataAddUserId', 'resourcemetadataPrivate'));
			if($this->db->numRows($recordset2))
			{
				$index2 = 0;
				while($rowComment = $this->db->fetchRow($recordset2))
				{
// Read only access
					if($this->session->getVar('setup_ReadOnly') &&
						(($rowComment['resourcemetadataPrivate'] == 'Y') || ($rowComment['resourcemetadataPrivate'] == 'G')))
						continue;
					else if(($rowComment['resourcemetadataPrivate'] == 'Y') &&
						($thisUserId != $rowComment['resourcemetadataAddUserId']))
						continue;
// If 'G' or numeric, this comment may be viewed by members of user groups of which $row[$addUserId] is a member
					else if(is_numeric($rowComment['resourcemetadataPrivate']))
					{
						$this->db->formatConditions(array('usergroupsusersUserId' => $this->userId));
						$this->db->formatConditions(array('usergroupsusersGroupId' => $rowComment['resourcemetadataPrivate']));
						$recordset3 = $this->db->select('user_groups_users', 'usergroupsusersId');
						if(!$this->db->numRows($recordset3))
							continue;
					}
// Else, comment is public
					$text = $this->cite->parseCitations(\HTML\dbToHtmlTidy($rowComment['resourcemetadataText']), 'html');
					$users = $this->user->displayUserAddEdit($rowComment['resourcemetadataAddUserId'], TRUE, 'comment');
					$this->{$type}[$index]['comments'][$index2]['userAdd'] = $users[0];
					$this->{$type}[$index]['comments'][$index2]['comment'] = $this->common->doHighlight($text);
					$this->{$type}[$index]['comments'][$index2]['timestamp'] = $rowComment['resourcemetadataTimestamp'];
					$index2++;
				}
			}
			if($write)
				$this->{$type}[$index]['editLink'] = \HTML\a($this->icons->editLink, $this->icons->edit,
				"index.php?action=$phpFile&method=$action" .
				htmlentities("&resourceId=" . $resourceId . "&resourcemetadataId=" . $row['resourcemetadataId']));
			$index++;
		}
	}
/**
* Get metadata details such as pages, section etc.
*/
	private function getDetails($row)
	{
		$page_start = $row['resourcemetadataPageStart'] ? $row['resourcemetadataPageStart'] : FALSE;
		$page_end = $row['resourcemetadataPageEnd'] ? "-" . $row['resourcemetadataPageEnd'] : FALSE;
		if($page_start && $page_end)
			$page_start = 'pp.' . $page_start;
		else if($page_start)
			$page_start = 'p.' . $page_start;
		$page = $page_start? $page_start . $page_end : FALSE;
		if($page)
			$details[] = $page;
		$paragraph = $row['resourcemetadataParagraph'] ? $row['resourcemetadataParagraph'] : FALSE;
		if($paragraph)
			$details[] = $this->messages->text("resources", "paragraph") . "&nbsp;" . $paragraph;
		$section = $row['resourcemetadataSection'] ? $row['resourcemetadataSection'] : FALSE;
		if($section)
			$details[] = $this->messages->text("resources", "section") . "&nbsp;" . $section;
		$chapter = $row['resourcemetadataChapter'] ? $row['resourcemetadataChapter'] : FALSE;
		if($chapter)
			$details[] = $this->messages->text("resources", "chapter") . "&nbsp;" . $chapter;
		return isset($details) ? join(",&nbsp;", $details) : FALSE;
	}
}
?>