/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

Creative Commons
Creative Commons Legal Code
Attribution-NonCommercial-ShareAlike 2.0
THE WORK IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSE”) — docs/LICENSE.txt. 
THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER 
THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.

BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE. 
THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.

The WIKINDX Team 2016
sirfragalot@users.sourceforge.net

**********************************************************************************/

/**
* Javascript functions for core/modules/resource/RESOURCECATEGORYEDIT.php and TEXTQP.php
*
* @version 1.1
* @date March 2013
* @author Mark Grimshaw
*/

function transferKeyword()
{
	coreSelectToTextarea('keywords', 'fromKeywords');
}
function transferUserTag()
{
	coreSelectToTextarea('userTags', 'fromUserTags');
}
/**
* Transfer an option from the main categories selectbox to the selected categories selectbox
*/
function selectCategory()
{
	var target = 'categoryIds';
	var source = 'availableCategory';
	coreSelectToSelect(target, source);
}
/**
* Transfer an option from the selected categories selectbox to the main categories selectbox
*/
function discardCategory()
{
	var target = 'availableCategory';
	var source = 'categoryIds';
	coreSelectToSelect(target, source);
}
/**
* Transfer an option from the main subcategories selectbox to the selected subcategories selectbox
*/
function selectSubcategory()
{
	var target = 'subcategoryIds';
	var source = 'availableSubcategory';
	coreSelectToSelect(target, source);
}
/**
* Transfer an option from the selected subcategories selectbox to the main subcategories selectbox
*/
function discardSubcategory()
{
	var target = 'availableSubcategory';
	var source = 'subcategoryIds';
	coreSelectToSelect(target, source);
}
/**
* On submit, select all options in select boxes -- this allows PHP to pick up those options
*/
function selectAll()
{
	selectAllProcess('categoryIds');
	selectAllProcess('subcategoryIds');
}
/**
* Select selected options
*/
function selectAllProcess(box)
{
	var element = box;
	var obj = coreGetElementById(element);
	if(obj == null)
		return;
	for(i = obj.options.length - 1; i >= 0; i--)
		obj.options[i].selected = true;
}