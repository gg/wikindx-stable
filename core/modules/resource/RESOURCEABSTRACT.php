<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* RESOURCEABSTRACT class
*
* View resource's abstract
*/
class RESOURCEABSTRACT
{
private $db;
private $vars;
private $session;
private $messages;
private $user;
private $icons;
private $common;
private $config;
private $cite;
private $userId;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->session = FACTORY_SESSION::getInstance();

		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->user = FACTORY_USER::getInstance();
		$this->icons = FACTORY_LOADICONS::getInstance();
		$this->common = FACTORY_RESOURCECOMMON::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->cite = FACTORY_CITE::getInstance();
		$this->userId = $this->session->getVar('setup_UserId');
	}
// Display resource's abstract
	public function view($row)
	{
		$abstract = array();
		$write = $this->session->getVar('setup_Write') ? TRUE : FALSE;
		if(!$row['resourcetextAbstract'] && !$write)
			return $abstract;
		if($this->session->getVar("setup_Superadmin") ||
			($write && (!$this->config->WIKINDX_ORIGINATOR_EDITONLY || ($row['resourcemiscAddUserIdResource'] == $this->userId))))
		{
			if(!$row['resourcetextAbstract'])
			{
				$abstract['title'] = $this->messages->text("resources", "abstract");
				$abstract['editLink'] = \HTML\a($this->icons->addLink, $this->icons->add,
					"index.php?action=metadata_EDITMETADATA_CORE" .
					htmlentities("&type=abstractInit&id=" . $row['resourceId']));
				return $abstract;
			}
			else if($row['resourcetextAbstract'])
			{
				$abstract['editLink'] = \HTML\a($this->icons->editLink, $this->icons->edit,
					"index.php?action=metadata_EDITMETADATA_CORE" .
					htmlentities("&type=abstractInit&id=" . $row['resourceId']));
				$abstract['deleteLink'] = \HTML\a($this->icons->deleteLink, $this->icons->delete,
					"index.php?action=metadata_EDITMETADATA_CORE" .
					htmlentities("&type=abstractDeleteInit&id=" . $row['resourceId']));
			}
		}
		if($row['resourcetextAbstract'])
		{
			$abstract['title'] = $this->messages->text("resources", "abstract");
			list($abstract['userAdd'], $abstract['userEdit']) = $this->user->displayUserAddEdit($row, TRUE, 'abstract');
			$abstract['abstract'] =
				$this->cite->parseCitations($this->common->doHighlight(\HTML\dbToHtmlTidy($row['resourcetextAbstract'])), 'html');
		}
		return $abstract;
	}
}
?>