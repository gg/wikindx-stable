<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
*	CHOOSEBIB class.
*
*	User bibliographies
*/
class CHOOSEBIB
{
private $db;
private $vars;
private $session;
private $errors;
private $messages;
private $success;
private $badInput;
private $common;

	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->session = FACTORY_SESSION::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->success = FACTORY_SUCCESS::getInstance();


		$this->badInput = FACTORY_BADINPUT::getInstance();
		$this->common = FACTORY_BIBLIOGRAPHYCOMMON::getInstance();
	}
// List user's bibliographies with options to use one of them or the WIKINDX master bibliography when listing, searching etc.
// Also option to view bibliography description and bibliography owner.
	public function init($message = FALSE)
	{
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "bibs"));
		$pString = $message ? $message : '';
		$pString .= \FORM\formHeader("bibliography_CHOOSEBIB_CORE");
		$bibsArray = $this->common->getBibsArray();
		$selected = $this->session->getVar('mywikindx_Bibliography_use');
		$size = sizeof($bibsArray);
		if($size > 20)
			$size = 10;
		if($selected)
			$pString .= \HTML\p(\FORM\selectedBoxValue(FALSE, "BibId", $bibsArray, $selected, $size));
		else
			$pString .= \HTML\p(\FORM\selectFBoxValue(FALSE, "BibId", $bibsArray, $size));
// Radio buttons are the querystring's method
		if(!$this->session->getVar('mywikindx_Bibliography_method'))
			$checked = TRUE;
		else
			$checked = $this->session->getVar('mywikindx_Bibliography_method') == 'displayBib' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "method", "displayBib", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "displayBib"));
		$checked = $this->session->getVar('mywikindx_Bibliography_method') == 'useBib' ? TRUE : FALSE;
		$pString .= \HTML\p(\FORM\radioButton(FALSE, "method", "useBib", $checked) . "&nbsp;&nbsp;" .
			$this->messages->text("user", "useBib"));
		$pString .= \HTML\p(\FORM\formSubmit("Proceed"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// Display bibliography details and owner's details
	public function displayBib()
	{
		$pString = '';
		if(!array_key_exists('BibId', $this->vars) || !$this->vars['BibId'])
			$pString .= $this->messages->text("user", "masterBib");
		else if(array_key_exists('BibId', $this->vars) && ($this->vars['BibId'] < 0))
			$this->badInput->close($this->errors->text("inputError", "invalid"), $this, 'init');
		else
		{
			$this->db->leftJoin('user_bibliography_resource', $this->db->formatFields('userbibliographyresourceBibliographyId'),
				$this->db->tidyInput($this->vars['BibId']), FALSE);
			$this->db->leftJoin('users', 'userbibliographyUserId', 'usersId');
			$this->db->formatConditions(array('userbibliographyId' => $this->vars['BibId']));
			$recordset = $this->db->selectCounts('user_bibliography', 'userbibliographyresourceBibliographyId',
				array('userbibliographyTitle', 'usersFullname', 'usersUsername', 'userbibliographyDescription'));
			$row = $this->db->fetchRow($recordset);
			$text = \HTML\strong($this->messages->text("user", "username") . ":&nbsp;&nbsp;") .
				\HTML\dbToHtmlTidy($row['usersUsername']) . BR;
			$text .= \HTML\strong($this->messages->text("user", "fullname") . ":&nbsp;&nbsp;") .
				\HTML\dbToHtmlTidy($row['usersFullname']);
			$pString = \HTML\p($text);
			$text = '';
			if($row['count'])
				$text .= \HTML\strong($this->messages->text("user", "numResources") . ":&nbsp;&nbsp;") .
					$row['count'] . BR;
			$text .= \HTML\strong($this->messages->text("user", "bibTitle") . ":&nbsp;&nbsp;") .
				\HTML\dbToHtmlTidy($row['userbibliographyTitle']) . BR;
			$text .= \HTML\strong($this->messages->text("user", "bibDescription") . ":&nbsp;&nbsp;") .
				\HTML\dbToHtmlTidy($row['userbibliographyDescription']);
			$pString .= \HTML\p($text);
		}
		GLOBALS::setTplVar('heading', $this->messages->text("heading", "bibs"));
		$pString .= \FORM\formHeader("bibliography_CHOOSEBIB_CORE");
		$pString .= \FORM\hidden("method", "useBib");
		$pString .= \FORM\hidden("BibId", $this->vars['BibId']);
		$pString .= \HTML\p($this->messages->text("user", "useBib") . '&nbsp;&nbsp;' .
			\FORM\formSubmit("Proceed"));
		$pString .= \FORM\formEnd();
		GLOBALS::addTplVar('content', $pString);
	}
// Set a bibliography for browsing
	public function useBib()
	{
// bibId of 0 == master bibliography
// bibId of < 0 == a label
		if(!array_key_exists('BibId', $this->vars) || !$this->vars['BibId'])
			$this->session->delVar('mywikindx_Bibliography_use');
		else if(array_key_exists('BibId', $this->vars) && ($this->vars['BibId'] < 0))
			$this->badInput->close($this->errors->text("inputError", "invalid"), $this, 'init');
		else
			$this->session->setVar('mywikindx_Bibliography_use', $this->vars['BibId']);
		$this->session->delVar('mywikindx_Bibliography_add');
		$this->session->delVar('mywikindx_PagingStart');
		$this->session->delVar('mywikindx_PagingStartAlpha');
		$this->session->delVar('sql_LastMulti');
		$this->init($this->success->text("bibliographySet"));
	}
}
?>