<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Interface for AJAX elements in various pages.
*
*
* @version 2
* @author Mark Grimshaw
* @package wikindx5\core\ajax
*/
namespace AJAX
{
/**
* AJAX
*/
/**
* Load the ajax javascript and the user javascript(s) into the HTML page output.
*
* @param mixed $scripts Either an array of .js scripts to load or a single string.  It can be FALSE.
*/
	function loadJavascript($scripts = FALSE)
	{
		$pString = '';
		if(!is_array($scripts)) $scripts = array($scripts);
		foreach($scripts AS $script)
		{
			$pString .= \HTML\jsInlineExternal($script);
		}
		\GLOBALS::addTplVar('content', $pString);
	}
/**
* Create and load the HTML form element insert for the AJAX action and function.
*
* @param string $action The javascript action such as 'onclick' etc.
* @param array $jsonArray The parameter to be passed to gateway() as an array ready to be converted to JSON
* @param boolean $return If TRUE, generated javascript has a return from the gateway() function. Default is FALSE.
*/
	function jActionForm($action, $jsonArray, $return = FALSE)
	{
		$json = \AJAX\encode_jArray($jsonArray, TRUE);
		if($return) $return = 'return';
		return "$action=\"$return gateway('$json');\"";
	}
/**
* Create and load the IMG element insert for the AJAX action and function.  Works with core/display/LOADICONS.php.
*
* @param string $icon The icon to make an image of ('add', 'delete', 'view' etc.)
* @param string $action The javascript action such as 'onclick' etc.
* @param array $jsonArray The parameter to be passed to gateway() as an array ready to be converted to JSON
* @param boolean $return If TRUE, generated javascript has a return from the gateway() function. Default is FALSE.
* @return string The image tag
*/
	function jActionIcon($icon, $action, $jsonArray, $return = FALSE)
	{
		if($return) $return = 'return';
		$icons = \FACTORY_LOADICONS::getInstance();
		$json = \AJAX\encode_jArray($jsonArray, TRUE);
		$icons->jscriptActions[$icon] = "$action=\"$return gateway('$json')\"";
		$icons->resetSetup();
		$icons->setupIcons($icon);
		return \HTML\span($icons->{$icon}, 'cursorPointer');
	}
/**
* Convert $jsonArray to JSON string and format any array elements referencing javascript functions.
*
* @param array $jsonArray The unformatted JSON array
* @param boolean $quote No encoding of quotation marks (") if set to FALSE. Default is FALSE.
* @return string The JSON string
*/
	function encode_jArray($jsonArray, $quote = FALSE)
	{
		if($quote)
		{
		    array_walk_recursive($jsonArray, function (&$value) { $value = addslashes($value); });
		    $json = json_encode($jsonArray);
			\AJAX\_json_error('encode');
			$json = htmlspecialchars($json, ENT_QUOTES);
		}
		else
		{
    		$json = json_encode($jsonArray);
    		\AJAX\_json_error('encode');
		}
    	return $json;
	}
/**
* Convert JSON-formatted $jsonString to an array or object.
*
* @param string $jsonString The JSON string
* @return mixed Array or object
*/
	function decode_jString($jsonString)
	{
	    // Always return an associative array
		$return = json_decode(stripslashes($jsonString), TRUE);
		\AJAX\_json_error('decode');
		return $return;
	}
/**
* echo JSON error or return if none.
*
* @param string $type
*/
	function _json_error($type)
	{
		if (json_last_error() != JSON_ERROR_NONE)
		{
			\GLOBALS::addTplVar('content', 'JSON ' . $type . ' error - ' . json_last_error_msg() . ': ');
		}
	}
}
?>