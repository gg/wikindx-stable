<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* Common FILE routines
*
* @version	1
*
*	@package wikindx5\core\file
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
namespace FILE
{
/**
* Set download headers
*
* @param string $type
* @param int $size
* @param string $filename
* @param string $lastmodified
* @param string $charset Default is ''
*/
	function setHeaders($type, $size, $filename, $lastmodified, $charset = '')
	{
		header("Content-type: $type" . (($charset != '') ? "; charset=$charset" : ''));
		header("Content-Disposition: inline; filename=\"$filename\"; size=\"$size\"");
		header("Content-Length: $size" );
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Pragma: public");
		header("Last-Modified: $lastmodified");
	}

/**
* Download file to user
*
* From http://uk3.php.net/function.readfile
* @param string $file
*/
	function readfile_chunked($file)
	{

		if ($handle = fopen($file, 'rb'))
		{
		    // Always clear the buffer before sending
		    // because sometimes there are barbage
		    // and it's very difficult to find
		    // where it is badded to the buffer
		    ob_get_clean();
		    ob_start();

    		$chunksize = (1024 * 1024); // how many bytes per chunk

    		// Never remove the buffer variable otherwise the fread()
    		// while not send the data to the output
    		$buffer = '';

		    // Send the file by chunk
			while (!feof($handle))
			{
				$buffer = fread($handle, $chunksize);
				echo $buffer;
				ob_flush();
				flush();
			}

			fclose($handle);
		}
		else
			return false;
	}

/**
* create a fileName for this file.  If directory based on session ID does not exist, create it.
*
* @param string $dirName
* @param string $string File contents
* @param string $extension File extension
*
* @return array (filename, full filepath)
*/
	function createFileName($dirName, $string, $extension)
	{
		$fileName = sha1(session_id() . $string) . $extension;
		return array($fileName, $dirName . DIRECTORY_SEPARATOR . $fileName);
	}

/**
* Get file max upload size
*
* @return int
*/
	function fileMaxSize()
	{
		$postMax = return_bytes(ini_get('post_max_size'));
		$uploadMax = return_bytes(ini_get('upload_max_filesize'));
		if($postMax < $uploadMax)
			return $postMax;
		else
			return $uploadMax;
	}

/**
* Convert some ini values to numerical values (to bytes)
*
* @param string $val
* @return int
*/
	function return_bytes($val)
	{
		// cf. https://secure.php.net/manual/en/ini.core.php#ini.post-max-size
		// cf. https://secure.php.net/manual/en/faq.using.php#faq.using.shorthandbytes

		// Unit extraction
		$val = mb_strtolower(trim($val));
		$unit = mb_substr($val, -1);

	    // If the unit is not defined, VAL is already in bytes
	    // Otherwise, compute it
	    if (!is_int($unit))
	    {
    	    $val = intval(mb_substr($val, 0, mb_strlen($val) - 1));

    	    $factor = 1024;
    		switch($unit)
    		{
                case 'g':
                    $val *= $factor;
                case 'm':
                    $val *= $factor;
                case 'k':
                    $val *= $factor;
    		}
		}

		return intval($val);
	}

/**
* Enumerate files and subdirectories of a directory except . and .. subdirectories
*
* @param string $dir A directory to explore
* @return array An array of file and subdirectory names
*/
	function dirToArray($dir)
	{
	    $result = array();

	    if (file_exists($dir))
	    {
    	    $cdir = scandir($dir);

    	    if ($cdir !== FALSE)
    	    {
    	        foreach ($cdir as $v)
    	        {
    	            if (!in_array($v, array('.', '..')))
    	                $result[] = $v;
    	        }
    	    }

    	    unset($cdir);
	    }

	    return $result;
	}

/**
* Enumerate subdirectories of a directory except . and .. subdirectories
*
* @param string $dir A directory to explore
* @return array An array of subdirectory names
*/
	function dirInDirToArray($dir)
	{
	    $result = array();

	    $cdir = dirToArray($dir);

	    if (count($cdir) > 0)
	    {
	        foreach ($cdir as $v)
	        {
	            if (is_dir($dir . DIRECTORY_SEPARATOR . $v))
	                $result[] = $v;
	        }
	    }

	    unset($cdir);

	    return $result;
	}

/**
* Enumerate files of a directory except . and .. subdirectories
*
* @param string $dir A directory to explore
* @return array An array of filenames
*/
	function fileInDirToArray($dir)
	{
	    $result = array();

	    $cdir = dirToArray($dir);

	    if (count($cdir) > 0)
	    {
	        foreach ($cdir as $v)
	        {
	            if (is_file($dir . DIRECTORY_SEPARATOR . $v))
	                $result[] = $v;
	        }
	    }

	    unset($cdir);

	    return $result;
	}

/**
* Return filename, hash, type and size of an uploaded file or an array of such information for each file uploaded
*
* @param string $filename
* @param boolean multiple files (default FALSE)
* @return array (filename, hash, type, size) or array of these
*/
	function fileUpload($filename = FALSE, $multiple = FALSE)
	{
		if(isset($_FILES) && array_key_exists('file', $_FILES))
		{
			$finfo = new \finfo(FILEINFO_MIME); // return mime type ala mimetype extension
			if($multiple)
			{
				$fileArrays = rearrangeFilesArray($_FILES);
				$array = array();
				$fileArray = $fileArrays['file'];
				foreach($fileArray['name'] as $index => $value)
				{
					if(!$value)
						return array();
					if(trim($filename))
						$fileName = trim($filename);
					else
						$fileName = addslashes($value);
					if(($fileName == '.') || ($fileName == '..'))
						continue;
					$info = \UTF8::mb_explode(';', $finfo->file($fileArray['tmp_name'][$index]));
					$array[] = array($fileName, sha1_file($fileArray['tmp_name'][$index]), $info[0], $fileArray['size'][$index], $index);
				}
				return $array;
			}
			else if($_FILES['file']['tmp_name'])
			{
				if(trim($filename))
					$fileName = trim($filename);
				else
					$fileName = addslashes($_FILES['file']['name']);
				if(($fileName == '.') || ($fileName == '..'))
					return array(FALSE, FALSE, FALSE, FALSE);
				$info = \UTF8::mb_explode(';', $finfo->file($_FILES['file']['tmp_name']));
				return array($fileName, sha1_file($_FILES['file']['tmp_name']),
					$info[0], $_FILES['file']['size']);
			}
			else
				return array(FALSE, FALSE, FALSE, FALSE);
		}
		return array(FALSE, FALSE, FALSE, FALSE);
	}
/**
* Rearrange the $_FILES array for multiple file uploads
*
* @param array
* @return array
*/
	function rearrangeFilesArray($files)
	{
		$names = array( 'name' => 1, 'type' => 1, 'tmp_name' => 1, 'error' => 1, 'size' => 1);
		foreach($files as $key => $part) 
		{
			// only deal with valid keys and multiple files
			$key = (string) $key;
			if(isset($names[$key]) && is_array($part)) 
			{
				foreach ($part as $position => $value)
					$files[$position][$key] = $value;
// remove old key reference
				unset($files[$key]);
			}
		}
		return $files;
	}

/**
* Store uploaded file in given directory with given name
*
* @param string $dirName
* @param string $name
* @param int non-FALSE if moving multiple file uploads
* @return boolean
*/
	function fileStore($dirName, $name, $index = FALSE)
	{
		if($index !== FALSE)
		{
			if(!move_uploaded_file($_FILES['file']['tmp_name'][$index], $dirName . DIRECTORY_SEPARATOR . $name))
				return FALSE;
		}
		else
		{
			if(!move_uploaded_file($_FILES['file']['tmp_name'], $dirName . DIRECTORY_SEPARATOR . $name))
				return FALSE;
		}
		return TRUE;
	}

/**
* list and HTML format all files for sessionID
*
* @return array (filesDir, fileDeleteSecs, fileArray)
*/
	function listFiles()
	{
		$session = \FACTORY_SESSION::getInstance();
	    $filesDir = \FACTORY_CONFIG::getInstance()->WIKINDX_FILES_DIR;

		$fileExports = $session->getVar('fileExports');
		if(!$fileExports) // no files in directory
			return array(FALSE, FALSE, FALSE);
		if(!file_exists($filesDir))
			return array(FALSE, FALSE, TRUE);

		$fileArray = array();

		if($fileExports)
		{
			$sessArray = unserialize($fileExports);

			# Without Wikindx special files
			$sessArray = array_diff($sessArray, array('index.html', 'CVS', 'FILES'));
			$files = array_intersect($sessArray, fileInDirToArray($filesDir));

			foreach ($files as $file)
			{
			    # Without hidden files
				if(mb_strpos($file, '.') === 0)
					continue;
				$fileArray[$file] = filemtime($filesDir . DIRECTORY_SEPARATOR . $file);
			}

			asort($fileArray, SORT_NUMERIC);
			$fileArray = array_reverse($fileArray);
		}

		$co = \FACTORY_CONFIGDBSTRUCTURE::getInstance();
		$db = \FACTORY_DB::getInstance();
		$db->formatConditions(array('configName' => 'configFileDeleteSeconds'));
		$row = $db->selectFirstRow('config', '*');
		$deleteSecs = $row[$co->dbStructure[$row['configName']]];
		return array($filesDir, $deleteSecs, $fileArray);
	}

/**
* tidy up the files directory by removing all files and folders older than 'configFileDeleteSeconds'
*/
	function tidyFiles()
	{
		$session = \FACTORY_SESSION::getInstance();
	    $filesDir = \FACTORY_CONFIG::getInstance()->WIKINDX_FILES_DIR;

	    if(file_exists($filesDir))
		{
		    $dh = opendir($filesDir);

			$now = time();
			$co = \FACTORY_CONFIGDBSTRUCTURE::getInstance();
			$maxTime = $co->getOne('configFileDeleteSeconds');
			$fileDeleteArray = $fileKeepArray = array();
			while(($f = readdir($dh)) !== false)
			{
				if(($f == 'CVS') || ($f == 'FILES') || (mb_strpos($f, '.') === 0))
					continue;
				$file = $filesDir . DIRECTORY_SEPARATOR . $f;
				if(($now - filemtime($file)) >= $maxTime)
				{
					@unlink($file);
					$fileDeleteArray[] = $f;
				}
				else
					$fileKeepArray[] = $f;
			}

			closedir($dh);

			// Remove reference to these files in session
			if(($sessVar = $session->getVar('fileExports')) && !empty($fileDeleteArray))
			{
				$sessArray = unserialize($sessVar);
				foreach($fileDeleteArray as $f)
					unset($sessArray[array_search($f, $sessArray)]);
				if(!empty($sessArray))
					$session->setVar('fileExports', serialize($sessArray));
				else
					$session->delVar('fileExports');
			}
			else if(!empty($fileKeepArray))
				$session->setVar('fileExports', serialize($fileKeepArray));
		}
		else
			// failure is an option;
			return;
	}

/**
* Zip up an array of files.  File is stored in files dir.
*
* @param array $files unqualified filenames (key is label of file, value is filename on disk)
* @param string $path file path
* @return mixed unqualified SHA1'ed filename of zip or FALSE if failure
*/
	function zip($files, $path)
	{
		// Compute a filename with a SHA1 hash of all filenames concatenated
		$zipFile  = \FACTORY_CONFIG::getInstance()->WIKINDX_FILES_DIR;
		$zipFile .= DIRECTORY_SEPARATOR . sha1(implode('', $files)) . '.zip';

		// If we can't create a Zip archive or add all files to it,
		// abort, clean Zip archive and return FALSE
		$allFilesZipped = TRUE;
		if(!class_exists('ZipArchive'))
			return FALSE;
		$zip = new \ZipArchive;

		if ($zip->open($zipFile, \ZIPARCHIVE::CREATE))
		{
			foreach($files as $label => $file)
				if (!$zip->addFile($path . DIRECTORY_SEPARATOR . $file, $label))
				{
					$allFilesZipped = FALSE;
					break;
				}

			$zip->close();
		}
		else
			$allFilesZipped = FALSE;


		if ($allFilesZipped)
			return $zipFile;
		else
		{
			if (file_exists($zipFile))
				unlink($zipFile);

			return FALSE;
		}
	}

/**
* Does an unix command exist?
*
* @param string $command Command to test with the default shell
* @return boolean TRUE on success
*/
    function command_exists($command)
    {
        if (suhosin_function_exists('proc_open'))
        {
            $whereIsCommand = (PHP_OS == 'WINNT') ? 'where' : 'which';

            $process = proc_open(
                "$whereIsCommand $command",
                array(
                    0 => array("pipe", "r"), //STDIN
                    1 => array("pipe", "w"), //STDOUT
                    2 => array("pipe", "w"), //STDERR
                ),
                $pipes
            );

            if ($process !== FALSE)
            {
                $stdout = stream_get_contents($pipes[1]);
                $stderr = stream_get_contents($pipes[2]);
                fclose($pipes[1]);
                fclose($pipes[2]);
                proc_close($process);

                return ($stdout != '');
            }
        }

        // Fallback
        return FALSE;
    }

/**
* Does a function exist ou is enabled with or without the suhosin security extension?
*
* https://suhosin.org/stories/index.html
* Author webmaster@mamo-net.de
*
* @param string $func Function name
* @return boolean TRUE if the function is enabled
*/
    function suhosin_function_exists($func)
    {
        if (extension_loaded('suhosin')) {
            $suhosin = @ini_get("suhosin.executor.func.blacklist");
            if (empty($suhosin) == false) {
                $suhosin = explode(',', $suhosin);
                $suhosin = array_map('trim', $suhosin);
                $suhosin = array_map('strtolower', $suhosin);
                return (function_exists($func) == true && array_search($func, $suhosin) === false);
            }
        }
        return function_exists($func);
    }
}
?>