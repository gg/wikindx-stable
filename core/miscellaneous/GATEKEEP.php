<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* GATEKEEP
*
* Test authority to do write operations.
*
* @version	1
*
*	@package wikindx5\core\miscellaneous
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class GATEKEEP
{
/** object */
private $session;
/** object */
private $config;
/** boolean */
public $requireSuper = FALSE;

/**
* GATEKEEP class
*/
	public function __construct()
	{
		$this->session = FACTORY_SESSION::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
	}
/**
* Admins can do everything
*
* Either return TRUE or the login prompt
* @param boolean $globalEdit If TRUE, config.php's $WIKINDX_GLOBAL_EDIT must be be set to TRUE. Default is FALSE
* @param boolean $originatorEditOnly If TRUE, config.php's $WIKINDX_ORIGINATOR_EDITONLY must be TRUE. Default is FALSE
*/
	public function init($globalEdit = FALSE, $originatorEditOnly = FALSE)
	{
		if($this->session->getVar("setup_Superadmin"))
			return TRUE;
		else if($this->requireSuper)
		{
			$authorize = FACTORY_AUTHORIZE::getInstance();
			$authorize->initLogon();
			FACTORY_CLOSENOMENU::getInstance(); // die
		}
		if($this->session->getVar('setup_Write'))
		{
			if($globalEdit && isset($this->config->WIKINDX_GLOBAL_EDIT) && !$this->config->WIKINDX_GLOBAL_EDIT)
			{
				$authorize = FACTORY_AUTHORIZE::getInstance();
				$authorize->initLogon();
				FACTORY_CLOSENOMENU::getInstance(); // die
			}
			if($originatorEditOnly && isset($this->config->WIKINDX_ORIGINATOR_EDITONLY) &&
				!$this->config->WIKINDX_ORIGINATOR_EDITONLY)
			{
				$authorize = FACTORY_AUTHORIZE::getInstance();
				$authorize->initLogon();
				FACTORY_CLOSENOMENU::getInstance(); // die
			}
			return TRUE;
		}
// Failure to authorize so provide logon prompt
		$authorize = FACTORY_AUTHORIZE::getInstance();
		$authorize->initLogon();
		FACTORY_CLOSENOMENU::getInstance(); // die
	}
}
?>