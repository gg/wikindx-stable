<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* DATE
*
* Common date and time functions
*
* @version	1
*
*	@package wikindx5\core\miscellaneous
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class DATE
{

/**
* DATE
*
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
	}
/**
* Take input from HTML FORM <input type=date> and split into separate fields.
* Date comes in as 'yyyy-mm-dd' (but displayed on web form as 'dd / mm / yyyy').
* All three fields must have a valid value else the form input is FALSE. This should be tested before calling this function.
*
* @param string $dateInput
* @return array array(year, month, day)
*/
	public function splitDate($dateInput)
	{
			$date = UTF8::mb_explode('-', $dateInput);
			return array($date[0], $date[1], $date[2]);
	}
}
?>