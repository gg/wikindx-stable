<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* ATTACHMENT
*
* Handle attachments
*
* @version	1
*
*	@package wikindx5\core\miscellaneous
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class ATTACHMENT
{
/** object */
private $db;
/** object */
private $config;
/** array */
private $vars;
/** string */
public $primary = FALSE;

/**
* ATTACHMENT
*
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->config = FACTORY_CONFIG::getInstance();
	}
/**
* Make hyperlink of attachment
*
* @param array $row
* @param boolean $list
* @param boolean $reduce
* @param boolean $hyperlink
* @return string
*/
	public function makeLink($row, $list = FALSE, $reduce = TRUE, $hyperlink = TRUE)
	{
		$id = $row['resourceattachmentsId'];
		$icons = FACTORY_LOADICONS::getInstance();
		$session = FACTORY_SESSION::getInstance();

		if(!$list)
		{
			if(!$hyperlink)
				return "index.php?action=attachments_ATTACHMENTS_CORE" .
					htmlentities("&method=downloadAttachment&id=$id&filename=" . $row['resourceattachmentsHashFilename']);
			else
			{
    			if(array_key_exists('resourceattachmentsFileName', $row) && !empty($row['resourceattachmentsFileName']))
            		$name = \HTML\dbToHtmlTidy($row['resourceattachmentsFileName']) . " " .
            			$icons->getIconForAFileExtension($row['resourceattachmentsFileName']);
            	else
            	    $name = $icons->file;
    			return \HTML\a($icons->fileLink, $name, "index.php?action=attachments_ATTACHMENTS_CORE" .
    				htmlentities("&method=downloadAttachment&id=$id&filename=" . $row['resourceattachmentsHashFilename']), "_new");
    		}
		}
		else
		{
			if(array_key_exists('resourceattachmentsFileName', $row) && !empty($row['resourceattachmentsFileName']))
			{
				$name = \HTML\dbToHtmlTidy($row['resourceattachmentsFileName']) . " " .
					$icons->getIconForAFileExtension($row['resourceattachmentsFileName']);
				$label = $row['resourceattachmentsFileName'];
			}
			else
			{
				if($reduce)
					$name = \FORM\reduceLongText(\HTML\dbToHtmlTidy($row['resourceattachmentsFileName']));
				else
					$name = \HTML\dbToHtmlTidy($row['resourceattachmentsFileName']);
			}
			if(!$hyperlink)
				return "index.php?action=attachments_ATTACHMENTS_CORE" .
					htmlentities("&method=downloadAttachment&id=$id&filename=" . $row['resourceattachmentsHashFilename']);
			else
    			return \HTML\a('link', $name, "index.php?action=attachments_ATTACHMENTS_CORE" .
    				htmlentities("&method=downloadAttachment&id=$id&filename=" . $row['resourceattachmentsHashFilename']), "_new");
		}
	}
/**
* List all attachments for this resource
*
* @param int $resourceId
* @return array
*/
	public function listFiles($resourceId)
	{
		$array = array();
		$this->db->formatConditions(array('resourceattachmentsResourceId' => $resourceId));
		$this->db->orderBy('resourceattachmentsFileName');
		$recordSet = $this->db->select('resource_attachments', array('resourceattachmentsHashFilename',
			'resourceattachmentsFileName', 'resourceattachmentsPrimary'));
		while($row = $this->db->fetchRow($recordSet))
		{
			$array[$row['resourceattachmentsHashFilename']] = \HTML\dbToFormTidy($row['resourceattachmentsFileName']);
			if($row['resourceattachmentsPrimary'] == 'Y')
				$this->primary = $row['resourceattachmentsHashFilename'];
		}
		return $array;
	}
/**
* Increment the accesses and downloads counter for this resource
*
* @param int $id
*/
	public function incrementDownloadCounter($id)
	{
		$this->db->formatConditions(array('resourceattachmentsId' => $id));
		$this->db->updateSingle('resource_attachments',
			$this->db->formatFields('resourceattachmentsDownloads') . "=" .
			$this->db->formatFields('resourceattachmentsDownloads') . "+" . $this->db->tidyInput(1));
		$this->db->formatConditions(array('resourceattachmentsId' => $id));
		$this->db->updateSingle('resource_attachments',
			$this->db->formatFields('resourceattachmentsDownloadsPeriod') . "=" .
			$this->db->formatFields('resourceattachmentsDownloadsPeriod') . "+" . $this->db->tidyInput(1));
	}
/**
* 1. Check for any rows in table that do not have attachments – if found, delete the row
* 2. Check for attachments not in the table – if found, delete the attachment
*/
	public function checkAttachmentRows()
	{
// Delete uncorrelated rows
		$deletes = array();
		$recordSet = $this->db->select('resource_attachments', array('resourceattachmentsId', 'resourceattachmentsHashFilename'));
		while($row = $this->db->fetchRow($recordSet))
		{
			$fileName = $this->config->WIKINDX_ATTACHMENTS_DIR . DIRECTORY_SEPARATOR . $row['resourceattachmentsHashFilename'];
			if(!file_exists($fileName))
			{
				$deletes[] = $row['resourceattachmentsId'];
				$fileNameCache = $this->config->WIKINDX_ATTACHMENTSCACHE_DIR . DIRECTORY_SEPARATOR . $row['resourceattachmentsHashFilename'];
				if(file_exists($fileNameCache))
					@unlink($fileNameCache);
			}
		}
		if(!empty($deletes))
		{
			foreach($deletes AS $id)
			{
				$this->db->formatConditions(array("resourceattachmentsId" => $id));
				$this->db->delete('resource_attachments');
			}
		}
// Delete uncorrelated attachments
		$deletes = $files = array();
	    $cdir = FILE\dirToArray($this->config->WIKINDX_ATTACHMENTS_DIR);
	    if(count($cdir) > 0)
	    {
	        foreach($cdir as $v)
	        {
	        	if(($v == 'ATTACHMENTS') || ($v == 'index.html'))
	        		continue;
	            if(is_file($this->config->WIKINDX_ATTACHMENTS_DIR . DIRECTORY_SEPARATOR . $v))
	                $files[] = $v;
	        }
			foreach($files as $file)
			{
				$this->db->formatConditions(array('resourceattachmentsHashFilename' => $file));
				if(!$this->db->numRows($this->db->select('resource_attachments', 'resourceattachmentsId')))
					$deletes[] = $file;
			}
			foreach($deletes AS $file)
			{
				$fileName = $this->config->WIKINDX_ATTACHMENTS_DIR . DIRECTORY_SEPARATOR . $file;
				@unlink($fileName);
				$fileNameCache = $this->config->WIKINDX_ATTACHMENTSCACHE_DIR . DIRECTORY_SEPARATOR . $file;
				@unlink($fileNameCache);
			}
	    }
	}
}
?>