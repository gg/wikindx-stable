<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* UPDATEDATABASE
*
* Update the database as required
*
* @version	1
*
*	@package wikindx5\core\startup
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class UPDATEDATABASE
{
/** object */
private $session;
/** object */
private $config;
/** object */
private $db;
/** array */
private $vars;
/** object */
private $messages;
/** object */
private $errors;
/**  int */
private $oldTime;
/** string */
private $stageInterruptMessage = FALSE;
/** string */
private $statusString;
/** string */
private $email = FALSE;
/** integer */
private $numStages;
/** boolean */
public $upgradeCompleted = FALSE;
/**
* UPDATEDATABASE
*/
	public function __construct()
	{
// Turn on error reporting:
        error_reporting(E_ALL);
		ini_set('display_errors', 'On');

		$this->db = FACTORY_DB::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->session = FACTORY_SESSION::getInstance();

		$this->messages = FACTORY_MESSAGES::getInstance();
		$this->errors = FACTORY_ERRORS::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->oldTime = time();

		// Before upgrade process, clear all template cache
		$this->config->WIKINDX_BYPASS_SMARTYCOMPILE = TRUE;
		FACTORY_TEMPLATE::getInstance()->clearAllCache();

		$this->checkDatabase();

// Restore defaut error reporting level
        $this->session->clearSessionData();
		FACTORY_LOADCONFIG::getInstance()->load();
		FACTORY_LOADCONFIG::getInstance()->configureErrorReporting();

		if(GLOBALS::tplVarExists('content'))
			FACTORY_CLOSE::getInstance(); // die;
	}
/**
* Check and print status of update scripts
*
* @param string $stage
*/
	private function checkStatus($stage)
	{
		$pString = "MAX EXECUTION TIME: " . ini_get("max_execution_time") .  ' secs' . BR;
		$pString .= "ELAPSED TIME ($stage): ";
		$pString .= time() - $this->oldTime . " secs.";
		$pString .= BR;
		$pString .= "Database queries: " . GLOBALS::getDbQueries() . BR;
		$pString .= 'MEMORY LIMIT: ' . ini_get("memory_limit") . BR;
		$pString .= 'MEMORY USED: ' . memory_get_peak_usage() / 1000000 . 'MB';
		$this->statusString = $pString;
	}
/**
* We know we have a database as, if we've reached this stage, we're able to connect to it.
*
* Here,
* 1/ we check we have tables, if not, populate the database with tables and set defaults in
* database_summary and category table and
* 2/ populate the config table displaying configuration interface if necessary
* If no admins yet exist, ask for admin configuration to force the input of at least one admin username/password.
*/
	private function checkDatabase()
	{
// Some users may have one database shared for several different purposes so check for presence of config table
// 1/
		if (!$this->db->tableExists('config'))
		{
			$this->createDbSchema();
			$this->configDefaults();
			$this->session->setVar("setup_Superadmin", TRUE); // required for gatekeep function in CONFIG.php
		}
		if(array_key_exists('action', $this->vars) && $this->session->getVar('setup_Superadmin') &&
			(($this->vars['action'] == 'continueExecution') || ($this->vars['action'] == 'upgradeDB')))
			$confirm = TRUE;
		else
			$confirm = FALSE;
		if(!$this->updateDatabase($confirm))
			GLOBALS::addTplVar('content', $this->error->text("dbError", "updateMySQL"));
// 2/
		$this->checkUsersTable();
	}
/**
* Update the database if required
*
* @param boolean $confirm
* @return boolean
*/
	private function updateDatabase($confirm)
	{
	    // At this stage problems with case sensitive name of tables can occur.
	    // So we use hand written SQL to avoid errors before an update of table names
		if($this->db->queryNoError('SELECT databasesummaryDbVersion FROM ' . $this->config->WIKINDX_DB_TABLEPREFIX . 'database_summary;') === FALSE)
		{
		    $dbVersion = $this->db->queryFetchFirstField('SELECT dbVersion FROM ' . $this->config->WIKINDX_DB_TABLEPREFIX . 'database_summary;');
		    $this->email = $this->db->queryFetchFirstField('SELECT contactEmail FROM ' . $this->config->WIKINDX_DB_TABLEPREFIX . 'config');
		}
		else
		{
		    $dbVersion = $this->db->queryFetchFirstField('SELECT databasesummaryDbVersion FROM ' . $this->config->WIKINDX_DB_TABLEPREFIX . 'database_summary;');
			if($dbVersion >= 5.4)
			{
				$this->db->formatConditions(array('configName' => 'configContactEmail'));
				$this->email = $this->db->queryFetchFirstField('SELECT configVarchar FROM ' . $this->config->WIKINDX_DB_TABLEPREFIX . 'config');
			}
			else
				$this->email = $this->db->queryFetchFirstField('SELECT configContactEmail FROM ' . $this->config->WIKINDX_DB_TABLEPREFIX . 'config');
		}

// WIKINDX 4 cleared out a lot of database upgrade code....
		if($dbVersion < 3.8)
		{
			GLOBALS::addTplVar('content', "Your WIKINDX database version is $dbVersion.  WIKINDX requires
				that you first upgrade to WIKINDX v3.8.2 before attempting to upgrade to the latest version.");
			FACTORY_CLOSENOMENU::getInstance(); // die
		}
		$wV = explode('.', WIKINDX_VERSION);
		$wVersion = $wV[0] . '.' . $wV[1];
		if($dbVersion < $wVersion) // v3.8 and beyond
		{
// As WIKINDX v5.3 (DB version 5.4) transfers config.php variables to the database, config.php must be writeable before we can proceed
			if($dbVersion < 5.4)
				$this->checkConfigFile(); // dies if not writeable or file does not exist.
			if(!$confirm)
			{
				$this->confirmUpdateDisplay($dbVersion);
				return FALSE;
			}

			// Disable temporarily all SQL mode to update old databases
			$this->db->setSqlMode('');

			if($dbVersion < 4.0)      // upgrade v3.8.2 to v4.1
			{
				$this->numStages = 16;
				$this->upgrade41();
			}
			else if($dbVersion < 4.2) // upgrade v4.1 to v4.2
			{
				$this->numStages = 1;
				$this->stage4_2__1();
			}
			else if($dbVersion < 5.1) // upgrade v4.2 to v5.1
			{
				$this->numStages = 1;
				$this->stage5_1__1();
			}
			else if($dbVersion < 5.2) // upgrade v5.1 to 5.2.2
			{
                $this->numStages = 1;
                $this->stage5_2__1();
			}
			else if($dbVersion < 5.4) // upgrade v5.2.2 to 5.4
			{
                $this->numStages = 1;
                $this->stage5_4__1();
			}
			else if($dbVersion < 5.5) // upgrade v5.4 to 5.5
			{
                $this->numStages = 1;
                $this->stage5_5__1();
			}
			else if($dbVersion < 5.6) // upgrade v5.5 to 5.6
			{
                $this->numStages = 1;
                $this->stage5_6__1();
			}
			else if($dbVersion < 5.7) // upgrade v5.6 to 5.7
			{
                $this->numStages = 1;
                $this->stage5_7__1();
			}
			else if($dbVersion < 5.8) // upgrade v5.7 to 5.8
			{
                $this->numStages = 1;
                $this->stage5_8__1();
			}
			$attachment = FACTORY_ATTACHMENT::getInstance();
			$attachment->checkAttachmentRows();
			$this->db->update('database_summary', array("databasesummaryDbVersion" => $wVersion));
			$this->session->delVar('upgrade_function');
			$this->upgradeCompleted = TRUE;
		}
		return TRUE;
	}
/**
* Check permissions on config.php
*/
	private function checkConfigFile()
	{
		$message = HTML\p("Part of the upgrade process for a WIKINDX younger than v5.3 is the transfer of many settings in config.php to the database (from
			where they can be configured via the Admin|Configure menu). In order to accomplish this, config.php must be writeable by the web server
			user and the upgrade will not proceed until this is done. Once the upgrade has completed, you can then return the file permissions on config.php
			to read only.");
		if(file_exists('config.php') && !is_writable('config.php'))
		{
			$permissions = mb_substr(sprintf('%o', fileperms('config.php')), -4);
			$message .= HTML\p("The permissions on config.php are currently: " . $permissions . ". The upgrade requires the file to be writeable.");
			die($message);
		}
		else if(!file_exists('config.php'))
			die("Fatal error: config.php does not exist.");
	}
/**
* Create the database structure with the definitions of the dbschema store
*
* @author Stéphane Aulery
* @version 1
* @param string $pluginPath is the path to the root directory of a plugin. Default is the constant DIRECTORY_SEPARATOR for the core
*/
	public function createDbSchema($pluginPath = DIRECTORY_SEPARATOR)
	{
		// The db schema is stored in a series of SQL file in the directory /dbschema/full for the core
		// or /plugins/<PluginDirectory>/dbschema/full
		$dbSchemaPath =
		    $this->session->getVar('wikindxBasePath')
		    . $pluginPath . 'dbschema'
    		. DIRECTORY_SEPARATOR . 'full';
		foreach(FILE\fileInDirToArray($dbSchemaPath) as $sqlfile)
		{
		    $sql = file_get_contents($dbSchemaPath . DIRECTORY_SEPARATOR . $sqlfile);
		    $sql = str_replace('%%WIKINDX_DB_TABLEPREFIX%%', $this->config->WIKINDX_DB_TABLEPREFIX, $sql);
		    $this->db->queryNoError($sql);
		}
	}
/**
* Update the database structure with the definitions of the dbschema store for a specific version
*
* @author Stéphane Aulery
* @version 1
* @param string $wkxVersion Version number of Wikindx
* @param string $pluginPath is the path to the root directory of a plugin. Default is the constant DIRECTORY_SEPARATOR for the core
*/
	public function updateDbSchema($wkxVersion, $pluginPath = DIRECTORY_SEPARATOR)
	{
		// The db schema is stored in a serie of SQL file in the directory /dbschema/update/<$wkxVersion> for the core
		// or /plugins/<PluginDirectory>/dbschema/update/<$wkxVersion>
		$dbSchemaPath =
		    $this->session->getVar('wikindxBasePath')
		    . $pluginPath . 'dbschema'
    		. DIRECTORY_SEPARATOR . 'update'
    		. DIRECTORY_SEPARATOR . $wkxVersion;
		if (is_dir($dbSchemaPath))
		{
    		foreach(FILE\fileInDirToArray($dbSchemaPath) as $sqlfile)
    		{
    		    $sql = file_get_contents($dbSchemaPath . DIRECTORY_SEPARATOR . $sqlfile);
    		    $sql = str_replace('%%WIKINDX_DB_TABLEPREFIX%%', $this->config->WIKINDX_DB_TABLEPREFIX, $sql);
    		    $this->db->queryNoError($sql);
    		}
		}
	}
/**
* Fill new config table (>= WIKINDX v5.3) with some default configuration values
*
* @author Mark Grimshaw-Aagaard
* @version 1
*/
	public function configDefaults()
	{
		$this->db->insert('config', array('configName', 'configVarchar'), array('configTitle', WIKINDX_TITLE_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configContactEmail', WIKINDX_CONTACTEMAIL_DEFAULT));
		$this->db->insert('config', array('configName', 'configText'), array('configDescription', WIKINDX_DESCRIPTION_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configFileDeleteSeconds', WIKINDX_FILEDELETESECONDS_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configPaging', WIKINDX_PAGING_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configPagingMaxLinks', WIKINDX_PAGINGMAXLINKS_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configStringLimit', WIKINDX_STRINGLIMIT_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configLanguage', WIKINDX_LANGUAGE_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configStyle', WIKINDX_STYLE_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configTemplate', WIKINDX_TEMPLATE_DEFAULT));
		$value = WIKINDX_MULTIUSER_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configMultiUser', $value));
		$value = WIKINDX_USERREGISTRATION_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configUserRegistration', $value));
		$value = WIKINDX_USERREGISTRATIONMODERATE_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configUserRegistrationModerate', $value));
		$value = WIKINDX_NOTIFY_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configNotify', $value));
		$this->db->insert('config', array('configName', 'configInt'), array('configImgWidthLimit', WIKINDX_IMGWIDTHLIMIT_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configImgHeightLimit', WIKINDX_IMGHEIGHTLIMIT_DEFAULT));
		$value = WIKINDX_FILEATTACH_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configFileAttach', $value));
		$value = WIKINDX_VIEWFILELOGGEDONONLY_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configFileViewLoggedOnOnly', $value));
		$this->db->insert('config', array('configName', 'configInt'), array('configMaxPaste', WIKINDX_MAXPASTE_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configLastChanges', WIKINDX_LASTCHANGES_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configLastChangesType', WIKINDX_LASTCHANGESTYPE_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configLastChangesDayLimit', WIKINDX_LASTCHANGESDAYLIMIT_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configPagingTagCloud', WIKINDX_PAGINGTAGCLOUD_DEFAULT));
		$value = WIKINDX_IMPORTBIB_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configImportBib', $value));
		$value = WIKINDX_EMAILNEWS_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configEmailNews', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configEmailNewRegistrations', WIKINDX_EMAILNEWREGISTRATIONS_DEFAULT));
		$value = WIKINDX_QUARANTINE_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configQuarantine', $value));
		$value = WIKINDX_LISTLINK_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configListLink', $value));
		$value = WIKINDX_EMAILSTATISTICS_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configEmailStatistics', $value));
		$this->db->insert('config', array('configName', 'configDatetime'), array('configStatisticsCompiled', WIKINDX_STATISTICSCOMPILED_DEFAULT));
		$value = WIKINDX_METADATAALLOW_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configMetadataAllow', $value));
		$value = WIKINDX_METADATAUSERONLY_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configMetadataUserOnly', $value));
		$this->db->insert('config', array('configName', 'configText'), array('configNoSort', WIKINDX_NOSORT_DEFAULT));
		$this->db->insert('config', array('configName', 'configText'), array('configSearchFilter', WIKINDX_SEARCHFILTER_DEFAULT));
		$value = WIKINDX_DENYREADONLY_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configDenyReadOnly', $value));
		$value = WIKINDX_READONLYACCESS_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configReadOnlyAccess', $value));
		$value = WIKINDX_ORIGINATOREDITONLY_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configOriginatorEditOnly', $value));
		$value = WIKINDX_GLOBALEDIT_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configGlobalEdit', $value));
		$this->db->insert('config', array('configName', 'configText'), array('configTimezone', WIKINDX_TIMEZONE_DEFAULT));
		if(WIKINDX_RESTRICT_USERID_DEFAULT === FALSE)
			$this->db->insert('config', array('configName'), array('configRestrictUserId'));
		else
			$this->db->insert('config', array('configName', 'configInt'), array('configRestrictUserId', WIKINDX_RESTRICT_USERID_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configMaxWriteChunk', WIKINDX_MAX_WRITECHUNK_DEFAULT));
		$this->db->insert('config', array('configName', 'configText'), array('configDeactivateResourceTypes', base64_encode(serialize(array()))));
		$value = WIKINDX_RSS_ALLOW_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configRssAllow', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configRssBibstyle', WIKINDX_RSS_BIBSTYLE_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configRssLimit', WIKINDX_RSS_LIMIT_DEFAULT));
		$value = WIKINDX_RSS_DISPLAY_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configRssDisplay', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configRssTitle', WIKINDX_RSS_TITLE_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configRssDescription', WIKINDX_RSS_DESCRIPTION_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configRssLanguage', WIKINDX_RSS_LANGUAGE_DEFAULT));
		$value = WIKINDX_MAIL_SERVER_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configMailServer', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configMailFrom', WIKINDX_MAIL_FROM_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configMailReplyTo', WIKINDX_MAIL_REPLYTO_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configMailReturnPath', WIKINDX_MAIL_RETURN_PATH_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configMailBackend', WIKINDX_MAIL_BACKEND_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configMailSmPath', WIKINDX_MAIL_SMPATH_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configMailSmtpServer', WIKINDX_MAIL_SMTPSERVER_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configMailSmtpPort', WIKINDX_MAIL_SMTPPORT_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configMailSmtpEncrypt', WIKINDX_MAIL_SMTPENCRYPT_DEFAULT));
		$value = WIKINDX_MAIL_SMTPPERSIST_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configMailSmtpPersist', $value));
		$value = WIKINDX_MAIL_SMTPAUTH_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configMailSmtpAuth', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configMailSmtpUsername', WIKINDX_MAIL_SMTPUSERNAME_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configMailSmtpPassword', WIKINDX_MAIL_SMTPPASSWORD_DEFAULT));
		$value = WIKINDX_GS_ALLOW_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configGsAllow', $value));
		$value = WIKINDX_GS_ATTACHMENT_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configGsAttachment', $value));
		$value = WIKINDX_CMS_ALLOW_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configCmsAllow', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configCmsBibstyle', WIKINDX_CMS_BIBSTYLE_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configCmsLanguage', WIKINDX_CMS_LANGUAGE_DEFAULT));
		$value = WIKINDX_CMS_SQL_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configCmsSql', $value));
		$value = WIKINDX_CMS_SQL_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configVarchar'), array('configCmsDbUser', WIKINDX_CMS_DB_USER_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configCmsDbPassword', WIKINDX_CMS_DB_PASSWORD_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configTagLowColour', WIKINDX_TAG_LOW_COLOUR_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configTagHighColour', WIKINDX_TAG_HIGH_COLOUR_DEFAULT));
		$this->db->insert('config', array('configName', 'configFloat'), array('configTagLowSize', WIKINDX_TAG_LOW_SIZE_DEFAULT));
		$this->db->insert('config', array('configName', 'configFloat'), array('configTagHighSize', WIKINDX_TAG_HIGH_SIZE_DEFAULT));
		$value = WIKINDX_IMAGES_ALLOW_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configImagesAllow', $value));
		$this->db->insert('config', array('configName', 'configInt'), array('configImagesMaxSize', WIKINDX_IMAGES_MAXSIZE_DEFAULT));
		$value = WIKINDX_DEBUG_ERRORS_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configErrorReport', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configSqlEmail', WIKINDX_DEBUG_EMAIL_DEFAULT));
		$value = WIKINDX_DEBUG_SQL_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configPrintSql', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configSqlErrorOutput', WIKINDX_DEBUG_SQLERROROUTPUT_DEFAULT));
		$value = WIKINDX_BYPASS_SMARTYCOMPILE_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configBypassSmartyCompile', $value));
		$value = WIKINDX_DISPLAY_STATISTICS_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configDisplayStatistics', $value));
		$value = WIKINDX_DISPLAY_USER_STATISTICS_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configDisplayUserStatistics', $value));
		$value = WIKINDX_AUTHGATE_USE_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configAuthGate', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configAuthGateMessage', WIKINDX_AUTHGATE_MESSAGE_DEFAULT));
		$value = WIKINDX_LDAP_USE_DEFAULT === FALSE ? 0 : 1;
		$this->db->insert('config', array('configName', 'configBoolean'), array('configLdapUse', $value));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configLdapServer', WIKINDX_LDAP_SERVER_DEFAULT));
		if(WIKINDX_LDAP_DN_DEFAULT === FALSE)
			$this->db->insert('config', array('configName'), array('configLdapDn'));
		else
			$this->db->insert('config', array('configName', 'configVarchar'), array('configLdapDn', WIKINDX_LDAP_DN_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configLdapPort', WIKINDX_LDAP_PORT_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configLdapProtocolVersion', WIKINDX_LDAP_PROTOCOL_VERSION_DEFAULT));
		$this->db->insert('config', array('configName', 'configInt'), array('configPasswordSize', WIKINDX_PASSWORDSIZE_DEFAULT));
		$this->db->insert('config', array('configName', 'configVarchar'), array('configPasswordStrength', WIKINDX_PASSWORDSTRENGTH_DEFAULT));
		$fields = array(
		    'databasesummaryTotalResources',
		    'databasesummaryTotalQuotes',
			'databasesummaryTotalParaphrases',
			'databasesummaryTotalMusings',
			'databasesummaryDbVersion'
		);
		$wV = explode('.', WIKINDX_VERSION);
		$wVersion = $wV[0] . '.' . $wV[1];
		$values = array('0', '0', '0', '0', $wVersion);
		$this->db->insert('database_summary', $fields, $values);
		$this->db->insert('category', 'categoryCategory', 'General');
	}
/**
* Upgrade database to v4.1.
* Start with stage1();
*/
	private function upgrade41()
	{
		if(array_key_exists('action', $this->vars) && ($this->vars['action'] == 'continueExecution'))
		{
			$function = $this->session->getVar('upgrade_function');
			$this->{$function}();
			return;
		}
		$this->stage4_1__1();
	}
/**
* Stage 1
*/
	private function stage4_1__1()
	{
		$this->session->delVar('stage3UpgradeContinueView');
		$this->session->delVar('stage3UpgradeContinueAttach');

		$this->updateDbSchema('4.1.1');

		$value = base64_encode(serialize(array(
            'an', 'a', 'the', 'der', 'die', 'das',
            'ein', 'eine', 'einer', 'eines', 'le', 'la', 'las',
            'il', 'les', 'une', 'un', 'una', 'uno', 'lo', 'los',
            'i', 'gli', 'de', 'het', 'um', 'uma', 'o', 'os', 'as',
        )));
		$this->db->update('config', array('noSort' => $value));

		$value = base64_encode(serialize(array('an', 'a', 'the', 'and', 'to')));
		$this->db->update('config', array('searchFilter' => $value));

		$this->upgrade40Nulls();

		$this->checkStatus('stage4_1__1');
		$this->pauseExecution('stage4_1__2', 'stage4_1__1');
	}
/**
* Stage 2
*/
	private function stage4_1__2()
	{
		$this->updateDbSchema('4.1.2');

// v3.3 corrected additional backslashes that had crept in but forgot to deal with the resource_note table
		$resultset = $this->db->select('resource_note', array('id', 'text'));
		while($row = $this->db->fetchRow($resultset))
		{
			$update = array();
			$update['text'] = stripslashes(stripslashes($row['text']));
			$this->db->formatConditions(array('id' => $row['id']));
			$this->db->update('resource_note', $update);
		}
		unset($update);


// Transfer resource_note and resource_abstract fields to it
		$ids = array();
// Transfer URLs (now stores multiple URLs so base_64 encode and serialize before storing)
		$recordSet = $this->db->select('resource', array('id', 'url'));
		while($row = $this->db->fetchRow($recordSet))
		{
			if($row['url'])
			{
				$fields = array('id', 'urls');
				$values = array($row['id'], base64_encode(serialize(array($row['url']))));
				$this->db->insert('resource_text', $fields, $values);
				$ids[$row['id']] = FALSE;
			}
		}
// Transfer notes
		$recordSet = $this->db->select('resource_note', array('id', 'text', 'editUserIdNote', 'addUserIdNote'));
		while($row = $this->db->fetchRow($recordSet))
		{
			if($row['text'] && array_key_exists($row['id'], $ids)) // update row
			{
				$updateArray = array('note' => UTF8::smartUtf8_decode(\HTML\nlToHtml($row['text'], TRUE)));
				if($row['editUserIdNote'])
					$updateArray['editUserIdNote'] = $row['editUserIdNote'];
				if($row['addUserIdNote'])
					$updateArray['addUserIdNote'] = $row['addUserIdNote'];
				$this->db->formatConditions(array('id' => $row['id']));
				$this->db->update('resource_text', $updateArray);
			}
			else if($row['text']) // insert
			{
				$fields = array('id', 'note');
				$values = array($row['id'], UTF8::smartUtf8_decode(\HTML\nlToHtml($row['text'], TRUE)));
				if($row['editUserIdNote'])
				{
					$fields[] = 'editUserIdNote';
					$values[] = $row['editUserIdNote'];
				}
				if($row['addUserIdNote'])
				{
					$fields[] = 'addUserIdNote';
					$values[] = $row['addUserIdNote'];
				}
				$this->db->insert('resource_text', $fields, $values);
				$ids[$row['id']] = FALSE;
			}
		}
// Transfer abstracts
		$recordSet = $this->db->select('resource_abstract', array('id', 'abstract', 'editUserIdAbstract', 'addUserIdAbstract'));
		while($row = $this->db->fetchRow($recordSet))
		{
			if($row['abstract'] && array_key_exists($row['id'], $ids)) // update row
			{
				$updateArray = array('abstract' => UTF8::smartUtf8_decode(\HTML\nlToHtml($row['abstract'], TRUE)));
				if($row['editUserIdAbstract'])
					$updateArray['editUserIdAbstract'] = $row['editUserIdAbstract'];
				if($row['addUserIdAbstract'])
					$updateArray['addUserIdAbstract'] = $row['addUserIdAbstract'];
				$this->db->formatConditions(array('id' => $row['id']));
				$this->db->update('resource_text', $updateArray);
			}
			else if($row['abstract']) // insert
			{
				$fields = array('id', 'abstract');
				$values = array($row['id'], UTF8::smartUtf8_decode(\HTML\nlToHtml($row['abstract'], TRUE)));
				if($row['editUserIdAbstract'])
				{
					$fields[] = 'editUserIdAbstract';
					$values[] = $row['editUserIdAbstract'];
				}
				if($row['addUserIdAbstract'])
				{
					$fields[] = 'addUserIdAbstract';
					$values[] = $row['addUserIdAbstract'];
				}
				$this->db->insert('resource_text', $fields, $values);
			}
		}

		$this->checkStatus('stage4_1__2');
		$this->pauseExecution('stage4_1__3', 'stage4_1__2');
	}
/**
* Stage 3
*/
	private function stage4_1__3()
	{
		$this->updateDbSchema('4.1.3');

// Transfer attachments fields to resource_attachments.
//
// Delete attachment rows if attachments don't exist.

// Add resource view and attachment download data to statistics table
// First insert statistics rows by averaging out total resource views over months resource has been available.
// If we've paused execution, ensure we only select those that have not yet been done.
		$this->db->formatConditions(array('attachmentId' => ' IS NULL'));
		$subStmt = $this->db->subQuery($this->db->selectNoExecute('statistics', 'resourceId'), FALSE, FALSE, TRUE);
		$this->db->formatConditions($this->db->formatFields('resource_misc.id') . $this->db->inClause($subStmt, TRUE));
		$recordset = $this->db->query('SELECT `' .
			$this->config->WIKINDX_DB_TABLEPREFIX . "resource_misc`.`id` AS `id`,
			`accesses`,
			`timestampAdd`,
			date_format(`timestampAdd`, '%Y%m') AS `date`,
			period_diff(date_format(CURRENT_TIMESTAMP, '%Y%m'), date_format(`timestampAdd`, '%Y%m')) AS `months` FROM `" .
			$this->config->WIKINDX_DB_TABLEPREFIX . 'resource_misc` LEFT JOIN `' .
			$this->config->WIKINDX_DB_TABLEPREFIX . 'resource_timestamp` ON `' .
				$this->config->WIKINDX_DB_TABLEPREFIX . 'resource_timestamp`.`id` = `' .
				$this->config->WIKINDX_DB_TABLEPREFIX . 'resource_misc`.`id`');
		$count = 0;
		while($row = $this->db->fetchRow($recordset))
		{
			$array = array();
			$year = mb_substr($row['date'], 0, 4);
			$month = mb_substr($row['date'], 4, 2);
			if(!$row['months'])
				$array[$row['date']] = $row['accesses'];
			else
			{
				$average = ceil($row['accesses'] / $row['months']);
				for($i = 0; $i < $row['months']; $i++)
				{
					$addYears = floor($i/12);
					$addMonths = $i % 12;
					$monthPart = $month + $addMonths;
					if($monthPart > 12)
					{
						++$addYears;
						$monthPart = $monthPart - 12;
					}
					$monthPart = $monthPart < 10 ? '0' . $monthPart : (string)$monthPart;
					$yearPart = $year + $addYears;
					$date = (string)$yearPart . $monthPart;
					$array[$date] = $average;
				}
			}
			$this->db->insert('statistics', array('resourceId', 'statistics'),
				array($row['id'], base64_encode(serialize($array))));
			$this->db->formatConditions(array('id' => $row['id']));
			$this->db->update('resource_misc', array('accessesPeriod' => $average));
			$count++;
// Check we have more than 3 seconds buffer before max_execution_time times out.
			if((time() - $this->oldTime) >= (ini_get("max_execution_time") - 3))
			{
				$this->session->setVar('stage3UpgradeContinueView', TRUE);
				$this->checkStatus('stage4_1__3');
				$this->stageInterruptMessage = "stage3 continuing: $count view statistics created this pass.&nbsp;&nbsp;";
				$this->pauseExecution('stage4_1__3', 'stage4_1__3');
			}
		}
		if(!$this->session->getVar('stage3UpgradeContinueAttach')) // First time through and no continue execution
		{
			$dirName = $this->config->WIKINDX_ATTACHMENTS_DIR;
			$recordSet = $this->db->select('resource_attachments', array('id', 'hashFilename'));
			$deletes = array();
			while($row = $this->db->fetchRow($recordSet))
			{
				if(!file_exists("$dirName/" . $row['hashFilename']))
					$deletes[] = $row['id'];
			}
			if(!empty($deletes)) // discard these entries as there is no attachment
			{
				$this->db->formatConditionsOneField($deletes, 'id');
				$this->db->delete('resource_attachments');
			}
			$this->db->leftJoin('resource_timestamp', 'resource_timestamp.id', 'resource_misc.id');
			$recordset = $this->db->select('resource_misc', array(array('resource_misc.id' => 'id'), 'attachDownloads', 'timestampAdd',));
			while($row = $this->db->fetchRow($recordset))
			{
				$this->db->formatConditions(array('resourceId' => $row['id']));
				$this->db->update('resource_attachments', array('downloads' => $row['attachDownloads'], 'timestamp' => $row['timestampAdd']));
			}
		}
// Insert statistics rows by averaging out total attachment downloads over months attachment has been available
// If we've paused execution, ensure we only select those that have not yet been done.
		$this->db->formatConditions(array('attachmentId' => ' IS NOT NULL'));
		$subStmt = $this->db->subQuery($this->db->selectNoExecute('statistics', 'resourceId'), FALSE, FALSE, TRUE);
		$this->db->formatConditions($this->db->formatFields('resourceId') . $this->db->inClause($subStmt, TRUE));
		$recordset = $this->db->query("SELECT `id`,
			`resourceId`,
			`downloads`,
			`timestamp`,
			date_format(`timestamp`, '%Y%m') AS `date`,
			period_diff(date_format(CURRENT_TIMESTAMP, '%Y%m'), date_format(`timestamp`, '%Y%m')) AS `months` FROM `" .
			$this->config->WIKINDX_DB_TABLEPREFIX . 'resource_attachments`');
		$count = 0;
		while($row = $this->db->fetchRow($recordset))
		{
			$array = array();
			$year = mb_substr($row['date'], 0, 4);
			$month = mb_substr($row['date'], 4, 2);
			if(!$row['months'])
				$array[$row['date']] = $row['downloads'];
			else
			{
				$average = ceil($row['downloads'] / $row['months']);
				for($i = 0; $i < $row['months']; $i++)
				{
					$addYears = floor($i/12);
					$addMonths = $i % 12;
					$monthPart = $month + $addMonths;
					if($monthPart > 12)
					{
						++$addYears;
						$monthPart = $monthPart - 12;
					}
					$monthPart = $monthPart < 10 ? '0' . $monthPart : (string)$monthPart;
					$yearPart = $year + $addYears;
					$date = (string)$yearPart . $monthPart;
					$array[$date] = $average;
				}
			}
			$this->db->insert('statistics', array('resourceId', 'attachmentId', 'statistics'),
				array($row['resourceId'], $row['id'], base64_encode(serialize($array))));
			$this->db->formatConditions(array('id' => $row['id']));
			$this->db->update('resource_attachments', array('downloadsPeriod' => $average));
			$count++;
// Check we have more than 3 seconds buffer before max_execution_time times out.
			if((time() - $this->oldTime) >= (ini_get("max_execution_time") - 3))
			{
				$this->session->setVar('stage3UpgradeContinueAttach', TRUE);
				$this->checkStatus('stage4_1__3');
				$this->stageInterruptMessage = "stage3 continuing: $count attachment statistics created this pass.&nbsp;&nbsp;";
				$this->pauseExecution('stage4_1__3', 'stage4_1__3');
			}
		}

		$this->checkStatus('stage4_1__3');
		$this->pauseExecution('stage4_1__4', 'stage4_1__3');
	}
/**
* stage 4
*/
	private function stage4_1__4()
	{
// Set for Latin1
		$this->db->query('SET NAMES latin1');
		$this->db->query('SET CHARACTER SET latin1');

// Pre v4.0 versions have a bug whereby creators may be duplicated in the database -- find these and fix.
		$resultset = $this->db->query("
		    SELECT `a`.`id` AS `aId`, `b`.`id` AS `bId`
			FROM `" . $this->config->WIKINDX_DB_TABLEPREFIX . "creator` AS a
			INNER JOIN `" . $this->config->WIKINDX_DB_TABLEPREFIX . "creator` AS b
			ON  (`a`.`firstname` = `b`.`firstname` OR (`a`.`firstname` IS NULL AND `b`.`firstname` IS NULL))
			AND (`a`.`initials`  = `b`.`initials`  OR (`a`.`initials` IS NULL  AND `b`.`initials` IS NULL))
			AND (`a`.`prefix`    = `b`.`prefix`    OR (`a`.`prefix` IS NULL    AND `b`.`prefix` IS NULL))
			AND `a`.`surname`    = `b`.`surname`
			AND `a`.`id` != `b`.`id`
			");
		$dupCreators = $delCreators = array();
		$count = 0;
		while($row = $this->db->fetchRow($resultset))
		{
			if(array_search($row['aId'], $delCreators) !== FALSE)
				continue;
			if(array_search($row['aId'], $dupCreators) === FALSE)
			{
				$dupCreators[$row['aId']] = $row['bId'];
				$resultset2 = $this->db->query("
				    SELECT `id`, `creator1`, `creator2`, `creator3`, `creator4`, `creator5`
					FROM `" . $this->config->WIKINDX_DB_TABLEPREFIX . "resource_creator`
					WHERE
					   FIND_IN_SET('" . $row['bId'] . "', `creator1`)
					OR FIND_IN_SET('" . $row['bId'] . "', `creator2`)
					OR FIND_IN_SET('" . $row['bId'] . "', `creator3`)
					OR FIND_IN_SET('" . $row['bId'] . "', `creator4`)
					OR FIND_IN_SET('" . $row['bId'] . "', `creator5`)"
					);
				while($row2 = $this->db->fetchRow($resultset2))
				{
					$updateArray = array();
					foreach(array('creator1', 'creator2', 'creator3', 'creator4', 'creator5') as $creator)
					{
// remove bId creator id and replace with aId if aId not already in set
						if($row2[$creator])
						{
							$oldArray = UTF8::mb_explode(",", $row2[$creator]);
// If $row['aId'] already in db field, simply remove $row['bId']
							if((array_search($row['aId'], $oldArray) !== FALSE) &&
								(($editIndex = array_search($row['bId'], $oldArray)) !== FALSE))
							{
								unset($oldArray[$editIndex]);
								$updateArray[$creator] = join(",", $oldArray);
							}
// else, if this creator field has $row['bId'] in it, replace it with $row['aId']
							else if(($editIndex = array_search($row['bId'], $oldArray)) !== FALSE)
							{
								$oldArray[$editIndex] = $row['aId'];
								$updateArray[$creator] = join(",", $oldArray);
							}
						}
					}
					if(!empty($updateArray))
					{
						$this->db->formatConditions(array('id' => $row2['id']));
						$this->db->update('resource_creator', $updateArray);
					}
				}
				if(array_search($row['bId'], $delCreators) === FALSE)
				{
					++$count;
					$delCreators[] = $row['bId'];
// delete bId
					$this->db->formatConditions(array('id' => $row['bId']));
					$this->db->delete('creator');
				}
			}
// Check we have more than 3 seconds buffer before max_execution_time times out.
			if((time() - $this->oldTime) >= (ini_get("max_execution_time") - 3))
			{
				$this->checkStatus('stage4_1__4');
				$this->stageInterruptMessage = "stage4 continuing: $count duplicate creators corrected this pass.&nbsp;&nbsp;";
				$this->pauseExecution('stage4_1__4', 'stage4_1__4');
			}
		}

		$this->checkStatus('stage4_1__4');
		$this->pauseExecution('stage4_1__5', 'stage4_1__4');
	}
/**
* stage 5
*/
	private function stage4_1__5()
	{
// Set for Latin1
		$this->db->query('SET NAMES latin1');
		$this->db->query('SET CHARACTER SET latin1');

// Remove duplicate collections
		$resultset = $this->db->query("
		    SELECT `a`.`id` AS `aId`, `b`.`id` AS `bId`
			FROM `" . $this->config->WIKINDX_DB_TABLEPREFIX . "collection` AS a
			INNER JOIN `" . $this->config->WIKINDX_DB_TABLEPREFIX . "collection` AS b
			ON  (`a`.`collectionTitle`      = `b`.`collectionTitle`)
			AND (`a`.`collectionTitleShort` = `b`.`collectionTitleShort` OR (`a`.`collectionTitleShort` IS NULL AND `b`.`collectionTitleShort` IS NULL))
			AND (`a`.`collectionType`       = `b`.`collectionType`)
			AND `a`.`id` != `b`.`id`
			");
		$dupCollections = $delCollections = array();
		while($row = $this->db->fetchRow($resultset))
		{
			if(array_search($row['aId'], $delCollections) === FALSE)
			{
				$dupCollections[$row['aId']][] = $row['bId'];
				$delCollections[] = $row['bId'];
			}
		}
		$updateArray = array();
		foreach($dupCollections AS $aId => $bIds)
		{
			foreach($bIds AS $bId)
			{
				$resultset2 = $this->db->query("
				    SELECT `id`, `collection`
					FROM `" . $this->config->WIKINDX_DB_TABLEPREFIX . "resource_misc`
					WHERE `collection` = '" . $bId . "'");
				while($row2 = $this->db->fetchRow($resultset2))
				{
// remove bId collection id and replace with aId
					if($row2['collection'] && ($row2['collection'] == $bId)) // duplicate check?
						$updateArray[$bId] = $aId;
				}
			}
// Check we have more than 5 seconds buffer before max_execution_time times out.
			if((time() - $this->oldTime) >= (ini_get("max_execution_time") - 5))
			{
// Do updates so far
				$this->db->multiUpdate('resource_misc', 'collection', 'collection', $updateArray);
// delete bIds thus far
				$this->db->formatConditionsOneField($delCollections, 'id');
				$this->db->delete('collection');
				$count = sizeOf($delCollections);
				$this->checkStatus('stage4_1__5');
				$this->stageInterruptMessage = "stage5 continuing: $count duplicate collections corrected this pass.&nbsp;&nbsp;";
				$this->pauseExecution('stage4_1__5', 'stage4_1__5');
			}
		}
		$this->db->multiUpdate('resource_misc', 'collection', 'collection', $updateArray);
		$this->db->formatConditionsOneField($delCollections, 'id');
		$this->db->delete('collection');

		$this->checkStatus('stage4_1__5');
		$this->pauseExecution('stage4_1__6', 'stage4_1__5');
	}
/**
* stage 6
*/
	private function stage4_1__6()
	{
// Rename fields in tables for v4.0 upgrade
		$this->updateDbSchema('4.1.6');

		$this->upgrade40charToBin();
		$this->checkStatus('stage4_1__6');
		$this->pauseExecution('stage4_1__7', 'stage4_1__6');
	}
/**
* stage 7
*/
	private function stage4_1__7()
	{
        $this->updateDbSchema('4.1.7');

		$this->upgrade40Tables();

		$this->checkStatus('stage4_1__7');
		$this->pauseExecution('stage4_1__8', 'stage4_1__7');
	}
/**
* stage 8
*/
	private function stage4_1__8()
	{
/**
* Rewrite resource_creator tables
*
* In v4, we no longer use MySQL's FIND_IN_SET and therefore no longer want comma-delimited field values such as creator1, creator2 etc. in
* the resource_creator table.  Write a new resource_creator table that has a row comprising:
* resourceId references resource table), creatorId (references creator table), role (creator1, creator2 etc.) and order (1,2, 3 etc.
* for first author, second author, third author etc.)
* resourcecreatorCreatorMain is the creatorId that is the main creator for that resource used when ordering lists.
*/
        $this->updateDbSchema('4.1.8');

		$creatorFields = array(
		    'resourcecreatorCreator1',
		    'resourcecreatorCreator2',
		    'resourcecreatorCreator3',
		    'resourcecreatorCreator4',
		    'resourcecreatorCreator5'
		);

		$this->db->formatConditions(array('creatorSurname' => ' IS NOT NULL'));
		$recordSetCreator = $this->db->select('creator', array('creatorId', 'creatorSurname'));

		$recordSet = $this->db->select(
    		'resource_creator',
    		array('resourcecreatorId',
        		'resourcecreatorCreator1',
        		'resourcecreatorCreator2',
        		'resourcecreatorCreator3',
        		'resourcecreatorCreator4',
        		'resourcecreatorCreator5'
        	)
		);
		while($row = $this->db->fetchRow($recordSet))
		{
			$creatorMain = FALSE;
			foreach($creatorFields as $creatorField)
			{
				if(!$row[$creatorField])
				    continue;

				$order = 1;

				$creatorIds = UTF8::mb_explode(',', $row[$creatorField]);

				foreach($creatorIds as $creatorId)
				{
					$fields = array();
					$values = array();

					$fields[] = 'resourcecreatorResourceId';
					$values[] = $row['resourcecreatorId'];

					$fields[] = 'resourcecreatorCreatorId';
					$values[] = $creatorId;

					$fields[] = 'resourcecreatorOrder';
					$values[] = $order;

					if ($creatorField != 'resourcecreatorId')
					{
						$fields[] = 'resourcecreatorRole';
						$values[] = mb_substr($creatorField, -1);

						if(!$creatorMain) $creatorMain = $creatorId;

						$fields[] = 'resourcecreatorCreatorMain';
						$values[] = $creatorMain;
					}

					$this->db->goToRow($recordSetCreator, 1);
					while($rowCreator = $this->db->fetchRow($recordSetCreator))
					{
					    if ($rowCreator['creatorId'] == $creatorMain)
					    {
							$fields[] = 'resourcecreatorCreatorSurname';
							$values[] = $rowCreator['creatorSurname'];
							break;
					    }
					}

					$this->db->insert('temp_resource_creator', $fields, $values);
					++$order;
				}
			}
		}
// Select, and insert into temp_resource_creator, those resourceIds without a creator
		$this->db->formatConditions($this->db->formatFields('resourceId') . ' NOT IN ' .
			$this->db->subQuery($this->db->selectNoExecute('resource_creator', 'resourcecreatorId'), FALSE, FALSE));
		$recordSet = $this->db->select('resource', 'resourceId');

		while($row = $this->db->fetchRow($recordSet))
		{
			$fields = $values = array();
			$fields[] = 'resourcecreatorResourceId';
			$values[] = $row['resourceId'];
			$this->db->insert('temp_resource_creator', $fields, $values);
		}

		$this->checkStatus('stage4_1__8');
		$this->pauseExecution('stage4_1__9', 'stage4_1__8');
	}
/**
* stage 9
*/
	private function stage4_1__9()
	{
// Rewrite resource_summary
// Fix quote, paraphrase, musing counts per resource.
		$ids = array();
// quotes
		$this->db->groupBy('resourcequoteResourceId');
		$resultset = $this->db->query('SELECT `resourcequoteResourceId`, COUNT(`resourcequoteResourceId`) AS `count` FROM ' .
			$this->config->WIKINDX_DB_TABLEPREFIX . 'resource_quote');
		while($row = $this->db->fetchRow($resultset))
			$ids[$row['resourcequoteResourceId']]['resourcesummaryQuotes'] = $row['count'];
// paraphrases
		$this->db->groupBy('resourceparaphraseResourceId');
		$resultset = $this->db->query('SELECT `resourceparaphraseResourceId`, COUNT(`resourceparaphraseResourceId`) AS `count` FROM ' .
			$this->config->WIKINDX_DB_TABLEPREFIX . 'resource_paraphrase');
		while($row = $this->db->fetchRow($resultset))
			$ids[$row['resourceparaphraseResourceId']]['resourcesummaryParaphrases'] = $row['count'];
// musings
		$this->db->groupBy('resourcemusingResourceId');
		$resultset = $this->db->query('SELECT `resourcemusingResourceId`, COUNT(`resourcemusingResourceId`) AS `count` FROM ' .
			$this->config->WIKINDX_DB_TABLEPREFIX . 'resource_musing');
		while($row = $this->db->fetchRow($resultset))
			$ids[$row['resourcemusingResourceId']]['resourcesummaryMusings'] = $row['count'];
		foreach($ids as $id => $fieldArray)
		{
			$update = $nulls = array();
			foreach($fieldArray as $field => $count)
				$update[$field] = $count;
			if(!array_key_exists('resourcesummaryQuotes', $fieldArray))
				$nulls[] = 'resourcesummaryQuotes';
			if(!array_key_exists('resourcesummaryParaphrases', $fieldArray))
				$nulls[] = 'resourcesummaryParaphrases';
			if(!array_key_exists('resourcesummaryMusings', $fieldArray))
				$nulls[] = 'resourcesummaryMusings';

			$this->db->formatConditions(array('resourcesummaryId' => $id));
			$this->db->update('resource_summary', $update);
			if(!empty($nulls))
			{
				$this->db->formatConditions(array('resourcesummaryId' => $id));
				$this->db->updateNull('resource_summary', $nulls);
			}
		}

		$this->checkStatus('stage4_1__9');
		$this->pauseExecution('stage4_1__10', 'stage4_1__9');
	}
/**
* stage 10
*/
	private function stage4_1__10()
	{
	    $this->updateDbSchema('4.1.10');

// Rewrite resource_keyword table
		$this->db->formatConditions($this->db->formatFields('resourcekeywordKeywords') . ' IS NOT NULL');
		$recordSet = $this->db->select('resource_keyword', array('resourcekeywordId', 'resourcekeywordKeywords'));
		while($row = $this->db->fetchRow($recordSet))
		{
			foreach(UTF8::mb_explode(',', $row['resourcekeywordKeywords']) as $kId)
				$this->db->insert('temp_resource_keyword', array('resourcekeywordResourceId', 'resourcekeywordKeywordId'),
					array($row['resourcekeywordId'], $kId));
		}
		$this->db->formatConditions($this->db->formatFields('resourcequoteKeywords') . ' IS NOT NULL');
		$recordSet = $this->db->select('resource_quote', array('resourcequoteId', 'resourcequoteKeywords'));
		while($row = $this->db->fetchRow($recordSet))
		{
			foreach(UTF8::mb_explode(',', $row['resourcequoteKeywords']) as $kId)
				$this->db->insert('temp_resource_keyword', array('resourcekeywordQuoteId', 'resourcekeywordKeywordId'),
					array($row['resourcequoteId'], $kId));
		}
		$this->db->formatConditions($this->db->formatFields('resourceparaphraseKeywords') . ' IS NOT NULL');
		$recordSet = $this->db->select('resource_paraphrase', array('resourceparaphraseId', 'resourceparaphraseKeywords'));
		while($row = $this->db->fetchRow($recordSet))
		{
			foreach(UTF8::mb_explode(',', $row['resourceparaphraseKeywords']) as $kId)
				$this->db->insert('temp_resource_keyword', array('resourcekeywordParaphraseId', 'resourcekeywordKeywordId'),
					array($row['resourceparaphraseId'], $kId));
		}
		$this->db->formatConditions($this->db->formatFields('resourcemusingKeywords') . ' IS NOT NULL');
		$recordSet = $this->db->select('resource_musing', array('resourcemusingId', 'resourcemusingKeywords'));
		while($row = $this->db->fetchRow($recordSet))
		{
			foreach(UTF8::mb_explode(',', $row['resourcemusingKeywords']) as $kId)
				$this->db->insert('temp_resource_keyword', array('resourcekeywordMusingId', 'resourcekeywordKeywordId'),
					array($row['resourcemusingId'], $kId));
		}

		$this->checkStatus('stage4_1__10');
		$this->pauseExecution('stage4_1__11', 'stage4_1__10');
	}
/**
* stage 11
*/
	private function stage4_1__11()
	{
	    $this->updateDbSchema('4.1.11');

// Rewrite resource_category table
		$recordSet = $this->db->select('resource_category', array('resourcecategoryId', 'resourcecategoryCategories'));
		while($row = $this->db->fetchRow($recordSet))
		{
			foreach(UTF8::mb_explode(',', $row['resourcecategoryCategories']) as $cId)
				$this->db->insert('temp_resource_category', array('resourcecategoryResourceId', 'resourcecategoryCategoryId'),
					array($row['resourcecategoryId'], $cId));
		}

		$this->checkStatus('stage4_1__11');
		$this->pauseExecution('stage4_1__12', 'stage4_1__11');
	}
/**
* stage 12
*/
	private function stage4_1__12()
	{
	    $this->updateDbSchema('4.1.12');

// Rewrite bibliographies groups
		$recordSet = $this->db->select('user_bibliography', '*');
		while($row = $this->db->fetchRow($recordSet))
		{
			if($row['userbibliographyBibliography'])
			{
				foreach(UTF8::mb_explode(',', $row['userbibliographyBibliography']) as $rId)
					$this->db->insert('user_bibliography_resource',
						array('userbibliographyresourceBibliographyId', 'userbibliographyresourceResourceId'),
						array($row['userbibliographyId'], $rId));
			}
		}
		$recordSet = $this->db->select('user_groups', array('usergroupsId', 'usergroupsUserIds', 'usergroupsBibliographyIds'));
		while($row = $this->db->fetchRow($recordSet))
		{
			foreach(UTF8::mb_explode(',', $row['usergroupsUserIds']) as $uId)
				$this->db->insert('user_groups_users',
					array('usergroupsusersGroupId', 'usergroupsusersUserId'),
					array($row['usergroupsId'], $uId));
		}

		$this->checkStatus('stage4_1__12');
		$this->pauseExecution('stage4_1__13', 'stage4_1__12');
	}
/**
* stage 13
*
* convert bbcode to html for use with tinymce and correct transTitle errors
*/
	private function stage4_1__13()
	{
	    include_once("core/display/BBCODE.php");
		$string = $this->db->selectFirstField('config', 'configDescription');
		$string = BBCODE::bbCodeToHtml($string);
		$this->db->update('config', array('configDescription' => $string));
// correct transTitle errors
		$resultset = $this->db->select('resource', array('resourceId', 'resourceTransTitle', 'resourceTransSubtitle'));
		while($row = $this->db->fetchRow($resultset))
		{
			$nulls = array();
			if(($row['resourceTransTitle'] == '') || ($row['resourceTransTitle'] == '(no title)'))
				$nulls[] = 'resourceTransTitle';
			if($row['resourceTransSubtitle'] == '')
				$nulls[] = 'resourceTransSubtitle';
			if(!empty($nulls))
			{
				$this->db->formatConditions(array('resourceId' => $row['resourceId']));
				$this->db->updateNull('resource', $nulls);
			}
		}
// titles and subtitles
		$this->db->formatConditions($this->db->formatFields('resourceTitle') . $this->db->like('%', '[', '%') .
			$this->db->or . $this->db->formatFields('resourceSubtitle') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('resource', array('resourceId', 'resourceTitle', 'resourceSubtitle',
			'resourceTransTitle', 'resourceTransSubtitle'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['resourceTitle'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourceTitle']);
				$updateArray['resourceTitle'] = $string;
			}
			if($row['resourceSubtitle'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourceSubtitle']);
				$updateArray['resourceSubtitle'] = $string;
			}
			if($row['resourceTransTitle'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourceTransTitle']);
				$updateArray['resourceTransTitle'] = $string;
			}
			if($row['resourceTransSubtitle'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourceTransSubtitle']);
				$updateArray['resourceTransSubtitle'] = $string;
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('resourceId' => $row['resourceId']));
				$this->db->update('resource', $updateArray);
			}
		}
// Collection titles
		$this->db->formatConditions($this->db->formatFields('collectionTitle') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('collection', array('collectionId', 'collectionTitle'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['collectionTitle'])
			{
				$string = BBCODE::bbCodeToHtml($row['collectionTitle']);
				$updateArray['collectionTitle'] = $string;
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('collectionId' => $row['collectionId']));
				$this->db->update('collection', $updateArray);
			}
		}
// notes and abstracts
		$this->db->formatConditions($this->db->formatFields('resourcetextNote') . $this->db->like('%', '[', '%') .
			$this->db->or . $this->db->formatFields('resourcetextAbstract') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('resource_text', array('resourcetextId', 'resourcetextNote', 'resourcetextAbstract'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['resourcetextNote'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourcetextNote']);
				$updateArray['resourcetextNote'] = $string;
			}
			if($row['resourcetextAbstract'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourcetextAbstract']);
				$updateArray['resourcetextAbstract'] = $string;
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('resourcetextId' => $row['resourcetextId']));
				$this->db->update('resource_text', $updateArray);
			}
		}
// Custom large fields -- don't filter results because all fields need newlines converting to HTML
//		$this->db->formatConditions($this->db->formatFields('resourcecustomLong') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('resource_custom', array('resourcecustomId', 'resourcecustomLong'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['resourcecustomLong'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourcecustomLong']);
				$updateArray['resourcecustomLong'] = \HTML\nlToHtml($string, TRUE);
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('resourcecustomId' => $row['resourcecustomId']));
				$this->db->update('resource_custom', $updateArray);
			}
		}
// User bibliography
//		$this->db->formatConditions($this->db->formatFields('userbibliographyDescription') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('user_bibliography', array('userbibliographyId', 'userbibliographyDescription'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['userbibliographyDescription'])
			{
				$string = BBCODE::bbCodeToHtml($row['userbibliographyDescription']);
				$updateArray['userbibliographyDescription'] = \HTML\nlToHtml($string, TRUE);
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('userbibliographyId' => $row['userbibliographyId']));
				$this->db->update('user_bibliography', $updateArray);
			}
		}
// News
//		$this->db->formatConditions($this->db->formatFields('newsNews') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('news', array('newsId', 'newsNews'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['newsNews'])
			{
				$string = BBCODE::bbCodeToHtml($row['newsNews']);
				$updateArray['newsNews'] = \HTML\nlToHtml($string, TRUE);
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('newsId' => $row['newsId']));
				$this->db->update('news', $updateArray);
			}
		}
// Musings
//		$this->db->formatConditions($this->db->formatFields('resourcemusingtextText') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('resource_musing_text', array('resourcemusingtextId', 'resourcemusingtextText'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['resourcemusingtextText'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourcemusingtextText']);
				$updateArray['resourcemusingtextText'] = \HTML\nlToHtml($string, TRUE);
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('resourcemusingtextId' => $row['resourcemusingtextId']));
				$this->db->update('resource_musing_text', $updateArray);
			}
		}
// Quotes
//		$this->db->formatConditions($this->db->formatFields('resourcequotetextText') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('resource_quote_text', array('resourcequotetextId', 'resourcequotetextText'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['resourcequotetextText'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourcequotetextText']);
				$updateArray['resourcequotetextText'] = \HTML\nlToHtml($string, TRUE);
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('resourcequotetextId' => $row['resourcequotetextId']));
				$this->db->update('resource_quote_text', $updateArray);
			}
		}
// Quote comments
//		$this->db->formatConditions($this->db->formatFields('resourcequotecommentComment') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('resource_quote_comment', array('resourcequotecommentId', 'resourcequotecommentComment'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['resourcequotecommentComment'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourcequotecommentComment']);
				$updateArray['resourcequotecommentComment'] = \HTML\nlToHtml($string, TRUE);
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('resourcequotecommentId' => $row['resourcequotecommentId']));
				$this->db->update('resource_quote_comment', $updateArray);
			}
		}
// Paraphrases
//		$this->db->formatConditions($this->db->formatFields('resourceparaphrasetextText') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('resource_paraphrase_text', array('resourceparaphrasetextId', 'resourceparaphrasetextText'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['resourceparaphrasetextText'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourceparaphrasetextText']);
				$updateArray['resourceparaphrasetextText'] = \HTML\nlToHtml($string, TRUE);
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('resourceparaphrasetextId' => $row['resourceparaphrasetextId']));
				$this->db->update('resource_paraphrase_text', $updateArray);
			}
		}
// Paraphrase comments
//		$this->db->formatConditions($this->db->formatFields('resourceparaphrasecommentComment') . $this->db->like('%', '[', '%'));
		$resultset = $this->db->select('resource_paraphrase_comment', array('resourceparaphrasecommentId', 'resourceparaphrasecommentComment'));
		while($row = $this->db->fetchRow($resultset))
		{
			$updateArray = array();
			if($row['resourceparaphrasecommentComment'])
			{
				$string = BBCODE::bbCodeToHtml($row['resourceparaphrasecommentComment']);
				$updateArray['resourceparaphrasecommentComment'] = \HTML\nlToHtml($string, TRUE);
			}
			if(!empty($updateArray))
			{
				$this->db->formatConditions(array('resourceparaphrasecommentId' => $row['resourceparaphrasecommentId']));
				$this->db->update('resource_paraphrase_comment', $updateArray);
			}
		}

		$this->checkStatus('stage4_1__13');
		$this->pauseExecution('stage4_1__14', 'stage4_1__13');
	}
/**
* stage 14
*/
	private function stage4_1__14()
	{
		$this->addMissingRows();
// Session setup required in penultimate stage Рremoved for v5.3 upgrade
//		$this->session->setVar("setup_UserId", 1);
//		$user = FACTORY_USER::getInstance();
//		$user->writeSessionPreferences(FALSE, 'config', TRUE);
        $this->checkStatus('stage4_1__14');
		$this->pauseExecution('stage4_1__15', 'stage4_1__14');
	}
/**
* stage 15
*/
	private function stage4_1__15()
	{
	    $this->updateDbSchema('4.1.15');

// Create a new column on the resource table that holds the title-subtitle without {}, ' or " in order to facilitate fast sorting
		$resultset = $this->db->query('SELECT ' . $this->db->replace($this->db->replace($this->db->replace($this->db->replace('resourceTitle',
			'{', ''), '}', '', FALSE), '"', '', FALSE), '\\\'', '', FALSE) . " AS 't', " .
			$this->db->replace($this->db->replace($this->db->replace($this->db->replace('resourceSubtitle',
			'{', ''), '}', '', FALSE), '"', '', FALSE), '\\\'', '', FALSE) . " AS 's', `resourceId` FROM " .
			$this->config->WIKINDX_DB_TABLEPREFIX . 'resource');
		while($row = $this->db->fetchRow($resultset))
		{
			$title = '';
			if($row['s'])
				$title = $row['t'] . ' ' . $row['s'];
			else
				$title = $row['t'];
			$this->db->formatConditions(array('resourceId' => $row['resourceId']));
			$this->db->update('resource', array('resourceTitleSort' => $title));
		}

//		$this->recreate40Cache();
		ini_set('memory_limit', $this->config->WIKINDX_MEMORY_LIMIT);
		if(array_key_exists('fixUTF8', $this->vars)) $this->fixUTF8();
		$this->checkStatus('stage4_1__15');
		$this->pauseExecution('stage4_2__1', 'stage4_1__15');
	}
/**
* Upgrade 4.1 to 4.2 -- Stage 1
*/
	private function stage4_2__1()
	{
	    $this->updateDbSchema('4.2.1');

// v4.x code before 4.2 incorrectly named statistics field
		$fields = $this->db->listFields('statistics');
		if(array_search('statistics', $fields) !== FALSE)
			$this->db->query('ALTER TABLE `' . $this->config->WIKINDX_DB_TABLEPREFIX . 'statistics` CHANGE `statistics` `statisticsStatistics` TEXT DEFAULT NULL');
		$this->stage5_1__1();
        $this->checkStatus('stage4_2__1');
	}
/**
* Upgrade 4.2 to 5.1 -- Stage 1
*
* Transfer all metadata tables to new unified resource_metadata table
*/
	private function stage5_1__1()
	{
	    $this->updateDbSchema('5.1.1-begin');

		$maxPacket = $this->db->getMaxPacket();
// For each 1MB max_allowed_packet (1048576 bytes), 600 updates in one go seems fine as a value for $maxCounts (based on trial and error)
		$maxCounts = floor(600 * ($maxPacket/1048576));
// Transfer quotes
		$updateArray = array();
		$count = 0;
		$insertArray = array();
		$countI = 0;
		$fields_c[] = 'resourcemetadataMetadataId';
		$fields_c[] = 'resourcemetadataText';
		$fields_c[] = 'resourcemetadataAddUserId';
		$fields_c[] = 'resourcemetadataTimestamp';
		$fields_c[] = 'resourcemetadataPrivate';
		$fields_c[] = 'resourcemetadataType';
		$this->db->leftJoin('resource_quote_text', 'resourcequotetextId', 'resourcequoteId');
		$resultset = $this->db->select('resource_quote', array('resourcequoteId', 'resourcequoteResourceId', 'resourcequotePageStart', 'resourcequotePageEnd',
			'resourcequoteParagraph', 'resourcequoteSection', 'resourcequoteChapter', 'resourcequotetextText', 'resourcequotetextAddUserIdQuote'));
		while($row = $this->db->fetchRow($resultset))
		{
			++$count;
			$fields = array();
			$values = array();
			$fields[] = 'resourcemetadataResourceId';
			$values[] = $row['resourcequoteResourceId'];
			if($row['resourcequotePageStart'])
			{
				$fields[] = 'resourcemetadataPageStart';
				$values[] = $row['resourcequotePageStart'];
			}
			if($row['resourcequotePageEnd'])
			{
				$fields[] = 'resourcemetadataPageEnd';
				$values[] = $row['resourcequotePageEnd'];
			}
			if($row['resourcequoteParagraph'])
			{
				$fields[] = 'resourcemetadataParagraph';
				$values[] = $row['resourcequoteParagraph'];
			}
			if($row['resourcequoteSection'])
			{
				$fields[] = 'resourcemetadataSection';
				$values[] = $row['resourcequoteSection'];
			}
			if($row['resourcequoteChapter'])
			{
				$fields[] = 'resourcemetadataChapter';
				$values[] = $row['resourcequoteChapter'];
			}
			$fields[] = 'resourcemetadataType';
			$values[] = 'q';
			$fields[] = 'resourcemetadataText';
			$values[] = $row['resourcequotetextText'];
			$fields[] = 'resourcemetadataAddUserId';
			$values[] = $row['resourcequotetextAddUserIdQuote'];
			$this->db->insert('resource_metadata', $fields, $values);
			$id = $this->db->lastAutoID();
			$updateArray[$row['resourcequoteId']] = $id;
			if($count >= $maxCounts)
			{
				$this->db->multiUpdate('resource_keyword', 'resourcekeywordMetadataId', 'resourcekeywordQuoteId', $updateArray);
				$updateArray = array();
				$count = 0;
			}
// insert quote comments
			$this->db->formatConditions(array('resourcequotecommentQuoteId' => $row['resourcequoteId']));
			$resultset1 = $this->db->select('resource_quote_comment',
				array('resourcequotecommentComment', 'resourcequotecommentAddUserIdQuote', 'resourcequotecommentTimestamp', 'resourcequotecommentPrivate'));
			while($row1 = $this->db->fetchRow($resultset1))
			{
				++$countI;
				$values_c = array();
				$values_c[] = $this->db->tidyInput($id);
				$values_c[] = $this->db->tidyInput($row1['resourcequotecommentComment']);
				$values_c[] = $this->db->tidyInput($row1['resourcequotecommentAddUserIdQuote']);
				$values_c[] = $this->db->tidyInput($row1['resourcequotecommentTimestamp']);
				$values_c[] = $this->db->tidyInput($row1['resourcequotecommentPrivate']);
				$values_c[] = $this->db->tidyInput('qc');
				$insertArray[] = '(' . join(',', $values_c) . ')';
			}
			if($countI >= $maxCounts)
			{
				$this->db->multiInsert('resource_metadata', $fields_c, join(',', $insertArray));
				$insertArray = array();
				$countI = 0;
			}
		}
		if(!empty($insertArray)) // do the remainder
			$this->db->multiInsert('resource_metadata', $fields_c, join(',', $insertArray));
		if(!empty($updateArray)) // do the remainder
			$this->db->multiUpdate('resource_keyword', 'resourcekeywordMetadataId', 'resourcekeywordQuoteId', $updateArray);
		$updateArray = array();
		$count = 0;
		$insertArray = array();
		$countI = 0;
// Transfer paraphrases
		$this->db->leftJoin('resource_paraphrase_text', 'resourceparaphrasetextId', 'resourceparaphraseId');
		$resultset = $this->db->select('resource_paraphrase', array('resourceparaphraseId', 'resourceparaphraseResourceId', 'resourceparaphrasePageStart',
			'resourceparaphrasePageEnd', 'resourceparaphraseParagraph', 'resourceparaphraseSection', 'resourceparaphraseChapter',
			'resourceparaphrasetextText', 'resourceparaphrasetextAddUserIdParaphrase'));
		while($row = $this->db->fetchRow($resultset))
		{
			++$count;
			$fields = array();
			$values = array();
			$fields[] = 'resourcemetadataResourceId';
			$values[] = $row['resourceparaphraseResourceId'];
			if($row['resourceparaphrasePageStart'])
			{
				$fields[] = 'resourcemetadataPageStart';
				$values[] = $row['resourceparaphrasePageStart'];
			}
			if($row['resourceparaphrasePageEnd'])
			{
				$fields[] = 'resourcemetadataPageEnd';
				$values[] = $row['resourceparaphrasePageEnd'];
			}
			if($row['resourceparaphraseParagraph'])
			{
				$fields[] = 'resourcemetadataParagraph';
				$values[] = $row['resourceparaphraseParagraph'];
			}
			if($row['resourceparaphraseSection'])
			{
				$fields[] = 'resourcemetadataSection';
				$values[] = $row['resourceparaphraseSection'];
			}
			if($row['resourceparaphraseChapter'])
			{
				$fields[] = 'resourcemetadataChapter';
				$values[] = $row['resourceparaphraseChapter'];
			}
			$fields[] = 'resourcemetadataType';
			$values[] = 'p';
			$fields[] = 'resourcemetadataText';
			$values[] = $row['resourceparaphrasetextText'];
			$fields[] = 'resourcemetadataAddUserId';
			$values[] = $row['resourceparaphrasetextAddUserIdParaphrase'];
			$this->db->insert('resource_metadata', $fields, $values);
			$id = $this->db->lastAutoID();
			$updateArray[$row['resourceparaphraseId']] = $id;
			if($count >= $maxCounts)
			{
				$this->db->multiUpdate('resource_keyword', 'resourcekeywordMetadataId', 'resourcekeywordParaphraseId', $updateArray);
				$updateArray = array();
				$count = 0;
			}
// insert paraphrase comments
			$this->db->formatConditions(array('resourceparaphrasecommentParaphraseId' => $row['resourceparaphraseId']));
			$resultset1 = $this->db->select('resource_paraphrase_comment', array('resourceparaphrasecommentComment',
				'resourceparaphrasecommentAddUserIdParaphrase', 'resourceparaphrasecommentTimestamp', 'resourceparaphrasecommentPrivate'));
			while($row1 = $this->db->fetchRow($resultset1))
			{
				++$countI;
				$values_c = array();
				$values_c[] = $this->db->tidyInput($id);
				$values_c[] = $this->db->tidyInput($row1['resourceparaphrasecommentComment']);
				$values_c[] = $this->db->tidyInput($row1['resourceparaphrasecommentAddUserIdParaphrase']);
				$values_c[] = $this->db->tidyInput($row1['resourceparaphrasecommentTimestamp']);
				$values_c[] = $this->db->tidyInput($row1['resourceparaphrasecommentPrivate']);
				$values_c[] = $this->db->tidyInput('pc');
				$insertArray[] = '(' . join(',', $values_c) . ')';
			}
			if($countI >= $maxCounts)
			{
				$this->db->multiInsert('resource_metadata', $fields_c, join(',', $insertArray));
				$insertArray = array();
				$countI = 0;
			}
		}
		if(!empty($insertArray)) // do the remainder
			$this->db->multiInsert('resource_metadata', $fields_c, join(',', $insertArray));
		if(!empty($updateArray)) // do the remainder
			$this->db->multiUpdate('resource_keyword', 'resourcekeywordMetadataId', 'resourcekeywordParaphraseId', $updateArray);
// Transfer musings
		$updateArray = array();
		$count = 0;
		$resultset = $this->db->select('resource_musing', '*');
		while($row = $this->db->fetchRow($resultset))
		{
			++$count;
			$fields = array();
			$values = array();
			$fields[] = 'resourcemetadataResourceId';
			$values[] = $row['resourcemusingResourceId'];
			if($row['resourcemusingPageStart'])
			{
				$fields[] = 'resourcemetadataPageStart';
				$values[] = $row['resourcemusingPageStart'];
			}
			if($row['resourcemusingPageEnd'])
			{
				$fields[] = 'resourcemetadataPageEnd';
				$values[] = $row['resourcemusingPageEnd'];
			}
			if($row['resourcemusingParagraph'])
			{
				$fields[] = 'resourcemetadataParagraph';
				$values[] = $row['resourcemusingParagraph'];
			}
			if($row['resourcemusingSection'])
			{
				$fields[] = 'resourcemetadataSection';
				$values[] = $row['resourcemusingSection'];
			}
			if($row['resourcemusingChapter'])
			{
				$fields[] = 'resourcemetadataChapter';
				$values[] = $row['resourcemusingChapter'];
			}
			$fields[] = 'resourcemetadataType';
			$values[] = 'm';
			$this->db->formatConditions(array('resourcemusingtextId' => $row['resourcemusingId']));
			$row1 = $this->db->selectFirstRow('resource_musing_text',
				array('resourcemusingtextText', 'resourcemusingtextAddUserIdMusing', 'resourcemusingtextTimestamp', 'resourcemusingtextPrivate'));
			$fields[] = 'resourcemetadataText';
			$values[] = $row1['resourcemusingtextText'];
			$fields[] = 'resourcemetadataAddUserId';
			$values[] = $row1['resourcemusingtextAddUserIdMusing'];
			$fields[] = 'resourcemetadataTimestamp';
			$values[] = $row1['resourcemusingtextTimestamp'];
			$fields[] = 'resourcemetadataPrivate';
			$values[] = $row1['resourcemusingtextPrivate'];
			$this->db->insert('resource_metadata', $fields, $values);
			$id = $this->db->lastAutoID();
			$updateArray[$row['resourcemusingId']] = $id;
			if($count >= $maxCounts)
			{
				$this->db->multiUpdate('resource_keyword', 'resourcekeywordMetadataId', 'resourcekeywordMusingId', $updateArray);
				$updateArray = array();
				$count = 0;
			}
		}
		if(!empty($updateArray)) // do the remainder
			$this->db->multiUpdate('resource_keyword', 'resourcekeywordMetadataId', 'resourcekeywordMusingId', $updateArray);
/**
* Do some corrections
*/
// Correct incorrect database summary figures for metadata
		$this->db->formatConditions(array('resourcekeywordKeywordId' => ' IS NULL'));
		$this->db->delete('resource_keyword');
		$mIds = array();
		$resultSet = $this->db->select('resource_metadata', array('resourcemetadataId', 'resourcemetadataText'));
		while($row = $this->db->fetchRow($resultSet))
		{
			if(!$row['resourcemetadataText'])
				$mIds[] = $row['resourcemetadataId'];
		}
		if(!empty($mIds))
		{
			$this->db->formatConditionsOneField($mIds, 'resourcemetadataId');
			$this->db->delete('resource_metadata');
			$this->db->formatConditions(array('resourcemetadataType' => 'q'));
			$num = $this->db->numRows($this->db->select('resource_metadata', 'resourcemetadataId'));
			$this->db->update('database_summary', array('databasesummaryTotalQuotes' => $num));
			$this->db->formatConditions(array('resourcemetadataType' => 'p'));
			$num = $this->db->numRows($this->db->select('resource_metadata', 'resourcemetadataId'));
			$this->db->update('database_summary', array('databasesummaryTotalParaphrases' => $num));
			$this->db->formatConditions(array('resourcemetadataType' => 'm'));
			$num = $this->db->numRows($this->db->select('resource_metadata', 'resourcemetadataId'));
			$this->db->update('database_summary', array('databasesummaryTotalMusings' => $num));
		}
// Correct empty year column error
		$this->db->formatConditions(array('resourceyearYear2' => ' IS NOT NULL'));
		$this->db->formatConditions(array('resourceyearYear2' => ''));
		$resultSet = $this->db->select('resource_year', array('resourceyearId'));
		while($row = $this->db->fetchRow($resultSet))
			$yIds[] = $row['resourceyearId'];
		if(!empty($yIds))
		{
			$this->db->formatConditionsOneField($yIds, 'resourceyearId');
			$this->db->updateNull('resource_year', array('resourceyearYear2'));
		}
		$yIds = array();
		$this->db->formatConditions(array('resourceyearYear3' => ' IS NOT NULL'));
		$this->db->formatConditions(array('resourceyearYear3' => ''));
		$resultSet = $this->db->select('resource_year', array('resourceyearId'));
		while($row = $this->db->fetchRow($resultSet))
			$yIds[] = $row['resourceyearId'];
		if(!empty($yIds))
		{
			$this->db->formatConditionsOneField($yIds, 'resourceyearId');
			$this->db->updateNull('resource_year', array('resourceyearYear2'));
		}
// Correct inclusion of HTML in resource::resourceTitleSort and remove all symbols
		$updateArray = array();
		$count = 0;
		$resultSet = $this->db->select('resource', array('resourceId', 'resourceTitleSort'));
		while($row = $this->db->fetchRow($resultSet))
		{
			$count++;
			$string = \HTML\stripHtml($row['resourceTitleSort']);
// remove all punctuation (keep apostrophe and dash for names such as Grimshaw-Aagaard and D'Eath)
			$updateArray[$row['resourceId']] = preg_replace('/[^\p{L}\p{N}\s\-\']/u', '', $string);
			if($count >= $maxCounts)
			{
				$this->db->multiUpdate('resource', 'resourceTitleSort', 'resourceId', $updateArray);
				$updateArray = array();
				$count = 0;
			}
		}
// Sessionstate fields for all users should be set to NULL as they may contain (SQL) code that relates to old formats and structures
		$this->db->updateNull('users', 'usersUserSession');
// set collectionDefault column in the collections table
		$this->collectionDefaults();
// Strip bbCode from users table
		$this->stripBBCode($maxCounts);
// Alter resourcecreator::creatorSurname field to allow for more accurate ordering
		$this->alterCreatorSurname($maxCounts);

	    $this->updateDbSchema('5.1.1-end');

        $this->stage5_2__1();
		$this->checkStatus('stage5_1__1');
	}
/**
* Upgrade database schema to version 5.2
* Use MySQL utf8 encode and collation utf8_unicode_520_ci
* Lowercase all table names
* Use InnoDB for all tables
*/
	private function stage5_2__1()
	{
        $this->updateDbSchema('5.2');

        $this->stage5_4__1();

        $this->checkStatus('stage5_2__1');
	}
/**
* Upgrade database schema to version 5.4.
* Reconfiguration of config table and shifting many variables to it from config.php
*/
	private function stage5_4__1()
	{
		$this->writeConfigFile(); // dies if not possible
        $pf = $this->config->WIKINDX_DB_TABLEPREFIX;

        $this->updateDbSchema('5.4-begin');

		if(!$this->db->tableExists('configtemp'))
		{
// Update db summary no. in case we have attempted to upgrade a database less than 5.3 (we've been through the previous stages successfully)
			$this->db->update('database_summary', array("databasesummaryDbVersion" => 5.3));
			die("Unable to create 'configtemp' table in the database. Check the database permissions.");
		}
// Read old config and transfer values to temp table
		$row = $this->db->queryFetchFirstRow($this->db->selectNoExecute('config', '*'));
		foreach($row as $key => $value)
		{
		    // In all cases, if a config parameter has an empty value we only keep its name
		    if(!$value)
		    {
		        $this->db->insert('configtemp', array('configName'), array($key));
		        continue; // Jump to the next parameter
		    }

		    // Paramters no longer used
			if(
			    ($key == 'configCaptchaPublicKey') ||
			    ($key == 'configCaptchaPrivateKey') ||
			    ($key == 'configErrorReport') ||
			    ($key == 'configPrintSql')
			)
			{
				continue;
			}
			// Deal with varchar(255) type
			else if(
			    ($key == 'configTitle') ||
				($key == 'configContactEmail') ||
				($key == 'configLanguage') ||
				($key == 'configStyle') ||
				($key == 'configTemplate') ||
				($key == 'configEmailNewRegistrations'))
			{
				$this->db->insert('configtemp', array('configName', 'configVarchar'), array($key, $value));
			}
			// Deal with text type
			else if(
			    ($key == 'configDescription') ||
			    ($key == 'configNoSort') ||
			    ($key == 'configSearchFilter')
			)
			{
				$this->db->insert('configtemp', array('configName', 'configText'), array($key, $value));
			}
			// Deal with varChar(1) type. These are not converted to boolean
			else if(
			    ($key == 'configLastChangesType')
			)
			{
			    // Can be 'D' for 'last days' or 'N' for set number of resources Рconvert to varChar row
			    $tValue = ($value == 'D') ? 'days' : 'number';
				$this->db->insert('configtemp', array('configName', 'configVarchar'), array($key, $tValue));
			}
			// Deal with varChar(1) type. These are converted to boolean
			else if(
			    ($key == 'configMultiUser') ||
		        ($key == 'configUserRegistration') ||
		        ($key == 'configRegistrationModerate') ||
			    ($key == 'configNotify') ||
			    ($key == 'configFileAttach') ||
			    ($key == 'configFileViewLoggedOnOnly') ||
			    ($key == 'configImportBib') ||
			    ($key == 'configEmailNews') ||
			    ($key == 'configQuarantine') ||
			    ($key == 'configListlink') ||
			    ($key == 'configEmailStatistics') ||
			    ($key == 'configMetadataAllow') ||
			    ($key == 'configMetadataUserOnly') ||
			    ($key == 'configDenyReadOnly') ||
			    ($key == 'configReadOnlyAccess') ||
			    ($key == 'configOriginatorEditOnly') ||
			    ($key == 'configGlobalEdit')
			)
			{
				// 'N' or 'Y' Рconvert to 0 and 1 respectively
				$bValue = ($value == 'N') ? 0 : 1;
				$this->db->insert('configtemp', array('configName', 'configBoolean'), array($key, $bValue));
			}
			// Deal with int(11) type
			else if(
			    ($key == 'configFileDeleteSeconds') ||
			    ($key == 'configPaging') ||
			    ($key == 'configPagingMaxLinks') ||
			    ($key == 'configStringLimit') ||
			    ($key == 'configImgWidthLimit') ||
			    ($key == 'configImgHeightLimit') ||
			    ($key == 'configMaxPaste') ||
			    ($key == 'configLastChanges') ||
			    ($key == 'configLastChangesDayLimit') ||
			    ($key == 'configPagingTagCloud')
			)
			{
				$this->db->insert('configtemp', array('configName', 'configInt'), array($key, $value));
			}
			// Deal with datetime type
			else if($key == 'configStatisticsCompiled')
			{
				$this->db->insert('configtemp', array('configName', 'configDatetime'), array($key, $value));
			}
		}
// Now copy across selected config.php variables
		if(isset($this->config->WIKINDX_TIMEZONE) && $this->config->WIKINDX_TIMEZONE)
			$this->db->insert('configtemp', array('configName', 'configText'), array('configTimezone', $this->config->WIKINDX_TIMEZONE));
		else
			$this->db->insert('configtemp', array('configName', 'configText'), array('configTimezone', WIKINDX_TIMEZONE_DEFAULT));
		if(isset($this->config->WIKINDX_RESTRICT_USERID) && $this->config->WIKINDX_RESTRICT_USERID)
			$this->db->insert('configtemp', array('configName', 'configInt'), array('configRestrictUserId', $this->config->WIKINDX_RESTRICT_USERID));
		else
			$this->db->insert('configtemp', array('configName', 'configInt'), array('configRestrictUserId', WIKINDX_RESTRICT_USERID_DEFAULT));
		if(isset($this->config->WIKINDX_DEACTIVATE_RESOURCE_TYPES) &&
			is_array($this->config->WIKINDX_DEACTIVATE_RESOURCE_TYPES) && !empty($this->config->WIKINDX_DEACTIVATE_RESOURCE_TYPES))
			$this->db->insert('configtemp', array('configName', 'configText'), array('configDeactivateResourceTypes',
			base64_encode(serialize($this->config->WIKINDX_DEACTIVATE_RESOURCE_TYPES))));
		else
			$this->db->insert('configtemp', array('configName', 'configText'), array('configDeactivateResourceTypes', base64_encode(serialize(array()))));
		if(isset($this->config->WIKINDX_RSS_ALLOW) && $this->config->WIKINDX_RSS_ALLOW)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configRssAllow', $this->config->WIKINDX_RSS_ALLOW));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configRssAllow', WIKINDX_RSS_ALLOW_DEFAULT));
		if(isset($this->config->WIKINDX_RSS_BIBSTYLE) && $this->config->WIKINDX_RSS_BIBSTYLE)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configRssBibstyle', $this->config->WIKINDX_RSS_BIBSTYLE));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configRssBibstyle', WIKINDX_RSS_BIBSTYLE_DEFAULT));
		if(isset($this->config->WIKINDX_RSS_LIMIT) && $this->config->WIKINDX_RSS_LIMIT)
			$this->db->insert('configtemp', array('configName', 'configInt'), array('configRssLimit', $this->config->WIKINDX_RSS_LIMIT));
		else
			$this->db->insert('configtemp', array('configName', 'configInt'), array('configRssLimit', WIKINDX_RSS_LIMIT_DEFAULT));
		if(isset($this->config->WIKINDX_RSS_DISPLAY) && $this->config->WIKINDX_RSS_DISPLAY)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configRssDisplay', $this->config->WIKINDX_RSS_DISPLAY));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configRssDisplay', WIKINDX_RSS_DISPLAY_DEFAULT));
		if(isset($this->config->WIKINDX_RSS_TITLE) && $this->config->WIKINDX_RSS_TITLE)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configRssTitle', $this->config->WIKINDX_RSS_TITLE));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configRssTitle', WIKINDX_RSS_TITLE_DEFAULT));
		if(isset($this->config->WIKINDX_RSS_DESCRIPTION) && $this->config->WIKINDX_RSS_DESCRIPTION)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configRssDescription', $this->config->WIKINDX_RSS_DESCRIPTION));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configRssDescription', WIKINDX_RSS_DESCRIPTION_DEFAULT));
		if(isset($this->config->WIKINDX_RSS_LANGUAGE) && $this->config->WIKINDX_RSS_LANGUAGE)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configRssLanguage', $this->config->WIKINDX_RSS_LANGUAGE));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configRssLanguage', WIKINDX_RSS_LANGUAGE_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_SERVER) && $this->config->WIKINDX_MAIL_SERVER)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configMailServer', $this->config->WIKINDX_MAIL_SERVER));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configMailServer', WIKINDX_MAIL_SERVER_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_FROM) && $this->config->WIKINDX_MAIL_FROM)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailFrom', $this->config->WIKINDX_MAIL_FROM));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailFrom', WIKINDX_MAIL_FROM_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_REPLYTO) && $this->config->WIKINDX_MAIL_REPLYTO)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailReplyTo', $this->config->WIKINDX_MAIL_REPLYTO));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailReplyTo', WIKINDX_MAIL_REPLYTO_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_RETURN_PATH) && $this->config->WIKINDX_MAIL_RETURN_PATH)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailReturnPath', $this->config->WIKINDX_MAIL_RETURN_PATH));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailReturnPath', WIKINDX_MAIL_RETURN_PATH_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_BACKEND) && $this->config->WIKINDX_MAIL_BACKEND)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailBackend', $this->config->WIKINDX_MAIL_BACKEND));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailBackend', WIKINDX_MAIL_BACKEND_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_SMPATH) && $this->config->WIKINDX_MAIL_SMPATH)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmPath', $this->config->WIKINDX_MAIL_SMPATH));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmPath', WIKINDX_MAIL_SMPATH_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_SMTPSERVER) && $this->config->WIKINDX_MAIL_SMTPSERVER)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmtpServer', $this->config->WIKINDX_MAIL_SMTPSERVER));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmtpServer', WIKINDX_MAIL_SMTPSERVER_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_SMTPPORT) && $this->config->WIKINDX_MAIL_SMTPPORT)
			$this->db->insert('configtemp', array('configName', 'configInt'), array('configMailSmtpPort', $this->config->WIKINDX_MAIL_SMTPPORT));
		else
			$this->db->insert('configtemp', array('configName', 'configInt'), array('configMailSmtpPort', WIKINDX_MAIL_SMTPPORT_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_SMTPENCRYPT) && $this->config->WIKINDX_MAIL_SMTPENCRYPT)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmtpEncrypt', $this->config->WIKINDX_MAIL_SMTPENCRYPT));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmtpEncrypt', WIKINDX_MAIL_SMTPENCRYPT_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_SMTPPERSIST) && $this->config->WIKINDX_MAIL_SMTPPERSIST)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configMailSmtpPersist', $this->config->WIKINDX_MAIL_SMTPPERSIST));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configMailSmtpPersist', WIKINDX_MAIL_SMTPPERSIST_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_SMTPAUTH) && $this->config->WIKINDX_MAIL_SMTPAUTH)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configMailSmtpAuth', $this->config->WIKINDX_MAIL_SMTPAUTH));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configMailSmtpAuth', WIKINDX_MAIL_SMTPAUTH_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_SMTPUSERNAME) && $this->config->WIKINDX_MAIL_SMTPUSERNAME)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmtpUsername', $this->config->WIKINDX_MAIL_SMTPUSERNAME));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmtpUsername', WIKINDX_MAIL_SMTPUSERNAME_DEFAULT));
		if(isset($this->config->WIKINDX_MAIL_SMTPPASSWORD) && $this->config->WIKINDX_MAIL_SMTPPASSWORD)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmtpPassword', $this->config->WIKINDX_MAIL_SMTPPASSWORD));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configMailSmtpPassword', WIKINDX_MAIL_SMTPPASSWORD_DEFAULT));
		if(isset($this->config->WIKINDX_GS_ALLOW) && $this->config->WIKINDX_GS_ALLOW)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configGsAllow', $this->config->WIKINDX_GS_ALLOW));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configGsAllow', WIKINDX_GS_ALLOW_DEFAULT));
		if(isset($this->config->WIKINDX_GS_ATTACHMENT) && $this->config->WIKINDX_GS_ATTACHMENT)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configGsAttachment', $this->config->WIKINDX_GS_ATTACHMENT));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configGsAttachment', WIKINDX_GS_ATTACHMENT_DEFAULT));
		if(isset($this->config->WIKINDX_CMS_ALLOW) && $this->config->WIKINDX_CMS_ALLOW)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configCmsAllow', $this->config->WIKINDX_CMS_ALLOW));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configCmsAllow', WIKINDX_CMS_ALLOW_DEFAULT));
		if(isset($this->config->WIKINDX_CMS_BIBSTYLE) && $this->config->WIKINDX_CMS_BIBSTYLE)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configCmsBibstyle', $this->config->WIKINDX_CMS_BIBSTYLE));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configCmsBibstyle', WIKINDX_CMS_BIBSTYLE_DEFAULT));
		if(isset($this->config->WIKINDX_CMS_LANGUAGE) && $this->config->WIKINDX_CMS_LANGUAGE)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configCmsLanguage', $this->config->WIKINDX_CMS_LANGUAGE));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configCmsLanguage', WIKINDX_CMS_LANGUAGE_DEFAULT));
		if(isset($this->config->WIKINDX_CMS_SQL) && $this->config->WIKINDX_CMS_SQL)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configCmsSql', $this->config->WIKINDX_CMS_SQL));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configCmsSql', WIKINDX_CMS_SQL_DEFAULT));
		if(isset($this->config->WIKINDX_CMS_DB_USER) && $this->config->WIKINDX_CMS_DB_USER)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configCmsDbUser', $this->config->WIKINDX_CMS_DB_USER));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configCmsDbUser', WIKINDX_CMS_DB_USER_DEFAULT));
		if(isset($this->config->WIKINDX_CMS_DB_PASSWORD) && $this->config->WIKINDX_CMS_DB_PASSWORD)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configCmsDbPassword', $this->config->WIKINDX_CMS_DB_PASSWORD));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configCmsDbPassword', WIKINDX_CMS_DB_PASSWORD_DEFAULT));
		if(isset($this->config->WIKINDX_TAG_LOW_COLOUR) && $this->config->WIKINDX_TAG_LOW_COLOUR)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configTagLowColour', $this->config->WIKINDX_TAG_LOW_COLOUR));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configTagLowColour', WIKINDX_TAG_LOW_COLOUR_DEFAULT));
		if(isset($this->config->WIKINDX_TAG_HIGH_COLOUR) && $this->config->WIKINDX_TAG_HIGH_COLOUR)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configTagHighColour', $this->config->WIKINDX_TAG_HIGH_COLOUR));
		else
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configTagHighColour', WIKINDX_TAG_HIGH_COLOUR_DEFAULT));
		if(isset($this->config->WIKINDX_TAG_LOW_SIZE) && $this->config->WIKINDX_TAG_LOW_SIZE)
			$this->db->insert('configtemp', array('configName', 'configFloat'), array('configTagLowSize', $this->config->WIKINDX_TAG_LOW_SIZE));
		else
			$this->db->insert('configtemp', array('configName', 'configFloat'), array('configTagLowSize', WIKINDX_TAG_LOW_SIZE_DEFAULT));
		if(isset($this->config->WIKINDX_TAG_HIGH_SIZE) && $this->config->WIKINDX_TAG_HIGH_SIZE)
			$this->db->insert('configtemp', array('configName', 'configFloat'), array('configTagHighSize', $this->config->WIKINDX_TAG_HIGH_SIZE));
		else
			$this->db->insert('configtemp', array('configName', 'configFloat'), array('configTagHighSize', WIKINDX_TAG_HIGH_SIZE_DEFAULT));
		if(isset($this->config->WIKINDX_IMAGES_ALLOW) && $this->config->WIKINDX_IMAGES_ALLOW)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configImagesAllow', $this->config->WIKINDX_IMAGES_ALLOW));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configImagesAllow', WIKINDX_IMAGES_ALLOW_DEFAULT));
		if(isset($this->config->WIKINDX_IMAGES_MAXSIZE) && $this->config->WIKINDX_IMAGES_MAXSIZE)
			$this->db->insert('configtemp', array('configName', 'configInt'), array('configImagesMaxSize', $this->config->WIKINDX_IMAGES_MAXSIZE));
		else
			$this->db->insert('configtemp', array('configName', 'configInt'), array('configImagesMaxSize', WIKINDX_IMAGES_MAXSIZE_DEFAULT));
		if(isset($this->config->WIKINDX_DEBUG_ERRORS) && $this->config->WIKINDX_DEBUG_ERRORS)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configErrorReport', $this->config->WIKINDX_DEBUG_ERRORS));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configErrorReport', WIKINDX_DEBUG_ERRORS_DEFAULT));
		if(isset($this->config->WIKINDX_DEBUG_EMAIL) && $this->config->WIKINDX_DEBUG_EMAIL)
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configSqlEmail', $this->config->WIKINDX_DEBUG_EMAIL));
		else // NB database name change for this field!
			$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configSqlEmail', WIKINDX_DEBUG_EMAIL_DEFAULT));
		if(isset($this->config->WIKINDX_DEBUG_SQL) && $this->config->WIKINDX_DEBUG_SQL)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configPrintSql', $this->config->WIKINDX_DEBUG_SQL));
		else // NB database name change for this field!
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configPrintSql', WIKINDX_DEBUG_SQL_DEFAULT));
// Add extra fields
		$this->db->insert('configtemp', array('configName', 'configVarchar'), array('configSqlErrorOutput', 'printSql'));
		if(isset($this->config->WIKINDX_BYPASS_SMARTYCOMPILE) && $this->config->WIKINDX_BYPASS_SMARTYCOMPILE)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configBypassSmartyCompile',
			$this->config->WIKINDX_BYPASS_SMARTYCOMPILE));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configBypassSmartyCompile', WIKINDX_BYPASS_SMARTYCOMPILE_DEFAULT));
		if(isset($this->config->WIKINDX_DISPLAY_STATISTICS) && $this->config->WIKINDX_DISPLAY_STATISTICS)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configDisplayStatistics', $this->config->WIKINDX_DISPLAY_STATISTICS));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configDisplayStatistics', WIKINDX_DISPLAY_STATISTICS_DEFAULT));
		if(isset($this->config->WIKINDX_DISPLAY_USER_STATISTICS) && $this->config->WIKINDX_DISPLAY_USER_STATISTICS)
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configDisplayUserStatistics', $this->config->WIKINDX_DISPLAY_USER_STATISTICS));
		else
			$this->db->insert('configtemp', array('configName', 'configBoolean'), array('configDisplayUserStatistics', WIKINDX_DISPLAY_USER_STATISTICS_DEFAULT));

		$this->updateDbSchema('5.4-end');

		$this->session->setVar("setup_UserId", 1);
		$user = FACTORY_USER::getInstance();
		$user->writeSessionPreferences(FALSE, 'config', TRUE);

        $this->stage5_5__1();
        $this->checkStatus('stage5_4__1');
    }
/**
* Upgrade database schema to version 5.5.
* Addition of new fields to users table for auth security and GDPR
*/
	private function stage5_5__1()
	{
        $this->updateDbSchema('5.5');
        $this->updatePluginTables();
        $this->stage5_6__1();
        $this->checkStatus('stage5_5__1');
    }
/**
* Upgrade database schema to version 5.6.
* Convert the database to utf8 charset and utf8_unicode_ci collation
*/
	private function stage5_6__1()
	{
        $this->updateDbSchema('5.6');
        $this->stage5_7__1();
        $this->checkStatus('stage5_6__1');
    }
/**
* Upgrade database schema to version 5.7.
* Convert the database to utf8mb4 charset and utf8mb4_unicode_520_ci collation
* Fix resource_metadata.resourcemetadataPrivate size to 1 character
*/
	private function stage5_7__1()
	{
        $this->correctIndices();
        $this->updateDbSchema('5.7');
        $this->correctDatetimeFields();
        $this->stage5_8__1();
        $this->checkStatus('stage5_7__1');
    }
/**
* Upgrade database schema to version 5.8. There are no changes to DB structure so no call to updateDbSchema('5.8').
* Check resource totals are correct
* Check creator correlations are correct
*/
	private function stage5_8__1()
	{
        $this->correctTotals();
        $this->correctCreators();
        $this->checkStatus('stage5_8__1');
    }
/**
* Correct resource totals. 
*/
	private function correctTotals()
	{
		$num = $this->db->numRows($this->db->select('resource', 'resourceId'));
		$this->db->update('database_summary', array('databasesummaryTotalResources' => $num));
		$this->db->formatConditions(array('resourcemetadataType' => 'q'));
		$num = $this->db->numRows($this->db->select('resource_metadata', 'resourcemetadataId'));
		$this->db->update('database_summary', array('databasesummaryTotalQuotes' => $num));
		$this->db->formatConditions(array('resourcemetadataType' => 'p'));
		$num = $this->db->numRows($this->db->select('resource_metadata', 'resourcemetadataId'));
		$this->db->update('database_summary', array('databasesummaryTotalParaphrases' => $num));
		$this->db->formatConditions(array('resourcemetadataType' => 'm'));
		$num = $this->db->numRows($this->db->select('resource_metadata', 'resourcemetadataId'));
		$this->db->update('database_summary', array('databasesummaryTotalMusings' => $num));
	}
/**
* Fix creator errors
* In some cases, 'resourcecreatorCreatorSurname' does not match the id in resourcecreatorCreatorMain
*/
	private function correctCreators()
	{
		$creatorIds = array();
		$this->db->formatConditions(array('resourcecreatorCreatorMain' => 'IS NOT NULL'));
		$resultSet1 = $this->db->select('resource_creator', array('resourcecreatorCreatorMain', 'resourcecreatorCreatorSurname'));
		$resultSet2 = $this->db->select('creator', array('creatorId', 'creatorSurname'));
		while($row = $this->db->fetchRow($resultSet2))
			$creatorIds[$row['creatorId']] = mb_strtolower(preg_replace("/[^[:alnum:][:space:]]/u", '', $row['creatorSurname']));
		while($row = $this->db->fetchRow($resultSet1))
		{
			$rcSurname = mb_strtolower(preg_replace("/[^[:alnum:][:space:]]/u", '', $row['resourcecreatorCreatorSurname']));
			if($rcSurname != $creatorIds[$row['resourcecreatorCreatorMain']])
			{
				$this->db->formatConditions(array('resourcecreatorCreatorMain' => $row['resourcecreatorCreatorMain']));
				$this->db->update('resource_creator', array('resourcecreatorCreatorSurname' => $creatorIds[$row['resourcecreatorCreatorMain']]));
			}
		}
	}
/**
* Correct parameters of indices that are varchars by ensuring they have a limited prefix of (100)
*
* There is no DROP INDEX IF EXISTS . . . syntax so another way must be found to check if the index exists before trying to drop it
*/
	private function correctIndices()
	{
		$db = $this->config->WIKINDX_DB;
		foreach(array('category', 'collection', 'config', 'creator', 'keyword', 'publisher', 'resource', 'resource_creator',
			'resource_metadata', 'resource_year', 'user_bibliography') as $table)
		{
			$table = $this->config->WIKINDX_DB_TABLEPREFIX . $table;
			$resultSet = $this->db->query("SHOW INDEX FROM `$table` FROM `$db`");
			while($row = $this->db->fetchRow($resultSet))
			{
				if($row['Key_name'] == 'categoryCategory')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'collectionTitle')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'configName')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'creatorSurname')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'keywordKeyword')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'publisherName')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'resourceType')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'resourceTitleSort') // NB not to be added again
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'resourcecreatorCreatorSurname')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'resourcemetadataResourceId') // NB not a varchar index . . .
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'resourceyearYear1')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
				else if($row['Key_name'] == 'userbibliographyTitle')
					$this->db->query("DROP INDEX `" . $row['Key_name'] . "` ON `$table`");
			}
		}
	}
/**
* Correct anomalies in the datetime fields Рthere should be no occurrence of '0000-00-00 00:00:00' as a value.
*
* The strategy is:
* 1. If default is NULL, set all incorrect values to that. Otherwise,
* 2. Find the minimum value in the table then set all incorrect fields to that. Otherwise,
* 3. If all values are incorrect, then set all values to default.
*/
	private function correctDatetimeFields()
	{
// user_register
		$this->db->formatConditions($this->db->formatFields('userregisterTimestamp'));
		$minArray = $this->db->selectMin('user_register', 'userregisterTimestamp');
		$min = $minArray[0]['userregisterTimestamp'];
		$this->db->formatConditions(array('userregisterTimestamp' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('user_register', array('userregisterTimestamp' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('user_register', array('userregisterTimestamp' => '')); // default is CURRENT_TIMESTAMP
// users
		$this->db->formatConditions($this->db->formatFields('usersTimestamp'));
		$minArray = $this->db->selectMin('users', 'usersTimestamp');
		$min = $minArray[0]['usersTimestamp'];
		$this->db->formatConditions(array('usersTimestamp' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('users', array('usersTimestamp' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('users', array('usersTimestamp' => '')); // default is CURRENT_TIMESTAMP
		$this->db->formatConditions($this->db->formatFields('usersNotifyTimestamp'));
		$minArray = $this->db->selectMin('users', 'usersNotifyTimestamp');
		$min = $minArray[0]['usersNotifyTimestamp'];
		$this->db->formatConditions(array('usersNotifyTimestamp' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('users', array('usersNotifyTimestamp' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('users', array('usersNotifyTimestamp' => '')); // default is CURRENT_TIMESTAMP
		$this->db->formatConditions($this->db->formatFields('usersChangePasswordTimestamp'));
		$minArray = $this->db->selectMin('users', 'usersChangePasswordTimestamp');
		$min = $minArray[0]['usersChangePasswordTimestamp'];
		$this->db->formatConditions(array('usersChangePasswordTimestamp' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('users', array('usersChangePasswordTimestamp' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('users', array('usersChangePasswordTimestamp' => '')); // default is CURRENT_TIMESTAMP
// resource_timestamp
		$this->db->formatConditions($this->db->formatFields('resourcetimestampTimestampAdd'));
		$minArray = $this->db->selectMin('resource_timestamp', 'resourcetimestampTimestampAdd');
		$min = $minArray[0]['resourcetimestampTimestampAdd'];
		$this->db->formatConditions(array('resourcetimestampTimestampAdd' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('resource_timestamp', array('resourcetimestampTimestampAdd' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('resource_timestamp', array('resourcetimestampTimestampAdd' => '')); // default is CURRENT_TIMESTAMP
		$this->db->formatConditions($this->db->formatFields('resourcetimestampTimestamp'));
		$minArray = $this->db->selectMin('resource_timestamp', 'resourcetimestampTimestamp');
		$min = $minArray[0]['resourcetimestampTimestamp'];
		$this->db->formatConditions(array('resourcetimestampTimestamp' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('resource_timestamp', array('resourcetimestampTimestamp' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('resource_timestamp', array('resourcetimestampTimestamp' => '')); // default is CURRENT_TIMESTAMP
// resource_attachments
		$this->db->formatConditions($this->db->formatFields('resourceattachmentsEmbargoUntil'));
		$minArray = $this->db->selectMin('resource_attachments', 'resourceattachmentsEmbargoUntil');
		$min = $minArray[0]['resourceattachmentsEmbargoUntil'];
		$this->db->formatConditions(array('resourceattachmentsEmbargoUntil' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('resource_attachments', array('resourceattachmentsEmbargoUntil' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('resource_attachments', array('resourceattachmentsEmbargoUntil' => '')); // default is CURRENT_TIMESTAMP
		$this->db->formatConditions($this->db->formatFields('resourceattachmentsTimestamp'));
		$minArray = $this->db->selectMin('resource_attachments', 'resourceattachmentsTimestamp');
		$min = $minArray[0]['resourceattachmentsTimestamp'];
		$this->db->formatConditions(array('resourceattachmentsTimestamp' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('resource_attachments', array('resourceattachmentsTimestamp' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('resource_attachments', array('resourceattachmentsTimestamp' => '')); // default is CURRENT_TIMESTAMP
// news
		$this->db->formatConditions($this->db->formatFields('newsTimestamp'));
		$minArray = $this->db->selectMin('news', 'newsTimestamp');
		$min = $minArray[0]['newsTimestamp'];
		$this->db->formatConditions(array('newsTimestamp' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('news', array('newsTimestamp' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('news', array('newsTimestamp' => '')); // default is CURRENT_TIMESTAMP
// resource_metadata
		$this->db->formatConditions($this->db->formatFields('resourcemetadataTimestamp'));
		$minArray = $this->db->selectMin('resource_metadata', 'resourcemetadataTimestamp');
		$min = $minArray[0]['resourcemetadataTimestamp'];
		$this->db->formatConditions(array('resourcemetadataTimestamp' => '0000-00-00 00:00:00'));
		if($min)
			$this->db->updateTimestamp('resource_metadata', array('resourcemetadataTimestamp' => $this->db->tidyInput($min)));
		else
			$this->db->updateTimestamp('resource_metadata', array('resourcemetadataTimestamp' => ''));
		$this->db->formatConditions(array('resourcemetadataTimestampEdited' => '0000-00-00 00:00:00'));
		$this->db->updateNull('resource_metadata', 'resourcemetadataTimestampEdited'); // default is NULL
	}
/**
* Write new config.php with upgrade to >= WIKINDX v5.3
*/
	private function writeConfigFile()
	{
$string = <<< END
<?php
/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

Creative Commons
Creative Commons Legal Code
Attribution-NonCommercial-ShareAlike 2.0
THE WORK IS PROVIDED UNDER THE TERMS OF THIS CREATIVE COMMONS PUBLIC LICENSE ("CCPL" OR "LICENSEө Ѡdocs/LICENSE.txt.
THE WORK IS PROTECTED BY COPYRIGHT AND/OR OTHER APPLICABLE LAW. ANY USE OF THE WORK OTHER THAN AS AUTHORIZED UNDER
THIS LICENSE OR COPYRIGHT LAW IS PROHIBITED.

BY EXERCISING ANY RIGHTS TO THE WORK PROVIDED HERE, YOU ACCEPT AND AGREE TO BE BOUND BY THE TERMS OF THIS LICENSE.
THE LICENSOR GRANTS YOU THE RIGHTS CONTAINED HERE IN CONSIDERATION OF YOUR ACCEPTANCE OF SUCH TERMS AND CONDITIONS.

The WIKINDX Team 2018
sirfragalot@users.sourceforge.net
**********************************************************************************/
/**
*
* WIKINDX v5.x CONFIGURATION FILE
*
* NB. BEFORE YOU MAKE CHANGES TO THIS FILE, BACK IT UP!
* NB. BEFORE YOU MAKE CHANGES TO THIS FILE, BACK IT UP!
* NB. BEFORE YOU MAKE CHANGES TO THIS FILE, BACK IT UP!
*
* If you make changes, backup the edited file as future upgrades of WIKINDX might overwrite this file - no questions asked!
*/

/**********************************************************************************/

class CONFIG
{
/*****
* START DATABASE CONFIGURATION
*****/
// NB:
// wikindx supports only MySQL with mysqli PHP driver (WIKINDX_DB_TYPE parameter is deprecated).
//
// The database and permissions for accessing it must be created using your RDBMS client. Wikindx
// will NOT do this for you.  If unsure how to do this, contact your server admin. After you have
// set up an empty database with the correct permissions (GRANT ALL), the first running of Wikindx
// will create the necessary database tables.
//
// WIKINDX uses caching in the database _cache table for lists of creators, keywords etc.  If you have a large
// database, you may get SQL errors as WIKINDX attempts to write these cache data.  You will need to increase
// max allowed packet in my.cnf and restart the MySQL server.
//
// Host on which the relational db management system (i.e. the MySQL server) is running (usually localhost if
// the web files are on the same server as the RDBMS although some web hosting services may specify something like
// localhost:/tmp/mysql5.sock)
// If your DB server is on a non-standard socket (i.e. not port 3306), then you should set something like localhost:xxxx 
// where 'xxxx' is the non-standard socket.

END;
		$string .= 'public $WIKINDX_DB_HOST = "' . $this->config->WIKINDX_DB_HOST . '";' . "\n";
		$string .= '// name of the database which these scripts interface with (case-sensitive):' . "\n" .
		           'public $WIKINDX_DB = "' . $this->config->WIKINDX_DB . '";' . "\n";
		$string .= '// username and password required to connect to and open the database' . "\n" .
			       '// (it is strongly recommended that you change these default values):' . "\n" .
			       'public $WIKINDX_DB_USER = "' . $this->config->WIKINDX_DB_USER . '";' . "\n" .
			       'public $WIKINDX_DB_PASSWORD = "' . $this->config->WIKINDX_DB_PASSWORD . '";' . "\n";
		$string .= '// If using WIKINDX on a shared database, set the WIKINDX table prefix here (lowercase only)' . "\n" .
			       '// (do not change after running WIKINDX and creating the tables!):' . "\n" .
			       'public $WIKINDX_DB_TABLEPREFIX = "' . $this->config->WIKINDX_DB_TABLEPREFIX . '";' . "\n";
		$string .= '// WIKINDX uses MySQL persistent connections by default.' . "\n" .
			       '// Some hosting services are not configured for this: if you have problems' . "\n" .
			       "// connecting to your MySQL server and/or receive error messages about 'too many connections'," . "\n" .
			       '// set $WIKINDX_DB_PERSISTENT to FALSE' . "\n";
		if($this->config->WIKINDX_DB_PERSISTENT === TRUE)
			$string .= 'public $WIKINDX_DB_PERSISTENT = TRUE;' . "\n";
		else
			$string .= 'public $WIKINDX_DB_PERSISTENT = FALSE;' . "\n";
$string .= <<< END
/*****
* END DATABASE CONFIGURATION
*****/

/**********************************************************************************/

/*****
* START PATHS CONFIGURATION
*****/
// You must define the base URL for the WIKINDX installation.
// You have to indicate protocol HTTP / HTTPS and remove the terminal /.
// e.g. if wikindx's index.php file is in /wikindx5/ under the httpd/ (or similar)
// folder on the www.myserver.com, then set the variable
// to http://www.myserver.com/wikindx5

END;
// We set it because we know best ;)
		$this->config->WIKINDX_BASE_URL = FACTORY_URL::getInstance()->getBaseUrl();
		$string .= 'public $WIKINDX_BASE_URL = "' . $this->config->WIKINDX_BASE_URL . '";' . "\n";

$string .= <<< END
// The TinyMCE editor needs the WIKINDX server installation path.
// WIKINDX tries to get this through getcwd() but this is not always possible.
// In this case, you will receive an error message and WIKINDX will die and you should then set that path here.
// The path should be the full path from the root folder to your wikindx5 folder with no trailing '/'.
// On Apple OSX running XAMPP, for example, the case-sensitive path is:
// '/Applications/XAMPP/xamppfiles/htdocs/wikindx5'.
// The script will continue to die until it has a valid installation path.
// If you get no error message and WIKINDX runs fine, then you can leave this value as FALSE.

END;
		if(property_exists($this->config, 'WIKINDX_WIKINDX_PATH') && ($this->config->WIKINDX_WIKINDX_PATH !== FALSE))
			$string .= 'public $WIKINDX_WIKINDX_PATH = "' . $this->config->WIKINDX_WIKINDX_PATH . '";' . "\n";
		else
			$string .= 'public $WIKINDX_WIKINDX_PATH = FALSE;' . "\n";
$string .= <<< END
// On some web-hosting services, disk quotas are set on session storage directories.
// WIKINDX is a heavy user of sessions and the folder set for storing session data may fill up quickly causing session write errors.
// This may manifest itself as XML files only being partly written or not all data being added to the database when creating a new resource.
// Additionally, large numbers of old session files can cause significant slow-down of the system.
// See here for a report on the problem: https://sourceforge.net/projects/wikindx/forums/forum/326884/topic/4477010
// This variable sets an alternate path for storing sessions (which can be in your home directory).
// If this variable is FALSE, the default session  path, as set in php.ini, will be used.
// Otherwise, the directory must exist and be writable by the web server user.
// There should be no trailing '\' or '/'.
// For example, for a windows system, WIKINDX_SESSION_PATH might be "D:\sessions"

END;
		if(property_exists($this->config, 'WIKINDX_SESSION_PATH') && ($this->config->WIKINDX_SESSION_PATH !== FALSE))
			$string .= 'public $WIKINDX_SESSION_PATH = "' . $this->config->WIKINDX_SESSION_PATH . '";' . "\n";
		else
			$string .= 'public $WIKINDX_SESSION_PATH = FALSE;' . "\n";
$string .= <<< END
// If WIKINDX_SESSION_PATH is set, its value will remove sessions in the above directory older than xx days.
// Set to 0 to never remove sessions.
// Do NOT use quotes around the value.

END;
		if(property_exists($this->config, 'WIKINDX_SESSION_PATH_CLEAR') && ($this->config->WIKINDX_SESSION_PATH_CLEAR !== FALSE))
			$string .= 'public $WIKINDX_SESSION_PATH_CLEAR = ' . $this->config->WIKINDX_SESSION_PATH_CLEAR . ';' . "\n";
		else
			$string .= 'public $WIKINDX_SESSION_PATH_CLEAR = FALSE;' . "\n";
$string .= <<< END
// Alternate locations for storing attachments and exported files.
// If these are FALSE, the default locations at the top level of wikindx5/ will be used.
// It is the administrator's responsibility to ensure that these directories are web-server user readable and writeable.
// There should be no trailing '\' or '/'.
// For example, for a windows system, WIKINDX_ATTACHMENTS_DIR might be "D:\attachments"
// For example, for a *NIX system, WIKINDX_FILE_PATH might be "files"

END;
		if(property_exists($this->config, 'WIKINDX_ATTACHMENTS_PATH') && ($this->config->WIKINDX_ATTACHMENTS_PATH !== FALSE))
			$string .= 'public $WIKINDX_ATTACHMENTS_PATH = "' . $this->config->WIKINDX_ATTACHMENTS_PATH . '";' . "\n";
		else
			$string .= 'public $WIKINDX_ATTACHMENTS_PATH = FALSE;' . "\n";
		if(property_exists($this->config, 'WIKINDX_FILE_PATH') && ($this->config->WIKINDX_FILE_PATH !== FALSE))
			$string .= 'public $WIKINDX_FILE_PATH = "' . $this->config->WIKINDX_FILE_PATH . '";' . "\n";
		else
			$string .= 'public $WIKINDX_FILE_PATH = FALSE;' . "\n";
$string .= <<< END
/*****
* END PATH CONFIGURATION
*****/

/**********************************************************************************/

/*****
* START PHP MEMORY AND EXECUTION CONFIGURATION
*****/
// WIKINDX usually runs with the standard PHP memory_limit of 32MB.
// With some PHP configurations, however, this is not enough -- a mysterious blank page is often the result.
// If you are unable to update php.ini's memory_limit yourself, WIKINDX_MEMORY_LIMIT may be set (an integer such as 64 or 128 followed by 'M').
// Despite the PHP manual stating that this may not be set outside of php.ini, it seems to work most of the time.
// It is not, however, guaranteed to do so and editing php.ini is the preferred method particularly if your PHP is in 'safe' mode.
// Use double quotes around the value.

END;
		if(property_exists($this->config, 'WIKINDX_MEMORY_LIMIT') && ($this->config->WIKINDX_MEMORY_LIMIT !== FALSE))
			$string .= 'public $WIKINDX_MEMORY_LIMIT = "' . $this->config->WIKINDX_MEMORY_LIMIT . '";' . "\n";
		else
			$string .= 'public $WIKINDX_MEMORY_LIMIT = FALSE;' . "\n";
$string .= <<< END
// WIKINDX should run fine with the PHP standard execution timeouts (typically 30 seconds) but,
// in some cases such as database upgrading of a large database on a slow server, you will need to increase the timeout figure.
// If this is FALSE, the value set in php.ini is used.
// Despite the PHP manual stating that this may not be set outside of php.ini, it seems to work most of the time.
// It is not, however, guaranteed to do so and editing php.ini is the preferred method particularly if your PHP is in 'safe' mode.
// The value is in seconds.
// Do NOT use quotes around the value.

END;
		if(property_exists($this->config, 'WIKINDX_MAX_EXECUTION_TIMEOUT') && ($this->config->WIKINDX_MAX_EXECUTION_TIMEOUT !== FALSE))
			$string .= 'public $WIKINDX_MAX_EXECUTION_TIMEOUT = ' . $this->config->WIKINDX_MAX_EXECUTION_TIMEOUT . ';' . "\n";
		else
			$string .= 'public $WIKINDX_MAX_EXECUTION_TIMEOUT = FALSE;' . "\n";
$string .= <<< END
// WIKINDX_MAX_WRITECHUNK concerns how many resources are exported and written to file in one go.
// If your WIKINDX contains several thousands of resources and you wish to export them all (e.g. to bibTeX or Endnote),
// then you may run into memory problems which will manifest as either
// a blank page when you attempt to export or an error report (if you have error reporting turned on).
// WIKINDX_MAX_WRITECHUNK breaks down the SQL querying of resources and subsequent writing of resources to file into manageable chunks.
// As a rough guide, with a WIKINDX_MEMORY_LIMIT of 32M, WIKINDX_MAX_WRITECHUNK of 700 should work fine and with 64M, 1500 works fine.
// If WIKINDX_MAX_WRITECHUNK is FALSE, the chunk is set to 10,000.
// This can be a tricky figure to set as setting the figure too low increases SQL and PHP execution times significantly.
// Do NOT use quotes around the value.

END;
		if(property_exists($this->config, 'WIKINDX_MAX_WRITECHUNK') && ($this->config->WIKINDX_MAX_WRITECHUNK !== FALSE))
			$string .= 'public $WIKINDX_MAX_WRITECHUNK = ' . $this->config->WIKINDX_MAX_WRITECHUNK . ';' . "\n";
		else
			$string .= 'public $WIKINDX_MAX_WRITECHUNK = FALSE;' . "\n";
$string .= <<< END
/*****
* END PHP MEMORY AND EXECUTION CONFIGURATION
*****/
}
END;
		$string .= "\n" . '?>';


		// Save the old config file before writing it
		// Something could go wrong and configuration lost otherwise
		$cf = 'config.php';
		$bf = $this->config->WIKINDX_FILES_DIR . DIRECTORY_SEPARATOR . $cf . '.' . date('YmdHis');
		if (copy($cf, $bf))
		{
		    if (is_writable($cf))
		    {
		        if (file_put_contents($cf, $string) === FALSE)
		            die("Fatal error: an error occurred when writing to $cf");
		    }
		    else
		        die("Fatal error: $cf is not writable");
		}
		else
		    die("Fatal error: could not backup $cf to $bf");
	}
/**
* Copy papers table (word processor) to new format if it exists then drop it. Upgrade the soundExplorer table
*/
	private function updatePluginTables()
	{
// NB: Windows MySQL lowercases any table name
// To be sure, it is necessary to lowercase all table elements
		$tables = $this->db->listTables(FALSE);
		foreach($tables as $k => $v)
			$tables[$k] = mb_strtolower($v);
// If there is an existing papers table (from wikindx v3.8.x), copy fields across and drop table
		if(array_search('papers', $tables) !== FALSE)
		{
			if(array_search('plugin_wordprocessor', $tables) === FALSE)
				$this->db->queryNoError("
					CREATE TABLE `" . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_wordprocessor` (
						`pluginwordprocessorId` int(11) NOT NULL AUTO_INCREMENT,
						`pluginwordprocessorHashFilename` varchar(1020) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
						`pluginwordprocessorFilename` varchar(1020) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
						`pluginwordprocessorUserId` int(11) NOT NULL,
						`pluginwordprocessorTimestamp` datetime NOT NULL,
						PRIMARY KEY (`pluginwordprocessorId`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
				");
			$resultset = $this->db->select('papers', array('papersId', 'papersHashFilename', 'papersUserId', 'papersFilename', 'papersTimestamp'));
			while($row = $this->db->fetchRow($resultset))
			{
				$fields = $values = array();
				$fields[] = 'pluginwordprocessorId';
				$values[] = $row['papersId'];
				$fields[] = 'pluginwordprocessorHashFilename';
				$values[] = $row['papersHashFilename'];
				$fields[] = 'pluginwordprocessorUserId';
				$values[] = $row['papersUserId'];
				$fields[] = 'pluginwordprocessorFilename';
				$values[] = $row['papersFilename'];
				$fields[] = 'pluginwordprocessorTimestamp';
				$values[] = $row['papersTimestamp'];
				$this->db->insert('plugin_wordprocessor', $fields, $values);
			}
			$this->db->queryNoError("DROP TABLE IF EXISTS " . $this->config->WIKINDX_DB_TABLEPREFIX . "papers;");
		}
		else if(array_search('plugin_wordprocessor', $tables) !== FALSE)
		{
		    $this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_wordprocessor ENGINE=InnoDB;");
		    $this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_wordprocessor CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;");
		    $this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_wordprocessor MODIFY COLUMN `pluginwordprocessorHashFilename` varchar(1020) DEFAULT NULL;");
		    $this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_wordprocessor MODIFY COLUMN `pluginwordprocessorFilename` varchar(1020) DEFAULT NULL;");
		}
		if(array_search('plugin_soundexplorer', $tables) !== FALSE)
		{
			$this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_soundexplorer RENAME `"  . $this->config->WIKINDX_DB_TABLEPREFIX . "4fc387ba1ae34ac28e6dee712679d7b5`");
			$this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "4fc387ba1ae34ac28e6dee712679d7b5 RENAME `"  . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_soundexplorer`");
		    $this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_soundexplorer ENGINE=InnoDB;");
		    $this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_soundexplorer CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci;");
		    $this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_soundexplorer MODIFY COLUMN `pluginsoundexplorerLabel` varchar(1020) DEFAULT NOT NULL;");
		    $this->db->queryNoError("ALTER TABLE " . $this->config->WIKINDX_DB_TABLEPREFIX . "plugin_soundexplorer MODIFY COLUMN `pluginsoundexplorerArray` text DEFAULT NOT NULL;");
		}
	}
/**
* Alter resourcecreator::creatorSurname field to allow for more accurate ordering
*
* @param int $maxCounts
*/
	private function alterCreatorSurname($maxCounts)
	{
		$count = 0;
		$updateArray = array();
		$this->db->formatConditions($this->db->formatFields('resourcecreatorCreatorSurname') . ' IS NOT NULL');
		$this->db->leftJoin('creator', 'creatorId', 'resourcecreatorCreatorMain');
		$resultset = $this->db->select('resource_creator', array('creatorSurname', 'resourcecreatorId'));
		while($row = $this->db->fetchRow($resultset))
		{
			$count++;
// remove all punctuation (keep apostrophe and dash for names such as Grimshaw-Aagaard and D'Eath)
			$updateArray[$row['resourcecreatorId']] = mb_strtolower(preg_replace('/[^\p{L}\p{N}\s\-\']/u', '', $row['creatorSurname']));

			if($count >= $maxCounts)
			{
				$this->db->multiUpdate('resource_creator', 'resourcecreatorCreatorSurname', 'resourcecreatorId', $updateArray);
				$updateArray = array();
				$count = 0;
			}
		}
	}
/**
* Strip BBCode from users table
*
* @param int $maxCounts
*/
	private function stripBBCode($maxCounts)
	{
	    include_once("core/display/BBCODE.php");
		$fields = array('usersFullname', 'usersDepartment', 'usersInstitution');
		foreach ($fields as $field)
		{
    		$count = 0;
    		$updateArray = array();
    		$this->db->formatConditions($this->db->formatFields($field) . ' IS NOT NULL');
    		$this->db->formatConditions($this->db->formatFields($field) . $this->db->like('%', '\\[', '%'));
    		$resultset = $this->db->select('users', array('usersId', $field));
    		while($row = $this->db->fetchRow($resultset))
    		{
    			$count++;
    			$updateArray[$row['usersId']] = BBCODE::stripBBCode($row[$field]);

    			if($count >= $maxCounts)
    			{
    				$this->db->multiUpdate('users', 'usersId', $field, $updateArray);
    				$updateArray = array();
    				$count = 0;
    			}
    		}
		}
	}
/**
* Set collectionDefault column in the collections table
*/
	private function collectionDefaults()
	{
		include_once("core/collection/COLLECTIONDEFAULTMAP.php");
		$defaultMap = new COLLECTIONDEFAULTMAP();
		$typesArray = array_unique(array_values($defaultMap->collectionTypes));
		$collectionArray = array();
		$maxPacket = $this->db->getMaxPacket();
// For each 1MB max_allowed_packet (1048576 bytes), 600 updates in one go seems fine as a value for $maxCounts (based on trial and error)
		$maxCounts = floor(600 * ($maxPacket/1048576));
		foreach($typesArray as $type)
		{
			$fieldNames = array();
			foreach($defaultMap->{$type} as $typeKey => $typeKeyArray)
			{
				$typeKey = str_replace('_', '', $typeKey);
				if(($typeKey == 'resource') && !empty($typeKeyArray))
				{
					$this->db->leftJoin('resource', 'resourceId', 'resourcemiscId');
					foreach($typeKeyArray as $key => $value)
					{
						$fieldName = $typeKey . $key;
						$fieldNames[] = $fieldName;
					}
				}
				else if(($typeKey == 'resourcemisc') && !empty($typeKeyArray))
				{
					foreach($typeKeyArray as $key => $value)
					{
						$fieldName = $typeKey . $key;
						$fieldNames[] = $fieldName;
					}
				}
				else if(($typeKey == 'resourceyear') && !empty($typeKeyArray))
				{
					$this->db->leftJoin('resource_year', 'resourceyearId', 'resourcemiscId');
					foreach($typeKeyArray as $key => $value)
					{
						$fieldName = $typeKey . $key;
						$fieldNames[] = $fieldName;
					}
				}
			}
			if(empty($fieldNames))
				continue;
			$fieldNames[] = 'collectionId';
			$fieldNames[] = 'resourcemiscId';
			$this->db->formatConditions(array('resourcemiscCollection' => ' IS NOT NULL'));
			$this->db->formatConditions(array('collectionType' => $type));
			$this->db->leftJoin('collection', 'collectionId', 'resourcemiscCollection');
			$resultset = $this->db->select('resource_misc', $fieldNames, TRUE);
			while($row = $this->db->fetchRow($resultset))
			{
				foreach($fieldNames as $fieldName)
				{
					if (($fieldName == 'collectionId') || ($fieldName == 'resourcemiscId'))
						continue;
					if (
						!array_key_exists($row['collectionId'], $collectionArray)
						||
						(
						    array_key_exists($row['collectionId'], $collectionArray)
							&& (!array_key_exists($fieldName, $collectionArray[$row['collectionId']]))
						)
					)
					{
						if($row[$fieldName])
							$collectionArray[$row['collectionId']][$fieldName] = $row[$fieldName];
					}
				}
				if(array_key_exists('resource_creator', $defaultMap->{$type}) && !empty($defaultMap->{$type}['resource_creator']))
				{
					$creators = array();
					$roles = array_keys($defaultMap->{$type}['resource_creator']);
					$this->db->formatConditions(array('resourcecreatorResourceId' => $row['resourcemiscId']));
					$this->db->formatConditionsOneField($roles, 'resourcecreatorRole');
					$this->db->orderBy('resourcecreatorOrder', TRUE, FALSE);
					$resultsetC = $this->db->select('resource_creator', array('resourcecreatorCreatorId', 'resourcecreatorRole', 'resourcecreatorOrder'));
					while($rowC = $this->db->fetchRow($resultsetC))
					{
						$order = $rowC['resourcecreatorOrder'] - 1;
						$creators['Creator' . $rowC['resourcecreatorRole'] . '_' . $order . '_select'] = $rowC['resourcecreatorCreatorId'];
					}
					if(!empty($creators))
						$collectionArray[$row['collectionId']]['creators'] = $creators;
				}
			}
			if(!empty($collectionArray))
			{
				$count = 0;
				$updateArray = array();
				foreach($collectionArray as $collectionId => $array)
				{
					++$count;
					$updateArray[$collectionId] = base64_encode(serialize($array));
					if($count >= $maxCounts)
					{
						$this->db->multiUpdate('collection', 'collectiondefault', 'collectionId', $updateArray);
						$updateArray = array();
						$count = 0;
					}
				}
				if(!empty($updateArray)) // do the remainder
					$this->db->multiUpdate('collection', 'collectiondefault', 'collectionId', $updateArray);
			}
		}
	}
/**
* Fix bad UTF8
*/
	private function fixUTF8()
	{
		include_once("core/utf8/encoding.php");

// The following only have timestamps, numeric values or preset values such as 'N', 'Y' etc.
		$tableFilter = array(
		    'cache',
		    'database_summary',
		    'resource_category',
		    'resource_keyword',
		    'resource_language',
		    'resource_misc',
		    'resource_summary',
		    'resource_timestamp',
		    'statistics',
		    'user_bibliography_resource',
		    'user_groups_users',
		);
		$textFieldFilter = array('char', 'longtext', 'mediumtext', 'text', 'tinytext', 'varchar');

		$tables = $this->db->listTables(FALSE);
		$tables = array_intersect($tables, $tableFilter);
		foreach($tables as $table)
		{
			$selectedFields = array();

			$convType = 'lightFixutf8';
// Need to select text fields for conversion
			$fInfo = $this->db->getFieldsProperties($table);
		   	foreach ($fInfo as $val)
		   	{
	   			if (in_array($val['DATA_TYPE'], $textFieldFilter))
	   			    $selectedFields[] = $val['COLUMN_NAME'];
			}

			// Never convert the password of users
			if ($table == 'users') $selectedFields = array_diff($selectedFields, array('usersPassword'));

			if (count($selectedFields) > 0)
			{
    			$resultset = $this->db->select($table, $selectedFields);
    			while($row = $this->db->fetchRow($resultset))
    			{
    				$id = str_replace('_', '', $table) . 'Id';
    				$updateArray = array();
    				foreach($row as $field => $value)
    				{
    					if(!$value || is_numeric($value))
    						continue;
    					$value = stripslashes($value);
    					$original = $value;
    					if($convType == 'lightFixutf8')
    					{
    						$value = Encoding::toUTF8($value);
    						if($original != $value)
    							$updateArray[$field] = $value;
    					}
    					else if($convType == 'toughFixutf8')
    					{
    						$value = Encoding::fixUTF8($value);
    						if($original != $value)
    							$updateArray[$field] = $value;
    					}
    				}
    				if(empty($updateArray))
    					continue;
    				if(($table != 'config'))
    					$this->db->formatConditions(array($id => $row[$id]));
    				$this->db->update($table, $updateArray);
    			}
			}
		}
		$this->db->updateNull('cache', 'cacheResourceCreators');
		$this->db->updateNull('cache', 'cacheMetadataCreators');
		$this->db->updateNull('cache', 'cacheKeywords');
		$this->db->updateNull('cache', 'cacheResourceKeywords');
		$this->db->updateNull('cache', 'cacheMetadataKeywords');
		$this->db->updateNull('cache', 'cacheQuoteKeywords');
		$this->db->updateNull('cache', 'cacheParaphraseKeywords');
		$this->db->updateNull('cache', 'cacheMusingKeywords');
		$this->db->updateNull('cache', 'cacheResourcePublishers');
		$this->db->updateNull('cache', 'cacheMetadataPublishers');
		$this->db->updateNull('cache', 'cacheConferenceOrganisers');
		$this->db->updateNull('cache', 'cacheResourceCollections');
		$this->db->updateNull('cache', 'cacheMetadataCollections');
		$this->db->updateNull('cache', 'cacheResourceCollectionTitles');
		$this->db->updateNull('cache', 'cacheResourceCollectionShorts');
	}
/**
* To get total counts right in list operations, certain tables, having their ID columns the same as resource.resourceId,
* must have the same number of rows as the resource table.
*/
	private function addMissingRows()
	{
// resource_year
		$resultset = $this->db->query('SELECT `resourceId` FROM `' . $this->config->WIKINDX_DB_TABLEPREFIX . 'resource`
			WHERE `resourceId` NOT IN (SELECT `resourceyearId`
			FROM `' . $this->config->WIKINDX_DB_TABLEPREFIX . 'resource_year`)');
		while($row = $this->db->fetchRow($resultset))
			$this->db->insert('resource_year', 'resourceyearId', $row['resourceId']);
// resource_misc
		$resultset = $this->db->query('SELECT `resourceId` FROM `' . $this->config->WIKINDX_DB_TABLEPREFIX . 'resource`
			WHERE `resourceId` NOT IN (SELECT `resourcemiscId`
			FROM `' . $this->config->WIKINDX_DB_TABLEPREFIX . 'resource_misc`)');
		while($row = $this->db->fetchRow($resultset))
			$this->db->insert('resource_misc', 'resourcemiscId', $row['resourceId']);
// resource_timestamp
		$resultset = $this->db->query('SELECT `resourceId` FROM `' . $this->config->WIKINDX_DB_TABLEPREFIX . 'resource`
			WHERE `resourceId` NOT IN (SELECT `resourcetimestampId`
			FROM `' . $this->config->WIKINDX_DB_TABLEPREFIX . 'resource_timestamp`)');
		while($row = $this->db->fetchRow($resultset))
			$this->db->insert('resource_timestamp', 'resourcetimestampId', $row['resourceId']);
	}
/**
* If required to pause execution, store current position and any $tableArray arrays in session and present continution form to user
*
* @param string $function
* @param string $finished
* @param string $table Default is FALSE
*/
	private function pauseExecution($function, $finished, $table = FALSE)
	{
// Store data

		$this->session->setVar('upgrade_function', $function);
		if($table)
			$this->session->setVar('upgrade_table', $table);
		$tables = $this->db->listTables(TRUE);
		foreach($tables as $table)
		{
			$table .= 'Array';
			if(isset($this->{$table}))
				$tableArrays[$table] = $this->{$table};
		}
		if(isset($tableArrays))
			$this->session->setVar('upgrade_tableArrays', base64_encode(serialize($tableArrays)));
// Print form and die
//		$pString = "php.ini's max_execution time (" . ini_get("max_execution_time") . " seconds) was about
//			to be exceeded.  Please click on the button to continue the upgrade.&nbsp;&nbsp;Do <strong>not</strong> click
//			until each script has finished.";
		$pString = \HTML\p(\HTML\strong($this->messages->text("heading", "upgradeDB")));
		$pString .= \HTML\p($this->statusString);
		if($this->stageInterruptMessage)
			$pString .= \HTML\p($this->stageInterruptMessage);
		else
			$pString .= \HTML\p("`$finished` of " . $this->numStages .
			" finished.  Please click on the button to continue the upgrade.&nbsp;&nbsp;Do <b>not</b> click
			until each script has finished.");
		$pString .= \FORM\formHeader('continueExecution');
		if($function == 'stage4_1__15')
		{
			$pString .= \HTML\p($this->messages->text("config", "upgradeFixUTF8-1"));
			$pString .= \HTML\p($this->messages->text("config", "upgradeFixUTF8-2") . '&nbsp;&nbsp;' . \FORM\checkBox(FALSE, 'fixUTF8'));
		}
		$pString .= \HTML\p(\FORM\formSubmit("Continue") . \FORM\formEnd());
		$this->close($pString);
	}
/**
* Continue execution of upgrade after a pause
*/
	private function continueExecution()
	{
		$function = $this->session->getVar('upgrade_function');
		$tableArrays = $this->session->getVar('upgrade_tableArrays');
		if($tableArrays)
		{
			$tableArrays = unserialize(base64_decode($tableArrays));
			foreach($tableArrays as $table => $array)
				$this->{$table} = $array;
		}
		$table = $this->session->getVar('upgrade_table');
		$this->session->clearArray('upgrade');
		if($function == 'upgrade40charToBin')
		{
			if($table)
				$this->upgrade40charToBin($table);
			else
				$this->upgrade40charToBin();
			$this->upgrade40Nulls();
			$this->upgrade40Tables();
		}
		else if($function == 'upgrade40Tables')
		{
			if($table)
				$this->upgrade40Tables($table);
			else
				$this->upgrade40Tables();
		}
	}
/**
* Update VARCHARS to account for increased size of UTF8 fields (3* space required) and then temp and change char types to binary types on some tables
* @param string $table Default is FALSE
*/
	private function upgrade40charToBin($table = FALSE)
	{
        $tables = $this->db->listTables(FALSE);
		foreach($tables as $basicTable)
		{
			$change = $varchar = array();
			$table = "`" . $this->config->WIKINDX_DB_TABLEPREFIX . $basicTable . "`";
			$fields = array();
			$recordset = $this->db->query("SHOW COLUMNS FROM $table");
			while($row = $this->db->fetchRow($recordset))
			{
				if(($row['Type'] == 'varchar(10)') || $row['Type'] == 'varchar(30)')
				{
					$fields[] = $row['Field'];
					$varchar[] = '`' . $row['Field'] . '` `'  . $row['Field'] . '` varchar(255)';
				}
				else if($row['Type'] == 'varchar(255)')
					$fields[] = $row['Field'];
				else if($row['Type'] == 'mediumtext')
				{
					$fields[] = $row['Field'];
					$varchar[] = '`' . $row['Field'] . '` `'  . $row['Field'] . '` text';
				}
				else if($row['Type'] == 'text')
					$fields[] = $row['Field'];
			}
			if(!empty($fields))
				$this->upgrade40CheckForValidUtf8($basicTable, $fields);
// reset database conditions, join etc (upgrade40CheckForValidUtf8) may not have executed SQL query but condition still set
			$this->db->resetSubs();
			if(!empty($varchar))
			{
				foreach($varchar as $new)
					$change[] = ' CHANGE ' . $new;
				$this->db->query('ALTER TABLE ' . $table . join(',', $change));
			}
		}
		$tables = array('resource_attachments', 'category', 'collection', 'config', 'creator',
			'custom', 'keyword', 'news', 'papers', 'publisher', 'resource',
			'resource_custom', 'resource_musing', 'resource_musing_text', 'resource_text', 'resource_page',
			'resource_paraphrase', 'resource_paraphrase_comment', 'resource_paraphrase_text',
			'resource_quote', 'resource_quote_comment', 'resource_quote_text', 'resource_year', 'tag',
			'users', 'user_bibliography', 'user_groups', 'user_register',
			'user_tags', 'resource_user_tags', 'subcategory', 'statistics');
		foreach($tables as $basicTable)
		{
			$change = $varchar = array();
			$table = '`' . $this->config->WIKINDX_DB_TABLEPREFIX . "$basicTable`";
			$recordset = $this->db->query("SHOW COLUMNS FROM $table");
			while($row = $this->db->fetchRow($recordset))
			{
				if(($row['Type'] == 'varchar(255)'))
					$varchar[] = '`' . $row['Field'] . '` `'  . $row['Field'] . '` varbinary(255)';
				else if($row['Type'] == 'text')
					$varchar[] = '`' . $row['Field'] . '` `'  . $row['Field'] . '` blob';
			}
			if(!empty($varchar))
			{
				foreach($varchar as $new)
					$change[] = ' CHANGE ' . $new;
			}
			$this->db->query('ALTER TABLE ' . $table . join(',', $change));
		}
	}
/**
* Check for valid UTF-8.
*
* If invalid code found, store value in array for updating to database after UTF8 upgrade
* @param string $basicTable
* @param array $fields
*/
	private function upgrade40CheckForValidUtf8($basicTable, $fields)
	{
		if(($basicTable == 'cache') || ($basicTable == 'database_summary') ||
			($basicTable == 'resource_keyword') || ($basicTable == 'resource_category') ||
			($basicTable == 'resource_creator'))
			return;
		if($basicTable != 'config') // 'config' table has no id field
		{
			$id = str_replace('_', '', $basicTable) . 'Id';
			$fields[] = $id;
		}
		else
			$id = 'null';
		$resultset = $this->db->select($basicTable, $fields);
		$tableArray = $basicTable . 'Array';
		while($row = $this->db->fetchRow($resultset))
		{
			foreach($fields as $field)
			{
				if(($basicTable != 'config') && ($field == $id))
					continue;
				if(function_exists('mb_detect_encoding'))
				{
					$encoding = mb_detect_encoding($row[$field]);
					if(($encoding != 'ASCII') && ($encoding != 'UTF-8'))
					{
						if(!$encoding) // no idea what this is, so store for re-writing after upgrade later
							$value = $row[$field];
						else
						{
							if(!$value = iconv($encoding, 'utf-8', $row[$field]))
								$value = $row[$field];
						}
						if($basicTable == 'config')
							$this->{$tableArray}[][$field] = $value;
						else
							$this->{$tableArray}[$row[$id]][$field] = $value;
					}
				}
			}
		}
	}
/**
* Remove user's sessions and caches
*/
	private function upgrade40Nulls()
	{
// Remove any existing user session
		$this->db->updateNull('users', 'userSession');
// Remove cache fields so that v4's UTF8 handling and ordering of creators, keywords etc. is handled properly
		$this->db->updateNull('cache', 'resourceCreators');
		$this->db->updateNull('cache', 'metadataCreators');
		$this->db->updateNull('cache', 'resourceKeywords');
		$this->db->updateNull('cache', 'metadataKeywords');
		$this->db->updateNull('cache', 'quoteKeywords');
		$this->db->updateNull('cache', 'paraphraseKeywords');
		$this->db->updateNull('cache', 'musingKeywords');
		$this->db->updateNull('cache', 'resourcePublishers');
		$this->db->updateNull('cache', 'metadataPublishers');
		$this->db->updateNull('cache', 'conferenceOrganisers');
		$this->db->updateNull('cache', 'resourceCollections');
		$this->db->updateNull('cache', 'metadataCollections');
		$this->db->updateNull('cache', 'resourceCollectionTitles');
		$this->db->updateNull('cache', 'resourceCollectionShorts');
	}
/**
* recreate the 4.0 cache
*/
	private function recreate40Cache()
	{
		$keyword = FACTORY_KEYWORD::getInstance();
		$creator = FACTORY_CREATOR::getInstance();
		$publisher = FACTORY_PUBLISHER::getInstance();
		$collection = FACTORY_COLLECTION::getInstance();
		$creator->grabAll();
		$creator->grabAll(FALSE, FALSE, TRUE);  // creators in resources with metadata
		$keyword->grabAll(); // default resource_keyword table
		$keyword->grabAll(FALSE, 'resource');
		$keyword->grabAll(FALSE, 'quote');
		$keyword->grabAll(FALSE, 'paraphrase');
		$keyword->grabAll(FALSE, 'musing');
		$publisher->grabAll();
		$publisher->grabAll(FALSE, FALSE, FALSE, TRUE); // publishers in resources with metadata
		$collection->grabAll();
		$collection->grabAll(FALSE, FALSE, FALSE, TRUE); // collections in resources with metadata
	}
/**
* Upgrade tables to UTF-8 then reverse field type change for some tables from binary back to var
*
* @param string $tableContinue Default is FALSE
*/
	private function upgrade40Tables($tableContinue = FALSE)
	{
	   $tables = $this->db->listTables(TRUE);
		foreach($tables as $table)
		{
			$this->db->query('ALTER TABLE `' . $table . '` CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci');
		}
		$tables = array(
		    'resource_attachments', 'category', 'collection', 'config', 'creator',
			'custom', 'keyword', 'news', 'papers', 'publisher', 'resource',
			'resource_custom', 'resource_musing', 'resource_musing_text', 'resource_text', 'resource_page',
			'resource_paraphrase', 'resource_paraphrase_comment', 'resource_paraphrase_text',
			'resource_quote', 'resource_quote_comment', 'resource_quote_text', 'resource_year', 'tag', 'statistics',
			'users', 'user_bibliography', 'user_groups', 'user_register', 'user_tags', 'resource_user_tags', 'subcategory'
		);
		foreach($tables as $basicTable)
		{
			$change = $varchar = array();
			$table = '`' . $this->config->WIKINDX_DB_TABLEPREFIX . "$basicTable`";
			$recordset = $this->db->query("SHOW COLUMNS FROM $table");
			while($row = $this->db->fetchRow($recordset))
			{
				if(($row['Type'] == 'varbinary(255)'))
					$varchar[] = '`' . $row['Field'] . '` `'  . $row['Field'] . '` varchar(255)';
				else if($row['Type'] == 'blob')
					$varchar[] = '`' . $row['Field'] . '` `'  . $row['Field'] . '` text';
			}
			if(!empty($varchar))
			{
				foreach($varchar as $new)
					$change[] = ' CHANGE ' . $new;
			}
			$this->db->query('ALTER TABLE ' . $table . join(',', $change));
			$tableArray = $basicTable . 'Array';
			if(isset($this->{$tableArray}))
			{
				foreach($this->{$tableArray} as $id => $array)
				{
					$updateArray = array();
					if($basicTable != 'config')
						$this->db->formatConditions(array(str_replace('_', '', $basicTable) . 'Id' => $id));
					foreach($array as $key => $basicTable)
					{
						$updateArray[$key] = $basicTable;
					}
					if(!empty($updateArray))
						$this->db->update($basicTable, $updateArray);
				}
			}
		}
	}
/**
* Only the superadmin may update the database -- ask for login
*
* @param string $currentdbVersion
*/
	private function confirmUpdateDisplay($currentdbVersion)
	{
		$this->session->setVar("setup_Template", WIKINDX_TEMPLATE_DEFAULT);
		$pString = \HTML\p(\HTML\strong($this->messages->text("heading", "upgradeDB")));

		$vars = GLOBALS::getVars();
		$vars['username'] = isset($vars['username']) ? $vars['username'] : '';
		$vars['password'] = isset($vars['password']) ? $vars['password'] : '';
		FACTORY_AUTHORIZE::getInstance()->logonCheckUpgradeDB($vars['username'], $vars['password'], $currentdbVersion);

		if(!$this->session->getVar('setup_Superadmin'))
		{
			if($this->email)
				$pString .= \HTML\p($this->messages->text("config", "upgradeDB2", "&nbsp;({$this->email})"));
			else
				$pString .= \HTML\p($this->messages->text("config", "upgradeDB2"));
			$pString .= \HTML\p($this->messages->text("authorize", "logonSuperadmin"));
			$pString .= \FORM\formHeader("upgradeDBLogon");
			$pString .= \HTML\tableStart('left width50percent');
			$pString .= \HTML\trStart();
			$pString .= \HTML\td($this->messages->text("user", "username") . ":&nbsp;&nbsp;");
			$pString .= \HTML\td(\FORM\textInput(FALSE, "username"));
			$pString .= \HTML\trEnd();
			$pString .= \HTML\trStart();
			$pString .= \HTML\td($this->messages->text("user", "password") . ":&nbsp;&nbsp;");
			$pString .= \HTML\td(\FORM\passwordInput(FALSE, "password"));
			$pString .= \HTML\trEnd();
			$pString .= \HTML\trStart();
			$pString .= \HTML\td("&nbsp;");
			$pString .= \HTML\td(\FORM\formSubmit(), 'right');
			$pString .= \HTML\trEnd();
			$pString .= \HTML\tableEnd();
			$pString .= \FORM\formEnd();
			$this->close($pString);
		}
		else
		{
			$pString .= \HTML\p('CURRENT MAX EXECUTION TIME: ' . ini_get("max_execution_time") . ' secs'
				. BR . 'CURRENT PHP MEMORY LIMIT: ' . ini_get("memory_limit"));
			$pString .= \HTML\p($this->messages->text("config", "upgradeDB1"));
			$pString .= \HTML\p($this->messages->text("config", "upgradeDB3"));
			$pString .= \FORM\formHeader("upgradeDB");
			$pString .= \HTML\p(\FORM\formSubmit("upgradeDB"), FALSE, 'right');
			$pString .= \FORM\formEnd();
			$this->close($pString);
		}
	}
/**
* Intercept for initial configuration by admin and, if necessary, display admin configuration interface (new installation means users table is empty).
*/
	private function checkUsersTable()
	{
		if($this->db->tableIsEmpty('users'))
		{
			include_once("core/modules/admin/CONFIGURE.php");
			$config = new CONFIGURE(TRUE);
			$config->insert = TRUE; // force super initialization in CONFIGURE
			if(isset($this->vars['action']) && $this->vars['action'] == 'admin_CONFIGURE_CORE')
				GLOBALS::addTplVar('content', $config->writeDb());
			else
			{
// write preliminary stringLimit, write and superadmin to session and display super configuration screen
				$this->session->setVar("setup_StringLimit", 40);
				$this->session->setVar("setup_Write", TRUE);
				$this->session->setVar("setup_Superadmin", TRUE);
// superadmin userId is always 1
				$this->session->setVar("setup_UserId", 1);
				GLOBALS::addTplVar('content', $config->init(array(\HTML\p($this->messages->text("config", "start"), "error", "center"), 'super')));
				FACTORY_CLOSENOMENU::getInstance();
			}
		}
	}
/**
* Special CLOSE function for pre v4.0 databases
*
* @param string $pString
*/
	private function close($pString)
	{
		$string = <<< END
<!DOCTYPE html>
<html>
<head>
	<title>WIKINDX Upgrade</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="/templates/default/template.css" type="text/css">
	<link rel="shortcut icon" type="image/x-icon" href="templates/default/images/favicon.ico">
</head>
<body>
	$pString
</body>
</html>
END;
		print $string;
		ob_end_flush();
		die;
	}
}
?>