<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* ENVIRONMENT
*
* Set up the WIKINDX server environment
*
* @version	1
*
*	@package wikindx5\core\startup
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class ENVIRONMENT
{
/** object */
public $config;

/**
* ENVIRONMENT
*/
	public function __construct()
	{
		$this->config = FACTORY_CONFIG::getInstance();
		$this->runConfig();
		$this->checkFolders();
		$this->getVars();
	}

/**
* start the SESSION
*/
	public function startSession()
	{
	    // Protect from a session already launched by an other page but not well loaded (plugins)
	    if (session_status() === PHP_SESSION_ACTIVE) session_write_close();
		if (session_status() === PHP_SESSION_NONE)
		{
// Set session path if required
			if($this->config->WIKINDX_SESSION_PATH != '')
			{
				session_save_path($this->config->WIKINDX_SESSION_PATH);
// If required, do some house-keeping. $WIKINDX_SESSION_PATH_CLEAR of 0 means never delete
				if($this->config->WIKINDX_SESSION_PATH_CLEAR && ($this->config->WIKINDX_SESSION_PATH_CLEAR > 0))
				{
// failure is an option...
                    if(file_exists($this->config->WIKINDX_SESSION_PATH))
					{
					    $dh = opendir($this->config->WIKINDX_SESSION_PATH);
						$now = time();
						$maxTime = $this->config->WIKINDX_SESSION_PATH_CLEAR * 24 * 60 * 60; // days to seconds
						while(false !== ($f = readdir($dh)))
						{
							if(($f != ".") && ($f != ".."))
							{
								$file = $this->config->WIKINDX_SESSION_PATH . DIRECTORY_SEPARATOR . $f;
								if(($now - filemtime($file)) >= $maxTime)
									@unlink($file);
							}
						}
						closedir($dh);
					}
				}
			}
// start session
			session_start();
		}
	}
	
/*
 * Filter function for method GET parameter
 * 
 * Method should have the format of an array key of a MESSAGES.php catalog
 */
    private function filterMethod($value)
    {
        return preg_replace("/[^A-Za-z0-9]/u", "", $value);
    }
/**
* Make sure we get HTTP VARS in whatever format they come in
*
* Use $vars = GLOBALS::getVars(); to get querystrings and form elements
*/
	private function getVars()
	{
		if(!empty($_POST))
			$vars = $_POST;
		else if(!empty($_GET))
			$vars = $_GET;
		else
			return FALSE;

		$dirtyVars = $vars;

		$cleanVars = array_map(array($this, "stripHtmlTags"), $dirtyVars);
		
		if (array_key_exists('action', $cleanVars) && ($cleanVars['action'] == 'noMenu' || $cleanVars['action'] == 'noSubMenu'))
		{
		    if (array_key_exists('method', $cleanVars))
		    {
    		    $method = trim($cleanVars['method']);
                $method = filter_var($method, FILTER_CALLBACK, array('options' => [$this, "filterMethod"]));
                $cleanVars['method'] = $method;
		    }
		}
		
// Store globally
		GLOBALS::setVars($cleanVars, $dirtyVars);
	}
/**
* Check and load configuration from config.php
* From WIKINDX v5.3 on, we have transferred many variables from config.php to the database.
* After checking for basic MYSQL and PATH variables that remain in config.php, we decide whether to check
* further variables in this function (i.e. we are in the process of upgrading to >= v5.3) or to return.
* If we return, the checks below are carried out in LOADCONFIG::checkConfigValidity().
*/
	private function runConfig()
	{
		$dieMsgMissing = 'Missing configuration variable in config.php: ';
// Set base url (default if needed)
		if (!property_exists($this->config, 'WIKINDX_BASE_URL') || !$this->config->WIKINDX_BASE_URL)
			$this->config->WIKINDX_BASE_URL = FACTORY_URL::getInstance()->getBaseUrl();
		elseif(!is_string($this->config->WIKINDX_BASE_URL))
			die('WIKINDX_BASE_URL must be a valid URL');
		elseif(trim($this->config->WIKINDX_BASE_URL) == '')
			die('WIKINDX_BASE_URL must be a valid URL');
// Replace dynamically the protocole defined by config.php by the protocole requested (http or https)
		$this->config->WIKINDX_BASE_URL = preg_replace('/^https?/u', FACTORY_URL::getInstance()->getCurrentProtocole(), $this->config->WIKINDX_BASE_URL);

// Attachments directory (optional)
		if (!property_exists($this->config, 'WIKINDX_ATTACHMENTS_DIR'))
			$this->config->WIKINDX_ATTACHMENTS_DIR = WIKINDX_ATTACHMENTS_DIR_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_ATTACHMENTS_DIR))
			die('WIKINDX_ATTACHMENTS_DIR must be a string.');

// Attachments_cache directory (optional)
		if (!property_exists($this->config, 'WIKINDX_ATTACHMENTSCACHE_DIR'))
			$this->config->WIKINDX_ATTACHMENTSCACHE_DIR = WIKINDX_ATTACHMENTSCACHE_DIR_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_ATTACHMENTSCACHE_DIR))
			die('WIKINDX_ATTACHMENTSCACHE_DIR must be a string.');

// Files directory (optional)
		if (!property_exists($this->config, 'WIKINDX_FILES_DIR'))
			$this->config->WIKINDX_FILES_DIR = WIKINDX_FILES_DIR_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_FILES_DIR))
			die('WIKINDX_FILES_DIR must be a string.');

// Set expiration sessions after X days
		if (!property_exists($this->config, 'WIKINDX_SESSION_PATH_CLEAR'))
			$this->config->WIKINDX_SESSION_PATH_CLEAR = 10;
		elseif(empty($this->config->WIKINDX_SESSION_PATH_CLEAR))
			$this->config->WIKINDX_SESSION_PATH_CLEAR = 10;
		elseif(!is_int($this->config->WIKINDX_SESSION_PATH_CLEAR))
			die('WIKINDX_SESSION_PATH_CLEAR must be a positive number of days (or zero for unlimited).');
		elseif($this->config->WIKINDX_SESSION_PATH_CLEAR < 10)
			die('WIKINDX_SESSION_PATH_CLEAR must be a positive number of days (or zero for unlimited).');

// Set default path to persist sessions
// By default, an empty path which means do not use
		if (!property_exists($this->config, 'WIKINDX_SESSION_PATH'))
			$this->config->WIKINDX_SESSION_PATH = WIKINDX_SESSION_PATH_DEFAULT;
		elseif(is_bool($this->config->WIKINDX_SESSION_PATH))
			if($this->config->WIKINDX_SESSION_PATH != WIKINDX_SESSION_PATH_DEFAULT)
				$this->config->WIKINDX_SESSION_PATH = WIKINDX_SESSION_PATH_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_SESSION_PATH))
			$this->config->WIKINDX_SESSION_PATH = WIKINDX_SESSION_PATH_DEFAULT;
		elseif(!is_dir($this->config->WIKINDX_SESSION_PATH))
			$this->config->WIKINDX_SESSION_PATH = WIKINDX_SESSION_PATH_DEFAULT;

// Set database hostname
		if (!property_exists($this->config, 'WIKINDX_DB_HOST'))
			die($dieMsgMissing . 'WIKINDX_DB_HOST');
		elseif(!is_string($this->config->WIKINDX_DB_HOST))
			die('WIKINDX_DB_HOST must be a string.');

// Set database name
		if (!property_exists($this->config, 'WIKINDX_DB'))
			die($dieMsgMissing . 'WIKINDX_DB');
		elseif(!is_string($this->config->WIKINDX_DB))
			die('WIKINDX_DB must be a string.');

// Set database user
		if (!property_exists($this->config, 'WIKINDX_DB_USER'))
			die($dieMsgMissing . 'WIKINDX_DB_USER');
		elseif(!is_string($this->config->WIKINDX_DB_USER))
			die('WIKINDX_DB_USER must be a string.');

// Set database user password
		if (!property_exists($this->config, 'WIKINDX_DB_PASSWORD'))
			die($dieMsgMissing . 'WIKINDX_DB_PASSWORD');
		elseif(!is_string($this->config->WIKINDX_DB_PASSWORD))
			die('WIKINDX_DB_PASSWORD must be a string.');

// Set database table prefix
		if (!property_exists($this->config, 'WIKINDX_DB_TABLEPREFIX'))
			die($dieMsgMissing . 'WIKINDX_DB_TABLEPREFIX');
		elseif(!is_string($this->config->WIKINDX_DB_TABLEPREFIX))
			die('WIKINDX_DB_TABLEPREFIX must be a string.');
// Use always a lowercase prefix to prevent problem with case sensitive database
        $this->config->WIKINDX_DB_TABLEPREFIX = mb_strtolower($this->config->WIKINDX_DB_TABLEPREFIX);

// Set database persistent mode
		if (!property_exists($this->config, 'WIKINDX_DB_PERSISTENT'))
			die($dieMsgMissing . 'WIKINDX_DB_PERSISTENT');
		elseif(!is_bool($this->config->WIKINDX_DB_PERSISTENT))
			die('WIKINDX_DB_PERSISTENT must be a boolean (TRUE / FALSE).');

// Attempt to set the memory the script uses -- does not work in safe mode
		if (!property_exists($this->config, 'WIKINDX_MEMORY_LIMIT'))
			$this->config->WIKINDX_MEMORY_LIMIT = WIKINDX_MEMORY_LIMIT_DEFAULT;
		elseif(is_string($this->config->WIKINDX_MEMORY_LIMIT))
			if(preg_match('/^\d+[KMG]?$/u', $this->config->WIKINDX_MEMORY_LIMIT) === FALSE)
				die('Syntax Error in WIKINDX_MEMORY_LIMIT. See https://secure.php.net/manual/fr/faq.using.php#faq.using.shorthandbytes');
		elseif(is_int($this->config->WIKINDX_MEMORY_LIMIT))
			if($this->config->WIKINDX_MEMORY_LIMIT < -1)
				die('WIKINDX_MEMORY_LIMIT must be a positive integer.');
		ini_set("memory_limit", $this->config->WIKINDX_MEMORY_LIMIT);

// Attempt to set the max time the script runs for -- does not work in safe mode
		if (!property_exists($this->config, 'WIKINDX_MAX_EXECUTION_TIMEOUT'))
			$this->config->WIKINDX_MAX_EXECUTION_TIMEOUT = WIKINDX_MAX_EXECUTION_TIMEOUT_DEFAULT;
		elseif(is_string($this->config->WIKINDX_MAX_EXECUTION_TIMEOUT)) // v4 config.php required quotes around value
		{
			if(!$this->config->WIKINDX_MAX_EXECUTION_TIMEOUT = intVal($this->config->WIKINDX_MAX_EXECUTION_TIMEOUT))
				die('WIKINDX_MAX_EXECUTION_TIMEOUT must be a positive integer (or FALSE for default configuration of PHP).');
		}
		elseif(!is_int($this->config->WIKINDX_MAX_EXECUTION_TIMEOUT))
			if($this->config->WIKINDX_MAX_EXECUTION_TIMEOUT !== FALSE)
				die('WIKINDX_MAX_EXECUTION_TIMEOUT must be a positive integer (or FALSE for default configuration of PHP).');
		elseif($this->config->WIKINDX_MAX_EXECUTION_TIMEOUT < 0)
			die('WIKINDX_MAX_EXECUTION_TIMEOUT must be a positive integer (or FALSE for default configuration of PHP).');
// Configure it only if explicitely defined
		if($this->config->WIKINDX_MAX_EXECUTION_TIMEOUT !== FALSE)
			ini_set("max_execution_time", $this->config->WIKINDX_MAX_EXECUTION_TIMEOUT);

// Set max write chunk for file writing
		if (!property_exists($this->config, 'WIKINDX_MAX_WRITECHUNK'))
			$this->config->WIKINDX_MAX_WRITECHUNK = WIKINDX_MAX_WRITECHUNK_DEFAULT;
		elseif(!is_int($this->config->WIKINDX_MAX_WRITECHUNK))
			if($this->config->WIKINDX_MAX_WRITECHUNK !== FALSE)
				die('WIKINDX_MAX_WRITECHUNK must be a positive integer (or FALSE for default configuration).');
			else
			    $this->config->WIKINDX_MAX_WRITECHUNK = WIKINDX_MAX_WRITECHUNK_DEFAULT;
		elseif($this->config->WIKINDX_MAX_WRITECHUNK < 1)
			die('WIKINDX_MAX_WRITECHUNK must be a positive integer (or FALSE for default configuration).');

////////////////////////
// The above are the basic variables in config.php with WIKINDX >= v5.3
// We now check if we are indeed >= v5.3
////////////////////////
		if(WIKINDX_VERSION >= 5.3)
			return;
////////////////////////
// Everything else here is only used when upgrading to >= v5.3. After upgrading, the checks below are carried out in LOADCONFIG::checkConfigValidity().
////////////////////////

// Set timezone (default if needed)
		if (!property_exists($this->config, 'WIKINDX_TIMEZONE'))
			$this->config->WIKINDX_TIMEZONE = WIKINDX_TIMEZONE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_TIMEZONE))
			die('WIKINDX_TIMEZONE must be a valid PHP time zone. See https://secure.php.net/manual/fr/timezones.php');
		elseif(trim($this->config->WIKINDX_TIMEZONE) == '')
			die('WIKINDX_TIMEZONE must be a valid PHP time zone. See https://secure.php.net/manual/fr/timezones.php');

		date_default_timezone_set($this->config->WIKINDX_TIMEZONE);

// Special userId (FALSE or userID from database). Used on the test wikindx to stop this write-enabled user changing login details
		if (!property_exists($this->config, 'WIKINDX_RESTRICT_USERID'))
			die($dieMsgMissing . 'WIKINDX_RESTRICT_USERID');

// Set resource type
		if (!property_exists($this->config, 'WIKINDX_DEACTIVATE_RESOURCE_TYPES'))
			die($dieMsgMissing . 'WIKINDX_DEACTIVATE_RESOURCE_TYPES');
		elseif(!is_array($this->config->WIKINDX_DEACTIVATE_RESOURCE_TYPES))
			die('WIKINDX_DEACTIVATE_RESOURCE_TYPES must be an array.');

// Enable / disable RSS feed
		if (!property_exists($this->config, 'WIKINDX_RSS_ALLOW'))
			die($dieMsgMissing . 'WIKINDX_RSS_ALLOW');
		elseif(!is_bool($this->config->WIKINDX_RSS_ALLOW))
			die('WIKINDX_RSS_ALLOW must be a boolean (TRUE / FALSE).');

// Set number of items in RSS feed
		if (!property_exists($this->config, 'WIKINDX_RSS_LIMIT'))
			$this->config->WIKINDX_RSS_LIMIT = WIKINDX_RSS_LIMIT_DEFAULT;
		elseif(!is_int($this->config->WIKINDX_RSS_LIMIT))
			die('WIKINDX_RSS_LIMIT must be > 0.');
		elseif($this->config->WIKINDX_RSS_ALLOW && $this->config->WIKINDX_RSS_LIMIT < 1)
			die('WIKINDX_RSS_LIMIT must be > 0.');

// Set bibliographic style of RSS feed
		if (!property_exists($this->config, 'WIKINDX_RSS_BIBSTYLE'))
			$this->config->WIKINDX_RSS_BIBSTYLE = WIKINDX_RSS_BIBSTYLE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_RSS_BIBSTYLE))
			die('WIKINDX_RSS_BIBSTYLE must be an existing bibliographic style name.');

// Set recents display in RSS feed
		if (!property_exists($this->config, 'WIKINDX_RSS_DISPLAY'))
			$this->config->WIKINDX_RSS_DISPLAY = WIKINDX_RSS_DISPLAY_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_RSS_DISPLAY))
			die('WIKINDX_RSS_DISPLAY must be a boolean (TRUE / FALSE).');

// Set RSS feed title
		if (!property_exists($this->config, 'WIKINDX_RSS_TITLE'))
			$this->config->WIKINDX_RSS_TITLE = WIKINDX_RSS_TITLE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_RSS_TITLE))
			die('WIKINDX_RSS_TITLE must be a string.');

// Set RSS feed description
		if (!property_exists($this->config, 'WIKINDX_RSS_DESCRIPTION'))
			$this->config->WIKINDX_RSS_DESCRIPTION = WIKINDX_RSS_DESCRIPTION_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_RSS_DESCRIPTION))
			die('WIKINDX_RSS_DESCRIPTION must be a string.');

// Set RSS feed language
		if (!property_exists($this->config, 'WIKINDX_RSS_LANGUAGE'))
			$this->config->WIKINDX_RSS_LANGUAGE = WIKINDX_RSS_LANGUAGE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_RSS_LANGUAGE))
			die('WIKINDX_RSS_LANGUAGE must be a legal language code (en, fr, de...).');

// Set mailer configuration
		if (!property_exists($this->config, 'WIKINDX_MAIL_SERVER'))
			die($dieMsgMissing . 'WIKINDX_MAIL_SERVER');
		elseif(!is_bool($this->config->WIKINDX_MAIL_SERVER))
			die('WIKINDX_MAIL_SERVER must be a boolean (TRUE / FALSE).');

// Set email from header
		if (!property_exists($this->config, 'WIKINDX_MAIL_FROM'))
			$this->config->WIKINDX_MAIL_FROM = WIKINDX_MAIL_FROM_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_FROM))
			if($this->config->WIKINDX_MAIL_FROM !== FALSE)
				die('WIKINDX_MAIL_FROM must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_MAIL_FROM == ''))
			$this->config->WIKINDX_MAIL_FROM = WIKINDX_MAIL_FROM_DEFAULT;

// Set email reply-to header
		if (!property_exists($this->config, 'WIKINDX_MAIL_REPLYTO'))
			$this->config->WIKINDX_MAIL_REPLYTO = WIKINDX_MAIL_REPLYTO_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_REPLYTO))
			if($this->config->WIKINDX_MAIL_REPLYTO !== FALSE)
				die('WIKINDX_MAIL_REPLYTO must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_MAIL_REPLYTO == ''))
			$this->config->WIKINDX_MAIL_REPLYTO = WIKINDX_MAIL_REPLYTO_DEFAULT;

// Set email path return header
		if (!property_exists($this->config, 'WIKINDX_MAIL_RETURN_PATH'))
			$this->config->WIKINDX_MAIL_RETURN_PATH = WIKINDX_MAIL_RETURN_PATH_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_RETURN_PATH))
			if($this->config->WIKINDX_MAIL_RETURN_PATH !== FALSE)
				die('WIKINDX_MAIL_RETURN_PATH must be a string or FALSE.');
		elseif(trim($this->config->WIKINDX_MAIL_RETURN_PATH == ''))
			$this->config->WIKINDX_MAIL_RETURN_PATH = WIKINDX_MAIL_RETURN_PATH_DEFAULT;

// Set MAIL backend
		if (!property_exists($this->config, 'WIKINDX_MAIL_BACKEND'))
			$this->config->WIKINDX_MAIL_BACKEND = WIKINDX_MAIL_BACKEND_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_BACKEND))
			die('WIKINDX_MAIL_BACKEND must be of this value: ' . join(', ', array('smtp', 'sendmail', 'mail')));
		elseif(!in_array($this->config->WIKINDX_MAIL_BACKEND, array('smtp', 'sendmail', 'mail')))
			die('WIKINDX_MAIL_BACKEND must be of this value: ' . join(', ', array('smtp', 'sendmail', 'mail')));
		elseif($this->config->WIKINDX_MAIL_BACKEND == 'mail' && !function_exists('mail'))
			die('Mail backend unavailable [WIKINDX_MAIL_BACKEND] : mail() function is disabled in the configuration of PHP.');

// Set sendmail path
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMPATH'))
			$this->config->WIKINDX_MAIL_SMPATH = WIKINDX_MAIL_SMPATH_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_SMPATH))
			die('WIKINDX_MAIL_SMPATH must be a string.');

// Set sendmail optional parameters
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMARGS'))
			$this->config->WIKINDX_MAIL_SMARGS = WIKINDX_MAIL_SMARGS_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_SMARGS))
			die('WIKINDX_MAIL_SMARGS must be a string.');

// Set smtp hostname
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPSERVER'))
			$this->config->WIKINDX_MAIL_SMTPSERVER = WIKINDX_MAIL_SMTPSERVER_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_SMTPSERVER))
			die('WIKINDX_MAIL_SMTPSERVER must be a string.');

// Set smtp port
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPPORT'))
			$this->config->WIKINDX_MAIL_SMTPPORT = WIKINDX_MAIL_SMTPPORT_DEFAULT;
		elseif(is_int($this->config->WIKINDX_MAIL_SMTPPORT))
			if($this->config->WIKINDX_MAIL_SMTPPORT < 0)
				die('WIKINDX_MAIL_SMTPPORT must be a positive integer.');
		elseif(!is_string($this->config->WIKINDX_MAIL_SMTPPORT))
			die('WIKINDX_MAIL_SMTPPORT must be a positive integer.');
		elseif(!preg_match('/^\d+$/u', $this->config->WIKINDX_MAIL_SMTPPORT))
			die('WIKINDX_MAIL_SMTPPORT must be a positive integer.');

// Set smtp encryption
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPENCRYPT'))
			$this->config->WIKINDX_MAIL_SMTPENCRYPT = WIKINDX_MAIL_SMTPENCRYPT_DEFAULT;
// No encryption stored in the database as 'none' Рensure this is an empty string
		else if($this->config->WIKINDX_MAIL_SMTPENCRYPT == 'none')
			$this->config->WIKINDX_MAIL_SMTPENCRYPT = '';
		elseif(!is_string($this->config->WIKINDX_MAIL_SMTPENCRYPT))
			die('WIKINDX_MAIL_SMTPENCRYPT must be of this value: ' . join(', ', array('tls', 'ssl', 'or an empty string')));
		elseif(!in_array($this->config->WIKINDX_MAIL_SMTPENCRYPT, array('', 'tls', 'ssl')))
			die('WIKINDX_MAIL_SMTPENCRYPT must be of this value: ' . join(', ', array('tls', 'ssl', 'or an empty string')));
		elseif ($this->config->WIKINDX_MAIL_SMTPENCRYPT == '')
		    $this->config->WIKINDX_MAIL_SMTPENCRYPT == FALSE;

// Set smtp persist
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPPERSIST'))
			$this->config->WIKINDX_MAIL_SMTPPERSIST = WIKINDX_MAIL_SMTPPERSIST_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_MAIL_SMTPPERSIST))
			die('WIKINDX_MAIL_SMTPPERSIST must be a boolean (TRUE / FALSE).');

// Set smtp auth
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPAUTH'))
			$this->config->WIKINDX_MAIL_SMTPAUTH = WIKINDX_MAIL_SMTPAUTH_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_MAIL_SMTPAUTH))
			die('WIKINDX_MAIL_SMTPAUTH must be a boolean (TRUE / FALSE).');

// Set smtp user
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPUSERNAME'))
			$this->config->WIKINDX_MAIL_SMTPUSERNAME = WIKINDX_MAIL_SMTPUSERNAME_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_SMTPUSERNAME))
			die('WIKINDX_MAIL_SMTPUSERNAME must be a string.');

// Set smtp password
		if (!property_exists($this->config, 'WIKINDX_MAIL_SMTPPASSWORD'))
			$this->config->WIKINDX_MAIL_SMTPPASSWORD = WIKINDX_MAIL_SMTPPASSWORD_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_MAIL_SMTPPASSWORD))
			die('WIKINDX_MAIL_SMTPPASSWORD must be a string.');

// Enable / disable GS
		if (!property_exists($this->config, 'WIKINDX_GS_ALLOW'))
			die($dieMsgMissing . 'WIKINDX_GS_ALLOW');
		elseif(!is_bool($this->config->WIKINDX_GS_ALLOW))
			die('WIKINDX_GS_ALLOW must be a boolean (TRUE / FALSE).');

// Enable / disable GS attachments
		if (!property_exists($this->config, 'WIKINDX_GS_ATTACHMENT'))
			$this->config->WIKINDX_GS_ATTACHMENT = WIKINDX_GS_ATTACHMENT_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_GS_ATTACHMENT))
			die('WIKINDX_GS_ATTACHMENT must be a boolean (TRUE / FALSE).');

// Enable / disable CMS
		if (!property_exists($this->config, 'WIKINDX_CMS_ALLOW'))
			die($dieMsgMissing . 'WIKINDX_CMS_ALLOW');
		elseif(!is_bool($this->config->WIKINDX_CMS_ALLOW))
			die('WIKINDX_CMS_ALLOW must be a boolean (TRUE / FALSE).');

// Set bibliographic style of CMS
		if (!property_exists($this->config, 'WIKINDX_CMS_BIBSTYLE'))
			if($this->config->WIKINDX_CMS_ALLOW)
				die($dieMsgMissing . 'WIKINDX_CMS_BIBSTYLE');
			else
				$this->config->WIKINDX_CMS_BIBSTYLE = WIKINDX_CMS_BIBSTYLE_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_CMS_BIBSTYLE))
			die('WIKINDX_CMS_BIBSTYLE must an existing bibliographic style name.');

// Set CMS language
		if (!property_exists($this->config, 'WIKINDX_CMS_LANGUAGE'))
			$this->config->WIKINDX_CMS_LANGUAGE = WIKINDX_CMS_LANGUAGE_DEFAULT; // v4 config.php does not have this property so set it!
		elseif(!is_string($this->config->WIKINDX_CMS_LANGUAGE))
			die('WIKINDX_CMS_LANGUAGE must be a legal language code (en, fr, de...).');

// Enable / disable CMS database access
		if (!property_exists($this->config, 'WIKINDX_CMS_SQL'))
			if($this->config->WIKINDX_CMS_ALLOW)
				die($dieMsgMissing . 'WIKINDX_CMS_SQL');
			else
				$this->config->WIKINDX_CMS_SQL = WIKINDX_CMS_SQL_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_CMS_SQL))
			die('WIKINDX_CMS_SQL must be a boolean (TRUE / FALSE).');

// Set CMS database user
		if (!property_exists($this->config, 'WIKINDX_CMS_DB_USER'))
			if($this->config->WIKINDX_CMS_SQL)
				die($dieMsgMissing . 'WIKINDX_CMS_DB_USER');
			else
				$this->config->WIKINDX_CMS_DB_USER = WIKINDX_CMS_DB_USER_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_CMS_DB_USER))
			die('WIKINDX_CMS_DB_USER must be a string.');

// Set CMS database password
		if (!property_exists($this->config, 'WIKINDX_CMS_DB_PASSWORD'))
			if($this->config->WIKINDX_CMS_SQL)
				die($dieMsgMissing . 'WIKINDX_CMS_DB_PASSWORD');
			else
				$this->config->WIKINDX_CMS_DB_PASSWORD = WIKINDX_CMS_DB_PASSWORD_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_CMS_DB_PASSWORD))
			die('WIKINDX_CMS_DB_PASSWORD must be a string.');

// Set Tag cloud low frequency color
		if (!property_exists($this->config, 'WIKINDX_TAG_LOW_COLOUR'))
			$this->config->WIKINDX_TAG_LOW_COLOUR = WIKINDX_TAG_LOW_COLOUR_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_TAG_LOW_COLOUR))
			die('WIKINDX_TAG_LOW_COLOUR must be a valid HTML color (e.g. CCCCCC, gray).');

// Set Tag cloud high frequency color
		if (!property_exists($this->config, 'WIKINDX_TAG_HIGH_COLOUR'))
			$this->config->WIKINDX_TAG_HIGH_COLOUR = WIKINDX_TAG_HIGH_COLOUR_DEFAULT;
		elseif(!is_string($this->config->WIKINDX_TAG_HIGH_COLOUR))
			die('WIKINDX_TAG_HIGH_COLOUR must be a valid HTML color (e.g. FF0000, red).');

// Set Tag cloud low frequency size
		if (!property_exists($this->config, 'WIKINDX_TAG_LOW_SIZE'))
			$this->config->WIKINDX_TAG_LOW_SIZE = WIKINDX_TAG_LOW_SIZE_DEFAULT;
		elseif(!is_float($this->config->WIKINDX_TAG_LOW_SIZE) && !is_int($this->config->WIKINDX_TAG_LOW_SIZE))
			die('WIKINDX_TAG_LOW_SIZE must be a number.');

// Set Tag cloud low frequency size
		if (!property_exists($this->config, 'WIKINDX_TAG_HIGH_SIZE'))
			$this->config->WIKINDX_TAG_HIGH_SIZE = WIKINDX_TAG_HIGH_SIZE_DEFAULT;
		elseif(!is_float($this->config->WIKINDX_TAG_HIGH_SIZE) && !is_int($this->config->WIKINDX_TAG_HIGH_SIZE))
			die('WIKINDX_TAG_HIGH_SIZE must be a number.');

// Enable / disable images
		if (!property_exists($this->config, 'WIKINDX_IMAGES_ALLOW'))
			$this->config->WIKINDX_IMAGES_ALLOW = WIKINDX_IMAGES_ALLOW_DEFAULT; // v4 config.php does not have this property so set it!
		elseif(!is_bool($this->config->WIKINDX_IMAGES_ALLOW))
			die('WIKINDX_IMAGES_ALLOW must be a boolean (TRUE / FALSE).');

// Set image max size
		if (!property_exists($this->config, 'WIKINDX_IMAGES_MAXSIZE'))
			$this->config->WIKINDX_IMAGES_MAXSIZE = WIKINDX_IMAGES_MAXSIZE_DEFAULT;
		elseif(!is_int($this->config->WIKINDX_IMAGES_MAXSIZE))
			die('WIKINDX_IMAGES_MAXSIZE must be a positive integer (in Mo).');

// Enable / disable Debug mode
		if (!property_exists($this->config, 'WIKINDX_DEBUG_ERRORS'))
			$this->config->WIKINDX_DEBUG_ERRORS = WIKINDX_DEBUG_ERRORS_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DEBUG_ERRORS))
			die('WIKINDX_DEBUG_ERRORS must be a boolean (TRUE / FALSE).');

// Enable / disable email on Debug mode errors
		if (!property_exists($this->config, 'WIKINDX_DEBUG_EMAIL'))
			$this->config->WIKINDX_DEBUG_EMAIL = WIKINDX_DEBUG_EMAIL_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DEBUG_EMAIL))
			die('WIKINDX_DEBUG_EMAIL must be a boolean (TRUE / FALSE).');

// Display / hide SQL code debug output
		if (!property_exists($this->config, 'WIKINDX_DEBUG_SQL'))
			$this->config->WIKINDX_DEBUG_SQL = WIKINDX_DEBUG_SQL_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DEBUG_SQL))
			die('WIKINDX_DEBUG_SQL must be a boolean (TRUE / FALSE).');

// Enable / disable Smarty template compilation cache
		if (!property_exists($this->config, 'WIKINDX_BYPASS_SMARTYCOMPILE'))
			$this->config->WIKINDX_BYPASS_SMARTYCOMPILE = WIKINDX_BYPASS_SMARTYCOMPILE_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_BYPASS_SMARTYCOMPILE))
			die('WIKINDX_BYPASS_SMARTYCOMPILE must be a boolean (TRUE / FALSE).');

// Display statistics to read only users
		if (!property_exists($this->config, 'WIKINDX_DISPLAY_STATISTICS'))
			$this->config->WIKINDX_DISPLAY_STATISTICS = WIKINDX_DISPLAY_STATISTICS_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DISPLAY_STATISTICS))
			die('WIKINDX_DISPLAY_STATISTICS must be a boolean (TRUE / FALSE).');

// Display user statistics to read only users
		if (!property_exists($this->config, 'WIKINDX_DISPLAY_USER_STATISTICS'))
			$this->config->WIKINDX_DISPLAY_USER_STATISTICS = WIKINDX_DISPLAY_USER_STATISTICS_DEFAULT;
		elseif(!is_bool($this->config->WIKINDX_DISPLAY_USER_STATISTICS))
			die('WIKINDX_DISPLAY_USER_STATISTICS must be a boolean (TRUE / FALSE).');
	}
/**
* Strip some HTML tags from string.
*
* Since adding tiny_mce, we only strip javascript and the enclosing <p> tag tinymce adds
* @param string $element
* @return string
*/
	private function stripHtmlTags($element)
	{
		if(is_array($element))
			return array_map(array($this, "stripHtmlTags"), $element);
		else
		{
/*			$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
               '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
               '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
               '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
			);
*/			$search = array(
				'@<script[^>]*?>.*?</script>@usi',  // Strip out javascript
				'@<![\s\S]*?--[ \t\n\r]*>@u'         // Strip multi-line comments including CDATA
			);
			$element = preg_replace($search, '', $element);
			if(mb_strpos($element, '<p>') === 0)
				return preg_replace('@<p>(.*)</p>@u', '$1', $element, 1);
			else
				return $element;
		}
	}
/**
* Check the permissions of various folders and files which must be writable
*
*/
	private function checkFolders()
	{
		$array = array();
// Attachments
		$path = $this->config->WIKINDX_ATTACHMENTS_DIR;
		if(!is_writable($path))
			$array[$path] = mb_substr(sprintf('%o', fileperms($path)), -4);
// Attachments_cache
		$path = $this->config->WIKINDX_ATTACHMENTSCACHE_DIR;
		if(!is_writable($path))
			$array[$path] = mb_substr(sprintf('%o', fileperms($path)), -4);
// Files
		$path = $this->config->WIKINDX_FILES_DIR;
		if(!is_writable($path))
			$array[$path] = mb_substr(sprintf('%o', fileperms($path)), -4);
// Images
		if(!is_writable(WIKINDX_DIR_IMAGES))
			$array[WIKINDX_DIR_IMAGES] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_IMAGES)), -4);
// Templates
		if(!is_writable(WIKINDX_DIR_TEMPLATES_CACHE))
			$array[WIKINDX_DIR_TEMPLATES_CACHE] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_TEMPLATES_CACHE)), -4);

		if($handle = opendir(WIKINDX_DIR_TEMPLATES))
		{
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(mb_strpos($dir, '.') !== 0 && $dir != 'index.html')
				{
					if(!is_writable(WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . $dir))
						$array[WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . $dir] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . $dir)), -4);

					if(is_dir(WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . $dir))
					{
						$file = WIKINDX_DIR_TEMPLATES . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt';
						if(file_exists($file) && !is_writable($file))
							$array[$file] = mb_substr(sprintf('%o', fileperms($file)), -4);
						elseif(file_exists($file . '~') && !is_writable($file . '~'))
							$array[$file . '~'] = mb_substr(sprintf('%o', fileperms($file . '~')), -4);
					}
				}
			}

			closedir($handle);
		}
// Plugins
		if($handle = opendir(WIKINDX_DIR_PLUGINS))
		{
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(mb_strpos($dir, '.') !== 0 && $dir != 'index.html')
				{
					if(is_dir(WIKINDX_DIR_PLUGINS . DIRECTORY_SEPARATOR . $dir))
					{
						if(!is_writable(WIKINDX_DIR_PLUGINS . DIRECTORY_SEPARATOR . $dir))
							$array[WIKINDX_DIR_PLUGINS . DIRECTORY_SEPARATOR . $dir] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_PLUGINS . DIRECTORY_SEPARATOR . $dir)), -4);

						$file = WIKINDX_DIR_PLUGINS . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'config.php';
						if(file_exists($file) && !is_writable($file))
							$array[$file] = mb_substr(sprintf('%o', fileperms($file)), -4);

						$file = WIKINDX_DIR_PLUGINS . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'index.php';
						if(file_exists($file) && !is_writable($file))
							$array[$file] = mb_substr(sprintf('%o', fileperms($file)), -4);
						elseif(file_exists($file . '~') && !is_writable($file . '~'))
							$array[$file . '~'] = mb_substr(sprintf('%o', fileperms($file . '~')), -4);
					}
				}
			}

			closedir($handle);
		}
// styles
		if(!is_writable(WIKINDX_DIR_STYLES))
			$array[WIKINDX_DIR_STYLES] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_STYLES)), -4);

		if($handle1 = opendir(WIKINDX_DIR_STYLES))
		{
			while(FALSE !== ($dir1 = readdir($handle1)))
			{
				if(mb_strpos($dir1, '.') !== 0 && $dir1 != 'index.html')
				{
					if(!is_writable(WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . $dir1))
						$array[WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . $dir1] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . $dir1)), -4);

					if(is_dir(WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . $dir1))
					{
						if($dir1 == 'CACHE')
						{
							if(!is_writable(WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . $dir1))
								$array[WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . $dir1] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . $dir1)), -4);
							continue;
						}
						if($handle2 = opendir(WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . 'bibliography'))
						{
							while(FALSE !== ($dir2 = readdir($handle2)))
							{
								if(mb_strpos($dir2, '.') !== 0)
								{
									if(!is_writable(WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . 'bibliography' . DIRECTORY_SEPARATOR . $dir2))
									{
										$array[WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . 'bibliography' . DIRECTORY_SEPARATOR . $dir2] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . 'bibliography' . DIRECTORY_SEPARATOR . $dir2)), -4);
										continue;
									}
									$file = WIKINDX_DIR_STYLES . DIRECTORY_SEPARATOR . 'bibliography' . DIRECTORY_SEPARATOR . $dir2 . DIRECTORY_SEPARATOR . mb_strtoupper($dir2) . '.xml';
									if(file_exists($file) && !is_writable($file))
										$array[$file] = mb_substr(sprintf('%o', fileperms($file)), -4);
									elseif(file_exists($file . '~') && !is_writable($file . '~'))
										$array[$file . '~'] = mb_substr(sprintf('%o', fileperms($file . '~')), -4);
								}
							}

							closedir($handle2);
						}
					}
				}
			}

			closedir($handle1);
		}
// languages/
		if(!is_writable(WIKINDX_DIR_LANGUAGES))
			$array[WIKINDX_DIR_LANGUAGES] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_LANGUAGES)), -4);

		if($handle = opendir(WIKINDX_DIR_LANGUAGES))
		{
			while(FALSE !== ($dir = readdir($handle)))
			{
				if(mb_strpos($dir, '.') !== 0 && $dir != 'index.html' && $dir != 'CONSTANTS.php')
				{
					if(!is_writable(WIKINDX_DIR_LANGUAGES . DIRECTORY_SEPARATOR . $dir))
						$array[WIKINDX_DIR_LANGUAGES . DIRECTORY_SEPARATOR . $dir] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_LANGUAGES . DIRECTORY_SEPARATOR . $dir)), -4);

					if(is_dir(WIKINDX_DIR_LANGUAGES . DIRECTORY_SEPARATOR . $dir))
					{
						$file = WIKINDX_DIR_LANGUAGES . DIRECTORY_SEPARATOR . $dir . DIRECTORY_SEPARATOR . 'description.txt';
						if(file_exists($file) && !is_writable($file))
							$array[$file] = mb_substr(sprintf('%o', fileperms($file)), -4);
						elseif(file_exists($file . '~') && !is_writable($file . '~'))
							$array[$file . '~'] = mb_substr(sprintf('%o', fileperms($file . '~')), -4);
					}
				}
			}

			closedir($handle);
		}
// languages/reference/
		if($handle = opendir(WIKINDX_DIR_LANGUAGES . DIRECTORY_SEPARATOR . 'reference'))
		{
			while(FALSE !== ($file = readdir($handle)))
			{
				if(mb_strpos($dir, '.') !== 0)
				{
					if(!is_writable(WIKINDX_DIR_LANGUAGES . DIRECTORY_SEPARATOR . 'reference' . DIRECTORY_SEPARATOR . $file))
						$array[WIKINDX_DIR_LANGUAGES . DIRECTORY_SEPARATOR . 'reference' . DIRECTORY_SEPARATOR . $file] = mb_substr(sprintf('%o', fileperms(WIKINDX_DIR_LANGUAGES . DIRECTORY_SEPARATOR . 'reference' . DIRECTORY_SEPARATOR . $file)), -4);
				}
			}

			closedir($handle);
		}
		if(!empty($array))
			$this->checkFoldersDie($array);
	}
/**
* If permissions are incorrect on a folder or file, die with appropriate message if chmod() does not work.
*
* @param array $array An array of folders or filenames and their incorrect permissions
*/
	private function checkFoldersDie($array)
	{
		$string = '';
		foreach($array as $name => $perm)
		{
			if(!@chmod($name, 0655))
				$string .= $name . ':&nbsp;&nbsp;' . $perm  . BR;
		}
		die("WIKINDX will not function correctly if various folders and files are not writeable by the web server user.
			WIKINDX tried, but failed, to make certain folders and files writeable.
			The following, shown with their current permissions, should be made writeable by the web server user:<p>$string");
	}
}
?>