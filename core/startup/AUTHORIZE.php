<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* AUTHORIZE
*
* Logging on and system authorisation class.
*
* @version	1
*
*	@package wikindx5\core\startup
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class AUTHORIZE
{
/** object */
private $db;
/** object */
private $session;
/** object */
private $config;
/** object */
private $loadConfig;
/** array */
private $vars;
/** object */
private $configDbStructure;

/**
*	AUTHORIZE
*/
	public function __construct()
	{
		$this->session = FACTORY_SESSION::getInstance();
		$this->config = FACTORY_CONFIG::getInstance();
		$this->db = FACTORY_DB::getInstance();
		$this->vars = GLOBALS::getVars();
		$this->loadConfig = FACTORY_LOADCONFIG::getInstance();
		$this->configDbStructure = FACTORY_CONFIGDBSTRUCTURE::getInstance();
		if(!$this->session->getVar("setup_Language"))
		{
			$fields = $this->db->listFields('config');
// set the default language prior to displaying the login prompt
			if(in_array('language', $fields) !== FALSE) // perhaps this is a first install or upgrade to >= v4 (v3 has config.language not config.configLanguage)
				$this->session->setVar('setup_Language', WIKINDX_LANGUAGE_DEFAULT);
			else if(array_search('configLanguage', $fields) !== FALSE)
			{
				if($this->db->numRows($this->db->select('config', '*')) == 1) // Prior to v5.3
					$this->session->setVar('setup_Language', WIKINDX_LANGUAGE_DEFAULT);
				else // post v5.3
				{
					$user = FACTORY_USER::getInstance();
					$row = $this->configDbStructure->getData('configLanguage');
					if(empty($row)) // perhaps this is a first install
						$this->session->setVar('setup_Language', WIKINDX_LANGUAGE_DEFAULT);
					else
// populate session with default values from config
						$user->writeSessionPreferences(FALSE, 'config');
				}
			}
		}
	}
/**
* gatekeeper to the system.
*
* Order is of checking is important!
* @return boolean
*/
	public function gatekeep()
	{
		$this->loadConfig->load();
		if(array_key_exists("action", $this->vars))
		{
// Logged-on user clicked on 'OK' when asked to confirm GDPR or privacy statement
			if($this->vars["action"] == 'authGate')
			{
				$this->db->formatConditions(array('usersId' => $this->session->getVar('setup_UserId')));
				$this->db->update('users', array('usersGDPR' => 'Y'));
// FALSE means go to front of WIKINDX
				return FALSE;
			}
// logging out
			if($this->vars["action"] == 'logout')
			{
				$this->logout();
			}
// User logging in from readOnly mode
			else if($this->vars["action"] == 'initLogon')
			{
// First delete any pre-existing session in case this user has been logging on and off as different users --
// keep template and language etc.
				$language = $this->session->getVar('setup_Language');
				$template = $this->session->getVar('setup_Template');
				$userReg = $this->session->getVar('setup_UserRegistration');
				$multiUser = $this->session->getVar('setup_MultiUser');
				$this->session->setVar('setup_Language', $language);
				$this->session->setVar('setup_Template', $template);
				$this->session->setVar('setup_UserRegistration', $userReg);
				$this->session->setVar('setup_MultiUser', $multiUser);
				$this->initLogon(); // login prompt
				FACTORY_CLOSENOMENU::getInstance();
			}
			if(array_key_exists('method', $this->vars) &&
			(($this->vars["method"] == 'forgetInitStage1') ||
			($this->vars["method"] == 'forgetInitStage2') ||
			($this->vars["method"] == 'forgetProcess')))
				return TRUE;
// User supplying username and password to logon to WIKINDX.
// $auth->logonCheck() dies after printing logon screen if bad comparison.
			else if(($this->vars["action"] == 'logon') &&
				array_key_exists("password", $this->vars) && array_key_exists("username", $this->vars))
			{
				$this->logonCheck($this->vars['username'], $this->vars['password']);
// tidy up old files
				FILE\tidyFiles();
// FALSE means go to front of WIKINDX
				return FALSE;
			}
// superAdmin already logged in after upgrade so just set up the environment
			else if($this->vars["action"] == 'upgradeDB')
			{
				$user = FACTORY_USER::getInstance();
				$this->session->clearSessionData();
				$this->session->setVar("setup_UserId", '1'); // superAdmin always id = '1'
				$this->session->setVar("setup_Write", TRUE);
				$this->session->delVar("setup_ReadOnly");
				$user->writeSessionPreferences(FALSE, 'config');
// restore some session variables if stored from last logout
				$this->restoreEnvironment();
//				$this->loadConfig->load();
				return FALSE;
			}
// User requesting readOnly access - clear previous sessions
			else if($this->vars["action"] == 'readOnly')
			{
				if($this->config->WIKINDX_DENY_READONLY)
				{
					$this->initLogon(); // login prompt
					FACTORY_CLOSENOMENU::getInstance();
				}
// First delete any pre-existing session in case this user has been logging on and off as different users
// session array 'setup' is deleted at logout()
				$this->session->destroy();
				$this->session->setVar("setup_ReadOnly", TRUE);
// tidy up old files
				FILE\tidyFiles();
// populate session with default values from config
				$user = FACTORY_USER::getInstance();
				$user->writeSessionPreferences(FALSE, 'config');
				$this->clearEmbargoes();
				$this->checkNews();
// FALSE means go to front of WIKINDX
				return FALSE;
			}
// User registration
			else if($this->session->getVar("setup_UserRegistration") && $this->session->getVar("setup_MultiUser") && ($this->config->WIKINDX_MAIL_SERVER))
			{
				include_once("core/modules/usersgroups/REGISTER.php");
				$obj = new REGISTER();
				if($this->vars["action"] == 'initRegisterUser')
				{
					$obj->initRegister();
					if(!$this->session->getVar("setup_ReadOnly"))
						FACTORY_CLOSENOMENU::getInstance();
					else
						FACTORY_CLOSE::getInstance();
				}
				else if(array_key_exists('method', $this->vars) && $this->vars["method"] == 'registerConfirm')
				{
					GLOBALS::addTplVar('content', $obj->registerConfirm());
					if(!$this->session->getVar("setup_ReadOnly"))
						FACTORY_CLOSENOMENU::getInstance();
					else
						FACTORY_CLOSE::getInstance();
				}
				else if(array_key_exists('method', $this->vars) && $this->vars["method"] == 'registerUser')
				{
					$obj->registerUser();
					if(!$this->session->getVar("setup_ReadOnly"))
						FACTORY_CLOSENOMENU::getInstance();
					else
						FACTORY_CLOSE::getInstance();
				}
				else if(array_key_exists('method', $this->vars) && $this->vars["method"] == 'registerUserAdd')
				{
					GLOBALS::addTplVar('content', $obj->registerUserAdd());
					FACTORY_CLOSE::getInstance();
				}
				else if(array_key_exists('method', $this->vars) && $this->vars["method"] == 'registerRequest')
				{
					GLOBALS::addTplVar('content', $obj->registerRequest());
					FACTORY_CLOSE::getInstance();
				}
			}
		}
		if(isset($this->vars["method"]) && ($this->vars['method'] == 'RSS') && !$this->config->WIKINDX_DENY_READONLY)
		{
			$this->session->setVar("setup_ReadOnly", TRUE);
			return TRUE;
		}
// access already granted
		if($this->session->getVar('setup_Write'))
			return TRUE;
// access already granted
		if($this->session->getVar("setup_ReadOnly"))
		{
// populate session with default values from config
//			$user = FACTORY_USER::getInstance();
//			$user->writeSessionPreferences(FALSE, 'config');
			return TRUE;
		}
		if((!array_key_exists('action', $this->vars) || $this->vars['action'] != 'upgradeDBLogon'))
		{
//			$this->loadConfig->load();
			$cookie = FACTORY_COOKIE::getInstance();
// grabCookie() returns TRUE if valid cookie - otherwise, proceed to manual logon
			if($cookie->grabCookie())
			{
// Success - so restore some session variables if stored from last logout
				$this->restoreEnvironment();
				return TRUE;
			}
		}
		if(!$this->session->getVar("setup_Write") && !$this->session->getVar('setup_ReadOnly'))
		{
// Default == read only access.
			if($this->config->WIKINDX_READONLYACCESS && !$this->config->WIKINDX_DENY_READONLY)
			{
				$this->session->setVar("setup_ReadOnly", TRUE);
// populate session with default values from config
				$user = FACTORY_USER::getInstance();
				$user->writeSessionPreferences(FALSE, 'config');
				$this->checkNews();
				return TRUE;
			}
			$this->initLogon(); // login prompt
			FACTORY_CLOSENOMENU::getInstance();
		}
// FALSE indicates that index.php will print the front page of WIKINDX
		return FALSE;
	}
/**
* Display the empty form for logging on
*
* @param string $error Default is FALSE
*/
	public function initLogon($error = FALSE)
	{
		$this->session->delVar("setup_ReadOnly");
		$messages = FACTORY_MESSAGES::getFreshInstance();
		GLOBALS::setTplVar('heading', $messages->text("heading", "logon"));
		$pString = $error;
		$row = $this->configDbStructure->getData(array('configMultiUser', 'configUserRegistration'));
		if(!$row['configMultiUser'])
		{
			$errors = FACTORY_ERRORS::getFreshInstance();
			$pString .= \HTML\p($errors->text("warning", "superadminOnly"));
		}
		$pString .= \HTML\p($messages->text("authorize", "writeLogon"));
		$link1 = "index.php?action=readOnly";
		$link2 = "index.php?action=usersgroups_FORGET_CORE&method=forgetInitStage1";
		$link3 = "index.php?action=initRegisterUser";
		$links = FALSE;
/**
* For a test user (see index.php)
*/
		if($this->config->WIKINDX_RESTRICT_USERID)
			$pString .= \HTML\p("For test drive purposes, " .
				\HTML\strong($messages->text("user", "username") . ":&nbsp;&nbsp;") . "wikindx, " .
				\HTML\strong($messages->text("user", "password") . ":&nbsp;&nbsp;") . "wikindx");
		$forgot = $this->config->WIKINDX_MAIL_SERVER ? \HTML\a("link", $messages->text("user", "forget6"), $link2) : FALSE;
		$pString .= $this->printLogonTable();
// Give user the option to bypass logging in simply to read.
		if(!$this->config->WIKINDX_DENY_READONLY)
			$links = \HTML\a("link", $messages->text("authorize", "readOnly") .
			BR . $forgot, $link1);
		else
			$links = $forgot;
		if(($row['configUserRegistration']) && ($row['configMultiUser']) && ($this->config->WIKINDX_MAIL_SERVER))
			$links .= BR . \HTML\a("link", $messages->text("menu", "register"), $link3);
		$pString .= \HTML\p($links, FALSE, 'right');
		GLOBALS::addTplVar('content', $pString);
	}
/**
* print username/password text boxes
*
* @return string
*/
	private function printLogonTable()
	{
		$messages = FACTORY_MESSAGES::getFreshInstance();
		$pString = \FORM\formHeader("logon");
		$pString .= \HTML\tableStart('left width50percent');
		$pString .= \HTML\trStart();
		$pString .= \HTML\td($messages->text("user", "username") . ":&nbsp;&nbsp;");
		$pString .= \HTML\td(\FORM\textInput(FALSE, "username"));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td($messages->text("user", "password") . ":&nbsp;&nbsp;");
		$pString .= \HTML\td(\FORM\passwordInput(FALSE, "password"));
		$pString .= \HTML\trEnd();
		$pString .= \HTML\trStart();
		$pString .= \HTML\td("&nbsp;");
		$pString .= \HTML\td(\FORM\formSubmit(), 'right');
		$pString .= \HTML\trEnd();
		$pString .= \HTML\tableEnd();
		$pString .= \FORM\formEnd();
		return $pString;
	}
/**
* Initial logon to the system when upgrading the database.
*
* @param string $username
* @param string $password
* @param constant $dbVersion default = WIKINDX_VERSION
* @return boolean TRUE if not able to authenticate
*/
	public function logonCheckUpgradeDB($username, $password, $dbVersion = WIKINDX_VERSION)
	{
		$dbV = explode('.', $dbVersion);
		$dbVersion = $dbV[0] . '.' . $dbV[1];
		if($dbVersion < 4.0) // pre v4.0
		{
			$fields = array("id", "password", "admin", "cookie");
			$this->db->formatConditions(array('username' => $username));
			$passwordField = 'password';
			$idField = 'id';
		}
		else
		{
			$fields =  array("usersId", "usersPassword", "usersAdmin", "usersCookie");
			$this->db->formatConditions(array('usersUsername' => $username));
			$passwordField = 'usersPassword';
			$idField = 'usersId';
		}
		$recordset = $this->db->select('users', $fields);
		if(!$this->db->numRows($recordset))
			return TRUE; // NB -- unable to authenticate
		$row = $this->db->fetchRow($recordset);
		if(crypt($password, $row[$passwordField]) != $row[$passwordField])
			return TRUE;
		if($row[$idField] != '1') // superAdmin is id '1'
			return TRUE;
		$this->session->clearSessionData();
		$this->session->setVar("setup_Superadmin", TRUE);
		$this->session->setVar("setup_Write", TRUE);
		return FALSE;
	}
/**
* Initial logon to the system.
*
* Public for use with API.
* If cleared, user environment is set and embargoed resources checked for clearance, otherwise, user is presented with logon prompt again
* @param string $username
* @param string $password
*/
	public function logonCheck($username, $password)
	{
		$user = FACTORY_USER::getInstance();
// If checkPassword is successful, it also sets up some session variables to allow access without reauthentication.
		if(!$user->checkPassword($username, $password))
			$this->failure();
// Success - so restore some session variables if stored from last logout
		$this->restoreEnvironment();
		$this->clearEmbargoes();
// Run AuthGate (e.g. GDPR and/or privacy notification if required)
		$this->authGate();
	}
/**
* GDPR and/or privacy notification if required
*
*/
	private function authGate()
	{
		if(($this->config->WIKINDX_AUTHGATE_USE === TRUE) && ($this->session->getVar('setup_UserId') != 1))
		{
			$this->db->formatConditions(array('usersId' => $this->session->getVar('setup_UserId')));
			$recordset = $this->db->select('users', 'usersGDPR');
			if($this->db->fetchOne($recordset) == 'N')
			{
				$pString = \HTML\p($this->config->WIKINDX_AUTHGATE_MESSAGE);
				$pString .= \FORM\formHeader("authGate");
				$pString .= \HTML\td(\FORM\formSubmit('OK'));
				$pString .= \FORM\formEnd();
				GLOBALS::addTplVar('content', $pString);
				FACTORY_CLOSENOMENU::getInstance();
			}
		}
	}
/**
* Clear any expired embargoes
*/
	private function clearEmbargoes()
	{
		$this->db->formatConditions('resourceattachmentsEmbargoUntil' . $this->db->less . 'CURRENT_TIMESTAMP');
		$this->db->update('resource_attachments', array('resourceattachmentsEmbargo' => 'N'));
	}
/**
* Successful registered user logon so restore some session variables if they've been stored
*/
	private function restoreEnvironment()
	{
		$this->db->formatConditions(array('usersId' => $this->session->getVar('setup_UserId')));
		$state = $this->db->selectFirstField('users', 'usersUserSession');
		if($state)
		{
			$sessionData = unserialize(base64_decode($state));
			foreach($sessionData as $key => $array)
			{
				$array = unserialize(base64_decode($array));
				if(!is_array($array))
					continue;
				foreach($array as $subKey => $value)
					$this->session->setVar($key . '_' . $subKey, $value);
			}
		}
		$this->checkNews();
	}
/**
* Check for any news items in the database
*/
	private function checkNews()
	{
		$resultset = $this->db->select('news', 'newsId');
		if($this->db->numRows($resultset))
			$this->session->setVar('setup_News', TRUE);
		else
			$this->session->delVar('setup_News');
	}
/**
* log out user
*
* Various bits of garbage disposal, session is destroyed, cookie is deleted and user is presented with logon prompt
*/
	private function logout()
	{
// Garbage disposal
// remove this session's files
		$dir = $this->config->WIKINDX_FILES_DIR;
		if($sessVar = $this->session->getVar('FileExports'))
		{
			$sessArray = unserialize($sessVar);
			if(file_exists($dir))
			{
			    $d = opendir($dir);
				while(FALSE !== ($f = readdir($d)))
				{
					if(($f == ".") || ($f == ".."))
						continue;
					if(array_search($f, $sessArray) === FALSE)
						continue;
					$file = $dir . "/" . $f;
					unlink($file);
				}

				closedir($d);
			}
//			$this->session->delVar('fileExports');
		}
		if($sessVar = $this->session->getVar('PaperExports'))
		{
			$sessArray = unserialize($sessVar);
			if(file_exists($dir))
			{
			    $d = opendir($dir);
				while(FALSE !== ($f = readdir($d)))
				{
					if(($f == ".") || ($f == ".."))
						continue;
					if(!array_key_exists($f, $sessArray))
						continue;
					$file = $dir . "/" . $f;
					unlink($file);
				}

				closedir($d);
			}
//			$this->session->delVar('paperExports');
		}
		$this->session->destroy();
// set the default language prior to displaying the login prompt
		$row = $this->configDbStructure->getData('configLanguage');
		if(empty($row)) // perhaps this is a first install or upgrade to >= v4 (v3 has config.language not config.configLanguage)
			$this->session->setVar('setup_Language', WIKINDX_LANGUAGE_DEFAULT);
		else
		{
			$user = FACTORY_USER::getInstance();
// populate session with default values from config
			$user->writeSessionPreferences(FALSE, 'config');
		}
// remove any wikindx cookie that has been set
		$cookie = FACTORY_COOKIE::getInstance();
		$cookie->deleteCookie();
		if($this->config->WIKINDX_DENY_READONLY)
		{
			$this->initLogon(); // login prompt
			FACTORY_CLOSENOMENU::getInstance();
		}
		else // send back to front page
		{
			$this->session->setVar('setup_ReadOnly', TRUE);
			include_once("core/display/FRONT.php");
			$front = new FRONT('');
			unset($front);
			FACTORY_CLOSE::getInstance();
		}
	}
/**
* failure
*
* In case of failure, Exit back to logon prompt with optional error message
* @param string error Default is FALSE
*/
	private function failure($error = FALSE)
	{
		if(!$error && ($sessionError = $this->session->getVar('misc_ErrorMessage')))
		{
			$error = $sessionError;
			$this->session->delVar('misc_ErrorMessage');
		}
// Exit back to logon prompt
		FACTORY_CLOSENOMENU::getInstance($this->initLogon($error));
	}
/**
* isPluginExecutionAuthorised
*
* Check if the level of auth matchs the level requested by the calling plugin.
* If $promptForLogin is TRUE, displays the login page instead of returning FALSE,
* when the plugin is not authorised
*
* @param integer $pluginAuthLevelRequested Default is 0.
*        Auth level:
*           unknow => always unauthorised, menu item not displayed
*			0      => menu item displayed for all users (logged or not)
*			1      => menu item displayed for users logged (with write access)
*			2      => menu item displayed only for logged admins
* @param boolean $promptForLogin Default is FALSE.
* @return boolean
*/
	public function isPluginExecutionAuthorised($pluginAuthLevelRequested = 0, $promptForLogin = FALSE)
	{
	    $isAuthorised = FALSE;

	    switch ($pluginAuthLevelRequested)
	    {
	        case 0:
	            $isAuthorised = TRUE;
	        break;
	        case 1:
        		if ($this->session->getVar("setup_Write"))
        		    $isAuthorised = TRUE;
        		elseif ($promptForLogin == TRUE)
        			$this->initLogon();
	        break;
	        case 2:
        		if ($this->session->getVar("setup_Superadmin"))
        		    $isAuthorised = TRUE;
        		elseif ($promptForLogin == TRUE)
        			$this->initLogon();
	        break;
	    }

	    return $isAuthorised;
	}
}
?>