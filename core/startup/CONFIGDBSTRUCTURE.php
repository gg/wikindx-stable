<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @copyright 2018-2019 Stéphane Aulery <lkppo@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
*	CONFIGDBSTRUCTURE
*
*	Map out the structure of the config table
*
* @version	1
*
*	@package wikindx5\core\startup
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class CONFIGDBSTRUCTURE
{
/** array */
public $dbStructure;
/** array */
public $configToConstant;
/** object */
private $db;

/**
*	CONFIGDBSTRUCTURE
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$arrayVarchar = array('configTitle', 'configContactEmail', 'configLanguage', 'configStyle', 'configTemplate', 'configEmailNewRegistrations',
			'configLastChangesType', 'configRssBibstyle', 'configRssTitle', 'configRssDescription', 'configRssLanguage', 'configMailFrom',
			'configMailReplyTo', 'configMailReturnPath', 'configMailBackend', 'configMailSmPath', 'configMailSmtpServer', 'configMailSmtpEncrypt',
			'configMailSmtpUsername', 'configMailSmtpPassword', 'configCmsBibstyle', 'configCmsLanguage', 'configCmsDbUser', 'configCmsDbPassword',
			'configTagLowColour', 'configTagHighColour', 'configSqlEmail', 'configSqlErrorOutput', 'configLdapServer', 'configLdapDn',
			'configAuthGateMessage', 'configPasswordStrength');
		$arrayInt = array('configFileDeleteSeconds', 'configPaging', 'configPagingMaxLinks', 'configStringLimit', 'configImgWidthLimit',
			'configImgHeightLimit', 'configMaxPaste', 'configLastChanges', 'configLastChangesDayLimit', 'configPagingTagCloud',
			'configRestrictUserId', 'configRssLimit', 'configMailSmtpPort', 'configImagesMaxSize', 'configLdapPort', 'configLdapProtocolVersion',
			'configPasswordSize');
		$arrayBoolean = array('configDenyReadOnly', 'configReadOnlyAccess', 'configOriginatorEditOnly', 'configGlobalEdit', 'configMultiUser',
			'configUserRegistration', 'configRegistrationModerate', 'configNotify', 'configFileAttach', 'configFileViewLoggedOnOnly',
			'configImportBib', 'configEmailNews', 'configQuarantine', 'configListlink', 'configEmailStatistics', 'configMetadataAllow',
			'configMetadataUserOnly', 'configRssAllow', 'configRssDisplay', 'configMailServer', 'configMailSmtpPersist', 'configMailSmtpAuth',
			'configGsAllow', 'configGsAttachment', 'configCmsAllow', 'configCmsSql', 'configImagesAllow', 'configBypassSmartyCompile',
			'configErrorReport', 'configPrintSql', 'configDisplayStatistics', 'configDisplayUserStatistics', 'configLdapUse', 'configAuthGate');
		$arrayDatetime = array('configStatisticsCompiled');
		$arrayFloat = array('configTagLowSize', 'configTagHighSize');
		$arrayText = array('configNoSort', 'configSearchFilter', 'configDescription', 'configTimezone', 'configDeactivateResourceTypes');

		foreach($arrayVarchar as $name)
			$this->dbStructure[$name] = 'configVarchar';
		foreach($arrayInt as $name)
			$this->dbStructure[$name] = 'configInt';
		foreach($arrayBoolean as $name)
			$this->dbStructure[$name] = 'configBoolean';
		foreach($arrayDatetime as $name)
			$this->dbStructure[$name] = 'configDatetime';
		foreach($arrayFloat as $name)
			$this->dbStructure[$name] = 'configFloat';
		foreach($arrayText as $name)
			$this->dbStructure[$name] = 'configText';

		$this->configToConstant = array(
			'configTitle' => 'WIKINDX_TITLE',
			'configContactEmail' => 'WIKINDX_CONTACT_EMAIL',
			'configDescription' => 'WIKINDX_DESCRIPTION',
			'configFileDeleteSeconds' => 'WIKINDX_FILE_DELETESECONDS',
			'configPaging' => 'WIKINDX_PAGING',
			'configPagingMaxLinks' => 'WIKINDX_PAGING_MAXLINKS',
			'configStringLimit' => 'WIKINDX_STRINGLIMIT',
			'configLanguage' => 'WIKINDX_LANGUAGE',
			'configStyle' => 'WIKINDX_STYLE',
			'configTemplate' => 'WIKINDX_TEMPLATE',
			'configMultiUser' => 'WIKINDX_MULTIUSER',
			'configUserRegistration' => 'WIKINDX_USERREGISTRATION',
			'configNotify' => 'WIKINDX_NOTIFY',
			'configImgWidthLimit' => 'WIKINDX_IMG_WIDTHLIMIT',
			'configImgHeightLimit' => 'WIKINDX_IMG_HEIGHTLIMIT',
			'configFileAttach' => 'WIKINDX_FILE_ATTACH',
			'configFileViewLoggedOnOnly' => 'WIKINDX_FILE_VIEWLOGGEDONONLY',
			'configMaxPaste' => 'WIKINDX_MAXPASTE',
			'configLastChanges' => 'WIKINDX_LASTCHANGES',
			'configImportBib' => 'WIKINDX_IMPORTBIB',
			'configLastChangesType' => 'WIKINDX_LASTCHANGESTYPE',
			'configEmailNews' => 'WIKINDX_EMAIL_NEWS',
			'configEmailNewRegistrations' => 'WIKINDX_EMAIL_NEWREGISTRATIONS',
			'configRegistrationModerate' => 'WIKINDX_REGISTRATIONMODERATE',
			'configQuarantine' => 'WIKINDX_QUARANTINE',
			'configNoSort' => 'WIKINDX_NOSORT',
			'configSearchFilter' => 'WIKINDX_SEARCHFILTER',
			'configListLink' => 'WIKINDX_LISTLINK',
			'configEmailStatistics' => 'WIKINDX_EMAILSTATISTICS',
			'configStatisticsCompiled' => 'WIKINDX_STATISTICSCOMPILED',
			'configMetadataAllow' => 'WIKINDX_METADATA_ALLOW',
			'configMetadataUserOnly' => 'WIKINDX_METADATA_USERONLY',
			'configDenyReadOnly' => 'WIKINDX_DENY_READONLY',
			'configReadOnlyAccess' => 'WIKINDX_READONLYACCESS',
			'configOriginatorEditOnly' => 'WIKINDX_ORIGINATOR_EDITONLY',
			'configGlobalEdit' => 'WIKINDX_GLOBAL_EDIT',
			'configTimezone' => 'WIKINDX_TIMEZONE',
			'configRestrictUserId' => 'WIKINDX_RESTRICT_USERID',
			'configDeactivateResourceTypes' => 'WIKINDX_DEACTIVATE_RESOURCE_TYPES',
			'configRssAllow' => 'WIKINDX_RSS_ALLOW',
			'configRssLimit' => 'WIKINDX_RSS_LIMIT',
			'configRssBibstyle' => 'WIKINDX_RSS_BIBSTYLE',
			'configRssDisplay' => 'WIKINDX_RSS_DISPLAY',
			'configRssTitle' => 'WIKINDX_RSS_TITLE',
			'configRssDescription' => 'WIKINDX_RSS_DESCRIPTION',
			'configRssLanguage' => 'WIKINDX_RSS_LANGUAGE',
			'configLdapUse'	=>	'WIKINDX_LDAP_USE',
			'configLdapServer'	=>	'WIKINDX_LDAP_SERVER',
			'configLdapDn'	=>	'WIKINDX_LDAP_DN',
			'configLdapPort'	=>	'WIKINDX_LDAP_PORT',
			'configLdapProtocolVersion'	=>	'WIKINDX_LDAP_PROTOCOL_VERSION',
			'configAuthGate'	=>	'WIKINDX_AUTHGATE_USE',
			'configAuthGateMessage'	=>	'WIKINDX_AUTHGATE_MESSAGE',
			'configPasswordSize'	=>	'WIKINDX_PASSWORDSIZE',
			'configPasswordStrength'	=>	'WIKINDX_PASSWORDSTRENGTH',
			'configMailServer' => 'WIKINDX_MAIL_SERVER',
			'configMailFrom' => 'WIKINDX_MAIL_FROM',
			'configMailReplyTo' => 'WIKINDX_MAIL_REPLYTO',
			'configMailReturnPath' => 'WIKINDX_MAIL_RETURN_PATH',
			'configMailBackend' => 'WIKINDX_MAIL_BACKEND',
			'configMailSmPath' => 'WIKINDX_MAIL_SMPATH',
			'configMailSmtpServer' => 'WIKINDX_MAIL_SMTPSERVER',
			'configMailSmtpPort' => 'WIKINDX_MAIL_SMTPPORT',
			'configMailSmtpEncrypt' => 'WIKINDX_MAIL_SMTPENCRYPT',
			'configMailSmtpPersist' => 'WIKINDX_MAIL_SMTPPERSIST',
			'configMailSmtpAuth' => 'WIKINDX_MAIL_SMTPAUTH',
			'configMailSmtpUsername' => 'WIKINDX_MAIL_SMTPUSERNAME',
			'configMailSmtpPassword' => 'WIKINDX_MAIL_SMTPPASSWORD',
			'configGsAllow' => 'WIKINDX_GS_ALLOW',
			'configGsAttachment' => 'WIKINDX_GS_ATTACHMENT',
			'configCmsAllow' => 'WIKINDX_CMS_ALLOW',
			'configCmsBibstyle' => 'WIKINDX_CMS_BIBSTYLE',
			'configCmsLanguage' => 'WIKINDX_CMS_LANGUAGE',
			'configCmsSql' => 'WIKINDX_CMS_SQL',
			'configCmsDbUser' => 'WIKINDX_CMS_DB_USER',
			'configCmsDbPassword' => 'WIKINDX_CMS_DB_PASSWORD',
			'configTagLowColour' => 'WIKINDX_TAG_LOW_COLOUR',
			'configTagHighColour' => 'WIKINDX_TAG_HIGH_COLOUR',
			'configTagLowSize' => 'WIKINDX_TAG_LOW_SIZE',
			'configTagHighSize' => 'WIKINDX_TAG_HIGH_SIZE',
			'configImagesAllow' => 'WIKINDX_IMAGES_ALLOW',
			'configImagesMaxSize' => 'WIKINDX_IMAGES_MAXSIZE',
			'configErrorReport' => 'WIKINDX_DEBUG_ERRORS',
			'configSqlEmail' => 'WIKINDX_DEBUG_EMAIL',
			'configPrintSql' => 'WIKINDX_DEBUG_SQL',
			'configSqlErrorOutput' => 'WIKINDX_SQLERROROUTPUT',
			'configBypassSmartyCompile' => 'WIKINDX_BYPASS_SMARTYCOMPILE',
			'configDisplayStatistics' => 'WIKINDX_DISPLAY_STATISTICS',
			'configDisplayUserStatistics' => 'WIKINDX_DISPLAY_USER_STATISTICS',
		);
	}
/**
* Convert a value from Wikindx database format to PHP format
*
* @param string $configType can be: configVarchar, configInt, configBoolean, configDatetime, configFloat, or configText
* @param mixed $value to convert
* @return mixed The value converted
*/
	private function convertVarDB2PHP($configType, $value)
	{
        switch($configType)
        {
            // Cast to integer number
            case 'configInt':
                $value = (int)$value;
            break;
            // Cast to float number
            case 'configFloat':
                $value = (float)$value;
            break;
            // return boolean (stored as 0 or 1 in the db table)
            case 'configBoolean':
                $value = $value ? TRUE : FALSE;
            break;
        }
	    return $value;
	}
/**
* Convert a value from PHP format to Wikindx database format
*
* @param string $configType can be: configVarchar, configInt, configBoolean, configDatetime, configFloat, or configText
* @param mixed $value to convert
* @return mixed The value converted
*/
	private function convertVarPHP2DB($configType, $value)
	{
        switch($configType)
        {
            // Cast to integer number
            case 'configInt':
                $value = (string)$value;
            break;
            // Cast to float number
            case 'configFloat':
                $value = (string)$value;
            break;
            // return boolean (stored as 0 or 1 in the db table)
            case 'configBoolean':
                $value = $value ? 1 : 0;
            break;
        }
	    return $value;
	}
/**
* Get and return one value from the config table.
*
* Requested value must be a configName value that is in $this->dbStructure. For other values, use standard $db functions.
*
* Result is returned as a number if the value is stored in configInt or configFloat columns else the return result is a string or a boolean
*
* @param string $field – the table column to match the condition.
* @return mixed String, float, or boolean
*/
	public function getOne($field)
	{
	    $field = (string)$field;
		if(!array_key_exists($field, $this->dbStructure))
			die('Supply a configuration variable to search on');
		$column = $this->dbStructure[$field];
		$this->db->formatConditions(array('configName' => $field));
		$value = $this->db->fetchOne($this->db->select('config', $column));
		return $this->convertVarDB2PHP($column, $value);
	}
/**
* Get all data from the config table and return an array of ($field => 'value')
*
* @return array
*/
	public function getAllData()
	{
	    $field = 'configName';
		$row = array();
		$resultSet = $this->db->select('config', '*');
		while($coRow = $this->db->fetchRow($resultSet))
		{
// NB we grab only basic configuration variables – extra rows are added e.g. by localeDescription plugin
			if(array_key_exists($coRow[$field], $this->dbStructure))
				$row[$coRow[$field]] = $this->convertVarDB2PHP($this->dbStructure[$coRow[$field]], $coRow[$this->dbStructure[$coRow[$field]]]);
		}
		return $row;
	}
/**
* Get data from the config table for specific variables and return an array of ($field => 'value')
*
* @param mixed $match is the name of a variable or an array or variable names : array('var1, 'var2', ...).
* @return array
*/
	public function getData($match)
	{
	    $field = 'configName';
		$row = array();
		if(!is_array($match)) $match = array($match);
		$this->db->formatConditionsOneField($match, $field);
		$resultSet = $this->db->select('config', '*');
		while($coRow = $this->db->fetchRow($resultSet))
		{
// NB we grab only basic configuration variables – extra rows are added e.g. by localeDescription plugin
			if(array_key_exists($coRow[$field], $this->dbStructure))
				$row[$coRow[$field]] = $this->convertVarDB2PHP($this->dbStructure[$coRow[$field]], $coRow[$this->dbStructure[$coRow[$field]]]);
		}
		return $row;
	}
/**
* Update one value in the config table.
*
* @param string $name in the 'configName' column (i.e. which configuration variable to update)
* @param mixed $value to set
*/
	public function updateOne($name, $value)
	{
	    $name = (string)$name;
		if(!array_key_exists($name, $this->dbStructure))
			die('Supply a configuration variable to update');
		$value = $this->convertVarPHP2DB($this->dbStructure[$name], $value);
		$this->db->formatConditions(array('configName' => $name));
		$this->db->update('config', array($this->dbStructure[$name] => $value));
	}
}
?>