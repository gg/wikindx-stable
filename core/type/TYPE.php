<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2018 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* RESOURCE TYPE
*
* Handle resource types
*
* @version	1
*
*	@package wikindx5\core\type
*	@author Mark Grimshaw <sirfragalot@users.sourceforge.net>
*
*/
class TYPE
{
/** object */
private $db;
/** object */
private $commonBib;
/** object */
private $messages;

/**
*	RESOURCE TYPE
*/
	public function __construct()
	{
		$this->db = FACTORY_DB::getInstance();
		$this->commonBib = FACTORY_BIBLIOGRAPHYCOMMON::getInstance();
		$this->messages = FACTORY_MESSAGES::getInstance();
	}
/**
* Get resource types for which resources exist in database
*
* @param boolean $userBib FALSE (default) or TRUE (return only resource types within current user bibliography)
* @param mixed $metadata FALSE or array of metadata fields ('quote', 'quoteComment' etc.) which returned resource types must have. Default is array()
* @return mixed FALSE|array
*/
	function grabAll($userBib = FALSE, $metadata = array())
	{
		$subQuery = FALSE;
		if(is_array($metadata) && !empty($metadata))
		{
			$unions = array();
			foreach($metadata as $mType)
			{
				if($mType == 'quote')
				{
					$this->db->formatConditions(array('resourcemetadataType' => 'q'));
					$unions[] = $this->db->selectNoExecute('resource_metadata', array(array('resourcemetadataResourceId' => 'id')),
						TRUE, TRUE, TRUE);
				}
				else if($mType == 'quoteComment')
				{
					$this->db->formatConditions(array('resourcemetadataType' => 'q'));
					$unions[] = $this->db->selectNoExecute('resource_metadata', array(array('resourcemetadataResourceId' => 'id')),
						TRUE, TRUE, TRUE);
				}
				else if($mType == 'paraphrase')
				{
					$this->db->formatConditions(array('resourcemetadataType' => 'p'));
					$unions[] = $this->db->selectNoExecute('resource_metadata', array(array('resourcemetadataResourceId' => 'id')),
						TRUE, TRUE, TRUE);
				}
				else if($mType == 'paraphraseComment')
				{
					$this->db->formatConditions(array('resourcemetadataType' => 'p'));
					$unions[] = $this->db->selectNoExecute('resource_metadata', array(array('resourcemetadataResourceId' => 'id')),
						TRUE, TRUE, TRUE);
				}
				if($mType == 'musing')
				{
					$this->db->formatConditions(array('resourcemetadataType' => 'm'));
					$unions[] = $this->db->selectNoExecute('resource_metadata', array(array('resourcemetadataResourceId' => 'id')),
						TRUE, TRUE, TRUE);
				}
			}
			if(!empty($unions))
				$subQuery = $this->db->subQuery($this->db->union($unions), 't');
		}
		if($userBib)
		{
			if($subQuery)
				$this->commonBib->userBibCondition('id');
			else
				$this->commonBib->userBibCondition('resourceId');
		}
		$this->db->groupBy('resourceType');
		$this->db->orderBy('resourceType');
		if($subQuery)
		{
			$this->db->leftJoin('resource', 'resourceId', 'id');
			$recordset = $this->db->selectFromSubQuery(FALSE, 'resourceType', $subQuery);
		}
		else
			$recordset = $this->db->select('resource', 'resourceType');
		while($row = $this->db->fetchRow($recordset))
		{
			if(!$row['resourceType'])
				continue;
			$types[$row['resourceType']] = $this->messages->text("resourceType", $row['resourceType']);
		}
		if(isset($types))
			return $types;
		else
		    return FALSE;
	}
}
?>