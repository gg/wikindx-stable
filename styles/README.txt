WIKINDX v5.x bibliographic styles

Files to control the formatting of bibliographic and citation styles in WIKINDX v5.x.

Unzip into wikindx5/styles/bibliography/ then select in the Preferences menu.

Download and install the adminStyle plugin to compile and use your own styles.

Styles not included in the core release of WIKINDX:
	abnt.zip ~ Associação Brasileira de Normas Técnicas (COMPILER: Rui Alão, 2013)

Styles included in the core release of WIKINDX:
	apa.zip ~ American Psychological Association (COMPILER: Mark Grimshaw, 2013)
	britishmedicaljournal.zip ~ British Medical Journal (COMPILER: Mark Grimshaw, 2013)
	chicago.zip ~ Chicago (COMPILER: Mark Grimshaw, 2013)
	harvard.zip ~ Harvard (COMPILER: Mark Grimshaw, 2013)
	ieee.zip ~ Institute of Electrical and Electronic Engineers (COMPILER: Mark Grimshaw, 2013)
	mla.zip ~ Modern Language Association (COMPILER: Mark Grimshaw, 2013)
	turabian.zip ~ Turabian (COMPILER: Mark Grimshaw, 2013)
	wikindx.zip ~ WIKINDX (COMPILER: Mark Grimshaw, 2013)