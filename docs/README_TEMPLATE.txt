                          --o TEMPLATES o--

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---

WIKINDX v4 gives the template designer greater control over the visual  
design than did previous templates. It uses a combination of CSS and    
the Smarty HTML Templating System -- documentation on Smarty can be     
found at http://www.smarty.net/ but you will also find comments and     
help in the various .tpl files in wikindx5/templates/default/.          

Certain files must be present for the template to be available to the
user in Wikindx|Preferences:

 * display.tpl
   * header.tpl
   * menu.tpl
   * content.tpl
     * content_heading_block.tpl (optional if merged with content.tpl)
     * content_content.tpl (idem)
     * content_cite_fields.tpl (idem)
     * content_file_list.tpl (idem)
     * content_ressource.tpl (idem)
     * content_ressource_information.tpl (idem)
     * content_attachments.tpl (idem)
     * content_custom.tpl (idem)
     * content_abstract.tpl (idem)
     * content_note.tpl (idem)
     * content_quotes.tpl (idem)
     * content_paraphrases.tpl (idem)
     * content_musings.tpl (idem)
     * content_metadata.tpl (idem)
     * content_paging.tpl (idem)
     * content_ideas.tpl (idem)
   * footer.tpl
   * template.css (stylesheet of the template)
   * tinymce.css (stylesheet for inline html editor TinyMCE)
   * description.txt
   * template.js
   * images/* (images for css)
   * icons/* (icons included directly into Wikindx -- you must follow the same names as default theme)

To create a template, a good starting point is to copy the              
templates/default/ files to templates/xxxx/ where xxxx is a unique      
folder name for your template. You must also redit description.txt
as described below.

An option aside.txt file (which can be empty) ensures that WIKINDX help 
messages are opened not in a popup but in an HTML <aside> element with 
the id 'asideHelp'. Youir template files must account for this.

Template files:

>>>>>description.txt<<<<<

This comprises 1-3 lines. The first line gives the name of the template 
that is displayed to the user -- the other lines are optional and       
control how the multi-level drop-down menus in WIKINDX are displayed.   
WIKINDX uses such menus to efficiently display a large number of        
options in a limited screen space. However, such menus can be tricky    
to use in some circumstances and, in others such as mobile phone        
displays, cause more problems than they solve. Thus, both users and     
template designers can opt to reduce the number of menu levels. Users   
can edit their preferences while template designers can edit lines 2    
and 3 of description.txt. line 2: This can be 0, 1, 2, 0$, 1$, or 2$    
where 0 indicates that all menu levels should be used, 1 indicates that 
the number of levels should be reduced by 1 and 2 indicates that the    
number of levels should be reduced by 2. If there is only one line in   
description.txt, then 0 is assumed for line 2. The '$' indicates that   
that number of menu levels is mandated and the user has no choice --    
useful where the template is to be used in special circumstances such   
as on mobile devices. Without a '$', read-only users will initially be  
presented with the number of menu levels set by the template designer   
and this can be overridden in Wikindx|Preferences. line 3: This is only 
used if line 2 is either '1' or '1$'. When menu levels are reduced by   
one, the elements of the sub-submenu thus removed appear below the      
sub-submenu heading. Adding a string of text to line 3 will preface     
each of these elements with that text. Example description.txt:         
>>>>>
New Template
1
--> 
<<<<<

This will display sub-submenu elements as:

 * Some Menu Item
 * Sub-submenu Title...
   --> Sub-submenu Element 1
   --> Sub-submenu Element 2
   --> Sub-submenu Element 3
 * Next Menu Item
 * Next Menu Item

>>>>>display.tpl<<<<<

This is the first .tpl file to be loaded and it acts as a container for 
other .tpl files.                                                       

>>>>>header.tpl<<<<<

This requires some editing to set the paths and filenames for template.css  
and template.js files.                                                  

>>>>>menu.tpl<<<<<

Position the display of the drop-down menus.

>>>>>content.tpl<<<<<

This contains the main display logic for the body of the WIKINDX        
display between menus and footer. WIKINDX expects all such content to   
be displayed in an HTML table element with class 'mainTable'.           

>>>>>footer.tpl<<<<<

The final .tpl file to be read.


In addition to the template files, there are a number of GIF files that 
should be present in templates/xxx/icons/ See templates/default/icons/  
for the files to copy.                                                  

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---


-- 
The WIKINDX Team 2016
sirfragalot@users.sourceforge.net
lkppo@users.sourceforge.net
