                          --o RSS FEED o--

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---

This is for users wishing to display a RSS feed of the latest additions 
or edits on a RSS reader or web site such as a blog. The RSS is fed     
from wikindx5/rss.php which requires some minimal configuration in      
config.php.                                                             

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---


-- 
The WIKINDX Team 2016
sirfragalot@users.sourceforge.net
