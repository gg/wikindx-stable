                            --o README o--

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---

I originally started developing this as a means to help organise my PhD
research  by catalogueing  bibliographic notations,  references, quotes
and thoughts on a computer via a  program that was not tied to a single
operating system (like similar and expensively commercial software) and
that could be accessed from anywhere on the web (unlike other systems).
Additionally, I wanted a quick way to search, for example, for
all quotes and thoughts referencing a keyword or to be able to
automatically reformat bibliographies to different style guides.

As this is  a system designed to  run on any web server,  I thought its
use could be expanded to groups of researchers who could all contribute
to  and  read  the index. This  concept is  very  similar to  a Wiki
Encyclopaedia  where anyone can add or edit entries in an  on-line
encyclopaedia.

Since the original ideas, various others have been implemented such as
a wide variety of import and export options and, importantly, the
incorportation of a WYSIWYG word processor that can insert citations
and (re)format them with a few clicks of the mouse.  This was powerful
enough for me to write my entire PhD thesis in. (v4 removed this
feature to a plug-in rather than being a core feature.)

Developed under the Creative Commons CC-BY-NC-SA 2.0 license (since v5.1,
GPL 2.0 before that), the project homepage can be found at:
https://sourceforge.net/projects/wikindx/ and the required files/updates
and a variety of enhancements (language localizations and plug-ins) are
freely available there.

Wikindx includes and uses several free creations. I thank the authors
for their work:

 * FatCow, Free Icon Set (CC BY 3.0): www.fatcow.com
 * TinyMCE, JavaScript WYSIWYG HTML Editor (LGPL 2.1): www.tinymce.com
 * TinyMCE Compressor PHP (LGPL 3.0) : github.com/hakjoon/hak_tinymce/tree/master/tinymce_compressor_php
 * Smarty, PHP templating system (LGPL 3.0): www.smarty.net
 * SmartyMenu, menu plugin for Smarty (LGPL 2.1): www.phpinsider.com/php/code/SmartyMenu
 * PdfToText, PHP library to extract data from a PDF (LGPL 3.0): https://github.com/christian-vigh-phpclasses/PdfToText
 * PHPMailer, PHP email sending library (LGPL 2.1): github.com/PHPMailer/PHPMailer
 * CKEditor, JavaScript WYSIWYG HTML Editor (LGPL 2.1): ckeditor.com/
 * json2.js, JSON in JavaScript (Public Domain): https://github.com/douglascrockford/JSON-js

WIKINDX Requirements (and has been tested on):

 * PHP >= v5.6.x and <= 7.3.x
 * MySQL >= v5.7.5 or MariaDB >= 10.2 (mysqli driver)
 * Apache >= 2.x or nginx >= 1.11 (or any web Server able to run PHP scripts).

The  client  accesses  the  database via  a  standard  (graphical)  web
browser. WIKINDX  makes use of the tinyMCE editor  (v2.0.4) and this
limits the web browsers that can be used.

A list of compatible browsers can be found here:

    http://www.tinymce.com/wiki.php/Browser_compatiblity.

Generally, though, if you use the following, you should be OK:

 * Internet Explorer >= v9
 * Edge >= v10
 * Firefox >= v12
 * Chrome >= v4
 * Safari >= v10
 * Opera >= v12
 * WebKit based navigators (https://en.wikipedia.org/wiki/List_of_web_browsers#WebKit-based)

---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---


--
Mark Grimshaw-Aagaard
The WIKINDX Team 2019
sirfragalot@users.sourceforge.net
