      --o TEST DATABASE -- wikindx5_7_testDatabase.sql o--

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---

This is a v5.7 wikindx database provided for test purposes.

Use PhpMyAdmin (or similar) to create a database and                    
add a username/password to it then import the file                      
wikindx5_7_testDatabase.sql. Add the database name and                  
username/password to wikindx5/config.php and then run wikindx v5.7                           

Three users (username/password):

 * Administrator -- super/super
 * user1/user1
 * user2/user2

There are 83 resources entered with categories, keywords, abstracts,
notes and metadata (quotations, paraphrases, musings) and at least two
resources per resource type.

user2 has a private bibliography. There is a user group which has two
members (super and user1) and which has a private group bibliography
(superBibliography).

Some maturity indices have been set and there are some popularity
ratings/number of views.

No attachments have been added to any resource.

 ---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---

-- 
Mark Grimshaw-Aagaard
The WIKINDX Team 2019
sirfragalot@users.sourceforge.net
