                          --o UPGRADE o--

---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---

NB
///////////////////

If you are installing a fresh WIKINDX with a blank database, read
docs/INSTALL.txt.

If you are upgrading, read this document (UPGRADE.txt) carefully.

If you have an existing v3.8.2 WIKINDX database, you can upgrade it 
directly by pointing the v5 config.php at it.  This route is not 
recommended and it is safer to export your v3.8.2 database, import it 
into a new database then point the v5 config.php to the new database.
If you undertake the safer, second route, do 
not attempt to run WIKINDX through your browser until you have imported 
your v3.8.x database into your v5.x database. If you do, WIKINDX will   
treat it as a fresh install and will create blank tables which will     
then cause upgrade failures when you run WIKINDX in the browser -- if
this occurs, the solution is to drop all tables from the database,
import your v3.8.x tables then run WIKINDX from the browser.

If you are upgrading a previous installation of WIKINDX 4.x to 5.x, 
simply overwrite the existing installation. Optionally, you can install the software files in a different folder but see steps 7–9 below (especially step 9). In both cases, you should not use the existing config.php file but should copy config.php.dist to a new config.php. v5 has added several new settings: you can copy existing settings from the old config.php file but should also edit the new settings.
The first running of WIKINDX will upgrade the database in a single step. You should also  download and reinstall v5 plugins as, from v4.2, WIKINDX introduced a plugin compatibility check.

///////////////////

BACK UP YOUR DATABASE BEFORE ANY UPGRADING!!!!!!!
BACK UP YOUR DATABASE BEFORE ANY UPGRADING!!!!!!!
BACK UP YOUR DATABASE BEFORE ANY UPGRADING!!!!!!!
BACK UP YOUR DATABASE BEFORE ANY UPGRADING!!!!!!!
BACK UP YOUR DATABASE BEFORE ANY UPGRADING!!!!!!!


IMPORTANT <<

1/ v5 will automatically upgrade an existing v3.8.x or v4.x WIKINDX database.

2/ If your WIKINDX installation is less than v3.8.x, you should first
upgrade to v3.8.2, convert the database by running v3.8.2, then upgrade
further to v5.x. It's assumed that there are very few pre v3.8.2 users
left if any so this is a code cleanout. See the section below.

3/  WIKINDX uses caching in the database _cache table for lists of
creators, keywords etc. For large WIKINDX databases, if you receive
messages such as 'MySQL server has gone away', try increasing
max_allowed_packet in the MySQL server.

4/ If your database is over 1500 resources and you expect to export
(with the importexport plugin) lists of resources of at least this length,
then you should set public $WIKINDX_MEMORY_LIMIT = "64M"; in config.php
in order to avoid memory allocation errors.

>> IMPORTANT

UPGRADING 3.8.2 <<

5/ Unzip WIKINDX into a new folder on your web server -- it will create
a wikindx5/ folder.

6/ Create a new MySQL database (e.g. 'wikindx5'), GRANT ALL permissions
on it to your wikindx user, dump your v3.8.x WIKINDX database (including
table structure) then import that dump into your v5 database.

7/ Copy wikindx5/config.php.dist to wikindx5/config.php and edit the
latter (v4.x moved a lot of configuration options to the web browser
interface but, as a minimum, you will need to set the MySQL database
access protocols).

8/ Ensure that 
'attachments/',
'attachments_cache/',
'files/',
'images/',
'languages/',
'plugins/',
'templates/',
'tplcompilation/',
'styles/CACHE/' and
'styles/bibliography/' 
and the XML files within 
'styles/bibliography/xxx/' 
folders are writable by the web server user (usually 'nobody'). If you
are running WIKINDX locally on Windows (using something like XAMPP),
you can skip this step as the folders will be writable by default.

Additionally, in any plugin folder, index.php and the config.php file
(if it exists) should also be writable. If you are running WIKINDX
locally on Windows (using something like XAMPP), you can skip this step
as the folders will be writable by default.

9/ If you have attachments in the old wikindxXXX/attachments folder,
copy these to wikindx5/attachments/. The upgrade process will remove
references to resource attachments in the database if those attachments are not found in
the wikindx5/attachments/ folder.

10/ Finally, run v5 through your web browser (http://<server>/wikindx5)
and follow the instructions there to complete the upgrade. In order to
account for potential PHP timeouts when used on a large database, the 
database upgrade takes place over 16 stages.

   a/ Administrators who have written their own bibliographic styles
   for v3.x should edit and save them in the plug-in adminstyle editor
   (download the plug-in from the WIKINDX Sourceforge page) prior to
   their use as this will add additions and amendments to those styles
   that are required in v4.x and up.
   
   b/ Language localizations from v3.8.x will still work in WIKINDX v5 
   but will be missing some messages.  Any missing messages are automatically 
   replaced by English messages.  v3.8.x localization files can be 
   upgraded to v5 by running them through the localization plug-in that can 
   be downloaded from the WIKINDX Sourceforge site.  v4.x language packs 
   will work with v5 but will be missing messages that will instead be displayed 
   in English.

   c/ Administrators who have designed their own CSS/templates will     
   need to do some editing. The templating subsystem has greatly        
   changed in order to give template designers a lot more control over  
   the visual design of WIKINDX. See wikindx5/templates/default/ for    
   examples.    
   
	d/ If you want an embedded spell checker, you need to enable enchant extension
	in your php.ini or .htaccess file. See your webserver manual.                                                        

>> UPGRADING

---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---:::---


-- 
The WIKINDX Team 2019
sirfragalot@users.sourceforge.net
