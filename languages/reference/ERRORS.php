<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
* ERRORS_ref class (English)
* NOTE TO TRANSLATORS:  1/  '###' appearing anywhere in an array value will be replaced by text supplied by the core WIKINDX code.
*				Do not remove it and do not put space around it.
*			2/  Do not change the key (the first part) of an array element.
*			3/  Ensure that each array element value is enclosed in double quotes "..." and is followed by a comma "...",
*			4/  Both the class name and the constructor name should be changed to match the (case-sensitive) name of
*				the folder your language files are in.  For example, if you are supplying a Klingon translation and
*				your languages/ folder is languages/kn/, the class and constructor name for the file SUCCESS.php
*				must both be SUCCESS_kn.
*
* NOTE TO DEVELOPERS:  1/ Any comments not preceeded by '///' at the start of the line will be ignored by the localization module.
*				2/  All comments should be on one line (no line breaks) and must start at the beginning of the line for the localization module to work.
*				3/  Comments must be of the form '/// dbError_ This array does this' where 'dbError' is the array that the comment refers to or may be of the form
*					'/// dbError_open This is the comment' where 'dbError_open' is the exact array and array key that the comment refers to.
*
*****/
class ERRORS_ref
{
// English errors
	function loadArrays()
	{
		return array(
// START__LOCALIZATION__MODULE__EDIT
/// dbError_ General database errors
			"dbError" => array(
				"open"		=>	"Unable to open ### database.",
				"write"		=>	"Unable to write to database.",
				"read"		=>	"Unable to read database.",
				"config"	=>	"Missing Configuration Parameter in config.php.",
				"fileOpen"	=>	"Unable to open update/createMySQL.txt.",
				"updateMySQL"	=>	"Unable to update database with update/updateMySQL.txt.",
				"dbSpecified"	=>	"No RDBMS type selected in config.php.",
				"subQuery"	=>	"Missing subQuery statement.",
			),
/// sessionError_ PHP Session errors
			"sessionError" => array(
				"write"		=>	"Unable to write to session.",
			),
/// inputError_ General user input errors
			"inputError" => array(
				"nan"		=>	"Input is not a number ###",
				"missing"	=>	"Missing input ###",
				"invalid"	=>	"Invalid input ###",
				"userExists"	=>	"That username already exists",
				"groupExists" => "That group already exists",
				"bibExists" => "That bibliography already exists",
				"mail"		=>	"Unable to use email - please contact the WIKINDX administrator (ERROR: ###)",
/// inputError_noHashKey User registration - the hashKey supplied by email to a user has been deleted from the database as they did not confirm their registration within 10 days.
				"noHashKey"	=>	"That key no longer exists. Please re-register",
				"styleExists"	=>	"That style already exists",
/// inputError_duplicateCustomMap When exporting bibliographies to bibtex or endnote and custom fields are mapped, duplicate field names are not allowed.
				"duplicateCustomMap" => "Duplicate export field names",
/// inputError_incorrect Response given to an incorrect answer in the forgotten password system
				"incorrect"	=>	"A question has been answered incorrectly.  Please try again.",
/// inputError_duplicateFieldNames When exporting metadata to bibtex, the user has entered a 'unique' field name that already exists
				"duplicateFieldNames" =>	"Duplicate field names",
/// inputError_chapterNotNumeric book_chapter resource type:  chapter must be a number
				"chapterNotNumeric"	=>	"Chapter must be numeric",
				"userHasNoGroups" => "You do not own any user groups.  You must first create a user group before you can create user group bibliographies",
				"mail2"	=>	"Unable to complete the operation due to a mail server error (ERROR: ###)",
/// inputError_passwordMismatch typed passwords do not match
				"passwordMismatch" => "Passwords do not match",
				"userTagExists" => "That user tag already exists",
/// inputError_captcha CAPTCHA words entered incorrect
				"captcha"	=>	"Answer incorrect",
/// inputError_maxInputVars Too many input vars for PHP to handle
				"maxInputVars"	=>	"Too many input variables selected for PHP to handle (input > ###). You can adjust this increasing max_input_vars in php.ini.",
			),
/// file_ File operations (import/export)
			"file"	=> array(
				'write'			=>	"Unable to write to file ###",
				'noSql'			=>	"You must first list or select resources",
				"read"			=>	"Unable to read directory or file ###",
				"empty"			=>	"You have not yet exported any files",
				"upload"		=>	"File upload error",
				"folder"		=>	"Unable to create directory",
				"fieldMap"		=>	"You may not map more than one unknown field to the same WIKINDX field",
/// file_fileSize Don't translate 'post_max_size', 'upload_max_filesize' and 'php.ini'
				"fileSize"		=>	"Attachment exceeds maximum post_max_size or upload_max_filesize in php.ini (> ### bytes)",
				"attachmentExists"	=>	"Attachment already exists for this resource",
				"missing"	=>	"File not found",
				"imageExists"	=>	"Image already exists as ###",
				"uploadType"	=>	"File type not allowed (only JPEG, GIF and PNG)",
				"imageSize"	=>	"Image above maximum allowed size of ###MB",
			),
/// done_ Following input, editing operations etc., these messages are printed when a user attempts to redo the operation (usually by clicking on browser's 'reload' button)
			"done" => array(
				"resource" =>	"You have already created/edited/deleted that resource",
				"note" =>	"You have already created/edited/deleted that note",
				"quote" =>	"You have already created/edited/deleted that quote",
				"paraphrase" =>	"You have already created/edited/deleted that paraphrase",
				"musing" =>	"You have already created/edited/deleted that musing",
				"idea" =>	"You have already created/edited/deleted that idea",
				"abstract" =>	"You have already created/edited/deleted that abstract",
				"creator" =>	"You have already created/edited/deleted that creator",
				"publisher" =>	"You have already created/edited/deleted that publisher",
				"collection" =>	"You have already created/edited/deleted that collection",
				"fileImport" =>	"You have already imported that file",
				"keyword"	=>	"You have already edited that keyword",
				"keywordDelete"	=>	"You have already deleted that keyword",
				"register"	=>	"You have already registered",
				"bibliography" =>	"You have already created/edited/deleted that bibliography",
				"custom" =>	"You have already edited/deleted that custom field",
				"attachAdd"	=>	"You have already attached that file",
				"attachDelete"	=>	"You have already deleted those files",
				"group" =>	"You have already created/edited/deleted that user group",
				"urlAdd"	=>	"You have already added that URL",
				"userTag" =>	"You have already created/edited/deleted that user tag",
				"news" =>	"You have already created/edited/deleted that news item",
			),
/// warning_ Warning type messages
			"warning" => array(
/// warning_resourceExists When inputting a new resource
				"resourceExists" => "At least one resource of that type with that title already exists",
/// warning_superadminOnly WIKINDX not in multi user mode - this is displayed at the logon screen
				"superadminOnly" => "Multi user has not been enabled - only the superadmin may log on",
/// warning_creatorExists When editing a creator, the new one may be the same as an existing one.
				"creatorExists"	=>	"A creator of that name already exists in the database",
/// warning_publisherExists When editing a publisher, the new one may be the same as an existing one.
				"publisherExists"	=>	"A publisher of that name and location already exists in the database",
/// warning_collectionExists When editing a collection, the new one may be the same as an existing one.
				"collectionExists"	=>	"A collection with those details already exists in the database",
/// warning_keywordExists When editing a keyword, the new one may be the same as an existing one.
				"keywordExists"	=>	"A keyword of that name already exists in the database",
/// warning_forget1 For the forgotten password system: '###' is the admin's email address which may or may not be available.  If it is, the '###' may be something like '(me@blah.com).' otherwise it will be '.'.
				"forget1"	=>	"Unfortunately you have not stored any question/answer pairs or you have entered your details incorrectly so WIKINDX is unable to help you.  Please contact the WIKINDX administrator ###",
				"forget2"	=>	"There is more than one user with that email address so WIKINDX is unable to help you. Please contact the WIKINDX administrator for further help or enter your username.",
/// warning_forget3 Don't translate 'SMTP'.  In this case, '###' is the admin's email address which may or may not be available.  If it is, the '###' may be something like '(me@blah.com).' otherwise it will be '.'.
				"forget3"	=>	"WIKINDX is unable to email you because the SMTP server does not appear to be available.  Please contact the WIKINDX administrator ###",
				"quarantine"	=>	"Resource has been quarantined until an ADMIN approves it",
				"pluginConflict"	=>	"Two or more inline plugins have been assigned the same container: ###",
				"pluginVersion" => "Some plugins are not compatible with this version of WIKINDX, and so they will not be visible to users, because their \$wikindxVersion in the plugin's config.php is not equal to WIKINDX_PLUGIN_VERSION which is currently ###. Update these plugins in order to use them.",
				"ideaDelete" => "If you delete the main idea, the entire thread will be deleted",
			),
/// import bibliography error messages
			"import" => array(
				"empty" => "No valid entries found in the file",
			),
// END__LOCALIZATION__MODULE__EDIT
		);
	}
}
?>