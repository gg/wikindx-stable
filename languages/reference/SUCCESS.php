<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
* SUCCESS_ref class (English)
* NOTE TO TRANSLATORS:  1/  '###' appearing anywhere in an array value will be replaced by text supplied by the core WIKINDX code.
*			2/  Do not change the key (the first part) of an array element.
*			3/  Ensure that each array element value is enclosed in double quotes "..." and is followed by a comma "...",
*			4/  Both the class name and the constructor name should be changed to match the (case-sensitive) name of
*				the folder your language files are in.  For example, if you are supplying a Klingon translation and
*				your languages/ folder is languages/kn/, the class and constructor name for the file SUCCESS.php
*				must both be SUCCESS_kn.
*
* NOTE TO DEVELOPERS:  1/ Any comments not preceeded by '///' at the start of the line will be ignored by the localization module.
*				2/  All comments should be on one line (no line breaks) and must start at the beginning of the line for the localization module to work.
*				3/  Comments must be of the form '/// notes_ This array does this' where 'notes' is the array key that the comment refers to.
*****/
class SUCCESS_ref
{
// Constructor
	function __construct()
	{
	}
// English success messages
	function loadArrays()
	{
		return array(
// START__LOCALIZATION__MODULE__EDIT
				"noteAdd"		=>	"Successfully added notes",
				"noteEdit"		=>	"Successfully edited notes",
				"noteDelete"		=>	"Successfully deleted notes",
				"categoryKeywordAdd"		=>	"Successfully added categories and keywords",
				"categoryKeywordEdit"		=>	"Successfully edited categories and keywords",
				"categoryKeywordDelete"		=>	"Successfully deleted categories and keywords",
				"quoteAdd"		=>	"Successfully added quote",
				"quoteEdit"		=>	"Successfully edited quote",
				"quoteDelete"		=>	"Successfully deleted quote",
				"paraphraseAdd"		=>	"Successfully added paraphrase",
				"paraphraseEdit"		=>	"Successfully edited paraphrase",
				"paraphraseDelete"		=>	"Successfully deleted paraphrase",
				"musingAdd"		=>	"Successfully added musing",
				"musingEdit"		=>	"Successfully edited musing",
				"musingDelete"		=>	"Successfully deleted musing",
				"ideaAdd"		=>	"Successfully added idea",
				"ideaEdit"		=>	"Successfully edited idea",
				"ideaDelete"		=>	"Successfully deleted idea",
				"abstractAdd"		=>	"Successfully added abstract",
				"abstractEdit"		=>	"Successfully edited abstract",
				"abstractDelete"		=>	"Successfully deleted abstract",
				"config"	=>	"Successfully configured WIKINDX",
				"resourceAdd"		=>	"Successfully added resource(s)",
				"resourceEdit"		=>	"Successfully edited resource(s)",
				"resourceDelete"		=>	"Successfully deleted resource(s)",
				"categoryAdd"		=>	"Successfully added category(s)",
				"categoryEdit"		=>	"Successfully edited category(s)",
				"categoryDelete"		=>	"Successfully deleted category(s)",
				"languageAdd"		=>	"Successfully added language",
				"languageEdit"		=>	"Successfully edited language",
				"languageDelete"		=>	"Successfully deleted language",
				"subcategoryAdd"		=>	"Successfully added subcategory(s)",
				"subcategoryEdit"		=>	"Successfully edited subcategory(s)",
				"subcategoryDelete"		=>	"Successfully deleted subcategory(s)",
				"creator"	=>	"Successfully edited creator",
				"publisher"	=>	"Successfully edited publisher",
				"collection"	=>	"Successfully edited collection",
				"bibtexImport"	=>	"Successfully uploaded BibTeX bibliography",
				"endnoteImport"	=>	"Successfully uploaded Endnote XML bibliography",
				"keyword"	=>	"Successfully edited keyword",
				"keywordDelete"	=>	"Successfully deleted keyword(s)",
				"userAdd"		=>	"Successfully added user(s)",
				"userEdit"		=>	"Successfully edited user(s)",
				"userDelete"		=>	"Successfully deleted user(s)",
				"registerEmail"	=>	"An email has been sent to you with further instructions",
				"bibliographySet"		=>	"Successfully set bibliography",
				"bibliographyAdd"		=>	"Successfully added bibliography",
				"bibliographyEdit"		=>	"Successfully edited bibliography",
				"bibliographyDelete"		=>	"Successfully deleted bibliography",
				"usertagAdd"		=>	"Successfully added user tag",
				"usertagEdit"		=>	"Successfully edited user tag",
				"usertagDelete"		=>	"Successfully deleted user tag",
				"addBib"	=>	"Successfully added item to bibliography",
				"deleteFromBib"	=>	"Successfully deleted item(s) from bibliography",
				"notify"	=>	"Successfully set email notification",
				"newsAdd"		=>	"Successfully added news",
				"newsEdit"		=>	"Successfully edited news",
				"newsDelete"		=>	"Successfully deleted news",
				"fieldAdd"		=>	"Successfully added custom database field",
				"fieldEdit"		=>	"Successfully edited custom database field",
				"fieldDelete"		=>	"Successfully deleted custom database field",
				"attachAdd"		=>	"Successfully added attachment",
				"attachEdit"		=>	"Successfully edited attachment",
				"attachDelete"		=>	"Successfully deleted attachment",
				"urlAdd"		=>	"Successfully added URL",
				"urlEdit"		=>	"Successfully edited URL",
				"urlDelete"		=>	"Successfully deleted URL",
				"organized"	=>	"Successfully organized resources",
				"convertType"	=>	"Successfully converted resource type",
				"bookmark"	=>	"Successfully added bookmark",
				"bookmarkDelete"	=>	"Successfully deleted bookmark(s)",
				"keywordMerge"	=>	"Successfully merged keywords",
				"basketAdd"	=>	"Successfully added resource to basket",
				"basketRemove"	=>	"Successfully removed resource from basket",
				"basketDelete"	=>	"Successfully deleted basket",
				"imageDelete"	=>	"Successfully deleted images",
				"forgetUpdate"	=>	"Successfully updated the forgotten password system",
				"maturityIndex"	=>	"Successfully set the maturity index",
				"groupAdd"		=>	"Successfully added group",
				"groupEdit"		=>	"Successfully edited group",
				"groupDelete"		=>	"Successfully deleted group",
/// emailFriend_ User has emailed a friend a link to a resource
				"emailFriend" => "Successfully sent email",
				"creatorMerge"	=>	"Successfully merged creators",
				"creatorGroup"	=>	"Successfully grouped creators",
				"creatorUngroup"	=>	"Successfully removed creators from group",
				"registerRequest" => "Your registration request has been emailed to the WIKINDX administrator and you should receive an emailed response soon",
				"registerRequestManage"	=>	"Successfully managed registration requests.",
/// quarantineApprove_ ADMIN has approved a quarantined resource
				"quarantineApprove" => "Successfully approved resource",
				"quarantined"	=>	"Resource has been quarantined from public view until approved by an administrator",
				"plugins"	=>	"Successfully configured plugins",
				"upgradeDB"	=>	"Successfully upgraded WIKINDX database",
// END__LOCALIZATION__MODULE__EDIT
		);
	}
}
?>