<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
* CONSTANTS_fr class (Français)
*
* Various numbers, days, months etc.
*
*	NOTE TO TRANSLATORS:	Only translate between START TRANSLATION and END TRANSLATION tags.
*	NOTE FROM THE FRENCH TRANSLATOR :	This file has not been translated due to incompatibility
*	beetwen english end french resources referred in the bibliography. ENGLISH FORMATTING HAS BEEN PRESERVED.
*
*****/
class CONSTANTS_fr
{
// Constructor
	function __construct()
	{
// When using the word processor how are extra, unnamed authors indicated?  In English this would be 'et al.' as in:
// 'According to Grimshaw et al., "blah blah blah".
		$this->textEtAl = 'et al.';
// When using the word processor, how is singular possessive defined.  This is only for a single creator's surname and, like 'at al.' above, is used
// to detect if a creator name should be removed from the in-text citation if that name is in the same sentence as the citation.
// If this value contains an apostrophe (as in the English Grimshaw's), then it should be in double quotes (or else the PHP script will fail).
// English has two forms (the second below for names that end in 's').
// If there is no equivalent possessive form in another language, set these to FALSE.
		$this->possessive1 = "'s";
		$this->possessive2 = "'";
// What characters indicate the start and end of a quotation in the word processor text?  (Be careful to escape a double quote if it is inside
// double quotes by using '\'.  A single quote can be given safely as "'".)
		$this->startQuotation = "�";
		$this->endQuotation = "�";
// When using the word processor, for certain styles the text must be split up into sentences -- e.g. APA requires that year must follow the author name and
// page no. follows the citation if the citation appears in the same sentence as the author surname.  Normally sentences are recognized
// by '. ' (dot followed by at least one space).  This array handles other groups of characters (commonly abbreviations) that may appear in a sentence
// but do not indicate a sentence divider.  If there are no such abbreviations, the array must exist and be empty e.g. $this->abbreviations = array();
// The final '.' (dot) of the abbreviation must NOT be given.
// Matching is case insensitive.
// 'et al.' and equivalent is dealt with above.
// '...' and multiple dots consisting of two or more dots are dealt with in the code.
// Abreviations such as U.S.A., U.S.S.R. are dealt with in the code (must be capital English characters)
// The syntax of the matching rule is that a sentence divider has been found if there is a dot followed by a space where the dot is not preceeded a member of the array.
// For example, the 'e.g.' in "Blah blah blah, e.g. more blah, and even more blah" is not seen as a sentence divider if 'e.g' (no final dot) is a member of this array.
		$this->abbreviations = array(
				'e.g',
				'par ex',
				'etc', // etcetera
				'op. cit', //opus citatum
				'ibid', // ibidem
				'ad loc', //ad locum
				'i.e',
				'Bros', // short for Brothers (as in Warner Bros.)
				'no', // number
				'c', // circa
				'ca', // circa
				'cf', // compare with
				'secs', // seconds
				'msecs', // milliseconds
				'Ph. D', // doctorate
				'MS', // Manuscript
				'viz',
				'v', // voir
				'vs', //versus
				'N.B', // Nota bene
				'Fig',	// Figure
				'fig', // figure
				'co', // company
				'Cie', //company
			);
	}
// Convert cardinal to ordinal numbers.
// The coding here is likely to be different for each language.  If there is no logic that can be programmed for your language, then it is recommended that
// you use English ordinals as given here so that something is printed and the script does not fail.
// $modulo holds the remainder of $cardinal/100 or $cardinal/10.
// Optional $field allows conditions based on gender.  Currently, $field will be either 'edition' or 'dayMonth' (day of the month).  Additionally, $field can be used
// to determine whether ordinals will be used at all.  For example, if this is French and $field == 'dayMonth', ordinals are used solely for '1er'.
// (languages/fr/CONSTANTS.php has an example using $field.)
//  The French equivalent to this function would be:
/*
	function cardinalToOrdinal($cardinal, $field = FALSE)
	{
		if(($field == 'dayMonth') && ($cardinal != 1))
			return $cardinal; // no change
		$modulo = $cardinal % 10;
		if($modulo == 1) // 1st
		{
			if($field == 'edition')
				return $cardinal . 'ère';
			return $cardinal . 'er';
		}
		else // all other numbers
			return $cardinal . 'ème';
	}
*/
	function cardinalToOrdinal($cardinal, $field = FALSE)
	{
		if(($field == 'dayMonth') && ($cardinal != 1))
			return $cardinal; // no change
		if($cardinal == 1) // 1st
		{
			if($field == 'edition')
				return $cardinal . 'ère';
			return $cardinal . 'er';
		}
		else // all other numbers
			return $cardinal . 'ème';
	}
// convert cardinal numbers
	function convertNumbers($number = FALSE, $convert = FALSE)
	{
// month number to long name
		$longMonth = $this->monthToLongName();
// short month
		$shortMonth = $this->monthToShortName();
// cardinal -> ordinal word
		$ordinalWord = $this->cardinalToOrdinalWord();
// arabic -> roman numerals
		$roman = $this->cardinalToRoman();
/****************************
 START TRANSLATION
****************************/
// arabic -> ordinal (e.g. 3 -> 3rd, 10 -> 10th)
// Usually used for edition numbers in bibliographic styles that require e.g. '10th edition' rather than 'edition 10'.
// A maximum of 50 seems a reasonable number to go up to....
// If necessary, you may need to add more loops or individual array elements.
		for($i = 4; $i <= 20; $i++)
			$ordinal[$i] = $i . 'th';
		for($i = 24; $i <= 30; $i++)
			$ordinal[$i] = $i . 'th';
		for($i = 34; $i <= 40; $i++)
			$ordinal[$i] = $i . 'th';
		for($i = 44; $i <= 50; $i++)
			$ordinal[$i] = $i . 'th';
		$ordinal[1] = '1er';
		$ordinal[21] = '21eme';
		$ordinal[31] = '31eme';
		$ordinal[41] = '41eme';
		$ordinal[2] = '2eme';
		$ordinal[22] = '22eme';
		$ordinal[32] = '32eme';
		$ordinal[42] = '42eme';
		$ordinal[3] = '3eme';
		$ordinal[23] = '23eme';
		$ordinal[33] = '33eme';
		$ordinal[43] = '43eme';
/****************************
 END TRANSLATION
****************************/
// arabic -> cardinal (i.e. no change)
		$cardinal = range(0, 50);
// !$number, we are simply loading the arrays for use elsewhere, so return
		if(!$number)
			return;
// get rid of leading '0' if necessary
		if($number < 10)
			$number += 0;
// If that number is not actually in array, we return the number as is
		if(array_key_exists($number, ${$convert}))
			return ${$convert}[$number];
		return $number;
	}
// Convert month to long name
	function monthToLongName()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
				1	=>	'Janvier',
				2	=>	'Février',
				3	=>	'Mars',
				4	=>	'Avril',
				5	=>	'Mai',
				6	=>	'Juin',
				7	=>	'Juillet',
				8	=>	'Août',
				9	=>	'Septembre',
				10	=>	'Octobre',
				11	=>	'Novembre',
				12	=>	'Décembre',
/****************************
 END TRANSLATION
****************************/
			);
	}
// Convert month to short name
	function monthToShortName()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
				1	=>	'janv.',
				2	=>	'févr',
				3	=>	'mars',
				4	=>	'avr.',
				5	=>	'mai',
				6	=>	'juin',
				7	=>	'juil.',
				8	=>	'août',
				9	=>	'sept.',
				10	=>	'oct.',
				11	=>	'nov.',
				12	=>	'déc.',
/****************************
 END TRANSLATION
****************************/
			);
	}
// convert cardinal to roman
	function cardinalToRoman()
	{
// arabic -> roman numerals
// Do NOT translate
		return array(
				1	=>	'I',
				2	=>	'II',
				3	=>	'III',
				4	=>	'IV',
				5	=>	'V',
				6	=>	'VI',
				7	=>	'VII',
				8	=>	'VIII',
				9	=>	'IX',
				10	=>	'X',
				11	=>	'XI',
				12	=>	'XII',
				13	=>	'XIII',
				14	=>	'XIV',
				15	=>	'XV',
				16	=>	'XVI',
				17	=>	'XVII',
				18	=>	'XVIII',
				19	=>	'XIX',
				20	=>	'XX',
				21	=>	'XXI',
				22	=>	'XXII',
				23	=>	'XXIII',
				24	=>	'XXIV',
				25	=>	'XXV',
				26	=>	'XXVI',
				27	=>	'XXVII',
				28	=>	'XXVIII',
				29	=>	'XXIX',
				30	=>	'XXX',
				31	=>	'XXXI',
				32	=>	'XXXII',
				33	=>	'XXXIII',
				34	=>	'XXXIV',
				35	=>	'XXXV',
				36	=>	'XXXVI',
				37	=>	'XXXVII',
				38	=>	'XXXVIII',
				39	=>	'XXXIX',
				40	=>	'XXXX',
				41	=>	'XXXXI',
				42	=>	'XXXXII',
				43	=>	'XXXXIII',
				44	=>	'XXXXIV',
				45	=>	'XXXXV',
				46	=>	'XXXXVI',
				47	=>	'XXXXVII',
				48	=>	'XXXXVIII',
				49	=>	'XXXXIX',
				50	=>	'L',
			);
	}
// convert ordinal to word
	function cardinalToOrdinalWord()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
// Usually used for edition numbers in bibliographic styles that require words rather than arabic numerals.
// Any numbers not listed here will be returned without change.  e.g. here, 51 will be returned as 51 rather than 'fifty-first'.
// A maximum of 50 seems a reasonable number to go up to....
						'1'	=>	'Premier',
						'2'	=>	'Deuxième',
						'3'	=>	'Troisième',
						'4'	=>	'Quatrième',
						'5'	=>	'Cinquième',
						'6'	=>	'Sixième',
						'7'	=>	'Septième',
						'8'	=>	'Huitième',
						'9'	=>	'Neuvième',
						'10'	=>	'Dixième',
						'11'	=>	'Onzième',
						'12'	=>	'Douzième',
						'13'	=>	'Treizième',
						'14'	=>	'Quatorzième',
						'15'	=>	'Quinzième',
						'16'	=>	'Seizième',
						'17'	=>	'Dix-septième',
						'18'	=>	'Dix-huitième',
						'19'	=>	'Dix-neuvième',
						'20'	=>	'Vingtième',
						'21'	=>	'Vingt-et-unième',
						'22'	=>	'Vingt-deuxième',
						'23'	=>	'Vingt-troisième',
						'24'	=>	'Vingt-quatrième',
						'25'	=>	'Vingt-cinquième',
						'26'	=>	'Vingt-sixième',
						'27'	=>	'Vingt-septième',
						'28'	=>	'Vingt-huitième',
						'29'	=>	'Vingt-neuvième',
						'30'	=>	'Trentième',
						'31'	=>	'Trente-et-unième',
						'32'	=>	'Trente-deuxième',
						'33'	=>	'Trente-troisième',
						'34'	=>	'Trente-quatrième',
						'35'	=>	'Trente-cinquième',
						'36'	=>	'Trente-sixième',
						'37'	=>	'Trente-septième',
						'38'	=>	'Trente-huitième',
						'39'	=>	'Trente-neuvième',
						'40'	=>	'Quarantième',
						'41'	=>	'Quarante-et-unième',
						'42'	=>	'Quarante-deuxième',
						'43'	=>	'Quarante-troisième',
						'44'	=>	'Quarante-quatrième',
						'45'	=>	'Quarante-cinquième',
						'46'	=>	'Quarante-sixième',
						'47'	=>	'Quarante-septième',
						'48'	=>	'Quarante-huitième',
						'49'	=>	'Quarante-neuvième',
						'50'	=>	'Cinquantième',
/****************************
 END TRANSLATION
****************************/
			);
	}
/**
* Format dates and times for localization (e.g. when viewing lists of resources in a multi-user wikindx 'Last edited by: xxx <date/time>)
*
* $timestamp comes in from the database in the format 'YYYY-MM-DD HH:MM:SS' e.g. 2013-01-31 15:54:55
*/
	public function dateFormat($timestamp)
	{
 		setlocale(LC_ALL, 'fr_FR.utf8');
// If you're happy with $timestamp's current format, all you need to do is uncomment this line:
//		return $timestamp;
// Otherwise. . .
		$unixTime = strtotime($timestamp);
// You have two choices for setting the time.  The first attempts to translate day and month names but will not
// work if the chosen locale is not available on your system.
// See
// http://www.php.net/manual/en/function.setlocale.php
// and
// http://www.php.net/manual/en/function.strftime.php
//
// The second is locale independent
// See:
// http://www.php.net/manual/en/function.date.php
//
// For the first option, set $dateFormat = 1; otherwise set $dateFormat = 2;
/****************************
 START TRANSLATION
****************************/
		$dateFormat = 2;
		if($dateFormat == 1)
		{
			$format = '%c'; // e.g. Tue Feb 5 00:45:10 2009
			setlocale(LC_ALL, "fr_FR.utf8"); // French locale
			$timestamp = strftime($format, $unixTime);
		}
		else if($dateFormat == 2)
		{
			$config = FACTORY_CONFIG::getInstance();
			if(!isset($config->WIKINDX_TIMEZONE) || !$config->WIKINDX_TIMEZONE)
				date_default_timezone_set(date_default_timezone_get()); // default is UTC
			else
				date_default_timezone_set($config->WIKINDX_TIMEZONE);
			$format = "d M Y H:i:s e";
			$timestamp = date($format, $unixTime);
		}
		else
			$timestamp = '';
/****************************
 END TRANSLATION
****************************/
		return $timestamp;
	}
}
?>