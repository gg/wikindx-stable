<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
* HELP class
*
* NOTE TO TRANSLATORS:  1/  Both the class name and the constructor name should be changed to match the (case-sensitive) name of
*				the folder your language files are in.  For example, if you are supplying a Klingon translation and
*				your languages/ folder is languages/kn/, the class and constructor name for the file SUCCESS.php
*				must both be SUCCESS_kn.
*			2/  Do not change the key (the first part) of an array element.
*			3/  Ensure that each array element value is enclosed in double quotes "..." and is followed by a comma "...",
*			4/  The class name should be changed to match the (case-sensitive) name of
*				the folder your language files are in.  For example, if you are supplying a Klingon translation and
*				your languages/ folder is languages/kn/, the class name for the file SUCCESS.php
*				must both be SUCCESS_kn.
*
* NOTE TO DEVELOPERS:  1/ Any comments not preceeded by '///' at the start of the line will be ignored by the localization module.
*				2/  All comments should be on one line (no line breaks) and must start at the beginning of the line for the localization module to work.
*				3/  Comments must be of the form '/// dbError_ This array does this' where 'dbError' is the array that the comment refers to
*				or may be of the form '/// dbError_open This is the comment'
*				where 'dbError_open' is the exact array and array key that the comment refers to.
*****/
// START__LOCALIZATION__CLASSNAME__EDIT
class HELP_fr
// START__LOCALIZATION__CLASSNAME__EDIT
{
private $config;
public $translate = FALSE;
public $publicsearch;
public $publicpasteBibtex;
public $publictinyMCELink;
public $publicphpGDLink;
public $publicwikindxLink;

// Constructor
	public function __construct()
	{
		$this->config = FACTORY_CONFIG::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
	}
	public function loadArrays()
	{
		if($this->translate == TRUE) // translate = TRUE for localization module
		{
			if(!isset($this->config->WIKINDX_SEARCHFILTER)) // i.e. at first install of a blank database
				$this->publicsearch = $search = 'an, a, the, and, to';
			else
				$this->publicsearch = $search = join(', ', $this->config->WIKINDX_SEARCHFILTER);
			$this->publicpasteBibtex = $pasteBibtex = $this->session->getVar('setup_MaxPaste');
			$this->publictinyMCELink = $tinyMCELink = '"http://www.tinymce.com/wiki.php/Browser_compatiblity" target="_new"';
			$this->publicphpGDLink = $phpGDLink = "https://secure.php.net/manual/fr/book.image.php";
			$this->publicwikindxLink = $wikindxLink = "https://sourceforge.net/projects/wikindx/";
		}
		else
		{
			$search = '#search#';
			$pasteBibtex = '#pasteBibtex#';
			$tinyMCELink = '#tinyMCELink#';
			$phpGDLink = '#phpGDLink#';
			$wikindxLink = '#wikindxLink#';
		}
		return array(
// START__LOCALIZATION__MODULE__EDIT
				"search"	=>	"<h3>Recherche</h3>

<p>Lorsque vous recherchez des références ou des métadonnées, vous pouvez limiter par catégorie, mots-clés (et ainsi de suite) les résultats et sélectionner le champ de la base de données dans lequel chercher. Dans votre requête, vous pouvez utiliser les opérateurs booléens <strong>AND</strong>, <strong>OR</strong> et <strong>NOT</strong> et grouper les mots par expressions exactes en utilisant les guillemets doubles : <strong>\"</strong><em>terme(s) de recherche(s)</em><strong>\"</strong>.</p>

<p>Les sélections multiples sont possibles, lorsqu'indiqué, en cliquant et maintenant simultanément les touches \"Control\" ou \"Shift\" (sur Windows et Linux) ou les touches \"Command\" et \"Shift\" (sur Apple). Pour sélectionner certaines options de recherche, vous devez les sélectionner puis les transférer dans la boîte adjacente en cliquant sur les flèches.</p>

<p>Certaines boîtes de sélection permettent d'appliquer les opérateurs \"OR\" et \"AND\". Par exemple, en sélectionnant deux mots-clés ou plus avec l'option \"OR\" cochée, les résultats de la recherche contiendront au minimum l'un ou l'autre de ces mots-clés. En sélectionnant deux mots-clés ou plus avec l'option \"AND\" cochée, les résultats de la recherche contiendront tous ces mots-clés.</p>

<p>Les règles suivantes s'appliquent pour la formulation de la requête de recherche :</p>

<ul>
	<li><strong>AND</strong>, <strong>OR</strong> et <strong>NOT</strong> sont sensibles à la casse et fonctionnent comme mots de commande seulement à l'extérieur d'expressions exactes (avec les guillemets)</li>
	<li>Les requêtes sont sensibles à la casse</li>
	<li>La recherche  par défaut se fait par mots partiels à moins que le terme de recherche soit une expression exacte</li>
	<li>Une espace qui ne fait pas partie d'une expression exacte est traitée comme un opérateur <strong>OR</strong></li>
	<li>Toutes les ponctuations non contenues dans une expression exacte est ignorée</li>
	<li>Les opérateurs <strong>OR</strong> qui suivent <strong>AND</strong> ou <strong>NOT</strong> seront regroupés. Par ex. la requête \"mot1 AND mot2 OR mot3 OR mot4 NOT mot5 OR mot6\" sera traitée \"mot1 AND (mot2 OR mot3 OR mot4) NOT (mot5 OR mot6)\"</li>

</ul>",

				"resource"	=>	"<h3>Références</h3>

<p>Tout utilisateur peut:</p>
<ul>
	<li>Stocker des références dans un panier qui fonctionne comme une collection temporaire de références que vous pouvez accéder à n'importe quel moment via le menu «Références».</li>
	<li>Marquer une référence avec un signet qui peut ensuite être consulté via le menu «Références». Les signets sont sauvegardés d'une session à l'autre pour les utilisateurs enregsitrés.</li>
</ul>

<p>Les utilisateurs enregistrés peuvent:</p>
<ul>
	<li>Éditer des éléments dans cette page. Certaines options, telles que l'édition et l'ajout de pièces jointes, doivent être approuvées par l'administrateur.</li>
	<li>Ajouter des éléments tels que des citations, des paraphrases et des idées. Les commentaires (sur les citations et paraphrases) et les idées peuvent être marqués comme étant privés, publics ou visibles par n'importe quel groupe d'utilisateur défini dans l'onglet «Mon Wikindx».</li>
</ul>

<p>Si un élément tel qu'un mot-clé ou une catégorie est hyperlié, cliquer sur le lien va faire afficher d'autres ressources en lien avec ce mot-clés ou cette catégorie. Toutes citations, paraphrases et idées peuvent optionnellement avoir des mots-clés hyperliés qui mènent à d'autres métadonnées.</p>

<p>Dépendamment des permissions, vous pourrez voir diverses statistiques sur cette page :</p>

<ul>
	<li>Consultations : Nombre de consultations ce mois-ci / total des visionnements.</li>
	<li>Indice de consultation : Une indication du nombre de fois que cette référence a été vue en comparaison des autres. Plus le pourcentage est élevé, plus est élevé le poids relatif de visionnement.</li>
	<li>Téléchargements : Une référence peut avoir un nombre illimité de pièces jointes et chacune affiche y le nombre de téléchargements : ce mois-ci / total.</li>
	<li>Indice téléchargement : Idem que l'index des visionnements, mais pour les téléchargements.</li>
	<li>Indice de popularité : Combinaison des indices de visionnement et de téléchargement indiquant la popularité de la référence.</li>
	<li>Maturité : Un chiffre de 1 à 10 assigné par l'administrateur qui indique la maturité «subjective» de la référence.</li>
</ul>",

				"collection"	=>	"<h3>Modification des collections</h3>

<p>Ici, vous pouvez modifié globalement les valeurs par défaut de toutes les collections :</p>

<ul>
	<li>Les nouvelles collections sont automatiquement créées lorsque est ajoutée ou modifiée une référence qui appartient à une collection précédemment inexistante.</li>
	<li>Les valeurs par défaut sont les collections affichées lors de l'ajout et la modification d'une nouvelle référence qui appartient à une collection comme une anthologie ou un journal. Lors de l'ajout ou de la modification d'une référence, les valeurs par défaut peuvent être remplacées pour cette référence, mais les valeurs par défaut de la collection peuvent uniquement être modifiées ici.</li>
	<li>Une valeur dans un champ 'Nom de l'éditeur' remplacera toute sélection dans la zone de sélection 'Éditeur'.</li>
	<li>Une valeur dans un champ créateur 'Nom' remplacera toute sélection dans la boîte de sélection 'Auteur' appropriée.</li>
	<li>La modification des valeurs par défaut d'une collection mettra à jour les informations relatives aux références de cette collection.</li>
</ul>",

				"categoryTree"	=>	"<h3>Catégories</h3>

<p>La page des catégories affiche toutes les catégories utilisées dans le WIKINDX et, sous chaque catégorie, les sous-catégories et mots-clés associés. Les nombre entre crochets montrent le nombre de références dans chaque catégorie, sous-catégorie ou mots-clés.</p>",

				"pasteBibtex"	=>	"<h3>Importer une bibliographie BibTeX</h3>

<p>Si vous avez une bibliographie BibTeX, vous pouvez importer des sélections d'entrées, incluant des valeurs «@string», en copiant et collant les entrées de votre fichier BibTeX dans la zone de texte. Si vous avez des champs non standard dans vos entrée BibTeX, WIKINDX va vous donner la possibilité de les mapper dans les champs WIKINDX : si vous êtes un administrateur, vous aimerez peut-être créer des champs personnalisés avant. Un administrateur peut publier un nombre illimité d'entrées en une seule session. Les autres utilisateurs peuvent en publier <strong>#pasteBibtex#</strong> à la fois.</p>",

				"configure"	=>	"<h3>Configuration WIKINDX</h3>

<p>La plupart des paramètres sont explicites, mais certains requièrent plus d'explication :</p>

<ul>
	<li>Le code \$QUICKSEARCH\$ fait apparaître la boîte de recherche rapide.</li>
	<li>Pour empêcher les utilisateurs enregistrés d'utiliser l'import de référence Bibtex, entrer la valeur 0 dans \"Nombre max. de fichiers BibTex qu'un utilisateur peut coller\". Cette restriction ne s'applique pas aux administrateurs.</li>
	<li>Lorsque WIKINDX créé des fichiers temporaires (comme lors d'export de bibliographies en divers format, par exemple), vous pouvez définir la durée de vie de ces fichiers avec l'option \"Nombre de secondes au-delà duquel le fichier d'export est marqué pour la suppression\". Ceci est calculé à partir du moment où l'utilisateur ce connecte de nouveau. </li>
	<li>Vous pouvez désactiver l'accès en lecture seule (voir \"Empêcher l'accès en lecture seule\"). Si l'accès en lecture seule est activée, l'invite de connexion peut être désactivée (voir \"Si l'accès en lecture seule est permise, ne pas afficher l'invite de connexion\"), ainsi les visiteurs accèdent directement à la page d'accueil. </li>
	<li>Si vous utilisez l'option CAPTCHA pour les requêtes de création de comptes, vous devrez activer l’<a href='https://secure.php.net/manual/fr/book.image.php'>extension GD</a> dans le fichier de configuration php.ini. Ceci est utile contre les pourriels et les bots.</li>
	<li>L'affichage d'erreurs PHP et SQL sert au débogage et ne devrait jamais être utilisé lorsque le site est en ligne. Des interférences avec AJAX dans les pages de recherche et de références sont possibles avec l'affichage SQL.</li>
	<li>Si l'option \"Ne permettre qu'aux utilisateurs enregistrés de voir les fichiers joints\" est activée et si la variable \"WIKINDX_GS_ALLOW\" dans le fichier \"config.php\" est activée avec \"TRUE\", alors Google Scholar ne sera pas autorisé à indexer les fichiers joints aux références(PDF, etc.). Google Scholar pourra toujours cependant indexer le reste du contenu.</li>
</ul>

<p>Certaines des options présentées ici, telles que \"Nombre de références affichées par défaut sur l'écran\" et \"Style bibliographique par défaut\", peuvent être changées et personnalisées par les utilisateurs dans leur menu \"Options d'affichage\".</p>

<p>Vous pouvez ajouter des administrateurs et des utilisateurs à partir du menu \"Administrateur > Utilisateurs... > Ajouter\".</p>

<p>Lorsque vous ajoutez ou modifiez des références, chaque référence peut appartenir à plusieurs catégories ou sous-catégories, avoir des champs personnalisés ou être assignée à une langue. Les administrateurs peuvent ajouter des nouvelles catégories, des sous-catégories, des champs personnalisés et des langues à partir du menu \"Administrateur\".</p>",

				"front"	=>	"<h3>WIKINDX</h3>

<p><strong>Conseils généraux</strong></p>

<ul>
	<li>Votre navigateur doit accepter les mouchards («cookies»).</li>
	<li>Étant donné que WIKINDX fonctionne comme un «programme dans un programme», vous devriez essayer de ne pas utiliser les boutons Précédent et Suivant de votre navigateur, mais plutôt utiliser les fonctions de navigations dans WIKINDX.</li>
	<li>WiKINDX fonctionne par sessions (séances de travail) pour stocker les données. Une session est déterminée par l'instance et le type de navigateur en cours. Vous pouvez connaître des résultats imprévus si vous utilisez WIKINDX avec plus d'une fenêtre ou plus d'un onglet de navigation à la fois. Vous pouvez cependant faire ceci avec des navigateurs différents.</li>
	<li>WIKINDX v4 emploie la version v3.5.5 de TinyMCE pour les fonctionnalités «tel écran - tel écrit» pour l'édition des champs texte. Une liste des navigateurs compatibles se trouve <a href=#tinyMCELink#>ici</a> et inclut Microsoft Internet Explorer, Firefox, Chrome, Safari et Opera.</li>
	<li>Dans WIKINDX, une <em>référence</em> est un ensemble de données qui comprend l'information de la notice bibliographique, les métadonnées, les catégories, les sous-catégories et les mots-clés. Il peut aussi y avoir des informations supplémentaires telles que des notes, des extraits, des pièces jointes et des URLs en plus de données statistiques.</li>
	<li>Si vous êtes un utilisateur enregistré, vous pouvez créer vos propres étiquettes et les assigner a des références.</li>
	<li>En tant qu'utilisateur enregistré, vous pouvez aussi créer vos propres bibliographies à partir de la bibliographie WIKINDX maitresse.</li>
	<li>Le formatage des bibliographies est appliqué dans WIKINDX «à la volée» en utilisant les styles bibliographiques définis et compilés par l'administrateur. Les données bibliographiques ne sont donc pas toujours toutes affichées.</li>
</ul>

<p><strong>Paramètres personnels</strong></p>

<p>L'administrateur va avoir défini des paramètres par défaut, mais vous pouvez configurer un certain nombre de paramètres d'affichage ou de performance dans le menu «Préférences». En tant qu'utilisateur enregistré, vous pouvez, dans le menu «Mon Wikindx», gérer vos détails personnels et notification par courriel, créer des groupes d'usagers et des bibliographiques, et définir des étiquettes personnalisées.</p>",

				"preferences"	=>	"<h3>Options d'affichage</h3>

<p>En tant qu'utilisateur enregistré, les paramètres sélectionnés ici seront sauvegardés pour votre porchaine session. La plupart des paramètres sont explicites, mais certains requièrent plus d'explication :
</p>

<ul>
	<li>Taille maximum pour la fenêtre de sélection : Quelque fois la longueur du texte affiché dans un formulaire cause problème au navigateur web. C'est le cas en autres avec des titres de revue ou des noms d'éditeurs. Cette option permet de limiter la longueur de texte affichée.</li>
	<li>BibTeX :  BibTeX est un format bibliographique familier aux ingénieurs et scientifiques.</li>
	<li>CMS : Si l'administrateur le permet, les données bibliographiques de WIKINDX peuvent être affichées dans un système de gestion des contenu («Content Management System»)tel que MediaWiki, Moodle et Wordpress. Pour faciliter ceci, vous pouvez afficher un lien «CMS» qui fourni les données requises.</li>
	<li>Afficher un hyperlien : Lors de la consultation d'une liste de références, la référence complète peut être vue en cliquant sur l'icône de visionnement. De plus, vous pouvez configurer l'hyperlien pour englober l'entièreté de la notice bibliographique. Dans ce cas, si la notice comprend une référence vers une URL externe, alors l'URL externe ne sera plus hyperliée </li>
	<li>Niveaux d'affichage du menu : Pour utiliser l'espace d'affiche efficacement, WIKINDX utilise des menus à plusieurs niveaux. Ces niveaux peuvent cependant être difficiles à utiliser : vous pouvez alors réduire le nombre de niveaux des menus.</li>
</ul>",

				"myWikindx"	=>	"<h3>Mes paramètres</h3>

<p>La plupart des paramètres sont explicites, mais certains requièrent plus d'explication :</p>

<ul>
	<li>Mes groupes d'utilisateurs: Dans un WIKINDX multi-utilisateurs, les utilisateurs enregistrés peuvent créer des groupes d'utilisateurs. Les informations potentiellement privées, telles que des commentaires sur des citations ou des idées, peuvent être configurées pour être vues seulement par les membres d'un groupe d'utilisateurs. De plus, les utilisateurs du groupe peuvent élaborer leur propre bibliographie de groupe.</li>
	<li>Bibliographies de groupe : Ces bibliographies sont générées à partir de la bibliographie maitresse de WIKINDX. Elles peuvent être personnelles ou gérées par un groupe d'utilisateurs. Les changements effectués sur une bibliograohie de groupe n'ont pas d'impact sur la bibliographie maitresse de WIKINDX. </li>
</ul>",

				"ideas"	=>	"<h3>Remarques</h3> <p>Les remarques sont indépendantes des références et sont conçues pour garder une trace des réflexions en lien avec les sujets de la base de données.</p> <ul> <li>Les remarques sont regroupées en fil -- des nouvelles sous-remarques peuvent y être ajoutées.</li> <li>Le créateur original de la première remarque du fil est le propriétaire du fil.</li> <li>Le propriétaire peut déterminer le niveau d'accès du fil (public, privé ou groupe).</li> <li>Si des fils sont publics ou partagés avec un groupe, d'autres usagers enregistrés peuvent ajouter des sous-remarques.</li> <li>SEul l'auteur d'une sous-remarque peut modifier et effacer cette sous-remarque.</li> <li>Si la remarque originale est effacée, alors l'entièreté du fil est effacé.</li> </ul>",

				"plugins"	=>	"<h3>Plugins</h3>

<p>Téléchargez les plugins, modèles graphiques, langues et styles
bibliographiques ici : <a href='#wikindxLink#'>le site WIKINDX Sourceforge</a> puis installez-les dans le répertoire approprié en lisant attentivement les instructions données pour chaque plugin.  Les plugins étendent les fonctionnalités de WIKINDX au delà de son objectif premier. Ils peuvent être de deux types : les plugins «inline» qui apparaissent directement dans WIKINDX ou les plugins «menu» qui sont accessibles à partir du menu «Plugins». En tant qu'administrateur, vous pouvez gérer les plugins via l'interface WIKINDX :</p>

<ul>
	<li>Désactiver les plugins (et aussi les modèles, styles et langues) :  Ceci n'efface pas le plugin, il le désactive temporairement jusquMà ce que vous le réactiviez.</li>
	<li>Positionner les plugins :  Vous pouvez positionner les plugins dans différentes hiérarchies des menus.</li>
	<li>Autoriser : Bloquer l'accès de certains utilisateurs aux plugins</li>
</ul>

<p>Positioning plugins and granting authorization is accomplished by editing the plugin's
config.php file (typically only \$menus and \$authorize need be edited) -- be sure you know what you are doing:</p>

<ul>
<li>\$menus should be an array of at least one of the following menu elements:</li>
	<ul>
	<li>'wikindx'</li>
	<li>'res'</li>
	<li>'search'</li>
	<li>'text'</li>
	<li>'admin'</li>
	<li>'plugin1'</li>
	<li>'plugin2'</li>
	<li>'plugin3'</li>
	</ul>
<li>
'admin' is only available when logged in as admin, 'text' will only show if there is metadata (quotes etc.),
and the three 'pluginX' menu trees only show if they are populated.</li>
<li>\$authorize should be one of the following numerals:</li>
	<ul>
	<li>0 (menu item displayed for all users)</li>
	<li>1 (menu item displayed for users with write access)</li>
	<li>2 (menu item displayed only for admins)</li>
	</ul>
</ul>

<p>Usually, you will insert a submenu into one of the pluginX menus. As a reference, a typical config.php
file will look like this:</p>
<p>
<?php<br>
class adminstyle_CONFIG {<br>

public \$menus = array('plugin1');<br>
public \$authorize = 2;<br>

}<br>
?>
</p>

<p>Ensure the wikindx4/plugins directories (and the
index.php and config.php files) and the wikindx4/styles,
wikindx4/languages and wikindx4/templates folders (and all files
therein) can be written to by the web server user.</p>

<p>Inline plugins
return output that is displayed in one of four containers that can
optionally be positioned in any of the template .tpl files.  To change
the position of a container, you will need to edit the appropriate .tpl
file.</p>

<p>At least one template, one bibliographic style and one
language must remain enabled. WIKINDX expects that the English language
pack is available on the server (i.e. that you do not physically remove
it from the wikindx4/languages/ folder) whether it has been disabled or
not.  This is because the English language pack is used to supply any
messages that might be missing from other language packs.</p>",

// END__LOCALIZATION__MODULE__EDIT
		);
	}
}
?>