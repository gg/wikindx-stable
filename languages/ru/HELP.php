<?php
/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

This program is free software; you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

The WIKINDX Team 2016
sirfragalot@users.sourceforge.net

HELP_ru WIKINDX localization class -- Russian
**********************************************************************************/

class HELP_ru
{
	public function __construct()
	{
	}
	public function loadArrays()
	{
		return array(
// START__LOCALIZATION__MODULE__EDIT
				"search"	=>	"<h3>Поиск</h3> <p> При поиске источников или метаданных, Вы можете ограничить число возвращаемых результатов по категориям, ключевым словам и так далее, и можно выбрать поле базы данных для поиска. В поисковой фразе, вы можете использовать контрольные слова <strong>AND</strong>, <strong>OR</strong> и <strong>NOT</strong> и можете сгруппировать слова в точные фразы, используя двойные кавычки: <strong>\"</strong>поисковый запрос<strong>\"</strong>.</p> <p> Некоторые параметры поиска должны быть указаны в соответствующем поле с помощью стрелок.</p>",

				"resource"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"collection"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"categoryTree"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"pasteBibtex"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"configure"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"front"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"preferences"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"myWikindx"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"ideas"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"plugins"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

// END__LOCALIZATION__MODULE__EDIT
		);
	}
}
?>