<?php
/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

This program is free software; you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program;
if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

The WIKINDX Team 2016
sirfragalot@users.sourceforge.net

CONSTANTS_ru WIKINDX localization class -- Russian
**********************************************************************************/

// START__LOCALIZATION__MODULE__COPY
class CONSTANTS_ru
{
public $textEtAl;
public $possessiveArray;
public $startQuotation;
public $endQuotation;
public $abbreviations;

	public function __construct()
	{
// When using the word processor how are extra, unnamed authors indicated?  In English this would be 'et al.' as in:
// 'According to Grimshaw et al., "blah blah blah".
		$this->textEtAl = 'et al.';
// When using the word processor, how is singular possessive defined.  This is only for a single creator's surname and, like 'et al.' above, is used
// to detect if a creator name should be removed from the in-text citation if that name is in the same sentence as the citation.
// If this value contains an apostrophe (as in the English Grimshaw's), then it should be in double quotes (or else the PHP script will fail).
// English has two forms (the second below for names that end in 's').
// From wikindx v3.4.7 onwards, $this->possessiveArray is used allowing unlimited possessive forms.  If it exists and is an array,
// $this->possessive1 and $this->possessive2 will be ignored.
// If there are no possessive forms in your language, $this->possessiveArray should exist and be empty.
// < v3.4.7 commented out.
//		$this->possessive1 = "'s";
//		$this->possessive2 = "'";
// >= v3.4.7 possessive form
		$this->possessiveArray = array(
											"'s",
											"'",
			);
// What characters indicate the start and end of a quotation in the word processor text?  (Be careful to escape a double quote if it is inside
// double quotes by using '\'.  A single quote can be given safely as "'".)
		$this->startQuotation = "\"";
		$this->endQuotation = "\"";
// When using the word processor, for certain styles the text must be split up into sentences -- e.g. APA requires that year must follow the author name and
// page no. follows the citation if the citation appears in the same sentence as the author surname.  Normally sentences are recognized
// by '. ' (dot followed by at least one space).  This array handles other groups of characters (commonly abbreviations) that may appear in a sentence
// but do not indicate a sentence divider.  If there are no such abbreviations, the array must exist and be empty e.g. $this->abbreviations = array();
// The final '.' (dot) of the abbreviation must NOT be given.
// Matching is case insensitive.
// 'et al.' and equivalent is dealt with above.
// '...' and multiple dots consisting of two or more dots are dealt with in the code.
// Abreviations such as U.S.A., U.S.S.R., N.B. are dealt with in the code (must be capital English characters and no spaces)
// The syntax of the matching rule is that a sentence divider has been found if there is a dot followed by a space where the dot is not preceeded by a member of the array.
// For example, the 'e.g.' in "Blah blah blah, e.g. more blah, and even more blah" is not seen as a sentence divider if 'e.g' (no final dot) is a member of this array.
// Remove the English abbreviations unless they are used in your language.
		$this->abbreviations = array(
				'e.g',
				'etc', // etcetera
				'i.e',
				'Bros', // short for Brothers (as in Warner Bros.)
				'no', // number
				'c', // circa
				'ca', // circa
				'cf', // compare with
				'secs', // seconds
				'msecs', // milliseconds
				'Ph. D', // doctorate
				'MS', // Manuscript
				'viz',
				'v', // versus
				'N. B', // Nota bene
				'Fig',	// Figure
				'fig', // figure
				'co', // company
			);
	}
// Convert cardinal to ordinal numbers.
// The coding here is likely to be different for each language.  If there is no logic that can be programmed for your language, then it is recommended that
// you use English ordinals as given here so that something is printed and the script does not fail.
// $modulo holds the remainder of $cardinal/100 or $cardinal/10.
// Optional $field allows conditions based on gender.  Currently, $field will be either 'edition' or 'dayMonth' (day of the month).  Additionally, $field can be used
// to determine whether ordinals will be used at all.  For example, if this is French and $field == 'dayMonth', ordinals are used solely for '1er'.
// (languages/fr/CONSTANTS.php has an example using $field.)
//  The French equivalent to this function would be:
/*
	function cardinalToOrdinal($cardinal, $field = FALSE)
	{
		if(($field == 'dayMonth') && ($cardinal != 1))
			return $cardinal; // no change
		$modulo = $cardinal % 10;
		if($modulo == 1) // 1st
		{
			if($field == 'edition')
				return $cardinal . 'ère';
			return $cardinal . 'er';
		}
		else // all other numbers
			return $cardinal . 'ème';
	}
*/
	public function cardinalToOrdinal($cardinal, $field = FALSE)
	{
		$modulo = $cardinal % 100;
		if(($modulo == 11) || ($modulo == 12) || ($modulo == 13)) // 11th, 12th and 13th
			return $cardinal . 'th';
		$modulo = $cardinal % 10;
		if(($modulo >= 4) || !$modulo) // all other numbers
			return $cardinal . 'th';
		if($modulo == 1) // 1st
			return $cardinal . 'st';
		if($modulo == 2) // 2nd
			return $cardinal . 'nd';
		if($modulo == 3) // 3rd
			return $cardinal . 'rd';
	}
// convert cardinal numbers
	public function convertNumbers($number = FALSE, $convert = FALSE)
	{
		if(!is_numeric($number))
			return $number;
// month number to long name
		$longMonth = $this->monthToLongName();
// short month
		$shortMonth = $this->monthToShortName();
// cardinal -> ordinal word
		$ordinalWord = $this->cardinalToOrdinalWord();
// arabic -> roman numerals
		$roman = $this->cardinalToRoman();
/****************************
 START TRANSLATION
****************************/
// arabic -> ordinal (e.g. 3 -> 3rd, 10 -> 10th)
// Usually used for edition numbers in bibliographic styles that require e.g. '10th edition' rather than 'edition 10' - superseded by cardianlToOrdinal() function above
// A maximum of 50 seems a reasonable number to go up to....
// If necessary, you may need to add more loops or individual array elements.
		for($i = 4; $i <= 20; $i++)
			$ordinal[$i] = $i . 'th';
		for($i = 24; $i <= 30; $i++)
			$ordinal[$i] = $i . 'th';
		for($i = 34; $i <= 40; $i++)
			$ordinal[$i] = $i . 'th';
		for($i = 44; $i <= 50; $i++)
			$ordinal[$i] = $i . 'th';
		$ordinal[1] = '1st';
		$ordinal[21] = '21st';
		$ordinal[31] = '31st';
		$ordinal[41] = '41st';
		$ordinal[2] = '2nd';
		$ordinal[22] = '22nd';
		$ordinal[32] = '32nd';
		$ordinal[42] = '42nd';
		$ordinal[3] = '3rd';
		$ordinal[23] = '23rd';
		$ordinal[33] = '33rd';
		$ordinal[43] = '43rd';
/****************************
 END TRANSLATION
****************************/
// arabic -> cardinal (i.e. no change)
		$cardinal = range(0, 50);
// !$number, we are simply loading the arrays for use elsewhere, so return
		if(!$number)
			return;
// get rid of leading '0' if necessary
		if($number < 10)
			$number += 0;
// If that number is not actually in array, we return the number as is
		if(array_key_exists($number, ${$convert}))
			return ${$convert}[$number];
		return $number;
	}
// Convert month to long name
	public function monthToLongName()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
				1	=>	'January',
				2	=>	'February',
				3	=>	'March',
				4	=>	'April',
				5	=>	'May',
				6	=>	'June',
				7	=>	'July',
				8	=>	'August',
				9	=>	'September',
				10	=>	'October',
				11	=>	'November',
				12	=>	'December',
				13	=>	'Spring',
				14	=>	'Summer',
				15	=>	'Autumn',
				16	=>	'Winter',
/****************************
 END TRANSLATION
****************************/
			);
	}
// Convert month to short name
	public function monthToShortName()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
				1	=>	'Jan',
				2	=>	'Feb',
				3	=>	'Mar',
				4	=>	'Apr',
				5	=>	'May',
				6	=>	'Jun',
				7	=>	'Jul',
				8	=>	'Aug',
				9	=>	'Sep',
				10	=>	'Oct',
				11	=>	'Nov',
				12	=>	'Dec',
				13	=>	'Spring',
				14	=>	'Summer',
				15	=>	'Autumn',
				16	=>	'Winter',
/****************************
 END TRANSLATION
****************************/
			);
	}
// convert cardinal to roman
	private function cardinalToRoman()
	{
// arabic -> roman numerals
// Do NOT translate
		return array(
				1	=>	'I',
				2	=>	'II',
				3	=>	'III',
				4	=>	'IV',
				5	=>	'V',
				6	=>	'VI',
				7	=>	'VII',
				8	=>	'VIII',
				9	=>	'IX',
				10	=>	'X',
				11	=>	'XI',
				12	=>	'XII',
				13	=>	'XIII',
				14	=>	'XIV',
				15	=>	'XV',
				16	=>	'XVI',
				17	=>	'XVII',
				18	=>	'XVIII',
				19	=>	'XIX',
				20	=>	'XX',
				21	=>	'XXI',
				22	=>	'XXII',
				23	=>	'XXIII',
				24	=>	'XXIV',
				25	=>	'XXV',
				26	=>	'XXVI',
				27	=>	'XXVII',
				28	=>	'XXVIII',
				29	=>	'XXIX',
				30	=>	'XXX',
				31	=>	'XXXI',
				32	=>	'XXXII',
				33	=>	'XXXIII',
				34	=>	'XXXIV',
				35	=>	'XXXV',
				36	=>	'XXXVI',
				37	=>	'XXXVII',
				38	=>	'XXXVIII',
				39	=>	'XXXIX',
				40	=>	'XXXX',
				41	=>	'XXXXI',
				42	=>	'XXXXII',
				43	=>	'XXXXIII',
				44	=>	'XXXXIV',
				45	=>	'XXXXV',
				46	=>	'XXXXVI',
				47	=>	'XXXXVII',
				48	=>	'XXXXVIII',
				49	=>	'XXXXIX',
				50	=>	'L',
			);
	}
// convert ordinal to word
	public function cardinalToOrdinalWord()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
// Usually used for edition numbers in bibliographic styles that require words rather than arabic numerals.
// Any numbers not listed here will be returned without change.  e.g. here, 51 will be returned as 51 rather than 'fifty-first'.
// A maximum of 50 seems a reasonable number to go up to....
						'1'	=>	'First',
						'2'	=>	'Second',
						'3'	=>	'Third',
						'4'	=>	'Fourth',
						'5'	=>	'Fifth',
						'6'	=>	'Sixth',
						'7'	=>	'Seventh',
						'8'	=>	'Eighth',
						'9'	=>	'Ninth',
						'10'	=>	'Tenth',
						'11'	=>	'Eleventh',
						'12'	=>	'Twelfth',
						'13'	=>	'Thirteenth',
						'14'	=>	'Fourteenth',
						'15'	=>	'Fifteenth',
						'16'	=>	'Sixteenth',
						'17'	=>	'Seventeenth',
						'18'	=>	'Eighteenth',
						'19'	=>	'Nineteenth',
						'20'	=>	'Twentieth',
						'21'	=>	'Twenty-first',
						'22'	=>	'Twenty-second',
						'23'	=>	'Twenty-third',
						'24'	=>	'Twenty-fourth',
						'25'	=>	'Twenty-fifth',
						'26'	=>	'Twenty-sixth',
						'27'	=>	'Twenty-seventh',
						'28'	=>	'Twenty-eighth',
						'29'	=>	'Twenty-ninth',
						'30'	=>	'Thirtieth',
						'31'	=>	'Thirty-first',
						'32'	=>	'Thirty-second',
						'33'	=>	'Thirty-third',
						'34'	=>	'Thirty-fourth',
						'35'	=>	'Thirty-fifth',
						'36'	=>	'Thirty-sixth',
						'37'	=>	'Thirty-seventh',
						'38'	=>	'Thirty-eighth',
						'39'	=>	'Thirty-ninth',
						'40'	=>	'Fourtieth',
						'41'	=>	'Fourty-first',
						'42'	=>	'Fourty-second',
						'43'	=>	'Fourty-third',
						'44'	=>	'Fourty-fourth',
						'45'	=>	'Fourty-fifth',
						'46'	=>	'Fourty-sixth',
						'47'	=>	'Fourty-seventh',
						'48'	=>	'Fourty-eighth',
						'49'	=>	'Fourty-ninth',
						'50'	=>	'Fiftieth',
/****************************
 END TRANSLATION
****************************/
			);
	}
/**
* Format dates and times for localization (e.g. when viewing lists of resources in a multi-user wikindx 'Last edited by: xxx <date/time>)
*
* $timestamp comes in from the database in the format 'YYYY-MM-DD HH:MM:SS' e.g. 2013-01-31 15:54:55
*/
	public function dateFormat($timestamp)
	{
 		setlocale(LC_ALL, 'ru_RU.utf8');
// If you're happy with $timestamp's current format, all you need to do is uncomment this line:
//		return $timestamp;
// Otherwise. . .
		$unixTime = strtotime($timestamp);
// You have two choices for setting the time.  The first attempts to translate day and month names but will not
// work if the chosen locale is not available on your system.
// See
// http://www.php.net/manual/en/function.setlocale.php
// and
// http://www.php.net/manual/en/function.strftime.php
//
// The second is locale independent
// See:
// http://www.php.net/manual/en/function.date.php
//
// For the first option, set $dateFormat = 1; otherwise set $dateFormat = 2;
/****************************
 START TRANSLATION
****************************/
		$dateFormat = 2;
		if($dateFormat == 1)
		{
			$format = '%c'; // e.g. Tue Feb 5 00:45:10 2009
			setlocale(LC_ALL, "ru_RU.utf8"); // Russian locale
			$timestamp = strftime($format, $unixTime);
		}
		else if($dateFormat == 2)
		{
			$config = FACTORY_CONFIG::getInstance();
			if(!isset($config->WIKINDX_TIMEZONE) || !$config->WIKINDX_TIMEZONE)
				date_default_timezone_set(date_default_timezone_get()); // default is UTC
			else
				date_default_timezone_set($config->WIKINDX_TIMEZONE);
			$format = "d M Y H:i:s e";
			$timestamp = date($format, $unixTime);
		}
		else
			$timestamp = '';
/****************************
 END TRANSLATION
****************************/
		return $timestamp;
	}
}
?>