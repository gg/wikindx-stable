<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**********************************************************************************
CONSTANTS_de WIKINDX localization class -- German
**********************************************************************************/

// START__LOCALIZATION__MODULE__COPY
class CONSTANTS_de
{
public $textEtAl;
public $possessiveArray;
public $startQuotation;
public $endQuotation;
public $abbreviations;

	public function __construct()
	{
// When using the word processor how are extra, unnamed authors indicated?  In English this would be 'et al.' as in:
// 'According to Grimshaw et al., "blah blah blah".
		$this->textEtAl = 'et al.';
// When using the word processor, how is singular possessive defined.  This is only for a single creator's surname and, like 'et al.' above, is used
// to detect if a creator name should be removed from the in-text citation if that name is in the same sentence as the citation.
// If this value contains an apostrophe (as in the English Grimshaw's), then it should be in double quotes (or else the PHP script will fail).
// English has two forms (the second below for names that end in 's').
// From wikindx v3.4.7 onwards, $this->possessiveArray is used allowing unlimited possessive forms.  If it exists and is an array,
// $this->possessive1 and $this->possessive2 will be ignored.
// If there are no possessive forms in your language, $this->possessiveArray should exist and be empty.
// < v3.4.7 commented out.
//		$this->possessive1 = "'s";
//		$this->possessive2 = "'";
// >= v3.4.7 possessive form
		$this->possessiveArray = array(
											"s",
											"'",
			);
// What characters indicate the start and end of a quotation in the word processor text?  (Be careful to escape a double quote if it is inside
// double quotes by using '\'.  A single quote can be given safely as "'".)
		$this->startQuotation = "\"";
		$this->endQuotation = "\"";
// When using the word processor, for certain styles the text must be split up into sentences -- e.g. APA requires that year must follow the author name and
// page no. follows the citation if the citation appears in the same sentence as the author surname.  Normally sentences are recognized
// by '. ' (dot followed by at least one space).  This array handles other groups of characters (commonly abbreviations) that may appear in a sentence
// but do not indicate a sentence divider.  If there are no such abbreviations, the array must exist and be empty e.g. $this->abbreviations = array();
// The final '.' (dot) of the abbreviation must NOT be given.
// Matching is case insensitive.
// 'et al.' and equivalent is dealt with above.
// '...' and multiple dots consisting of two or more dots are dealt with in the code.
// Abreviations such as U.S.A., U.S.S.R., N.B. are dealt with in the code (must be capital English characters)
// The syntax of the matching rule is that a sentence divider has been found if there is a dot followed by a space where the dot is not preceeded a member of the array.
// For example, the 'e.g.' in "Blah blah blah, e.g. more blah, and even more blah" is not seen as a sentence divider if 'e.g' (no final dot) is a member of this array.
// Remove the English abbreviations unless they are used in your language.
		$this->abbreviations = array(
				'e.g',
				'etc', // etcetera
				'usw', // und so weiter
				'i.e',
				'd.h', // das heißt
				'Nr', // Nummer
				'ca', // circa
				'cf', // compare with
				'sec', // seconds
				'msec', // milliseconds
				'Ph. D', // doctorate
				'Dr', // Doktor
				'MS', // Manuscript
				'viz',
				'v', // versus
				'N. B', // Nota bene
				'Fig',	// Figure
				'fig', // figure
				'Abb', // Abbildung
				'co', // company
				'GmbH', // GmbH
				'e.V', // eingetragener Verein
			);
	}
// Convert cardinal to ordinal numbers.
// The coding here is likely to be different for each language.  If there is no logic that can be programmed for your language, then it is recommended that
// you use English ordinals as given here so that something is printed and the script does not fail.
// $modulo holds the remainder of $cardinal/100 or $cardinal/10.
// Optional $field allows conditions based on gender.  Currently, $field will be either 'edition' or 'dayMonth' (day of the month).  Additionally, $field can be used
// to determine whether ordinals will be used at all.  For example, if this is French and $field == 'dayMonth', ordinals are used solely for '1er'.
// (languages/fr/CONSTANTS.php has an example using $field.)
//  The French equivalent to this function would be:
/*
	function cardinalToOrdinal($cardinal, $field = FALSE)
	{
		if(($field == 'dayMonth') && ($cardinal != 1))
			return $cardinal; // no change
		$modulo = $cardinal % 10;
		if($modulo == 1) // 1st
		{
			if($field == 'edition')
				return $cardinal . 'ère';
			return $cardinal . 'er';
		}
		else // all other numbers
			return $cardinal . 'ème';
	}
*/
/*
	public function cardinalToOrdinal($cardinal, $field = FALSE)
	{
		$modulo = $cardinal % 100;
		if(($modulo == 11) || ($modulo == 12) || ($modulo == 13)) // 11th, 12th and 13th
			return $cardinal . 'th';
		$modulo = $cardinal % 10;
		if(($modulo >= 4) || !$modulo) // all other numbers
			return $cardinal . 'th';
		if($modulo == 1) // 1st
			return $cardinal . 'st';
		if($modulo == 2) // 2nd
			return $cardinal . 'nd';
		if($modulo == 3) // 3rd
			return $cardinal . 'rd';
	}
*/
// In German, cardinal numbers are not written but marked by a dot, i.e. 1., 2. etc.
	public function cardinalToOrdinal($cardinal, $field = FALSE)
	{
		return $cardinal . '.';
	}
// convert cardinal numbers
	public function convertNumbers($number = FALSE, $convert = FALSE)
	{
		if(!is_numeric($number))
			return $number;
// month number to long name
		$longMonth = $this->monthToLongName();
// short month
		$shortMonth = $this->monthToShortName();
// cardinal -> ordinal word
		$ordinalWord = $this->cardinalToOrdinalWord();
// arabic -> roman numerals
		$roman = $this->cardinalToRoman();
/****************************
 START TRANSLATION
****************************/
// arabic -> ordinal (e.g. 3 -> 3rd, 10 -> 10th)
// Usually used for edition numbers in bibliographic styles that require e.g. '10th edition' rather than 'edition 10' - superseded by cardianlToOrdinal() function above
// A maximum of 50 seems a reasonable number to go up to....
// If necessary, you may need to add more loops or individual array elements.
		for($i = 1; $i <= 50; $i++)
			$ordinal[$i] = $i . '.';
/****************************
 END TRANSLATION
****************************/
// arabic -> cardinal (i.e. no change)
		$cardinal = range(0, 50);
// !$number, we are simply loading the arrays for use elsewhere, so return
		if(!$number)
			return;
// get rid of leading '0' if necessary
		if($number < 10)
			$number += 0;
// If that number is not actually in array, we return the number as is
		if(array_key_exists($number, ${$convert}))
			return ${$convert}[$number];
		return $number;
	}
// Convert month to long name
	public function monthToLongName()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
				1	=>	'Januar',
				2	=>	'Februar',
				3	=>	'März',
				4	=>	'April',
				5	=>	'Mai',
				6	=>	'Juni',
				7	=>	'Juli',
				8	=>	'August',
				9	=>	'September',
				10	=>	'Oktober',
				11	=>	'November',
				12	=>	'Dezember',
/****************************
 END TRANSLATION
****************************/
			);
	}
// Convert month to short name
	public function monthToShortName()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
				1	=>	'Jan',
				2	=>	'Feb',
				3	=>	'Mär',
				4	=>	'Apr',
				5	=>	'Mai',
				6	=>	'Jun',
				7	=>	'Jul',
				8	=>	'Aug',
				9	=>	'Sep',
				10	=>	'Okt',
				11	=>	'Nov',
				12	=>	'Dez',
/****************************
 END TRANSLATION
****************************/
			);
	}
// convert cardinal to roman
	private function cardinalToRoman()
	{
// arabic -> roman numerals
// Do NOT translate
		return array(
				1	=>	'I',
				2	=>	'II',
				3	=>	'III',
				4	=>	'IV',
				5	=>	'V',
				6	=>	'VI',
				7	=>	'VII',
				8	=>	'VIII',
				9	=>	'IX',
				10	=>	'X',
				11	=>	'XI',
				12	=>	'XII',
				13	=>	'XIII',
				14	=>	'XIV',
				15	=>	'XV',
				16	=>	'XVI',
				17	=>	'XVII',
				18	=>	'XVIII',
				19	=>	'XIX',
				20	=>	'XX',
				21	=>	'XXI',
				22	=>	'XXII',
				23	=>	'XXIII',
				24	=>	'XXIV',
				25	=>	'XXV',
				26	=>	'XXVI',
				27	=>	'XXVII',
				28	=>	'XXVIII',
				29	=>	'XXIX',
				30	=>	'XXX',
				31	=>	'XXXI',
				32	=>	'XXXII',
				33	=>	'XXXIII',
				34	=>	'XXXIV',
				35	=>	'XXXV',
				36	=>	'XXXVI',
				37	=>	'XXXVII',
				38	=>	'XXXVIII',
				39	=>	'XXXIX',
				40	=>	'XXXX',
				41	=>	'XXXXI',
				42	=>	'XXXXII',
				43	=>	'XXXXIII',
				44	=>	'XXXXIV',
				45	=>	'XXXXV',
				46	=>	'XXXXVI',
				47	=>	'XXXXVII',
				48	=>	'XXXXVIII',
				49	=>	'XXXXIX',
				50	=>	'L',
			);
	}
// convert ordinal to word
	public function cardinalToOrdinalWord()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
// Usually used for edition numbers in bibliographic styles that require words rather than arabic numerals.
// Any numbers not listed here will be returned without change.  e.g. here, 51 will be returned as 51 rather than 'fifty-first'.
// A maximum of 50 seems a reasonable number to go up to....
						'1' =>  'Erste',
						'2' =>  'Zweite',
						'3' =>  'Dritte',
						'4' =>  'Vierte',
						'5' =>  'Fünfte',
						'6' =>  'Sechste',
						'7' =>  'Siebte',
						'8' =>  'Achte',
						'9' =>  'Neunte',
						'10'    =>  'Zehnte',
						'11'    =>  'Elfte',
						'12'    =>  'Zwölfte',
						'13'    =>  'Dreizehnte',
						'14'    =>  'Vierzehnte',
						'15'    =>  'Fünfzehnte',
						'16'    =>  'Sechzehnte',
						'17'    =>  'Siebzehnte',
						'18'    =>  'Achtzehnte',
						'19'    =>  'Neuzehnte',
						'20'    =>  'Zwanzigste',
						'21'    =>  'Einundzwanzigste',
						'22'    =>  'Zweiundzwanzigste',
						'23'    =>  'Dreiundzwanzigste',
						'24'    =>  'Vierundzwanzigste',
						'25'    =>  'Fünfundzwanzigste',
						'26'    =>  'Sechsundzwanzigste',
						'27'    =>  'Siebenundzwanzigste',
						'28'    =>  'Achtundzwanzigste',
						'29'    =>  'Neunundzwanzigste',
						'30'    =>  '30.',
						'31'    =>  '31.',
						'32'    =>  '32.',
						'33'    =>  '33.',
						'34'    =>  '34.',
						'35'    =>  '35.',
						'36'    =>  '36.',
						'37'    =>  '37',
						'38'    =>  '38.',
						'39'    =>  '39.',
						'40'    =>  '40.',
						'41'    =>  '41.',
						'42'    =>  '42.',
						'43'    =>  '43.',
						'44'    =>  '44.',
						'45'    =>  '45.',
						'46'    =>  '46.',
						'47'    =>  '47.',
						'48'    =>  '48.',
						'49'    =>  '49.',
						'50'    =>  '50.',
/****************************
 END TRANSLATION
****************************/
			);
	}
/**
* Format dates and times for localization (e.g. when viewing lists of resources in a multi-user wikindx 'Last edited by: xxx <date/time>)
*
* $timestamp comes in from the database in the format 'YYYY-MM-DD HH:MM:SS' e.g. 2013-01-31 15:54:55
*/
	public function dateFormat($timestamp)
	{
 		setlocale(LC_ALL, 'de_DE.utf8');
// If you're happy with $timestamp's current format, all you need to do is uncomment this line:
//		return $timestamp;
// Otherwise. . .
		$unixTime = strtotime($timestamp);
// You have two choices for setting the time.  The first attempts to translate day and month names but will not
// work if the chosen locale is not available on your system.
// See
// http://www.php.net/manual/en/function.setlocale.php
// and
// http://www.php.net/manual/en/function.strftime.php
//
// The second is locale independent
// See:
// http://www.php.net/manual/en/function.date.php
//
// For the first option, set $dateFormat = 1; otherwise set $dateFormat = 2;
// You can fine-tune the format by editing the variable $format, some examples are below and you can just uncomment them as neede
/****************************
 START TRANSLATION
****************************/

		$dateFormat = 2;
		if($dateFormat == 1)
		{
			$format = '%c';                            // Default format
			// $format = "%a., %e. %b. %Y %H:%M:%S %Z";  // "Di., 10. Mär. 2013 20:15:00 MEZ"
			// $format = "%e.%m.%Y %H:%M:%S";            // "10.03.2013 20:15:00"
			setlocale(LC_ALL, "de_DE.utf8"); // German locale
			$timestamp = strftime($format, $unixTime);
		}
		else if($dateFormat == 2)
		{
			$config = FACTORY_CONFIG::getInstance();
			date_default_timezone_set($config->WIKINDX_TIMEZONE);
			// If you want to display times in a timezone different from the server timezone (set in config.php)
			// then uncomment the following line and set the desired timezone explicitly
			// date_default_timezone_set("Europe/Berlin");
			$format = "D., j. M. Y H:i:s e";  // "Thu., 10. Mar. 2013 20:15:00 Europe/Berlin"
			// $format = "j.n.Y H:i:s";       // "10.3.2013 20:15:00"
			$timestamp = date($format, $unixTime);
			// translate day and month names by hand
			$dates_en  = array("Mon","Tue","Wed","Thu","Fri","Sat","Sun","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
			$dates_loc = array("Mo" ,"Di" ,"Mi" ,"Do" ,"Fr" ,"Sa" ,"So" ,"Jan","Feb","Mär","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez");
			$timestamp = str_replace($dates_en,$dates_loc,$timestamp);
		}
		else
			$timestamp = '';
/****************************
 END TRANSLATION
****************************/
		return $timestamp;
	}
}
?>