Wikindx v4.2.1 <German Deutsch> localization.
Wikindx v4.2.1 <German Deutsch> Sprachanpassung.

By/von: Stephan Matthiesen, 2013-05-12

Installation:
Unzip with the folder structure into
	wikindx4/languages/

Mit der Verzeichnisstruktur entpacken nach
	wikindx4/languages/


Time and date formats / Datums- und Zeitformate

The format for dates and times is determined by the localization of your PHP installation. We have tried to use a method that should work with most installations. You can change the format by editing some settings the section \"Format date and time\" at the end of the file CONSTANTS.php. More detailed explanations can be found in that file.

Das Format für Datum und Zeit wird durch Ihre PHP-Installation bestimmt. Wir haben versucht, eine Methode zu wählen, die in den meisten Installationen funktionieren sollte. Sie können das Format ändern, indem Sie einige Einstellungen im Abschnitt \"Format date and time\" am Ende der Datei CONSTANTS.php bearbeiten. Genauere Erklärungen finden Sie als Kommentare in derselben Datei.