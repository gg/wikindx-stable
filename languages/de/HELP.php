<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/*****
* HELP class
*
* NOTE TO TRANSLATORS:  1/  Both the class name and the constructor name should be changed to match the (case-sensitive) name of
*				the folder your language files are in.  For example, if you are supplying a Klingon translation and
*				your languages/ folder is languages/kn/, the class and constructor name for the file SUCCESS.php
*				must both be SUCCESS_kn.
*			2/  Do not change the key (the first part) of an array element.
*			3/  Ensure that each array element value is enclosed in double quotes "..." and is followed by a comma "...",
*			4/  The class name should be changed to match the (case-sensitive) name of
*				the folder your language files are in.  For example, if you are supplying a Klingon translation and
*				your languages/ folder is languages/kn/, the class name for the file SUCCESS.php
*				must both be SUCCESS_kn.
*
* NOTE TO DEVELOPERS:  1/ Any comments not preceeded by '///' at the start of the line will be ignored by the localization module.
*				2/  All comments should be on one line (no line breaks) and must start at the beginning of the line for the localization module to work.
*				3/  Comments must be of the form '/// dbError_ This array does this' where 'dbError' is the array that the comment refers to
*				or may be of the form '/// dbError_open This is the comment'
*				where 'dbError_open' is the exact array and array key that the comment refers to.
*****/
// START__LOCALIZATION__CLASSNAME__EDIT
class HELP_de
// START__LOCALIZATION__CLASSNAME__EDIT
{
private $config;
public $translate = FALSE;

// Constructor
	public function __construct()
	{
		$this->config = FACTORY_CONFIG::getInstance();
		$this->session = FACTORY_SESSION::getInstance();
	}
	public function loadArrays()
	{
		if($this->translate == FALSE) // translate = TRUE for localization module
		{
			if(!isset($this->config->WIKINDX_SEARCHFILTER)) // i.e. at first install of a blank database
				$search = 'an, a, the, and, to';
			else
				$search = join(', ', $this->config->WIKINDX_SEARCHFILTER);
			$pasteBibtex = $this->session->getVar('setup_MaxPaste');
			$tinyMCELink = '"http://www.tinymce.com/wiki.php/Browser_compatiblity" target="_new"';
			$phpGDLink = "https://secure.php.net/manual/de/book.image.php";
			$wikindxLink = "https://sourceforge.net/projects/wikindx/";
		}
		else
		{
			$search = '$search';
			$pasteBibtex = '$pasteBibtex';
			$tinyMCELink = '$tinyMCELink';
			$phpGDLink = '$phpGDLink';
			$wikindxLink = '$wikindxLink';
		}
		return array(
// START__LOCALIZATION__MODULE__EDIT
				"search"	=>	"<h3>Suche</h3>

<p>Wenn Sie nach Ressourcen oder Metadaten suchen, können Sie die Ergebnisse auf bestimmte Kategorien, Schlagworte usw. beschränken, und Sie können das Datenbankfeld auswählen, in dem gesucht werden soll. Im Suchfeld können Sie die Befehlswörter <strong>AND</strong> (und), <strong>OR</strong> (oder) sowie  <strong>NOT</strong> (nicht) verwenden, oder Sie können Wörter, die als Gruppe exakt gesucht werden sollen, mit doppelten Anführungszeichen gruppieren: <strong>\"</strong><genaue Wortgruppe><strong>\"</strong>.</p>

<p>In einigen Feldern können Sie eine Mehrfachauswahl treffen, indem Sie (unter Windows und Linux) die Steuerungs- (Control-) und/oder die Umschalttaste während des Klickens gedrückt halten (auf Apple-Betriebssystemen entsprechend die Befehls- und Umschalttaste). Einige Suchoptionen müssen durch Klicken auf die Pfeile in die entsprechende Auswahlliste mit der Beschreibung \"Gewählte xxx\" übertragen werden. Dies macht die Auswahl aus Listen einfacher, die sehr lang sein können.</p>

<p>Einige Auswahlfelder nutzen Optionsfelder für 'OR' oder 'AND'. Wenn Sie beispielsweise zwei oder mehr Schlagworte auswählen und 'OR' wählen, dann müssen die Suchergebnisse mindestens eines dieser Schlagworte enthalten. Wenn Sie 'AND' wählen, müssen die Suchergebnisse alle ausgewählten Schlagworte enthalten.</p>

<p>Es gelten die folgenden Regeln:</p>

<ul>
	<li><strong>AND</strong> (und), <strong>OR</strong> (oder) sowie <strong>NOT</strong> (nicht) müssen in Großbuchstaben geschrieben werden und wirken nur außerhalb von genauen Suchwortgruppen als Befehlswörter</li>
	<li>Suchbegriffe sind unabhängig von Groß-/Kleinschreibung</li>
	<li>Teilweise Übereinstimmung funktioniert nicht mit genauen Suchwortgruppen</li>
	<li>Ein Leerzeichen gilt als <strong>AND</strong>-Verknüpfung, außer wenn es in eine genauen Suchwortgruppe steht</li>
	<li>Alle Satzzeichen in einer genauen Suchwortgruppe werden ignoriert</li>
	<li><strong>OR</strong> nach <strong>AND</strong> oder <strong>NOT</strong> werden gruppiert, d.h. 'Wort1 AND Wort2 OR Wort3 OR Wort4 NOT Wort5 OR Wort6' wird interpretiert als 'Wort1 AND (wort2 OR Wort3 OR Wort4) NOT (Wort5 OR Wort6)'</li>
	<li>Der Administrator hat festgelegt, dass folgende Wörter ignoriert werden, sofern sie nicht in einer genauen Suchwortgruppe stehen: <em>#search#</em></li>
</ul>",

				"resource"	=>	"<h3>Ressource</h3>

<p>Jeder Benutzer kann:</p>
<ul>
	<li>Ressourcen in eine Ablage legen; sie stellt eine zeitweilige Sammlung dar, die man jederzeit wieder aus dem Ressourcen-Menü aufrufen kann.</li>
	<li>Lesezeichen zu einer Ressource erstellen; die Lesezeichen können vom Ressourcen-Menü aufgerufen werden. Lesezeichen werden für registrierte Benutzer dauerhaft über die aktuelle Sitzung hinweg gespeichert.</li>
</ul>

<p>Als registrierter Benutzer können Sie:</p>
<ul>
	<li>Elemente auf dieser Seite bearbeiten. Bestimmte Funktionen, wie etwa das Bearbeiten oder das Hinzufügen von Anhängen, müssen vom Administrator aktiviert worden sein.</li>
	<li>Neue Elemente wie Zitate, Umschreibungen oder Gedanken erstellen. Kommentare (zu Zitaten und Umschreibungen) sowie Gedanken können als privat, öffentlich oder nur für bestimmte Benutzergruppen sichtbar eingestellt werden, die in Mein Wikindx definiert worden sind.</li>
</ul>

<p>Falls ein Element wie ein Schlagwort oder eine Kategorie als Link dargestellt werden, werden beim Klicken auf den Link andere Ressourcen mit diesem Schlagwort bzw. der Kategorie angezeigt. Alle Zitate, Umschreibungen und vorhandene Gedanken können optional auch verlinkte Schlagwörter haben, die sie zu anderen Metadaten verlinken.</p>

<p>Je nach Ihren Benutzerberechtigungen werden Sie verschiedene Statistiken auf der Seite sehen:</p>

<ul>
	<li>Seitenansichten: Anzahl der Seitenaufrufe in diesem Monat / insgesamt.</li>
	<li>Seitenansichts-Index: Eine Kennzahl dafür, wie oft diese Ressource im Vergleich zu anderen Ressourcen aufgerufen wurde, gewichtet mit der Zeit, die sie bereits verfügbar ist. Je höher der Prozentsatz, desto häufiger wurde die Ressource pro Zeiteinheit aufgerufen.</li>
	<li>Downloads: Eine Ressource kann beliebig viele Anhänge haben, und für jeden wird die Anzahl der Downloads in diesem Monat / insgesamt angezeigt.</li>
	<li>Download-Index: Wie der Seitenansichts-Index, der gewichtete Prozentsatz aller Downloads.</li>
	<li>Popularitäts-Index: Eine Kombination des Seitenansichts-Index und des Download-Index, der die Popularität der Ressource angibt.</li>
	<li>Reife-Index: Eine Zahl zwischen 0 und 10, die vom Administrator festgelegt wird und die subjektive 'Reife' der Ressource darstellt (z.B. wie bedeutend, wichtig oder umfassend sie für das jeweilige Thema ist).</li>
</ul>",

				"collection"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"categoryTree"	=>	"<h3>Kategorienbaum</h3>

<p>Der Kategorienbaum zeigt alle Kategorien, die in WIKINDX verwendet werden, und darunter die jeweiligen Unterkategorien und Schlagwörter. Die Zahl in Klammern ist die Anzahl der Ressourcen in jeder Kategorie, Unterkategorie bzw. Schlagwort.</p>",

				"pasteBibtex"	=>	"<h3>BibTeX einfügen</h3>

<p>Wenn Sie eine Bibliographie im BibTeX-Format haben, können Sie einzelne Einträge (ggfls. einschließlich ihrer @string-Variablen) importieren, indem sie sie aus Ihrer BibTeX-Datei kopieren und in das Textfeld einfügen. Wenn Nicht-Standard-Felder im BibTeX-Eintrag vorhanden sind, gibt Ihnen WIKINDX die Möglichkeit, sie in WIKINDX-Felder zu übertragen: Wenn Sie der Administrator sind, möchten Sie vielleicht zuvor zusätzliche Felder erstellen. Ein Administrator kann unbegrenzt viele Einträge auf einmal einfügen, andere Benutzer können nur <strong>\$pasteBibtex</strong> Einträge auf einmal einfügen.</p>",

				"configure"	=>	"<h3>WIKINDX konfigurieren</h3>

<p>Die meisten Konfigurationseinstellungen erklären sich von selbst, aber bitte beachten Sie das Folgende:</p>

<ul>
	<li>Wenn Sie den speziellen Ausdruck \$QUICKSEARCH\$ zur Bechreibungstext der Startseite hinzufügen, wird dort ein Schnellsuch-formular angezeigt.</li>
	<li>Um zu verhindern, dass registrierte Benutzer BibTeX-Einträge einfügen können, setzen Sie die Anzahl der erlaubte BibTeX-Einträge auf 0. Admininstratoren können stets BibTeX-Daten einfügen.</li>
	<li>Für Aktionen, bei denen WIKINDX temporäre Dateien erzeugt, etwa beim Export vom Bibliographien in verschiedene Formate, können Sie die Zeit (in Sekunden) festlegen, nach der die Datei gelöscht wird, sobald ein Benutzer einlogt.</li>
	<li>Wenn die Einstellung zur Benachrichtigung über Statistiken gesetzt ist, erhalten registrierte Benutzer, die zugleich Urheber einer Ressource sind, zu jedem Monatsanfang eine E-Mail mit den entsprechenden Statistiken.</li>
	<li>Sie können den Nur-Lesen-Zugriff deaktivieren. Wenn der Nur-Lese-Zugriff erlaubt ist, können nicht registrierte Besucher das Loginformular umgehen in direkt in WIKINDX gehen.</li>
	<li>Wenn Sie CAPTCHA für Benutzerregistrierungsanfragen verwenden möchten, müssen Sie die <a href='\$phpGDLink'>GD-Erweiterung</a> in Ihrer php.ini-Konfigurationsdatei aktivieren. Dies hilft gegen Bots (automatische, meist böswillige Programmzugriffe) und Spammer.</li>
	<li>Die Ausgabe von PHP-Fehlern und SQL-Anweisungen auf dem Bildschirm dient der Fehlersuche und sollten auf öffentlichen Servern nicht aktiviert werden. Die Ausgabe von SQL-Anweisungen kann AJAX-Operationen stören, etwa auf Seiten wie \"Ressourcen auswählen\", \"Suche\", \"Neue Ressource\" oder \"Ressource bearbeiten\".</li>
	<li>Falls die Variable WIKINDX_GS_ALLOW in der Datein config.php auf TRUE gesetzt ist, dann wird Google Scholar nicht erlaubt, Anhänge (PDFs etc.) zu indizieren, wenn Sie hier nur registrierten Benutzern den Zugriff auf Anhänge erlauben. (Google Scholar kann jedoch dennoch die bibliographischen Details und die Zusammenfassung der Ressource indizieren.)</li>
</ul>

<p>Einige der Einstellungen, etwa die Anzahl der Ressourcen, die pro Seite angezeigt werden, oder der Bibliographiestil, sind Standardwerte; Benutzer können in ihren Einstellungen andere Vorgaben machen.</p>

<p>Sie können Systembenutzer unter dem Administrations-Menü hinzugefügen.</p>

<p>Wenn Ressourcen hinzugefügt oder bearbeitet werden, kann jede Resssource mehreren Kategorien und Unterkategorien, zusätzlich definierten Feldern sowie Sprachen zugeordnet werden. Administratoren können neue Kategorien, Unterkategorien, zusätzliche Felder und Sprachen im Administrations-Menü defnieren.</p>",

				"front"	=>	"<h3>WIKINDX</h3>

<p><strong>Allgemeine Hinweise</strong></p>

<ul>
	<li>Ihr Webbrowser muss Cookies akzeptieren.</li>
	<li>Da WIKINDX als Programm im Browsers läuft, sollten Sie vermeiden, die \"Zurück\"- und \"Vorwärts\"-Knöpfe des Browsers zu nutzen. Verwenden Sie stattdessen die Navigationsmöglichkeiten innerhalb WIKINDX.</li>
	<li>WIKINDX verwendet Sitzungen, um Daten zeitweise zu speichern. Jede Sitzung betrifft nur ein einzelnes Browserfenster und einen Browsertyp. Es können unerwartete Ergebnisse auftreten, wenn Sie WIKINDX gleichzeitig in mehreren Browserfenstern oder -tabs benutzen; Sie können dies aber problemlos mit mehreren verschiedenen Browsern tun.</li>
	<li>WIKINDX v4 verwendet TinyMCE v3.5.5 für die WYSIWYG-Funktionen beim Bearbeiten vom Textfeldern. Eine Liste kombatibler Browser findet sich <a href=\$tinyMCELink>hier</a>, dazu gehören jedoch alle gängigen Browser wie MSIE, Firefox, Chrome, Safari und Opera.</li>
	<li>Eine <em>Ressource</em> in WIKINDX ist eine Sammlung von Daten, die die Information für einen bibliographischen Eintrag darstellen sowie Metadaten (Informationen ähnlich einer Karteikarte, die sich auf den Eintrag beziehen, etwa Zitate oder Gedanken zur Quelle -- der Administrator kann jedoch einige dieser Funktionen deaktivieren), Kategorien, Unterkategorien und Schlagworte. Es kann auch weitere Informationen geben, etwa Anmerkungen, eine Zusammenfassung, Anhänge und externe URLs sowie statistische Daten.</li>
	<li>Wenn Sie registrierter Benutzer sind, können Sie eigene Benutzer-Tags erstellen und mit ihnen Ressourcen markieren.</li>
	<li>Als registrierter Benutzer können Sie auch Ihre eigenen Bibliographien anlegen, die aus der WIKINDX-Hauptbibliothek extrahiert werden.</li>
	<li>Die Formatierung der bibliographischen Details geschieht dirch WIKINDX bei jedem Aufruf neu unter Verwendung der Bibliographiestile, die vom Administrator erstellt und definiert wurden. Dabei werden nicht unbedingt alle Details der Ressource angezeigt. Wenn Sie beispielsweise nach einer Ressource mit dem Urheber M. Maus suchen, dann werden evtl. Ressourcen gefunden, bei denen M. Maus ein Herausgeber ist, aber je nach bibliographischem Stil wird der Name des Herausgebers evtl. gar nicht angezeigt.</li>
</ul>

<p><strong>Persönliche Einstellungen</strong></p>

<p>Der Administrator hat bestimmte Standardeinstellungen festgelegt, aber unter dem Menüpunkt \"Einstellungen\" können Sie eine Reihe von Parametern verändern, die das Verhalten von WIKINDX und die Anzeige von Daten beeinflussen. Als registrierter Benutzer können Sie unter dem Menüpunkt \"Mein Wikindx\" einige persönliche Details und E-Mail-Benachrichtigungen einstellen sowie Benutzergruppen, Benutzerbibliographien und Benutzer-Tags verwalten.</p>",

				"preferences"	=>	"<h3>Einstellungen</h3>

<p>Wenn Sie registrierter Benutzer sind, werden die Einstellungen gespeichert und bei zukünftigen Aufrufen von WIKINDX wieder verwendet. Die meisten Einstellungen erklären sich von selbst, aber einige erfordern eine Erläuterung:</p>

<ul>
	<li>Anzahl der Ressourcen pro Seite: Wenn eine Aktion eine große Zahl an Ressourcen anzeigen muss, kann ihre Darstellung eine Weile dauern. Sie können aber festlegen, dass nur ein Teil der Ressourcen auf einmal angezeigt wird, mit Seitenlinks, die zur nächsten Seite mit den nächsten Ressourcen führen.</li>
	<li>Tag-Wolken: Ressourcen können (unter dem Suchmenü) als \"Tag-Wolken\" aufgelistet werden, wobei die Größe und Farbe der Schrift die relative Anzahl der aufgelisteten Daten anzeigt.</li>
	<li>Maximale Zeichenzahl in Auswahlliste: Manchmal ist der Text, der in einer Auswahlliste gezeigt wird, zu lang und kann zu Problemen mit dem Webbrowser führen. Dies ist oft bei den Namen von Zeitschriften oder Verlagen der Fall. Mit dieser Einstellung können Sie die Länge des angezeigten Textes begrenzen, der unterdrückte Teil wird als '...' dargestellt.</li>
	<li>BibTeX: BibTeX ist ein bibliographisches Format, das vor allem unter Ingenieuren und Naturwissenschaftlern weit verbreitet ist.</li>
	<li>CMS: Wenn der Administrator es zulässt, können bibliographische Daten aus WIKINDX in einem Redaktionssystem (Content-Management-System, CMS) wie MediaWiki, Moodle oder WordPress angezeigt werden. Um dies zu erleichtern, können Sie ein 'cms'-Link anzeigen lassen, das die nötigen Daten liefert.</li>
	<li>Link zu Ressourcen: Wenn Sie eine Liste von Ressourcen ansehen, können Sie durch Klicken auf das \"Ansehen\"-Symbol die Ressource in Einzelansicht aufrufen. Zudem können Sie hier einstellen, dass die gesamte Ressource als Link zur Einzelansicht führt. Wenn dies eingestellt ist und die Ressource enthält eine externe URL (wenn die Ressource z.B. eine Website ist), dann wird diese URL nicht mehr als Link gezeigt.</li>
	<li>Menü-Ebenen: Um den Platz auf dem Bildschirm optimal zu nutzen, verwendet WIKINDX Menüs mit mehreren Unterebenen. Diese können jedoch evtl. schwierig zu benutzen sein, daher können Sie hier die Anzahl der Menüebenen reduieren. In manchen Fällen hat jedoch der Vorlagendesigner eine Mindestzahl an Ebenen vorgegeben, sodass eine Verringerung der Zahl für diese Vorlagen keine Wirkung hat.</li>
</ul>",

				"myWikindx"	=>	"<h3>Mein Wikindx</h3>

<p>Die meisten Einstellungen erklären sich von selbst, aber einige erfordern Erläuterungen:</p>

<ul>
	<li>Benutzergruppen: In einem Mehrbenutzer-WIKINDX können registrierte Benutzer Gruppen definieren. Bei Informationen, die evtl. privat sein sollen, wie etwa Kommentare zu Zitaten oder Gedanken, können Sie dann festlegen, dass sie nur von Benutzern der Gruppe gelesen werden können. Außerdem können die Benutzer einer Gruppe gemeinsam eine Gruppenbibliographie erstellen.</li>
	<li>Benutzer-Bibliographien: Diese werden der WIKINDX-Hauptbibliographie entnommen, sie können persönlich oder von einer Benutzergruppe verwaltet werden. Aktionen in der Benutzer-Bibliographie (etwa das Entfernen von Ressourcen) haben keinen Einfluss auf die WIKINDX-Hauptbibliographie.</li>
</ul>",

				"ideas"	=>	"__WIKINDX__NOT_YET_TRANSLATED__",

				"plugins"	=>	"<h3>Plugins</h3>

<p>Sie können Plugins, Vorlagen, Sprachen und Bibliographie-Stilvorlagen von der <a href='\$wikindxLink'>WIKINDX-Website auf Sourceforge</a> herunterladen und sie im entsprechenden Verzeichnis installieren, nachdem Sie die jeweiligen Anweisungen sorgfältig durchgelesen haben. Plugins erweitern die Funktionalität von WIKINDX über die Grundfunktionen hinaus. Es gibt zwei Typen: Inline-Plugins, deren Ausgabe innerhalb des Textkörpers von WIKINDX dargestellt wird, und Menü-Plugins, bei denen das Plugin durch zusätzliche Menüpunkte aufgerufen wird. Als Administrator können Sie Plugins und andere Extras teilweise über das WIKINDX-Interface verwalten, darunter:</p>

<ul>
	<li>Plugins (und Vorlagen, Stile und Sprachen) deaktivieren: Das Plugin wird nicht entfernt, aber zeitweise deaktiviert, bis Sie es wieder aktivieren.</li>
	<li>Plugins positionieren: Sie können Plugins in verschiedenen Menü-Hierarchien platzieren.</li>
	<li>Autorisierung: Sie können bestimmte Typen von Benutzern blockieren, sodass sie das Plugin nicht verwenden können.</li>
</ul>

<p>Zur Positionierung und Autorisierung müssen Sie die Datei config.php bearbeiten (gewöhnlich müssen Sie nur \$menus und \$authorize ändern) -- seien Sie vorsichtig und ändern es nur, wenn Sie wissen, was Sie tun:</p>

<ul>
<li>\$menus muss ein Feld (array) mit mindestens einem der folgenden Menüelemente sein:</li>
	<ul>
	<li>'wikindx' (für das Wikindx-Menü)</li>
	<li>'file' (Dateien)</li>
	<li>'res' (Ressourcen)</li>
	<li>'search' (Suche)</li>
	<li>'text'</li>
	<li>'admin' (Administration)</li>
	<li>'plugin1'</li>
	<li>'plugin2'</li>
	<li>'plugin3'</li>
	</ul>
<li>
'admin' wird nur angezeigt, wenn Sie als Administrator eingeloggt sind, 'file' nur dann, wenn Dateien exportiert wurden, 'text' nur dann, wenn es Metadaten (Zitate etc.) gibt, und die drei 'pluginX'-Menüs werden nur gezeigt, wenn sie belegt sind.
</li>
<li> \$authorize muss eine der folgenden Zahlen sein:</li>
	<ul>
	<li>0 (Menüeintrag wird allen Benutzern gezeigt)</li>
	<li>1 (Menüeintrag wird nur Benutzern mit Schreibrechten gezeigt)</li>
	<li>2 (Menüeintrag wird nur Administratoren gezeigt)</li>
	</ul>
</ul>

<p>Normalerweise sollten Sie ein Untermenü nur in eines der pluginX-Menüs einfügen. Als Beispiel sieht ein typische config.php-Datei so aus:</p>
<p>
<code>
<?php<br>
class adminstyle_CONFIG {<br>

public \$menus = array('plugin1');<br>
public \$authorize = 2;<br>

}<br>
?>
</code>
</p>

<p>Stellen Sie sicher, dass das Verzeichnisse unter wikindx4/plugins (sowie die Dateien index.php und config.php) sowie die Verzeichnisse wikindx4/styles, wikindx4/languages und wikindx4/templates (einschließlich aller enthaltenen Dateien) für den Webserver Schreibrechte haben.</p>

<p>Inline-Plugins geben Informationen aus, die in einem von vier Containern dargestellt wird, die optional in einer Vorlagendatei (.tpl) positioniert werden können. Um die Position eines Containers zu ändern, müssen Sie die entsprechende .tpl-Datei bearbeiten.</p>

<p>Mindestens eine Vorlage, ein Bibliographiestil und eine Sprache müssen aktiviert bleiben. WIKINDX erwartet, dass das englische Sprachpaket auf dem Server verfügbar ist (d.h. dass Sie es nicht aus dem Verzeichnis wikindx4/languages/ gelöscht haben), ganz gleich ob es deaktiviert wurde oder nicht. Das englische Sprachpaket ist nötig, um alle Hinweistexte anzuzeigen, die evtl. in den anderen Sprachpaketen fehlen.</p>",

// END__LOCALIZATION__MODULE__EDIT
		);
	}
}
?>