<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017-2019 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**
* MESSAGES_en class (English)
* NOTE TO TRANSLATORS:  1/  '###' appearing anywhere in an array value will be replaced by text supplied by the core WIKINDX code.
*				Do not remove it.
*			2/  Do not change the key (the first part) of an array element.
*			3/  Ensure that each array element value is enclosed in double quotes "..." and is followed by a comma "...",
*			4/  The class name should be changed to match the (case-sensitive) name of
*				the folder your language files are in.  For example, if you are supplying a Klingon translation and
*				your languages/ folder is languages/kn/, the class name for the file SUCCESS.php
*				must be SUCCESS_kn.
*
* NOTE TO DEVELOPERS:  1/ Any comments not preceeded by '///' at the start of the line will be ignored by the localization module.
*				2/  All comments should be on one line (no line breaks) and must start at the beginning of the line for the localization module to work.
*				3/  Comments must be of the form '/// dbError_ This array does this' where 'dbError' is the array that the comment refers to or may be of the form
*					'/// dbError_open This is the comment' where 'dbError_open' is the exact array and array key that the comment refers to.
*/
class MESSAGES_en
{
// Constructor
	function __construct()
	{
	}
// English messages
	function loadArrays()
	{
		return array(
// START__LOCALIZATION__MODULE__EDIT
/// heading_ Page headings
		"heading" => array(
				"configure"	=>	"Configure Wikindx",
				"logon"		=>	"Logon",
/// heading_upgradeDB After installing a new version of wikindx, the database may require upgrading
				"upgradeDB" => "Upgrade the Database",
				"list"		=>	"List Resources",
				"search"	=>	"Search Resources",
				"select"	=>	"Select Resources",
				"selectMeta"	=>	"Select Metadata",
				"searchMeta"	=>	"Search Metadata",
				"addToBib"		=>	"Add selected to bibliography",
				"addToCategory"		=>	"Add selected to categories",
				"addToSubcategory"		=>	"Add selected to subcategories",
				"addToKeyword"		=>	"Add selected to keywords",
				"addToUserTag"		=>	"Add selected to user tags",
				"addToLanguage"		=>	"Add selected to languages",
				"newResource"	=>	"New Resource",
				"editResource"	=>	"Edit Resource",
				"editCategory"	=>	"Categories",
				"editSubcategory"	=>	"Subcategories",
				"edit"		=>	"Edit WIKINDX Resources ###",
				"add"		=>	"Add WIKINDX Resources ###",
				"delete2"	=>	"Delete WIKINDX Resources ###",
				"browseCreator"	=>	"Browse Creators",
				"browseCited"	=>	"Browse Cited Creators",
				"browseKeyword"	=>	"Browse Keywords",
				"browseCollection"	=>	"Browse Collections",
				"browseCategory"	=>	"Browse Categories",
				"browseSubcategory"	=>	"Browse Subcategories",
				"browseLanguage"	=>	"Languages",
				"browsePublisher"	=>	"Browse Publishers",
				"browseType"	=>	"Browse Resource Types",
				"browseYear"	=>	"Browse Publication Year",
				"browseBibliography"	=>	"Browse User Bibliogaphies",
				"browseUserTags"	=>	"User Tags",
				"browseUser"	=>	"Browse System Users",
				"browseDept"	=>	"Browse Departments",
				"browseInst"	=>	"Browse Institutions",
/// heading_categoryTree A category tree is a list of browsable categories with their associated keywords
				"categoryTree"	=>	"Category Tree",
				"statistics"	=>	"Statistics",
				"bibs"		=>	"Bibliographies",
				"preferences"	=>	"User Preferences",
				"resources"	=>	"WIKINDX Resources",
				"bookmark"	=>	"Bookmarks",
				"userAdd"		=>	"Add User",
				"userEdit"		=>	"Edit User",
				"userDelete"		=>	"Delete User",
				"userBlock"		=>	"Block and Unblock Users",
/// heading_basket Resource basket is a temporary collection of resources while the user is logged on
				"basket"	=>	"Resource basket",
				"basketDelete"	=>	"Delete Basket",
				"attach"	=>		"File Attachments ###",
				"url"	=>	"File URLs ###",
/// heading_myWikindx In a multi-user WIKINDX, settings for a user's bibliographies and personal details
				"myWikindx"	=>	"My Wikindx ###",
				"forget"	=>	"Forgotten Password",
				"delete"	=>	"Delete Resource",
				"adminKeywords"	=>	"Administer Keywords",
				"abstract"	=>	"Resource Abstract",
				"notes"		=>	"Resource Notes",
				"quotes"	=>	"Resource Quotes",
				"paraphrases"	=>	"Resource Paraphrases",
/// heading_musings Musings are thoughts about a resource
				"musings"	=>	"Resource Musings",
/// heading_ideas Ideas are general thoughts
				"ideas"	=>	"Ideas",
/// heading_ideaThread Ideas can be threaded as main idea and sub-ideas
				"ideaThread"	=>	"Idea Thread",
				"abstractDelete"	=>	"Delete Resource Abstract",
				"notesDelete"		=>	"Delete Resource Notes",
				"resourceEdit"	=>	"Edit Resource",
				"mergeCreators"	=>	"Merge Creators",
				"groupCreators"	=>	"Group Creators",
				"ungroupCreators"	=>	"Ungroup Creators",
				"adminCustom"	=>	"Administer Custom Fields",
				"adminLanguage"	=>	"Administer Languages",
				"adminImages"	=>	"Delete Images",
				"userEditField"	=>	"Resource Custom Field",
				"quarantine"	=>	"Quarantined Resources",
				"randomMetadata" => "Random Metadata",
				"cms"	=>	"Content Management System Gateway",
				"news"	=>	"News",
				"newsAdd"	=>	"Add News",
				"newsEdit"	=>	"Edit News",
				"newsDelete"	=>	"Delete News",
				"register"	=>	"Register User",
/// heading_emailFriend Email a single resource link to a friend
				"emailFriend" => "Email resource to friend",
				"bibtexImport"	=>	"Import BibTeX Bibliography",
				"exportBibtex"	=>	"Export BibTeX",
				"adminComponents"	=>	"Administer Components",
				"addCitation"	=>	"Add Citation",
/// heading_exportCoins COinS is a bibliographic format used by, for instance, Zotero. Don't translate 'COinS'
				"exportCoins"	=>	"Export COinS",
			),
/// authorize_ User authorization
		"authorize" => array(
				"writeLogon"	=>	"Enter your username and password to logon to the system. Your browser preferences must allow cookies for this domain.",
				"superLogon"	=>	"Enter your superadmin username and password to logon",
				"readOnly"	=>	"READ-ONLY access",
				"logonSuperadmin"	=>	"Logon as superAdmin:",
			),
/// config_ WIKINDX administrator/default configuration
		"config" => array(
				"start"		=>	"Before being able to use WIKINDX, you must configure it. The minimum initially required are username, password and email.
					From the Admin menu, after configuration, you can add and edit other settings.",
/// config_title This WIKINDX's title
				"title"		=>	"Title displayed on each page",
				"description"	=>	"Front Page Description",
				"superUsername"	=>	"Superadmin username",
				"superPassword"	=>	"Superadmin password",
				"deleteSeconds"	=>	"Seconds before export file is marked for deletion",
				"paging"	=>	"Default no. resources to display/screen (paging)",
/// config_pagingTagCloud Tag clouds are the display of creators, keywords, collections etc. found under the resources menu.
				"pagingTagCloud"	=>	"Default no. tag cloud items to display/screen (paging)",
				"maxPaging"	=>	"Default no. paging links to display/screen",
				"language"	=>	"Language",
				"template"	=>	"Template",
				"timezone"	=>	"Timezone",
				"imagesAllow" =>	"Allow images",
				"imagesMaxSize"	=>	"Images max. size",
				"tagLowColour"	=>	"Tag cloud, low colour",
				"tagHighColour"	=>	"Tag cloud, high colour",
				"tagLowSize"	=>	"Tag cloud, low size",
				"tagHighSize"	=>	"Tag cloud, high size",
				"deactivateResourceTypes"	=>	"Deactivated resource types",
				"activeResourceTypes"	=>	"Active resource types",
				"ldapUse"	=>	"Enable LDAP authentication",
				"ldapServer"	=>	"LDAP server",
				"ldapDn"	=>	"LDAP base DN",
				"ldapPort"	=>	"LDAP port",
				"ldapProtocolVersion"	=>	"LDAP protocol version",
				"ldapTestUsername"	=>	"Test LDAP user",
				"ldapTestPassword"	=>	"Test LDAP password",
				"ldapTransactionReport"	=>	"LDAP Test Transaction Report",
				"ldapTestSuccess"	=>	"LDAP successfully configured",
/// config_authGate If checked, user must click on OK after login in order to proceed. Used for situations such as notification about privacy policies such as that mandated by the EU's GDPR
				"authGate"	=>	"Authentication gate",
				"authGateMessage"	=>	"Message for user",
				"authGateReset"	=>	"Reset authentication gate flag for all users",
				"passwordSize"	=>	"No. chars in password",
				"passwordStrength"	=>	"Password strength",
				"passwordWeak"	=>	"Weak",
				"passwordMedium"	=>	"Medium",
				"passwordStrong"	=>	"Strong",
				"mailServer"	=>	"Enable mail operations",
				"mailFrom"	=>	"From address",
				"mailReplyTo"	=>	"Reply-to address",
				"mailBackend"	=>	"Mail backend",
				"mailReturnPath"	=>	"Return path",
				"mailSmPath"	=>	"Sendmail path",
				"mailSmtpServer"	=>	"SMTP server",
				"mailSmtpPort"	=>	"SMTP port",
				"mailSmtpEncrypt"	=>	"SMTP encryption",
				"mailSmtpPersist"	=>	"SMTP persist",
				"mailSmtpAuth"	=>	"SMTP authorization",
				"mailSmtpUsername"	=>	"SMTP username",
				"mailSmtpPassword"	=>	"SMTP password",
				"mailTest"	=>	"Test email address",
				"mailTestSuccess"	=>	"If you are reading this email, you have correctly configured email for WIKINDX",
				"mailTransactionReport"	=>	"Mail Test Transaction Report",
				"rssAllow"	=>	"Enable RSS",
				"rssDisplay"	=>	"Display",
				"rssLanguage"	=>	"Language",
				"rssBibstyle"	=>	"Bibliographic style",
				"rssTitle"	=>	"Title",
				"rssDescription"	=>	"Description",
				"rssLimit"	=>	"Display limit",
				"cmsAllow"	=>	"Enable CMS",
				"cmsDisplay"	=>	"Display",
				"cmsLanguage"	=>	"Language",
				"cmsBibstyle"	=>	"Bibliographic style",
				"cmsSql"	=>	"Enable SQL queries",
				"cmsDbUser"	=>	"Database username",
				"cmsDbPassword"	=>	"Database password",
				"gsAllow"	=>	"Enable Google Scholar",
				"gsAttachment"	=>	"Only attachments",
				'restrictUserId'	=>	"Restricted user",
				'options'	=>	"Configuration options",
				'superAdmin'	=>	"Super admin",
				'front'	=>	"Front page",
				'lists'	=>	"Resource lists",
				'display'	=>	"General display",
				'resources'	=>	"Resources",
				'users'	=>	"Users",
				'authentication'	=>	"Authentication",
				'email'	=>	"Email",
				'files'	=>	"Files/Attachments",
				'rss'	=>	"RSS",
				'cms'	=>	"CMS",
				'gs'	=>	"Google Scholar",
				'misc'	=>	"Miscellaneous",
				'debugging'	=>	"Debugging",
/// config_templateMenu Template menus can have their levels reduced from sub-submenus to submenus to menus
				"templateMenu"	=>	"Template menu",
				"templateMenu1"	=>	"All menu levels",
				"templateMenu2"	=>	"Reduce by one level",
				"templateMenu3"	=>	"Reduce by two levels",
				"style"		=>	"Bibliographic style",
				"missingXml"	=>	"If you have languages and styles that are in your WIKINDX installation folders but they do not show here, then they
					are not compatible with this version of WIKINDX.  Go to the WIKINDX Sourceforge download page
					(https://sourceforge.net/projects/wikindx/files/pluginsxxx/), download the latest localization and adminstyle plug-ins and use them
					to update your files.",
				"stringLimit"	=>	"Limit no. characters in select box text",
				"userRegistration"	=>	"Allow user registration",
/// config_registrationModerate Request emails be sent to the admin to approve or deny user registration requests
				"registrationModerate"	=>	"Moderate registration requests (you must provide a valid email address)",
/// config_registrationRequest1 Inform the user that his/her registration request requires approval by the admin
				"registrationRequest1"	=>	"Your request will require approval by the WIKINDX administrator",
				"registrationRequest2"	=>	"Please give the reasons for your registration request (this will be emailed to the administrator). At the
				very least you should give your name",
/// config_captcha1 User registration can use CAPTCHA technology
				"captcha1"	=>	"Answer the question if you are not a bot",
				"multiUser"	=>	"Multi user mode",
				"notify"	=>	"Email notification to registered users of resource additions and amendments",
				"statistics" => "Email view and download statistics to users",
				"imgWidthLimit"	=>	"Max. pixel width for images",
				"imgHeightLimit"	=>	"Max. pixel height for images",
// v3.0 - File attachments (uploading) to resources
				"fileAttach"	=>	"Allow file attachments",
				"fileViewLoggedOnOnly" => "Allow only registered users to view file attachments",
				"debug"	=>	"All these should be disabled on a production server",
				"errorReport"	=>	"Print PHP errors and warnings to the screen",
				"sqlStatements"	=>	"Display SQL statements",
				"printSql"	=>	"Print SQL statements to the screen",
				"emailSql1"	=> "Email SQL statements",
				"emailSql2"	=> "Email SQL statements to this address",
				"forceSmartyCompile"	=>	"Force Smarty templates compilation",
				"maxPaste"	=>	"Max. number of bibTeX entries a user can paste",
				"displayStatistics"	=>	"Display statistics",
				"displayUserStatistics"	=>	"Display user statistics",
				"kwBibliography"	=>	"When browsing a user bibliography, limit the keyword list to keywords found in that bibliography",
				"importBib"		=>	"Allow registered users to import BibTeX bibliographies",
/// config_lastChanges1 lastChanges2 and lastChanges3 are in a select box and syntactically follow on from lastChanges1 - * is a wildcard referring to the number entered by the user
				"lastChanges1"	=>	"Display on the front page",
				"lastChanges2"	=>	"Up to * recently added or edited resources",
				"lastChanges3"	=>	"Resources added or edited in the last * days",
/// config_lastChanges4 '...show up to a maximum of' is followed by a number
				"lastChanges4" => "If showing resources added or edited in the last number of days, show up to a maximum of",
/// config_useWikindxKey When displaying single resources or exporting a list to bibTeX,  (probably not necessary to translate 'ID' -- the database resource ID)
				"useWikindxKey" => "Use the WIKINDX-generated bibTeX key (author.ID) in preference to the default authorYear bibTeX key",
				"useBibtexKey"	=>	"Display the bibTeX key from the original bibliographic import (if available) in preference to either the
					WIKINDX-generated bibTeX key or the authorYear bibTeX key",
				"emailNews"	=>	"Email news items to registered users",
				"emailNewRegistrations" => "When a new user registers, notify the administrator at this email address",
				"metadataAllow" => "Enable the metadata subsystem (quotes, paraphrases, comments etc.) for all users",
				"metadataUserOnly" => "If disabled, allow registered users to still use the metadata subsystem",
/// config_updateDB1 A warning issued if a newly upgraded wikindx requires database upgrading.
				"upgradeDB1" => "<p><strong><font color=\"red\">If you have downloaded beta/SVN code, then you are strongly advised not to run it on a production server -- if you wish to test it, either create a new database or make a copy of your existing WIKINDX database and point config.php at it. Wait until all bugs have been dealt with, and the final release version provided, before using the WIKINDX code on a production server.</font>
				<p>To report bugs etc., go to: https://sourceforge.net/p/wikindx/v5bugs/</strong></p> WIKINDX has detected that this is the first time a new version is being run and that the database requires upgrading. This upgrading is automatic but only the WIKINDX superAdmin may carry it out.<p> You are <strong>strongly advised</strong> to <strong>back up your old database</strong> first. If you do not do this and you experience the memory errors detailed below when upgrading your only copy of the database, then you will have irrevocably corrupted the database: <em>caveat upgrader</em><p>",
				"upgradeDB2" => "You are not logged on as the superAdmin: please ask that person ### to carry out the upgrade through their web browser.",
				"upgradeDB3" => "The upgrade process may take some time depending upon a number of factors. If your database is large or your server is slow, it is advised to temporarily increase 'max_execution_time' and 'memory_limit' in php.ini and to restart the web server before upgrading.
				(You can try to increase memory first in config.php -- WIKINDX_MEMORY_LIMIT -- in which case you do not need to restart the server.)
				During upgrading, PHP error reporting is turned on regardless of your config.php settings.<p>
				If you get a blank page or an error similar to: 'Fatal error: Allowed memory size of 16777216 bytes exhausted (tried to allocate
				38 bytes)', then you must start the upgrade process from the <strong>beginning</strong>:<br>
				a) Reload your backed-up v.3.8.2 or v.4.x database or database tables (you have backed up haven't you?);<br>
				b) Increase PHP memory as per the instructions above (after upgrading, you can set it back to the default 32M);<br>
				c)  Restart the upgrade process.<p>
				The upgrade comprises several stages for a v3.8 database (a v4 database is upgraded in one step): as a very rough guide (and
				depending upon the speed of your server), the longest stages will take about
				25 seconds for a database of 23,000 records and the greatest memory use will be about 50MB. Some of the upgrade stages that are most likely
				to take the greatest time will safely halt if the scripts notice that 'max_execution_time' is about to be exceeded -- simply keep clicking
				on the Continue button.<p>
				WIKINDX v4 and higher ensures that all characters are encoded as UTF-8 (to account for character sets found in many of the world's languages)
					and the upgrade
					process does the necessary conversion.  If, however, after the upgrade is complete, you still see characters similar to 'ã¼' or 'ã¶'
					etc., then you should install and run the repairkit plugin from the Sourceforge.net WIKINDX site.<p>
					Do not break the process or use browser back or forward buttons.<p>
					<strong>Before upgrading, you should ensure that all attachments in wikindx3 or wikindx4 have been copied to the new wikindx5/attachments/
					folder -- the upgrade process will remove references to attachments in the database if the attachment files do not exist in the new
					location.</strong>",
				"upgradeFixUTF8-1"	=>	"Depending on what the sources were for your resources, you may have broken UTF-8 characters.  If you opt not
					to fix such instances now and discover after the upgrade that you do have broken UTF-8 characters, you can either run the upgrade again,
					reloading the original database, and check the checkbox here or you can download and run the repairkit plugin (which gives more
					control over what is fixed).",
				"upgradeFixUTF8-2"	=>	"Fix UTF8:",
				"displayBibtexLink"	=>	"When viewing lists of resources, display an icon to view the bibtex version of each resource",
				"displayCmsLink"	=>	"When viewing lists of resources, display a hyperlink to a pop-up window to generate a CMS (Content Management System)
					'replacement tag' for each resource",
				"pagingStyle"	=>	"When viewing lists ordered by creator or title, replace the numerical paging links with an alphabetical list",
				"quarantine"	=>	"Quarantine new resources from public view until approved by an ADMIN",
				"listlink" => "For resource lists, make each resource hyperlinked to viewing that resource",
				"noSort"	=>	"Ignore list (sorting)",
				"searchFilter"	=>	"Ignore list (searching)",
				"denyReadOnly" => "Deny read only access",
				"readOnlyAccess" => "If read only access is allowed, bypass the login prompt",
				"originatorEditOnly" => "Only admins and the resource originator can edit the resource",
				"globalEdit" => "Registered users can globally edit creators, collections, publishers and keywords",
			),
/// viewResource_ Viewing a single resource
		"viewResource" => array(
/// viewResource_viewIndex Total views / no. days available
				"viewIndex"	=>	"Views index: ###%",
/// viewResource_download Total downloads / no. days available
				"download"	=>	"Downloads index: ###%",
				"popIndex"	=>	"Popularity index: ###%",
				"numDownloads"	=>	"Downloads: ###",
/// viewResource_maturityIndex The maturity index is another index input by the administrator based on the popularity, num. downloads, number of metadata and user reviews.
				"maturityIndex"	=>	"Maturity index: ###",
				"numAccesses"		=>	"Views: ###",
				"type"		=>	"Resource type",
				"notes"		=>	"Notes",
				"quotes"	=>	"Quotes",
				"paraphrases"	=>	"Paraphrases",
				"musings"	=>	"Musings",
				"attachments"	=>		"Attachments",
				"urls"	=>	"URLs",
				"language"	=>	"Language",
				"viewDetails" => "View all bibliographic details",
			),
/// metadata_ Metadata are quotes, paraphrases, musings and comments
/// metadata_ Metadata are quotes, paraphrases, musings and comments
		"metadata" => array(
				"quotes"	=>	"Quotes",
				"paraphrases"	=>	"Paraphrases",
				"musings"	=>	"Musings",
				"ideas"	=>	"Ideas",
				"idea"	=>	"Idea",
				"subIdea"	=>	"Sub-idea",
				"quoteComments"	=>	"Quote comments",
				"paraphraseComments"	=>	"Paraphrase comments",
			),
/// resources_ Messages for resources
		"resources" => array(
				"new"		=>	"Do not put any punctuation at the end of text fields: WIKINDX will do this automatically depending on
				the bibliographic style. You are responsible for correct capitalization of titles and proper names. Fields marked with a###
				are required unless you have selected from the relevant select box. Text in a text field will override the selected option for any field.
				Generally, all numbers should be given as arabic cardinal numbers (1, 2, 3...) as the bibliographic style handles any reformatting
				they require. Pages can be roman numerals and years can be entered as a range (e.g. 2001-2004) or with a modifier (e.g. 'BC', 'BCE' etc.) –
				where there is a date field with a drop-down calendar, a valid entry here will override corresponding day, month, and year fields.
				Due to the automatic completion of some fields (e.g. with resources belonging to a collection or a conference, such as a journal
				or proceedings article), it is best to complete the resource details from the top of the form down.",
				"titleLabel"	=>	"Enter the resource titles. If the resource is a book chapter, the title should be an integer
				(the chapter number) and subtitle and short title will be ignored.",
/// resources_type book, article in book, web resource, thesis etc.
				"type"		=>	"Resource type and title",
				"title"		=>	"Title",
				"subtitle"	=>	"Subtitle",
/// resources_shortTitle Short title of a resource
				"shortTitle"	=>	"Short title",
				"bookTitle"	=>	"Title of book",
/// resources_numContributors 'Contributors' are creators, authors, editors, translators etc.
				"numContributors" =>	"Select the number of contributors to this resource",
/// resources_next Hyperlinks for displaying next resource and previous resource
				"next"			=>	"next resource",
				"previous"		=>	"previous resource",
				"lastChanges"	=>	"Recent additions or edits",
/// resources_pagingStart Paging system (number of resources to display/browser page.
				"pagingStart"	=>	"Start",
				"pagingEnd"	=>	"End",
				"noResult"	=>	"No resources found matching your selection",
/// resources_withChecked For adding or deleting resource lists or selected resources in the list to categories, keywords or user bibliographies.  This is followed by a select box of options as 'With checked: add to categories'
				"withChecked"	=>	"With checked",
				"addToBib"		=>	"Add to user bibliography",
/// resources_deleteFromBib Remove selected resources from the user bibliographay currently being browsed
				"deleteFromBib"	=>	"Remove from this user bibliography",
				"deleteResource"	=>	"Delete",
				"addToCategory"	=>	"Add to categories",
				"addToSubcategory"	=>	"Add to subcategories",
				"addToKeyword"	=>	"Add to keywords",
				"addToLanguage"	=>	"Add to languages",
				"addToUserTag"	=>	"Add to user tags",
/// resource_exportCoins1 COinS is the bibliographic format that is used, for instance, by zotero
				"exportCoins1"	=>	"Export to COinS",
				"exportCoins2"	=>	"This page contains hidden code in COinS format of the selected resources from the list. This allows you to
				import the resources into bibliographic software that recognizes the COinS format (e.g. Zotero)",
				"selectAll"		=>	"Use all in list:",
				"selectDisplay"		=>	"Use all displayed:",
				"selectCheck"		=>	"Use all checked:",
/// resources_organize This is displayed in a selectbox when viewing a list so that users can add/remove the list to/from categories, keywords etc.
				"organize"	=>	"Organize",
/// resources_general 'General' is the catch-all default category
				"general"	=>	"General",
				"keyword"	=>	"Keyword",
				"glossary"	=>	"Glossary",
				"glossaryMerge" => "One or more of the merged keywords has a glossary entry. You can add or edit the target keyword's glossary.",
				"creator"	=>	"Creator ###",
				"firstname"	=>	"Firstnames",
				"initials"	=>	"Initials",
				"surname"	=>	"Last name",
/// resources_prefix Prefix for surname - e.g. de Witt, della Croce, von Neumann
				"prefix"	=>	"Prefix",
				"collection"	=>	"Collection",
/// resources_collectionShort Abbreviated titles for journals etc.
				"collectionShort"	=>	"Short title",
				"publisher"	=>	"Publisher",
				"languages"	=>	"Languages",
				"languagesAdd"	=>	"Each resource can be defined as being available in one or more languages which are defined here",
				"languageDefault"	=>	"Default language",
				"categories"	=>	"Categories",
				"subcategories"	=>	"Subcategories",
				"userTags"	=>	"User tags",
				'subcategoryPart' => 'Subcategory is part of category',
				'subcategoryKeepCat' => 'Keep resources in original category',
				"keywords"	=>	"Keywords",
/// resources_publisherName Publisher name
				"publisherName"	=>	"Publisher name",
/// resources_publisherLocation Publisher location
				"publisherLocation" =>	"Publisher location",
				"institutionName"	=>	"Institution name",
				"institutionLocation" =>	"Institution location",
				"publicationYear" =>	"Publication year",
				"startPublicationYear" =>	"Start publication year",
				"endPublicationYear" =>	"End publication year",
				"date"		=>	"Date",
				"year"		=>	"Year",
				"startYear"	=>	"Start Year",
				"endYear"	=>	"End Year",
				"day"		=>	"Day",
				'startDay' => 'Start day',
				'endDay' => 'End day',
				"month"		=>	"Month",
				"startMonth"	=>	"Start month",
				"endMonth"	=>	"End month",
				"bibliographies"	=>	"Bibliographies",
				"quote"		=>	"Quote",
				"paraphrase"	=>	"Paraphrase",
				"musing"	=>	"Musing",
/// resources_commentPrivate For public/private comments and musings (first value is above next three which are in a select box)
				"commentPrivate"	=>	"Comment is",
				"musingPrivate"	=>	"Musing is",
				"ideaPrivate"	=>	"Idea is",
				"public"			=>	"Public",
				"private"			=>	"Private",
				"availableToGroups" => "Available to User Group: ###",
/// resources_isbn International Standard Book Number
				"isbn"		=>	"ID no. (ISBN etc.)",
/// resources_doi Digital Object Identifier (http://doi.org).  Don't translate this.
				"doi"	=> "DOI",
				"abstract"	=>	"Abstract",
				"note"		=>	"Note",
/// resources_citedResources Other resources citing this resource where '###' is the name of the user bibliography or 'WIKINDX Master Bibliography'
				"citedResources"	=>	"Resources citing this ###",
				"noUsers"	=>	"There are no users in the database",
/// resources_paragraph Paragraph and section in resources
				"paragraph"		=>	"Paragraph",
				"section"		=>	"Section",
/// resources_chapter A numeric chapter in a book
				"chapter"	=>	"Chapter",
				"comment"	=>	"Comments",
				"fileName"		=>	"Filename",
/// resources_primaryAttachment User can specify one attachment from the resources attachments to be displayed first
				"primaryAttachment"	=>	"Display this attachment first",
/// resources_attachmentDescription Descritpion of the attachment
				"attachmentDescription"	=>	"Attachment description",
/// resources_attachmentReadMe Open the attachment description.  Keep it short
				"attachmentReadMe"	=>	"(Desc.)",
				"fileAttachments" => "There are three ways to attach files to resources.",
/// resources_fileAttach For file attachments/uploading to resources
				"fileAttach"		=>	"Attach a single file to this resource",
/// resources_fileAttachMultiple For file attachments/uploading to resources
				"fileAttachMultiple"		=>	"Attach one or more files to this resource",
/// resources_fileAttachDragAndDrop For file attachments/uploading to resources
				"fileAttachDragAndDrop"		=>	"Drag and drop files here to attach them to this resource",
/// resources_fileAttachFallback Displayed when drag and drop is not avilable in the browser
				"fileAttachFallback"		=>	"Drag and drop is not available in this browser",
/// resources_attachEmbargo Store the attachment but keep it from public view for a period of time
				"attachEmbargo"	=>	"Embargo this attachment until the specified date:",
				"attachEmbargoMultiple"	=>	"Embargo these attachments until the specified date:",
				"embargoed"	=>	"(One or more attachments embargoed)",
				"fileName"		=>	"Filename",
				"deleteConfirmAttach"		=>	"Delete attachment(s) ###",
				"deleteConfirmKeywords"		=>	"Delete keyword(s) ###",
				"currentAttachments"	=>	"Current attachments",
/// resources_url For adding URLs to resources
				"url"	=>	"URL",
				"urlLabel"	=>	"URL label",
/// resources_primaryUrl User can specify one URL from the resources URLs to be displayed first
				"primaryUrl"	=>	"Display this URL first",
				"deleteConfirmUrl"		=>	"Delete URL(s) ###",
				"usertags"	=>	"User tags",
/// resources_warningOrganize A warning shown when organizing a list of resources (search, select etc.) into categories, keywords etc.
				"warningOrganize"	=>	"If you select nothing for item labels that are displayed, opt to replace the existing set
				and then save, you are removing those item labels from all selected resources. A resource must always belong to a category;
				if you attempt to save with categories removed, the resources will be placed in category 'General'.  ",
				"replaceExisting"	=>	"When adding items, you may either append the selections above to an existing set for
				each resource or replace the existing set with the selections.  Replace existing set:",
/// resources_availableKeywords Existing keywords stored in the database
				"availableKeywords"	=>	"Available keywords",
/// resources_availableUserTags Existing user tags stored in the database
				"availableUserTags"	=>	"Available user tags",
/// resources_translatedFrom  For details of original publication of a translated book
				"translatedFrom"	=>	"Original details of a translated book",
				"originalTitle"	=>	"Original title",
				'miscellaneous'	=>	'Miscellaneous details',
				'commonDetails'	=>	'Common details',
				'customFields'	=>	'Custom fields',
/// resources_addNewResourceToBib When adding a new resource, user can add the resource to their user bibliographies
				"addNewResourceToBib"	=>	"Add to user bibliographies",
				"series"	=> "Series",
				"seriesTitle"	=>	"Series title",
				"seriesNumber"	=>	"Series number",
				"volume"	=>	"Volume",
				"numberOfVolumes" =>	"Number of volumes in set",
				"bookVolumeNumber" =>	"This volume number",
				"volumeYear"	=>	"Volume set publication year",
				"numPages"	=>	"Number of pages",
				"reprintYear"	=>	"Reprint year",
				"revisionYear"	=>	"Revision year",
				"edition"	=>	"Edition",
				"pageStart"	=>	"Page start",
				"pageEnd"	=>	"Page end",
/// resources_peerReviewed Resource has been peer reviewed
				"peerReviewed"	=>	"Peer reviewed",
				"journalVolumeNumber" => "Journal volume number",
				"journalIssueNumber" => "Journal issue number",
				"journal"	=>	"Journal",
				'book'	=>	'Book',
				'newspaper'	=>	'Newspaper',
				'proceedings'	=>	'Proceedings',
				"encyclopedia"	=>	"Encyclopedia",
				"section"	=>	"Section",
				"city"		=>	"City",
				"proceedingsVolumeNumber"	=> "Proceedings volume number",
				"publicationDay" => "Publication day",
				"publicationMonth" => "Publication month",
/// resources_department Department reports and documentation were issued from
				"department"	=>	"Department",
				"thesis"	=>	"Thesis/Dissertation",
				"thesisType"	=>	"Degree level",
				"thesisLabel"	=>	"Thesis label",
				"thesisThesis"	=>	"thesis",
				"thesisDissertation"	=>	"dissertation",
				"institution"	=>	"Institution",
				"thesisYear"	=>	"Year of awarding",
/// resources_distributor Film distributor/studio
				"distributor"	=>	"Distributor",
				"country"	=>	"Country",
				"hours"		=>	"Hours",
				"minutes"	=>	"Minutes",
/// resources_channel TV or radio channel
				"channel"	=>	"Broadcast channel",
				'channelLocation'	=>	'Channel location',
/// resources_version Computer program
				"version"		=>	"Version",
				"typeOfSoftware"	=>	"Type of Software",
/// resources_medium Medium = 'oil on canvas', 'marble sculpture', 'multimedia show' etc. usually for art works
				"medium"	=>	"Medium",
/// resources_reporter For legal cases
				"reporter"	=>	"Reporter",
				"caseYear" 	=>	"Year",
				"court"		=>	"Court",
				"reporterVolume"	=>	"Reporter Volume",
/// resources_bill Parliamentary bills/laws
				"bill"		=>	"Bill",
				"code"		=>	"Code",
				"legislativeBody"	=>	"Legislative Body",
				"legislativeLocation" => "Location",
/// resources_session Parliamentary session
				"session"	=>	"Session",
				"codeVolume"	=>	"Code Volume",
				"billNumber"	=>	"Bill Number",
				"sessionYear"	=>	"Session Year",
				"conference"	=>	"Conference",
				"conferenceOrganiser"	=>	"Conference organizer",
				"conferenceLocation"	=>	"Conference location",
				"organiser"	=>	"Organizer",
/// resources_ruleType Legal Rulings/Regulations
				"ruleType"	=>	"Type of Ruling",
				"ruleNumber"	=>	"Rule Number",
/// resources_issueNumber report issue number
				"issueNumber"	=>	"Issue Number",
				"typeOfReport"	=>	"Type of Report",
/// resources_committee Government/legal hearings
				"committee"	=>	"Committee",
				"documentNumber"	=>	"Document Number",
				"magazine"	=>	"Magazine",
				"hearing"	=>	"Hearing",
/// resources_typeOfArticle Type of article (interview, review, advert etc.) in magazines
				"typeOfArticle"	=>	"Type of Article",
/// resources_typeOfManuscript Manuscripts
				"typeOfManuscript"	=>	"Type of Manuscript",
				"manuscriptNumber"	=>	"Manuscript Number",
/// resources_map Maps
				"map"		=>	"Map",
				"typeOfMap"	=>	"Type of Map",
/// resources_nameOfFile Charts/images
				"nameOfFile"	=>	"Name of File",
/// resources_imageProgram Software used to display image
				"imageProgram"	=>	"Image Program",
				"imageSize"	=>	"Image Size",
				"imageType"	=>	"Image Type",
				'number'	=>	'Number',
/// resources_publicLawNumber Legal Statutes
				"publicLawNumber"	=>	"Public Law Number",
				"codeNumber"	=>	"Code Number",
/// resources_publishedSource Patents
				"publishedSource"	=>	"Published Source",
				"patentVersionNumber"	=>	"Patent Version Number",
				"patentNumber"	=>	"Patent Number",
				"applicationNumber"	=>	"Application Number",
				"patentType"	=>	"Patent Type",
				"intPatentNumber"	=>	"International Patent Number",
				"intPatentClassification"	=>	"International Patent Classification",
				"intPatentTitle"	=>	"International Patent Title",
				"legalStatus"	=>	"Legal Status",
/// resources_assignee Who is the patent assigned to?
				"assignee"	=>	"Assignee",
/// resources_typeOfCommunication Personal Communication
				"typeOfCommunication"	=>	"Type of Communication",
/// resources_typeOfWork Unpublished work
				"typeOfWork"		=>	"Type of Work",
/// resources_recordLabel Music
				"recordLabel"	=>	"Record Label",
				"album"		=>	"Album",
/// resources_duplicate Add a new resource with the same title as an existing one (used when adding/editing a resource)
				"duplicate" => "Allow the duplicate resource",
				"page"		=>	"Page",
				"basketAdd"		=>	"Add to basket",
				"basketRemove"		=>	"Remove from basket",
			),
/// hint_ Hint messages.  Helpful tips usually displayed in smaller text
		"hint" => array(
				"hint"		=>	"Hint",
				"addedBy"	=>	"Added by: ###",
				"editedBy"	=>	"Last edited by: ###",
				"stringLimit" => "-1 is unlimited. Default is 40",
				"lastChanges"	=>	"-1 is unlimited. Default is 10",
				"lastChangesDayLimit"	=>	"-1 is unlimited. Default is 10",
				"pagingLimit"	=>	"-1 is unlimited. Default is 20",
				"pagingMaxLinks" => "Default is 11",
				"pagingTagCloud" => "-1 is unlimited. Default is 100",
				"title"	=>	"Default is 'WIKINDX'",
				"contactEmail"		=>	"Contact email displayed on the front page",
/// hint_pagingInfo Paging system e.g. 'Displaying 24-32 ...'
				"pagingInfo"	=>	"Displaying ### ",
/// hint_pagingInfoOf Follows hint_pagingInfo: '... of 345'
				"pagingInfoOf"	=>	"of ###",
				"mailServerRequired" =>	"Requires a mail server to be enabled and configured",
				"registerEmail"	=>	"An email will be sent with further instructions",
				"multiples"	=>	"Multiples can be chosen",
				"imgWidthLimit"	=>	"Default is 400",
				"imgHeightLimit"	=>	"Default is 400",
				"deleteSeconds"	=>	"Default is 3600",
				"timezone"	=>	"The default timezone. This should be set to the timezone of your server installation",
				"imagesAllow" =>	"Images can be inserted into abstracts, notes, and other metadata as well as the word processor if that plugin is being used. These images can be a link to an external URL or an image selected from a directory that registered users can upload to: the images/ directory at the top level of wikindx/. Checking the checkbox will allow the uploading of images",
				"imagesMaxSize"	=>	"Maximum filesize of uploaded images in megabytes (e.g. 10 or 5.6). Default is 5",
				"tagCloud"	=>	"When browsing tag clouds (for example, Browse Keywords in the Resources menu), you can indicate the frequency of the data by a range of colours and font sizes from low to high. Font sizes are given in em values. NB the background colour and the way the hyperlinks are displayed are given in the template's CSS file as the classes 'browseLink' and 'browseParagraph'. Default font size is 1 for low and 2 for high",
				"deactivateResourceTypes"	=>	"The importing and exporting of bibliographies and entry of new resources will ignore resource types that are deactivated. Deactivated resource types that are already in the database will not have their display affected. At least one type must remain active. Multiples can be chosen",
				"ldapDn"	=>	"For example, 'o=My Company, c=US'",
				"ldapTest"	=>	"Enter a valid LDAP username/password here to test the LDAP configuration when you click on Proceed",
				"authGate"	=>	"If checked, user must click on OK after login in order to proceed. Used for situations such as notification about privacy policies such as that mandated by the EU's GDPR. Once the user has clicked on OK, they are not required to do so again until the authentication gate flag is reset.",
				"passwordSize"	=>	"Minimum of 6 characters.",
				"passwordStrength"	=>	"Weak: Password must  be a mix of UPPER/lowercase. Spaces are not allowed. Medium: Password must be a mix of UPPER/lowercase and numbers. Spaces are not allowed. Strong: Password must be a mix of UPPER/lowercase, numbers, and non-alphanumeric characters ($ @ # ! % * ? & €). Spaces are not allowed.",
				"mailFrom"	=>	"Email address for the 'From: ' field. Default is the WIKINDX title. However, some email hosts will not accept email without a valid email address here",
				"mailReplyTo"	=>	"Email address for the 'Reply-to: ' field. Default is 'noreply@noreply.org'",
				"mailReturnPath"	=>	"If required, enter the 5th mail field here (the return path – possibly something like '-f postmaster@domain.dom' including single-quotes). Otherwise, or if unsure, leave blank. Setting this field will override the mail.force_extra_parameters parameter in php.ini",
				"mailSmPath"	=>	"The default sendmail path is '/usr/sbin/sendmail'",
				"mailSmtpServer"	=>	"e.g. smtp.mydomain.org. Default is 'localhost'",
				"mailSmtpPersist"	=>	"Check to keep the SMTP port open for multiple calls",
				"mailSmtpPort"	=>	"Default is 25",
				"mailSmtpAuth"	=>	"If checked, set the username and password",
				"mailTest"	=>	"Enter a valid email address here to test the mail configuration when you click on Proceed",
				"maxPaste"	=>	"To disable registered users from pasting BibTeX entries, set the value to 0. Default is 10",
				"rssAllow"	=>	"This is only required if you wish to run a RSS news feed for the latest additions to your WIKINDX. See README_RSS. If you do do not check this, WIKINDX will block RSS access and will not display any RSS icon in the Firefox location bar. If checked, to allow RSS feed users to then click on the RSS link and access WIKINDX, you must also enable read only access",
				"rssDisplay"	=>	"Uncheck to display only recently added resources or check to display recently added AND edited resources",
				"rssLimit"	=>	"Number of recent items to send to the feed. Default is 10",
				"cmsAllow"	=>	"This is only required if you wish to use WIKINDX to print individual resources or lists directly to a Content Management System. See README_CMS. If checked, any user, regardless of the general access settings may use the WIKINDX CMS hooks as shown in README_CMS to display WIKINDX content in their CMS. If unchecked, WIKINDX will block access",
				"cmsSql"	=>	"If checked (and CMS access is enabled), users can send a SQL string to the database for more complex queries. If this is the case, and users (in their WIKINDX preferences) have set CMS to display, they will have a 'cms' link provided for resource lists that will give them a base64-encoded text string to use in their CMS as they wish. If you enable this option, WIKINDX will access the WIKINDX database using the username::password combination supplied here. It is your responsibility to ensure this user _only_ has SELECT privileges on the database otherwise you are at risk of users sending INSERT, UPDATE, DROP, EXECUTE etc. SQL commands!",
				"gsAllow"	=>	"Allow Google Scholar to index resources",
				"gsAttachment"	=>	"If checked, Google Scholar indexing will only occur where the resource has an attachment (if multiple attachments, only the primary is used). Additionally, to enable this, 'Allow only registered users to view file attachments' must be checked",
				'restrictUserId'	=>	"Stop this write-enabled user changing login details. Typically this is used for a guest/test user (as on the WIKINDX testdrive database)",
				"forceSmartyCompile"	=>	"If checked, Smarty templates are re-compiled for each web page load",
				"displayStatistics"	=>	"Display statistics to read-only users",
				"displayUserStatistics"	=>	"If displaying statistics to read-only users, also display user statistics",
/// hint_initials For initials input in creator names (keep it short)
				"initials"	=>	"e.g. M.N. or Th.V.",
/// hint_maturityIndex For setting the maturity index for a resource, the range of allowable input is from 0 (inc. decimal points) to 10.
				"maturityIndex"	=>	"0.0 to 10",
				"keywordList"	=>	"comma-separated list",
				"categories"	=>	"Each resource must belong to at least one category",
				"keywords"	=>	"Select keywords from the list and/or enter them as a comma-separated list",
				"keywordsAlt"	=>	"Enter keywords as a comma-separated list",
				"userTags"	=>	"Select user tags from the list and/or enter them as a comma-separated list",
				"keywordsUserTags"	=>	"Select keywords and user tags from the lists and/or enter them as comma-separated lists",
/// hint_availableKeywords When editing the keywords after viewing a single resource, users can copy and paste keywords from one box to the other
				"availableKeywords"	=>	"Copy and paste from this list to the first list",
				"capitals"	=>	"Braces e.g. {BibTeX Specs.} maintain the case whatever the requirements of the bibliographic style",
				"publicationYear" =>	"Year of original publication",
				"revisionYear"	=>	"Year of last substantial revision",
				"url"		=>	"http://...",
				"doi"		=>	"e.g. 10.1234/56789",
				'arabicNumeral1'	=>	'Arabic numeral',
				'arabicNumeral2'	=>	'All numbers should be Arabic numerals.',
/// hint_dateAccessed Date of accessing a web-based resource
				"dateAccessed"	=>	"Date accessed",
				"thesisAbstract" =>	"If the thesis has had an abstract of it published in a journal, enter those details here",
				"thesisType"	=>	"e.g. PhD, masters...",
				"thesisLabel"	=>	"e.g. dissertation, thesis...",
/// hint_runningTime length of audiovisual material (film etc.)
				"runningTime"	=>	"Running time",
				"broadcastDate"	=>	"Broadcast Date",
/// hint_dateDecided Date legal case decided
				"dateDecided"	=>	"Date Case Decided",
				"conferenceDate"	=>	"Conference date",
				"hearingDate"	=>	"Date of Hearing",
				"dateEnacted"	=>	"Date Enacted",
				"issueDate"	=>	"Issuing Date",
				"collection" => "If adding a resource to an existing collection or conference, after selecting the resource type, select the collection
				or conference from the select box first to automatically fill in other fields",
				"quote"		=>	"(To allow for free-form quoting such as: 'it is claimed that WIKINDX is \"amongst the best\" resources available', you are expected to distinguish the actual quote from surrounding text yourself)",
				"emailFriendAddress"	=>	"Separate multiple addresses with commas or semicolons",
				"noSort"	=>	"When ordering resources by title, ignore the following list of case-insensitive words if they are the first word of the title. Comma-separated list",
				"searchFilter"	=>	"When searching resources or metadata, ignore the following list of case-insensitive words if they are not part of an exact phrase. Comma-separated list",
/// hint_wordLogic Optional control words and formatting to use with search strings.  Don't change uppercase and don't change '\"'
				"wordLogic"	=>	"You can use combinations of: AND, OR, NOT and \"exact phrase\" as well as the wildcards '?' and '*'.",
/// hint_deleteConfirmBib When deleting a user bibliography, assurance that doing so does not delete any resources
				"deleteConfirmBib"	=>	"(this does not delete resources)",
				"password1"	=>	"Password must contain at least ### characters and be a mix of UPPER/lowercase. Spaces are not allowed.",
				"password2"	=>	"Password must contain at least ### characters and be a mix of UPPER/lowercase and numbers. Spaces are not allowed.",
				"password3"	=>	"Password must contain at least ### characters and be a mix of UPPER/lowercase, numbers, and non-alphanumeric characters ($ @ # ! % * ? & €). Spaces are not allowed.",
			),
/// menu_ Menu subsystem.  The trick here is to use short terms that don't cause problems with overflowing the CSS drop-down boxes - some browsers may happily handle this, others won't. Up to 15-16 characters (depending on character width) is a good guide - but check! NB!!!!!  For this array, the values should be unique where the keys form part of the same menu item.  For example, in the File menu, the 'file' key and the 'show' key should not have the same value of, for example, 'Files'.
		"menu" => array(
/// menu_home Wikindx menu starts here
							'home'		=>	"Home",
							"news"	=>	"News",
							'prefs'		=>	"Preferences",
							"register"	=>	"Register",
							"myWikindx"	=>	"My Wikindx",
							"bibs"		=>	"Bibliographies",
							'userLogon'	=>	"User Logon",
							'logout'		=>	"Logout",
							"statistics"	=>	"Statistics",
							'statisticsSub'	=>	"Statistics...",
							"statisticsTotals"	=>	"Totals",
							"statisticsUsers"	=>	"Users",
							"statisticsKeywords"	=>	"Keywords",
							"statisticsYears"	=>	"Publication years",
							"statisticsAllCreators"	=>	"All creators",
							"statisticsMainCreators"	=>	"Main creators", // i.e. authors not editors
							"statisticsPublishers"	=>	"Publishers",
							"statisticsCollections"	=>	"Collections",
							'toggleHelpOn'		=>	"Turn Help on",
							'toggleHelpOff'		=>	"Turn Help off",
							'about'		=>	"About Wikindx",
/// menu_res Resources menu starts here
							'res'		=>	"Resources", // top menu item
							'new'		=>	"New",
							"files"	=>	"List Exported Files",
/// menu_bookmarkSub Bookmarks submenu
							"bookmarkSub"	=>		"Bookmarks...",
							"bookmarkAdd"	=>	"Add bookmark",
							"bookmarkDelete"	=>	"Delete bookmarks",
/// menu_editSub edit submenu
							'editSub' => 'Edit...',
							'editSubCollection' => 'Collections...',
							'creator'	=>	"Creator",
							'collection'	=>	"Collection",
							'publisher'	=>	"Publisher",
							"keyword"	=>	"Keyword",
							'lastMulti'	=>	"Last Multi View",
							'lastSolo'	=>	"Last Solo View",
							"randomResource"	=>	"Random Resource",
							"basketSub"	=>	"Basket...",
/// menu_basketView Resource basket is a temporary collection of resources while the user is logged on
							"basketView"	=>	"View Basket",
							"basketDelete"	=>	"Delete Basket",
/// menu_pasteBibtex Ordinary user can paste a bibtex file into a textarea for importing
							'pasteBibtex'	=>	"Paste BibTeX",
/// menu_search Search menu starts here
							'advancedSearch'	=>	"Advanced Search",
							'search'	=>	"Search",
							'quickSearch'		=>	"Quick Search",
							'selectResource'		=>	"Select Resources",
/// menu_listSub Quick list submenu
							'listSub'		=>	"Quick List All...",
/// menu_listCreator 'asc.' and 'desc.' mean 'ascending' and 'descending' and refers to the list ordering
							'listCreator'	=>	"Creator asc.",
							'listTitle'	=>	"Title asc.",
							'listPublisher'	=>	"Publisher asc.",
							'listYear'	=>	"Year desc.",
							'listTimestamp'	=>	"Timestamp desc.",
							'listViews'	=>	"Views desc.",
							'listDownloads'	=>	"Downloads desc.",
							'listPopularity'	=>	"Popularity desc.",
							'listMaturity'	=>	"Maturity desc.",
							'searchResource'		=>	"Search Resources",
							'searchMeta'	=>	'Search Metadata',
							'selectMeta'	=>	'Select Metadata',
/// menu_browseSub browse submenu
							'browseSub' => 'Browse...',
							'browseSubCollection' => 'Collections...',
							'browseSubPublisher' => 'Publishers...',
							"browseCreator"	=>	"Creators",
							"browseCited"	=>	"Cited",
							"browseKeyword"	=>	"Keywords",
							"browseCollection"	=>	"Collections",
							"browseCategory"	=>	"Categories",
							"browseSubcategory"	=>	"Subcategories",
							"browseLanguage"	=>	"Languages",
							"browsePublisher"	=>	"Publishers",
							"browseType"	=>	"Types",
							"browseYear"	=>	"Years",
							"browseBibliography"	=>	"Bibliographies",
							"browseUserTags"	=>	"User Tags",
							"browseUser"	=>	"System Users",
							"browseDept"	=>	"Departments",
							"browseInst"	=>	"Institutions",
/// menu_categoryTree A category tree is a list of browsable categories with their associated keywords
							"categoryTree"	=>	"Category Tree",
/// menu_text Metadata menu starts here -- Metadata are ideas, quotes, paraphrases and musings
							"text"		=>	"Metadata",
							"addIdea"	=>	"Add Idea",
							"lastIdea"	=>	"Last Idea Thread",
							"listIdeas"	=>	"List Ideas",
							'lastIdeaSearch'	=>	"Last Idea Search",
/// menu_randomSub random submenu
							'randomSub' => 'Random...',
							"randomParaphrases" =>	"Paraphrase",
							"randomQuotes" =>	"Quote",
							"randomMusings" =>	"Musing",
							"randomIdeas" =>	"Idea",
/// menu_lastMultiMeta Last list of metadata
							'lastMultiMeta'	=>	"Last Multi View",
/// menu_plugin1 menu system for plugins (1 of 3)
							'plugin1'	=>	'Plugins (1)',
							'plugin2'	=>	'Plugins (2)',
							'plugin3'	=>	'Plugins (3)',
/// menu_admin Administrator menu starts here
							'admin'		=>	"Admin", // top menu item
							'conf'		=>	"Configure",
							"newsSub"	=>	"News...",
							"newsAdd"	=>	"Add",
							"newsDelete"	=>	"Delete",
							"newsEdit"	=>	"Edit",
							'importhelp'	=>	"Import Help",
							'confighelp'	=>	"Config Help",
							'categories'	=>	"Categories",
							'subcategories'	=>	"Subcategories",
							'language'	=>	"Languages",
							"images"	=>	"Images",
							'keywordSub'	=>	"Keywords...",
							"keywordEdit"	=>	"Edit",
							"keywordMerge"	=>	"Merge",
							"keywordDelete"	=>	"Delete",
							"creatorSub"	=>	"Creators...",
							"creatorMerge"	=>	"Merge",
							"creatorGroup"	=>	"Group",
							"creatorUngroup"	=>	"Ungroup",
							"delete"	=>	"Delete Resource",
							"userSub"		=>	"Users...",
							"userAdd"	=>	"Add",
							"userEdit"	=>	"Edit",
							"userDelete"	=>	"Delete",
							"userBlock"	=>	"Block/Unblock",
							"userRegistration"	=>	"Registrations",
							'importBibtex'		=>	"Import BibTeX",
							"quarantine"	=>	"Quarantine",
							"components"	=>	"Components",
/// menu_customSub The admin can administer custom database fields for resources
							"custom"	=>	"Custom Fields",
							"creators"	=>	"Creators",
			),
/// misc_ Miscellaneous items that don't fit anywhere else
		"misc" => array(
/// misc_toLeft text for an arrow to shift items from a box on the right to the left
				'toLeft'	=>	'Shift item from right to left',
/// misc_toRight text for an arrow to shift items from a box on the left to the right
				'toRight'	=>	'Shift item from left to right',
/// misc_toBottom text for an arrow to shift items from a box on the top to the bottom
				'toBottom'	=>	'Shift item from top to bottom',
				'toTop'	=>	'Shift item from bottom to top',
				"add"		=>	"add",
				"remove"	=>	"remove",
				"edit"		=>	"edit",
				"delete"	=>	"delete",
				"bibtex"	=>	"bibtex",
				"view"	=>	"view",
				"download"	=>	"download",
				"quarantine" => "Quarantined",
				'noResources'	=>	"There are no resources in the database",
				"noMetadata"	=>	"There are no metadata in the database",
				"noQuotes"	=>	"There are no quotes in the database",
				"noParaphrases"	=>	"There are no paraphrases in the database",
				"noMusings"	=>	"There are no musings in the database",
				"noIdeas"	=>	"There are no ideas in the database",
				'noResourcesBib'	=>	"There are no resources in that user bibliography",
				'noCategories'	=>	"There are no categories in the database",
				'noSubcategories'	=>	"There are no subcategories in the database",
				'noBibliographies'	=>	"There are no user bibliographies in the database",
				'noLanguages'	=>	"There are no languages in the database",
				'noUsertags'	=>	"There are no resources with user tags in the database",
				'noImages'	=>	"There are no images available",
				'usedImages'	=>	"Images used in metadata",
				'unusedImages'	=>	"Images not used in metadata",
/// misc_popIndex When viewing resources on the front page of the WIKINDX, display the popularity index.  This should be a short abbreviation
				"popIndex"	=>	"Pop. ###%",
/// misc_downIndex When viewing resources on the front page of the WIKINDX, display the download index.  This should be a short abbreviation
				"downIndex"	=>	"Down. ###%",
/// misc_matIndex When viewing resources on the front page of the WIKINDX, display the maturity index.  This should be a short abbreviation
				"matIndex"	=>	"Mat. ###",
/// misc_ignore In select boxes - when it is not necessary to choose an existing selection.  WIKINDX will skip over this one. Could be '---'
				'ignore'	=>	"IGNORE",
				'noCreators'	=>	"There are no creators in the database",
				'noGroupMasterCreators'	=>	"There are no group master creators in the database",
				'noCollections'	=>	"There are no collections in the database",
				'noPublishers'	=>	"There are no publishers in the database",
				"noKeywords"	=>	"There are no keywords in the database",
				"noMetaKeywords"	=>	"There are no metadata keywords in the database",
				"noCitations"	=>	"There are no cited creators in the database",
				"noUsers"	=>	"There are no registered users with resources in the database",
/// misc_categoryTreeKeywords When browsing the category tree, display keywords with resources in each category
				"categoryTreeKeywords"	=>	"Resource keywords in this category:",
/// misc_categoryTreeSubcategories When browsing the category tree, display subcategories with resources in each category
				"categoryTreeSubcategories"	=>	"Resource subcategories in this category:",
/// misc_set Used in SUCCESS.php when a user chooses a user bibliography to browse.  The message is "Successfully set Bibliography".
				"set"		=>	"set",
/// misc_keywordExists Advice on what to do when editing a keyword name and the new name already exists in the database.
				"keywordExists"	=>	"If you proceed, this edited keyword will be deleted and all references in the database to it will be replaced by references to the pre-existing keyword.",
/// misc_collectionExists Advice on what to do when editing a collection name and the new name already exists in the database.
				"collectionExists"	=>	"If you proceed, this edited collection will be deleted and all references in the database to it will be replaced by references to the pre-existing collection.",
/// misc_publisherExists Advice on what to do when editing a publisher name and the new name already exists in the database.
				"publisherExists"	=>	"If you proceed, this edited publisher will be deleted and all references in the database to it will be replaced by references to the pre-existing publisher.",
/// misc_emailToFriend Email a single resource link to a friend.
				"emailToFriend"	=>	"Email resource to friend",
/// misc_bibtexKey When viewing a single resource, display the bibtex citation key (such as 'aarseth.321')
				"bibtexKey"	=>	"BibTeX citation key",
/// misc_bookmarks User bookmarks
				"bookmark"	=>	"You may store up to 20 bookmarks for returning to a single or multi-resource view.",
				"bookmarkDelete"	=>	"As you already have 20 bookmarks, you must choose one to be replaced by this new addition",
				"bookmarkDeleteInit"	=>	"Select the bookmarks to be deleted",
				"bookmarkName"	=>	"Enter a short name for your bookmark",
				"edited"	=>	"edited",
				"added"		=>	"added",
				"deleted"	=>	"deleted",
/// misc_confirmDelete Ask for confirmation deleting a large number of resources
				"confirmDelete"	=>	"Delete ### resources?",
				"confirmDeleteLanguage"	=>	"Delete language(s)?",
/// misc_keywordMerge Admins can merge multiple keywords into one keyword
				"keywordMerge"	=>	"Select and merge multiple keywords into one keyword (the merged keywords will be deleted)",
				"keywordMergeTarget"	=>	"New or target keyword",
/// misc_creatorMerge Admins can merge multiple creators into one creator
				"creatorMerge"	=>	"Select and merge multiple creators into one creator (the merged creators will be deleted).  A selection in the target select box overrides any text input.",
				"creatorGroup"	=>	"Select and group multiple creators into one creator (the grouped creators will not be deleted).  List operations on the group master will include grouped creators. This is intended for situations where one person may be represented by different creator names but you wish to keep those creator names accurate to the original bibliographic source.",
				"creatorMergeOriginal"	=>	"Original creators",
				"creatorMergeTarget"	=>	"Target creator",
				"creatorGroupMember"	=>	"Group members",
				"creatorGroupMaster"	=>	"Group master",
				"creatorUngroup"	=>	"Select the creators you wish to remove from the group",
/// misc_tag Tag - when importing bibtex resources, the administrator can give a label to each import which makes it possible to later delete in one go resources that were imported under this label.
				"tag"		=>	"Delete resources by import tag",
/// misc_emailToFriend Email a single resource link to a friend.
				"emailToFriend"	=>	"Email resource to friend",
				"emailFriendAddress"	=>	"Email address",
				"emailFriendSubject"	=>	"Email subject",
				"emailFriendText" => "Email text",
/// misc_keywordImport1 When importing bibliographies from external sources, these give the user the option to specify whether keywords, in the source bibliography, are separated by commas or semicolons
				"keywordImport"	=>	"Keywords are separated in the source bibliography by",
				"keywordImport1" => "Commas",
				"keywordImport2" => "Semicolons",
				"keywordImport3" => "Commas or Semicolons",
				"keywordImport4" => "SPACES",
				"keywordIgnore"	=> "Ignore keywords:",
/// misc_titleSubtitleSeparator When importing bibtex and endnote bibliographies, split title and subtitle on specified characters
				"titleSubtitleSeparator" => "Split the title and subtitle in the source bibliography on the first occurrence of",
				"titleSubtitleSeparator1" => "NO SPLIT",
				"titleSubtitleSeparator2" => ": (colon)",
				"titleSubtitleSeparator3" => "; (semicolon)",
				"titleSubtitleSeparator4" => ". or ! or ?",
				"titleSubtitleSeparator5" => "-- (dash dash)",
				"titleSubtitleSeparator6" => "- (dash)",
/// misc_mergeStored When importing a bibliography (e.g. bibTeX), the admin has the choice of storing fields that wikindx does not recognise. Later, when exporting a bibliography, the user will have the choice of merging these unrecognised fields into the wikindx export. Additionally, any stored citation keys from the original import can be used in preference to a WIKINDX-generated one.
				"mergeStored"	=>	"If any fields that WIKINDX does not recognise are stored from an original import, merge these fields into this export:",
				"useOriginalCitation"	=>	"Use the citation keys from the original import (where available) in preference to WIKINDX generated keys:",
/// misc_shortString When exporting to bibTeX.  Don't translate @STRING.
				"shortString"	=>	"Use short titles for any @STRING values:",
/// misc_bibExportQuotes When exporting to bibtex, use either double quotes or braces to enclose field values. \"...\" must be given exactly as it is here
				"bibExportQuotes"	=>	"Use double quotes \"...\" to enclose field values",
				"bibExportBraces"	=>	"Use braces {...} to enclose field values",
				"bibExportCharacterSetUTF"	=>	"Use UTF-8 character encoding",
				"bibExportCharacterSetTex"	=>	"Use ISO-8859-1 and TeX character encoding",
				"bibExportKeywordSeparatorSemicolon"	=>	"Use semicolons ';' to separate keywords",
				"bibExportKeywordSeparatorComma"	=>	"Use commas ',' to separate keywords",
/// misc_exportMetadata1 User has option of exporting metadata to bibtex when exporting a bibliography
				"exportMetadata1"	=>	"Export metadata to bibtex.",
				"exportMetadata2"	=>	"Enter a unique field name for each metadata field you wish to export:",
/// misc_customFieldMap When exporting a bibliography to bibtex or endnote, if any custom fields exist in the SQL set, the user can map these to specified bibtex or endnote custom fields
				"customFieldMap" => "Map WIKINDX custom fields to export fields.",
				"customFieldMap2"	=>	"Enter a unique field name for each custom field you wish to export:",
				"help"	=>	"Help",
				"pluginsEnabled"	=>	"Enabled Plugins",
				"pluginsDisabled"	=>	"Disabled Plugins",
				"stylesEnabled"	=>	"Enabled Styles",
				"stylesDisabled"	=>	"Disabled Styles",
				"languagesEnabled"	=>	"Enabled Languages",
				"languagesDisabled"	=>	"Disabled Languages",
				"templatesEnabled"	=>	"Enabled Templates",
				"templatesDisabled"	=>	"Disabled Templates",
				"pluginsConfigureMenu"	=>	"Configurable Menu Plugins",
				"pluginsConfigureInline"	=>	"Configurable Inline Plugins",
				"pluginsMenus"	=>	"Menu Location",
				"pluginsContainers"	=> "Container Location",
				"pluginConfig"	=> "Plugin Config.php File",
				"pluginCommands"	=>	"Plugin Commands",
				"openReadme"	=>	"Open README file",
				"noReadme"	=>	"Plugin has no README file",
				"language" => "Resources can be labelled with a language when adding or editing resources",
				"languageAdd"	=>	"Add Language",
				"languageEdit"	=>	"Edit Language",
				"languageDelete"	=>	"Delete Language",
				"newsAdd"	=>	"Add News",
				"newsEdit"	=>	"Edit News",
				"newsDelete"	=>	"Delete News",
/// misc_goBack On some pages (e.g. resource add), a javascript 'Go Back' button is displayed.
				"goBack"	=>	"Cancel",
				"closePopup"	=>	"Close",
				"attachmentCache1"	=>	"Before proceeding, attachments need to be converted to text files and cached in order to speed up full-text searches. This process might take some time if you have a large number of attachments and/or large files so leave the script running until activity stops. Keep on eye on activity in the attachments_cache/ folder. WIKINDX tries to deal with max execution timeouts but if you find that caching is not proceeding, you might need to adjust PHP's max_execution_time. If you get a blank page, it means a timeout has occurred. If CURL is part of the PHP installation and you select this option, WIKINDX will attempt to convert files in multiple, simultaneous instances and, if a timeout occurs, the error message will be written to the cached file: you should either increase max_execution_time or turn off the use of CURL. In the latter case, conversion proceeds sequentially and slowly and, unless the file to be converted is far too large and WIKINDX does not have enough time to convert the one file, the converted files are guaranteed to be cached. The option of limiting the number of files cached at any one time is also provided. At any time, you can skip over caching of files until the next time you login or go to the front page.",
				"attachmentCache2"	=>	"### attachments remain to be cached.",
				"attachmentCache3"	=>	"### attachments cached thus far.",
/// misc_attachmentCache4 Don't translate 'CURL'
				"attachmentCache4"	=>	"Use CURL",
				"attachmentCache5"	=>	"Attempt to only cache this number of files at a time: ###",
				"attachmentCache6"	=>	"Skip caching",
				"fileAttachDeleteAll"	=>	"Delete all attachments for this resource",
			),
/// collection_ Messages relating to collections and publisher types
		"collection" => array(
				"chooseTypeBrowse"	=>	"Choose the type of collection you wish to browse",
				"book"	=>	"Book",
				"journal"	=>	"Journal",
				"web"	=>	"Web Site",
				"proceedings"	=>	"Proceedings",
				"newspaper"	=>	"Newspaper",
				"magazine"	=>	"Magazine",
				"thesis"	=>	"Thesis Abstracts",
				"music"	=>	"Music Recording",
				"manuscript"	=>	"Manuscript",
				"miscellaneous"	=>	"Miscellaneous",
				"institution"	=>	"Institution",
				"conference"	=>	"Conference",
				"legal"	=>	"Legal",
				"music"	=>	"Music",
				"distributor"	=>	"Distributor",
				"chart"	=>	"Chart",
/// collection_all When browsing collections, the user may browse ALL collections or by collection type.  'ALL' is is displayed at the top of a select box so should not be many words
				"all"	=> "ALL",
			),
/// list_ Plain listing of resources
		"list" => array(
				'listBy'	=>	"List all by",
				'creator'	=>	"First Creator",
				'title'		=>	"Title",
				'publisher'	=>	"Publisher",
				'year'		=>	"Publication Year",
				'timestamp'	=>	"Timestamp",
				'order'	=>		"Order by",
				"views" => "Views Index",
				"downloads" => "Downloads Index",
				"popularity" => "Popularity Index",
				"maturity" => "Maturity Index",
				"ascending"	=>	"Ascending",
				"descending"	=>	"Descending",
				"addUserIdResource"	=>	"Resource added by",
				"editUserIdResource"	=>	"Resource last edited by",
			),
/// select_ Select resources by...
		"select" => array(
				'selectBy'	=>	"Select by ###",
				'notInUserBib'	=>	"NOT in bibliography",
				'metadata'	=>	"Metadata type",
				"option"	=>	"Display options",
				'noAttachment'	=>	"With no attachments",
				'attachment'	=>	"With at least one attachment",
				'displayAttachment'	=>	"Display only attachments",
				"url"	=>	"With at least one external URL",
				"doi"	=>	"With DOI",
/// select_displayAttachmentZip 'Tar' is the UNIX function tar so should not be translated.  'Compress' is as in file compression.  This statement follows on from the previous one
				'displayAttachmentZip'	=>	"Tar and compress attachments",
				'displayPeerReviewed'	=>	"If peer reviewed",
				"addedBy"	=>	"Resource added by",
				"editedBy"	=>	"Resource last edited by",
/// select_field Database field such as 'title', 'abstract', 'quotes' etc.
				'field'		=>	"Search on Fields",
				'availableType'	=>	"Types",
				'type'		=>	"Selected Types",
				'availableKeyword'	=>	"Keywords",
				'keyword'	=>	"Selected Keywords",
				'availableMetaKeyword'	=>	"Metadata Keywords",
				'metaKeyword'	=>	"Selected Metadata Keywords",
				'availableUserTag'	=>	"User Tags",
				'userTag'	=>	"Selected User Tags",
				'availableCategory'	=>	"Categories",
				'category'	=>	"Selected Categories",
				'availableSubcategory'	=>	"Subcategories",
				'subcategory'	=>	"Selected Subcategories",
				"language"	=>	"Selected Languages",
				"availableLanguage"	=>	"Languages",
				'publisher'	=>	"Selected Publishers",
				"availablePublisher"	=>	"Publishers",
				'creator'	=>	"Selected Creators",
				"availableCreator"	=>	"Creators",
				'collection'	=>	"Selected Collections",
				"availableCollection"	=>	"Collections",
				"availableTag"		=>	"Import Tags",
				"tag"	=>	"Selected tags",
				"availableAddedBy"		=>	"Added By",
				"addedBy"	=>	"Selected Users",
				"availableEditedBy"		=>	"Edited By",
				"editedBy"	=>	"Selected Users",
				"noIdeas"	=>	"No ideas found matching your search",
			),
/// search_ Search the database
		"search" => array(
				"method"	=>	"Method",
				"word"		=>	"Search word(s)",
/// search_partial Match part of a word or search term must equal whole word?
				"partial"	=>	"Partial word match",
				"exact"		=>	"Exact phrase",
/// search_type Type = book, journal article, thesis etc.
				'type'		=>	"Type",
				"language"	=>	"Language",
				"publicationYear"	=>	"Publication Year",
				"access"	=>	"Number of Views",
				"maturityIndex"	=>	"Maturity Index",
				"title"		=>	"Title",
				"note"		=>	"Notes",
				"abstract"	=>	"Abstract",
				"quote"		=>	"Quote",
				"paraphrase"	=>	"Paraphrase",
				"quoteComment"		=>	"Quote comment",
				"paraphraseComment"	=>	"Paraphrase comment",
				"musing"	=>	"Musing",
				"attachments"	=>	"Attachments",
				"idea"	=>	"Idea",
				'creator'	=>	"Creator",
				"ideasFound"	=>	"Ideas have been found",
/// search_field Database field such as 'title', 'abstract', 'quotes' etc.
				'field'		=>	"Search on Fields",
				'metadata'	=>	"Metadata Type",
				'availableKeyword'	=>	"Keywords",
				'availableMetaKeyword'	=>	"Metadata Keywords",
				'keyword'	=>	"Selected Keywords",
				'availableUserTag'	=>	"User Tags",
				'userTag'	=>	"Selected User Tags",
				'availableCategory'	=>	"Categories",
				'category'	=>	"Selected Categories",
				'availableSubcategory'	=>	"Subcategories",
				'subcategory'	=>	"Subcategory",
				"category"	=>	"Category",
				"keyword"	=>	"Keyword",
				"metaKeyword"	=>	"Metadata keyword",
				"usertag"	=>	"User tag",
				"publisher"	=>	"Publisher",
				"collection"	=>	"Collection",
				"tag"	=>	"Import tag",
				"addedBy"	=>	"Resource added by",
				"editedBy"	=>	"Resource edited by",
				"searchSelect"	=>	"Search/Select",
				"test"	=>"View natural language",
				"naturalLanguage"	=>	"Results where",
			),
/// listParams_ When displaying the results of a list, select or search, display the selection parameters
		"listParams" => array(
				"word"		=>	"Search word(s)",
				"partial"	=>	"Partial word match",
				'field'		=>	"Field",
				"language"	=>	"Language",
				'type'		=>	"Type",
				"tag"		=>	"Import Tag",
				'attachment'	=>	"With at least one attachment",
				'collection'	=>	"Collection",
				"category"	=>	"Category",
				"subcategory"	=>	"Subcategory",
				"userTag"	=>	"User tag",
				'notInUserBib'	=>	"NOT in user bibliography",
				'publisher'	=>	"Publisher",
				"keyword"	=>	"Keyword",
				'creator'	=>	"Creator",
				'list'		=>	'List (###)',
				'addedBy'	=>	'Resource added by ###',
				'editedBy'	=>	'Resource last edited by ###',
/// listParams_listParams List, search or select parameters when displaying a list
				"listParams"	=>	"Parameters",
/// listParams_listParamMultiple When displaying search, select or list parameters, this message is displayed if there are too many to reasonably display
				"listParamMultiple" => "Multiple",
				"cited"	=> "Cited",
				"year"	=>	"Year",
				"bibliography"	=>	"Bibliography",
				"listAll"	=>	"List all",
				"department"	=>	"Department",
				"institution"	=>	"Institution",
			),
/// resourceType_ Mapping WKX_resource.type to description.
		"resourceType" => array(
				'book'			=>	"Book",
/// resourceType_book_article Titled chapter in book (i.e. chapter has title not number)
				'book_article'		=>	"Book Article",
/// resourceType_book_chapter Numeric chapter in book
				'book_chapter'		=>	"Book Chapter Number",
				'web_article'		=>	"Web Article",
				'web_site'			=>	"Web Site",
				"web_encyclopedia"	=>	"Web Encyclopedia",
				"web_encyclopedia_article"	=>	"Web Encyclopedia Article",
				'journal_article'	=>	"Journal Article",
				'newspaper_article'	=>	"Newspaper Article",
				'thesis'		=>	"Thesis/Dissertation",
				'proceedings_article'	=>	"Proceedings Article",
/// resourceType_broadcast TV or Radio broadcast
				'broadcast'		=>	'Broadcast',
				'film'			=>	"Film",
/// resourceType_legal_ruling Legal Ruling or Regulation
				'legal_ruling'		=>	"Legal Rule/Regulation",
/// resourceType_software Computer software
				"software"	=>	"Software",
/// resourceType_artWork Art etc.
				"artwork"	=>	"Artwork",
/// resourceType_audiovisual Audiovisual material
				"audiovisual"	=>	"Audiovisual",
/// resourceType_case Legal cases
				"case"		=>	"Legal Case",
/// resourceType_bill Parliamentary bill (law)
				"bill"		=>	"Bill",
/// resourceType_classical Classical (historical) work
				"classical"	=>	"Classical Work",
				"conference_paper"	=>	"Conference Paper",
				"conference_poster"	=>	"Conference Poster",
/// resourceType_report Reports or documentation
				"report"	=>	"Report/Documentation",
/// resourceType_government_report Government report or documentation
				"government_report"	=>	"Government Report/Documentation",
/// resourceType_hearing Legal/Government Hearing
				"hearing"	=>	"Hearing",
/// resourceType_database Online databases
				"database"	=>	"Online Database",
				"magazine_article"	=>	"Magazine Article",
				"manuscript"	=>	"Manuscript",
/// resourceType_map Maps
				"map"		=>	"Map",
/// resourceType_chart Charts/images
				"chart"		=>	"Chart/Image",
/// resourceType_statute Statute
				"statute"	=>	"Statute",
/// resourceType_patent Patents
				"patent"	=>	"Patent",
/// resourceType_brochure Company brochure
				"brochure"	=>	"Brochure",
/// resourceType_personal Personal Communication
				"personal"	=>	"Personal Communication",
/// resourceType_unpublished Unpublished work
				"unpublished"	=>	"Unpublished Work",
/// resourceType_proceedings Conference proceedings (complete set)
				"proceedings"	=>	"Proceedings",
/// resourceType_music_album Recorded music
				"music_album"	=>	"Recorded Music Album",
				"music_track"	=>	"Recorded Music Track",
/// resourceType_music_score Sheet music
				"music_score"	=>	"Music Score",
/// resourceType_miscellaneous For anything else that does not fit into the above categories.
				'miscellaneous'		=>	"Miscellaneous",
/// resourceType_miscellaneous_section Similar to miscellaneous but a part of something else
				'miscellaneous_section'		=>	"Miscellaneous Section",
/// resourceType_genericBook Generic resource types used when creating bibliographic styles.
				"genericBook"		=>	"Generic book-type",
				"genericArticle"	=>	"Generic article-type",
				"genericMisc"		=>	"Generic miscellaneous",
			),
/// category_ Administration of categories
		"category" => array(
				"addCategory"	=>	"Add category",
				"addSubcategory"	=>	"Add subcategory",
				"editCategory"	=>	"Edit category",
				"editSubcategory"	=>	"Edit subcategory",
				"deleteCategory"	=>	"Delete category",
				"deleteSubcategory"	=>	"Delete subcategory",
				"deleteCatConfirm"		=>	"Delete category(s) ###",
				"deleteSubConfirm"		=>	"Delete subcategory(s) ###",
				"deleteWarning"		=>	"Any resource belonging to the category(s) you are deleting that does not belong to another category will be placed in the 'General' category",
			),
/// submit_ Form submit button text
		"submit" => array(
				"Submit"		=>	"Submit",
/// submit_upgradeDB To upgrade the database for an upgraded WIKINDX
				"upgradeDB" => "Upgrade the database",
				"List"			=>	"List",
				"Search"		=>	"Search",
				"Select"		=>	"Select",
				"Proceed"		=>	"Proceed",
/// submit_Reset Reset button for forms
				"Reset"			=>	"Reset",
				"Continue"		=>	"Continue",
				"Delete"		=>	"Delete",
				"Confirm"		=>	"Confirm",
				"Edit"			=>	"Edit",
				"basketAdd"		=>	"Add to basket",
				"basketRemove"	=>	"Remove from basket",
				"Add"			=>	"Add",
				"ApproveResource"	=>	"Approve resource",
				"QuarantineResource"	=>	"Quarantine resource",
				"Remove"	=>	"Remove",
				"Email"	=>	"Email",
/// submit_Cite Add citation
				"Cite"			=>	"Cite",
				"Save"	=> "Save",
				"Test"	=>	"Test",
/// submit_Cache Convert and cache attachments
				"Cache"	=>	"Convert attachments",
				"OK"	=>	"OK",
			),
/// import_ Bibliography import messages
		"import" => array(
				"category"		=>	"Category",
				"categoryPrompt"	=>	"All WIKINDX resources belong to at least one category which you chose here.  The category(s) a resource belongs to can always be edited later.",
/// import_pasteBibtex An ordinary user may cut 'n' paste bibtex entries into a textarea box for importing into the bibliography. '###' is the maximum number that the admin allows. Don't translate '@string'
				"pasteBibtex"		=>	"You may paste up to ### bibTeX entries here in addition to @string types.",
/// import_importDuplicates For file imports, allow duplicates?
				"importDuplicates"	=>	"Import duplicates",
				"storeRawLabel"		=>	"Store unused fields",
/// import_storeRawBibtex Do not translate '@string'
				"storeRawBibtex"	=>	"You may store BibTeX fields that WIKINDX does not use so that any resources later exported to BibTeX can include this original unchanged data.  Doing this, also stores the bibtex key and any @string strings that are in the imported BibTeX file.",
				"empty"		=>	"File is empty",
				"added"			=>	"No. resources added: ###",
				"discarded"		=>	"No. resources discarded (duplicates, no titles, or in the deactivated resource type list): ###",
/// import_invalidField1 If non-standard bibtex fields are found in the input file, invite the user to map these fields to wikindx fields
				"invalidField1"	=>	"Unknown fields have been found. You may map these fields to WIKINDX fields -- no duplicate mapping is allowed.",
				"invalidField2"	=>	"Where an unknown field is mapped to a WIKINDX field that would normally be automatically mapped to a standard input field, the unknown field mapping takes precedence.",
				"invalidField3"	=>	"Unknown fields have been found. You may map these fields to custom fields -- no duplicate mapping is allowed.",
				"file"			=>	"Import File",
				"tag"			=>	"Tag this import so you can do a mass select or delete later",
/// import_executionTimeExceeded With large imports that would go over php.ini's max_execution time, WIKINDX splits the imports into chunks
				"executionTimeExceeded"	=>	"'max_execution_time' (### seconds) in php.ini was about to be exceeded.  WIKINDX is importing the bibliography in chunks.",
				"addedChunk"			=>	"No. resources added this chunk: ###",
			),
/// user_ Users in a multi user WIKINDX
		"user" => array(
				"passwordConfirm"	=>	"Confirm password",
				"username"		=>	"Username",
				"deleteConfirm"		=>	"Delete user(s): ###",
				"fullname"		=>	"Full name",
				"department"	=>	"Department",
				"institution"	=>	"Institution",
				"isCreator"	=>	"User is creator",
				"password"		=>	"Password",
				"email"			=>	"Email address",
/// user_emailText Do not use any TAB or CR in this one or the message formatting will be messed up in the email client.
				"emailText"		=>	"You recently registered to use our WIKINDX. If this was not you, please ignore this email.  Otherwise, to complete the registration process, please go to the following address and follow the instructions there:",
/// user_emailText2 Do not use any TAB or CR in this one or the message formatting will be messed up in the email client.
				"emailText2"		=>	"Thank you for registering to use our WIKINDX.  Please keep this email for reference:",
/// user_emailText3 Do not use any TAB or CR in this one or the message formatting will be messed up in the email client.
				"emailText3"		=>	"You recently updated your details on our WIKINDX.  Please keep this email for reference:",
/// user_cookie For optional cookies
				"cookie"		=>	"Remember me",
/// user_forget1 If the user forgets a password, they can answer a series of questions they have earlier supplied to get a new temporary password emailed to them
				"forget1"	=>	"If you forget your password at some point in the future, you may have a temporary password emailed to you by correctly answering up to three questions. You should ensure that the email address stored by WIKINDX is always up to date.",
				"forget2"	=>	"Enter up to three short questions and answers:",
				"forget3"	=>	"Question ###",
				"forget4"	=>	"Answer ###",
				"forget5"	=>	"If you have just loaded this page and a question is displayed here but no answer, then the answer is already stored encrypted in the database. You must, however, supply answers when clicking on the 'Edit' button.",
				"forget6"	=>	"Forgotten your password?",
				"forget7"	=>	"Enter either your username or email as stored in the WIKINDX.",
				"forget8"	=>	"Answer the following questions:",
				"forget9"	=>	"You recently requested a reset password for your WIKINDX account. A temporary password is given below. You should log into this WIKINDX as soon as possible and change this password in your user settings.",
				"forget10"	=>	"Thank you for correctly answering the questions. An email has been sent to your address with a temporary password which you should change the next time you log into WIKINDX.",
				"forget11"	=>	"Return to log in prompt",
				"masterBib"		=>	"WIKINDX Master Bibliography",
				"bibliography"		=>	"Bibliography",
				"deleteConfirmBib"	=>	"Delete bibliography ###",
/// user_unknown When a user has been deleted but her input remains, display this when viewing a resource and its associated text
				"unknown"		=>	"Deleted user",
				"deleteUserMetadata1"	=>	"How do you wish to deal with the deleted users' quotes, paraphrases, musings, ideas, and
				comments (metadata)? Metadata left unchanged will be shown as
				added by 'Deleted user'. If you choose to delete users' metadata, other users' comments relating to
				those metadata will also be deleted. A deleted user's resources are not deleted. If you choose not
				to delete metadata, all private and group musings, comments, and ideas will be made public.",
				"deleteUserMetadata2"	=>	"Leave unchanged",
				"deleteUserMetadata3"	=>	"Transfer to superadmin",
				"deleteUserMetadata4"	=>	"Delete",
/// user_bib This user's bibliographies
				"bib"			=>	"My Bibliographies",
				"useBib"		=>	"Use this bibliography for browsing",
				"displayBib"		=>	"Display bibliography details",
				"bibTitle"		=>	"Title",
				"bibDescription"	=>	"Description",
				"noBibs"		=>	"You do not yet have any bibliographies",
				"createBib"		=>	"Create a new user bibliography",
				"createGroupBib"		=>	"Create a new group bibliography",
				"deleteBib"		=>	"Delete bibliography",
				"editBib"		=>	"Edit bibliography details",
				"deleteFromBib"		=>	"Delete from bibliography",
/// user_otherBibs displayed in select box - other users' bibliographies
				"otherBibs"		=>	"______OTHER USERS______",
/// user_userBibs displayed in select box
				"userBibs"		=>	"___MY BIBLIOGRAPHIES___",
/// user_userGroupBibs displayed in select box
				"userGroupBibs"		=>	"___GROUP BIBLIOGRAPHIES___",
				"numResources"	=>	"Number of resources",
				"admin"			=>	"Administrator",
/// user_notification Email notification of resource additions/edits etc.
				"notification"		=>	"Email notification",
				"notifyNone"		=>	"Never",
				"notifyAll"		=>	"When any resource or its text is added/edited by other users",
				"notifyMyBib"		=>	"When any resource from my bibliographies or its text is added/edited by other users",
				"notifyMyCreator"		=>	"When any resource or its text for which I am a creator is added/edited by other users",
				"notifyAdd"		=>	"When a resource has been added",
				"notifyEdit"	=>	"When a resource or its text has been edited",
				"notifyThreshold" => "Provide a digest of additions/edits after x number of days since the last notification",
				"notifyImmediate" => "Immediately",
				"notifyDigestThreshold" => "At or below this value, receive a list of all added or edited resources; above this value, receive just the number of added or edited resources",
				"notify"		=>	"### has added or edited the following resource or its text",
/// user_notifyMass1 Email notification of new resources when there has been a mass import of more than 10.  This will produce something like: "Mark has added 29 new resources"
				"notifyMass1"	=>	"### has added",
				"notifyMass2"	=>	"### new resources",
				"notifyMass3"	=>	"### resources have been added or edited since your last notification",
				"notifyMass4"	=>	"The following resources have been added or edited since your last notification",
/// user_addGroupsToBib When creating or editing a user group bibliography, the group admin can add user groups with access to this bibliography
				"addGroupsToBib" => "Give a user group write access to this bibliography",
				"noGroups" => "You are not yet the administrator of any user groups",
				"createGroup" => "Create a new user group",
				"deleteGroup"		=>	"Delete user group",
				"deleteGroup2"	=>	"(This will also delete any user group bibliographies for this group)",
				"deleteConfirmGroup"	=>	"Delete user group###",
				"editGroup"		=>	"Edit user group",
				"groups"	=> "My User Groups",
				"groupTitle"		=>	"Title",
				"groupDescription"	=>	"Description",
				"groupUserAdd" => "Add users to this group",
				"groupUserDelete" => "Remove users from this group",
				"userTags"	=>	"User tags",
				"noUserTags"	=>	"You do not yet have any user tags",
				"createUserTag"		=>	"Create a new user tag",
				"deleteUserTag"		=>	"Delete user tag",
				"editUserTag"		=>	"Edit user tag",
				"deleteConfirmUserTag"	=>	"Delete user tag ###",
/// user_emailText4 Text that is emailed to the WIKINDX admin advising of a request for user registration.  'Admin' and 'Users' should be the same translations as in the menu array.
				"emailText4"	=>	"There has been a request for WIKINDX registration.  In order to manage this request, please log on and use the 'Admin|Users' menu.",
/// user_emailText5 A request for user registration has been declined.  '###' is the URL of the WIKINDX
				"emailText5"	=>	"You recently requested registration at: ###. Unfortunately, the WIKINDX administrator has declined your request.",
/// user_pendingRegistration1 The Administrator has registration requests to manage
				"pendingRegistration1"	=>	"Pending registration requests",
				"pendingRegistration2"	=>	"Potential users will be emailed your decision with those accepted being invited to complete the registration process.",
				"registrationAccept"	=>	"Accept registration",
				"registrationDecline"	=>	"Decline registration",
				"noUsers"	=>	"There are no users requesting registration",
				"authorizedUsers"	=>	"Authorized users",
				"blockedUsers"	=>	"Blocked users",
			),
/// cite_ Messages for adding citations to quotes, notes, musings , comments etc. and for administration of citation templates within bibliographic style creation/editing
		"cite" => array(
				"cite"			=>	"Cite",
/// cite_preText Text preceeding and following citations e.g. (see Grimshaw 1999; Boulanger 2004 for example): 'see' is preText and 'for example' is postText
				"preText"	=>	"Preliminary text",
				"postText"	=>	"Following text",
				'pages'	=>	'Pages',
			),
/// creators_ Various types of creators
		"creators" => array(
				"author"	=>	"Authors",
				"editor"	=>	"Editors",
				"translator"	=>	"Translators",
				"reviser"	=>	"Revisers",
				"seriesEditor"	=>	"Series Editors",
/// creators_director For films etc.
				"director"	=>	"Director",
				"producer"	=>	"Producer",
				"company"	=>	"Company",
/// creators_artist For artwork
				"artist"	=>	"Artist",
				"performer"	=>	"Performer",
/// creators_counsel For legal cases
				"counsel"	=>	"Counsel",
				"judge"	=>	"Judge",
/// creators_attributedTo For classical works of doubtful provenance
				"attributedTo"	=>	"Attributed to",
/// creators_cartographer Map makers
				"cartographer"	=>	"Cartographer",
/// creators_creator Charts/images
				"creator"	=>	"Creator",
/// creators_inventor For patents
				"inventor"	=>	"Inventor",
				"issuingOrganisation"	=>	"Issuing Organisation",
				"agent"		=>	"Agent/Attorney",
/// creators_intAuthor International patent author
				"intAuthor"	=>	"International Author",
/// creators_recipient Personal Communication
				"recipient"	=>	"Recipient",
/// creators_composer For Musical works
				"composer"	=>	"Composer",
				"conductor"	=>	"Conductor",
/// creators_supervisor for theses
				"supervisor" => "Supervisors",
/// creators_creatorExists Advice on what to do when editing a creator name and the new name already exists in the database.
				"creatorExists"	=>	"If you proceed, this edited creator will be deleted and all references in the database to it will be replaced by references to the pre-existing creator.",
				"creators"	=>	"Creators",
/// creators_alias Some creators might have multiple names . . .
				"alias"	=>	"Alias: ###",
			),
/// custom_ For managing custom database fields
		"custom" => array(
/// custom_label The label given to the field
				"label"		=>	"Label",
/// custom_size The field storage space can be small or large
				"size"		=>	"The database size allocation for the field can be small (max. 255 characters) or large",
				"small"		=>	"Small",
				"large"		=>	"Large",
				"warning"	=>	"Deleting these fields will also remove any resource data belonging to the field.",
				"addLabel"		=>	"Add a field",
				"deleteLabel"		=>	"Delete fields",
				"editLabel"		=>	"Edit fields",
				"deleteConfirm"		=>	"Delete fields(s) ###",
				"customFields"		=>	"Custom fields",
			),
/// statistics_ Messages for the administrator statistics section
		"statistics" => array(
				"maxAccesses"	=>	"Highest number of views for any resource:",
				"minAccesses"	=>	"Lowest number of views for any resource:",
				"firstAdded"	=>	"Date the first resource was added:",
				"lastAdded"	=>	"Date the last resource was added:",
/// statistics_meanAddedResource 'mean' is a synonym for 'average'
				"meanAddedResource"	=>	"Mean date of added resources:",
				"totalResources"	=>	"Total resources:",
				"totalQuotes"	=>	"Total quotes:",
				"totalParaphrases"	=>	"Total paraphrases:",
				"totalMusings"	=>	"Total musings:",
				"userResourceTotal"	=>	"Greatest number of resources input by any one user:",
				"userQuoteTotal"	=>	"Greatest number of quotes input by any one user:",
				"userParaphraseTotal"	=>	"Greatest number of paraphrases input by any one user:",
				"userMusingTotal"	=>	"Greatest number of public musings input by any one user:",
				"resourceTypes"		=>	"Number of Resource Types:",
/// statistics_emailSubject Subject for email of statistics to users
				"emailSubject" => "### (Usage statistics)",
				"emailIntro" => "Your monthly statistics for ###. The indices are an indication of ranking weighted according to how long the resource has been available.  The closer to 100% the index is, the higher the ranking.  The downloads index is calculated across all attachments a resource has while the popularity index is a combination of views index and downloads index.",
				"emailViewsMonth" => "### views in the last month",
				"emailViewsTotal" => "(### views in total)",
				"emailDownloadsMonth" => "### attachment downloads in the last month",
				"emailDownloadsTotal" => "(### attachment downloads in total)",
				"userStats"	=>	"Statistics are shown for each user as: user/total (% of total)",
				"noUserStats"	=>	"No user statistics are available",
				"userResources"	=>	"Resources",
				"userQuotes"	=>	"Quotes",
				"userParaphrases"	=>	"Paraphrases",
				"userMusings"	=>	"Public musings",
			),
/// footer_ Messages for display in the WIKINDX footer for each page
		"footer" => array(
				"resources" => "Total resources:",
				"queries" => "Database queries:",
				"execution" => "Script execution:",
				"dbtime"	=>	"DB execution:",
				"user" => "Username:",
				"style" => "Style:",
				"bib" => "Bibliography:",
			),
/// cms_ Messages for handling CMS (Content Management System) code
		"cms"	=>	array(
/// cms_introduction1 Do not translate the README_CMS link
				"introduction1"	=>	"If you wish to display elements of your WIKINDX in a Content Management System (CMS) you may generate CMS 'replacement' tags here and also interrogate the database for WIKINDX ID numbers.  WIKINDX only generates the CMS 'replacement' tag for you to paste into your CMS source -- the coding and parsing of that tag in your CMS is a task for you to do.  Refer to <a href=\"docs/README_CMS.txt\">README_CMS</a>.",
				"introduction2"	=>	"If you wish to display a list from your WIKINDX in a Content Management System (CMS), copy the text
				below to a text file on your CMS system. Refer to <a href=\"docs/README_CMS.txt\">README_CMS</a>.",
				"displayIds"	=>	"Display WIKINDX ID numbers",
/// cms_generateCmsTag Generate a CMS (Content Management System) 'replacement' tag
				"generateCmsTag"	=>	"Generate a CMS tag",
/// cms_cmsTagStart The initial section of a CMS (Content Management System) 'replacement' tag
				"cmsTagStart"	=>	"CMS tag starts with",
/// cms_cmsTagEnd The closing section of a CMS (Content Management System) 'replacement' tag
				"cmsTagEnd"	=>	"CMS tag ends with",
				"pageStart"	=>	"Page start",
				"pageEnd"	=>	"Page end",
				"tag"	=>	"Generated tag",
/// cms_preText Text preceeding and following citations e.g. (see Grimshaw 1999; Boulanger 2004 for example): 'see' is preText and 'for example' is postText
				"preText"	=>	"Preliminary text",
				"postText"	=>	"Following text",
			),
/// news_ Administering and display of news items
		"news" => array(
				"noNews"		=>	"There is no news available to view",
				"deleteConfirm"		=>	"Delete news ###",
				"title"			=>	"Title",
				"body"			=>	"Main Text",
				"emailNews"		=>	"Email the edited news to registered users",
			),
/// tinymce_ Mesages for dialog tiny mce boxes
		"tinymce" => array(
				"headingAddTable"	=>	"Add Table",
				"headingAddImage"	=>	"Add Image",
				"headingAddLink"	=>	"Add Link",
				"tableColumns"	=>	"Columns",
				"tableRows"	=>	"Rows",
				"imagePath"	=>	"Image URL",
				"linkPath"	=>	"URL",
				"fileName"	=>	"File name",
				"size"	=>	"Size",
				"lastUpdated"	=>	"Last updated",
				"upload"	=>	"Upload file",
			),
// END__LOCALIZATION__MODULE__EDIT
		);
	}
}
?>