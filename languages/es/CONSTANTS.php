<?php
/**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 */

/**********************************************************************************
CONSTANTS_es WIKINDX localization class -- Español
**********************************************************************************/

// START__LOCALIZATION__MODULE__COPY
class CONSTANTS_es
{
// Constructor
	function __construct()
	{
// When using the word processor how are extra, unnamed authors indicated?  In English this would be 'et al.' as in:
// 'According to Grimshaw et al., "blah blah blah".
		$this->textEtAl = 'et al.';
// When using the word processor, how is singular possessive defined.  This is only for a single creator's surname and, like 'et al.' above, is used
// to detect if a creator name should be removed from the in-text citation if that name is in the same sentence as the citation.
// If this value contains an apostrophe (as in the English Grimshaw's), then it should be in double quotes (or else the PHP script will fail).
// English has two forms (the second below for names that end in 's').
// From wikindx v3.4.7 onwards, $this->possessiveArray is used allowing unlimited possessive forms.  If it exists and is an array,
// $this->possessive1 and $this->possessive2 will be ignored.
// If there are no possessive forms in your language, $this->possessiveArray should exist and be empty.
// < v3.4.7 commented out.
//		$this->possessive1 = "'s";
//		$this->possessive2 = "'";
// >= v3.4.7 possessive form
		$this->possessiveArray = array(
											"'s",
											"'",
			);
// What characters indicate the start and end of a quotation in the word processor text?  (Be careful to escape a double quote if it is inside
// double quotes by using '\'.  A single quote can be given safely as "'".)
		$this->startQuotation = "\"";
		$this->endQuotation = "\"";
// When using the word processor, for certain styles the text must be split up into sentences -- e.g. APA requires that year must follow the author name and
// page no. follows the citation if the citation appears in the same sentence as the author surname.  Normally sentences are recognized
// by '. ' (dot followed by at least one space).  This array handles other groups of characters (commonly abbreviations) that may appear in a sentence
// but do not indicate a sentence divider.  If there are no such abbreviations, the array must exist and be empty e.g. $this->abbreviations = array();
// The final '.' (dot) of the abbreviation must NOT be given.
// Matching is case insensitive.
// 'et al.' and equivalent is dealt with above.
// '...' and multiple dots consisting of two or more dots are dealt with in the code.
// Abreviations such as U.S.A., U.S.S.R. are dealt with in the code (must be capital English characters)
// The syntax of the matching rule is that a sentence divider has been found if there is a dot followed by a space where the dot is not preceeded a member of the array.
// For example, the 'e.g.' in "Blah blah blah, e.g. more blah, and even more blah" is not seen as a sentence divider if 'e.g' (no final dot) is a member of this array.
// Remove the English abbreviations unless they are used in your language.
		$this->abbreviations = array(
				'e.g',
				'etc', // etcetera
				'i.e',
				'Bros', // short for Brothers (as in Warner Bros.)
				'no', // number
				'c', // circa
				'ca', // circa
				'cf', // compare with
				'secs', // seconds
				'msecs', // milliseconds
				'Ph. D', // doctorate
				'MS', // Manuscript
				'viz',
				'v', // versus
				'N. B', // Nota bene
				'Fig',	// Figure
				'fig', // figure
				'co', // company
			);
	}
// Convert cardinal to ordinal numbers.
// The coding here is likely to be different for each language.  If there is no logic that can be programmed for your language, then it is recommended that
// you use English ordinals as given here so that something is printed and the script does not fail.
// $modulo holds the remainder of $cardinal/100 or $cardinal/10.
// Optional $field allows conditions based on gender.  Currently, $field will be either 'edition' or 'dayMonth' (day of the month).  Additionally, $field can be used
// to determine whether ordinals will be used at all.  For example, if this is French and $field == 'dayMonth', ordinals are used solely for '1er'.
// (languages/fr/CONSTANTS.php has an example using $field.)
//  The French equivalent to this function would be:
/*
	function cardinalToOrdinal($cardinal, $field = FALSE)
	{
		if(($field == 'dayMonth') && ($cardinal != 1))
			return $cardinal; // no change
		$modulo = $cardinal % 10;
		if($modulo == 1) // 1st
		{
			if($field == 'edition')
				return $cardinal . 'Ã¨re';
			return $cardinal . 'er';
		}
		else // all other numbers
			return $cardinal . 'Ã¨me';
	}
*/
	function cardinalToOrdinal($cardinal, $field = FALSE)
	{
		$modulo = $cardinal % 100;
		if(($modulo == 11) || ($modulo == 12) || ($modulo == 13)) // 11th, 12th and 13th
			return $cardinal . 'º';
		$modulo = $cardinal % 10;
		if(($modulo >= 4) || !$modulo) // all other numbers
			return $cardinal . 'º';
		if($modulo == 1) // 1st
			return $cardinal . 'º';
		if($modulo == 2) // 2nd
			return $cardinal . 'º';
		if($modulo == 3) // 3rd
			return $cardinal . 'º';
	}
// convert cardinal numbers
	function convertNumbers($number = FALSE, $convert = FALSE)
	{
		if(!is_numeric($number))
			return $number;
// month number to long name
		$longMonth = $this->monthToLongName();
// short month
		$shortMonth = $this->monthToShortName();
// cardinal -> ordinal word
		$ordinalWord = $this->cardinalToOrdinalWord();
// arabic -> roman numerals
		$roman = $this->cardinalToRoman();
/****************************
 START TRANSLATION
****************************/
// arabic -> ordinal (e.g. 3 -> 3rd, 10 -> 10th)
// Usually used for edition numbers in bibliographic styles that require e.g. '10th edition' rather than 'edition 10' - superseded by cardianlToOrdinal() function above
// A maximum of 50 seems a reasonable number to go up to....
// If necessary, you may need to add more loops or individual array elements.
		for($i = 4; $i <= 20; $i++)
			$ordinal[$i] = $i . 'th';
		for($i = 24; $i <= 30; $i++)
			$ordinal[$i] = $i . 'th';
		for($i = 34; $i <= 40; $i++)
			$ordinal[$i] = $i . 'th';
		for($i = 44; $i <= 50; $i++)
			$ordinal[$i] = $i . 'th';
		$ordinal[1] = '1º';
		$ordinal[21] = '21º';
		$ordinal[31] = '31º';
		$ordinal[41] = '41º';
		$ordinal[2] = '2º';
		$ordinal[22] = '22º';
		$ordinal[32] = '32º';
		$ordinal[42] = '42º';
		$ordinal[3] = '3º';
		$ordinal[23] = '23º';
		$ordinal[33] = '33º';
		$ordinal[43] = '43º';
/****************************
 END TRANSLATION
****************************/
// arabic -> cardinal (i.e. no change)
		$cardinal = range(0, 50);
// !$number, we are simply loading the arrays for use elsewhere, so return
		if(!$number)
			return;
// get rid of leading '0' if necessary
		if($number < 10)
			$number += 0;
// If that number is not actually in array, we return the number as is
		if(array_key_exists($number, ${$convert}))
			return ${$convert}[$number];
		return $number;
	}
// Convert month to long name
	function monthToLongName()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
				1	=>	'Enero',
				2	=>	'Febrero',
				3	=>	'Marzo',
				4	=>	'Abril',
				5	=>	'Mayo',
				6	=>	'Junio',
				7	=>	'Julio',
				8	=>	'Agosto',
				9	=>	'Septiembre',
				10	=>	'Octubre',
				11	=>	'Noviembre',
				12	=>	'Diciembre',
/****************************
 END TRANSLATION
****************************/
			);
	}
// Convert month to short name
	function monthToShortName()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
				1	=>	'Ene',
				2	=>	'Feb',
				3	=>	'Mar',
				4	=>	'Abr',
				5	=>	'May',
				6	=>	'Jun',
				7	=>	'Jul',
				8	=>	'Aug',
				9	=>	'Sep',
				10	=>	'Oct',
				11	=>	'Nov',
				12	=>	'Dec',
/****************************
 END TRANSLATION
****************************/
			);
	}
// convert cardinal to roman
	function cardinalToRoman()
	{
// arabic -> roman numerals
// Do NOT translate
		return array(
				1	=>	'I',
				2	=>	'II',
				3	=>	'III',
				4	=>	'IV',
				5	=>	'V',
				6	=>	'VI',
				7	=>	'VII',
				8	=>	'VIII',
				9	=>	'IX',
				10	=>	'X',
				11	=>	'XI',
				12	=>	'XII',
				13	=>	'XIII',
				14	=>	'XIV',
				15	=>	'XV',
				16	=>	'XVI',
				17	=>	'XVII',
				18	=>	'XVIII',
				19	=>	'XIX',
				20	=>	'XX',
				21	=>	'XXI',
				22	=>	'XXII',
				23	=>	'XXIII',
				24	=>	'XXIV',
				25	=>	'XXV',
				26	=>	'XXVI',
				27	=>	'XXVII',
				28	=>	'XXVIII',
				29	=>	'XXIX',
				30	=>	'XXX',
				31	=>	'XXXI',
				32	=>	'XXXII',
				33	=>	'XXXIII',
				34	=>	'XXXIV',
				35	=>	'XXXV',
				36	=>	'XXXVI',
				37	=>	'XXXVII',
				38	=>	'XXXVIII',
				39	=>	'XXXIX',
				40	=>	'XXXX',
				41	=>	'XXXXI',
				42	=>	'XXXXII',
				43	=>	'XXXXIII',
				44	=>	'XXXXIV',
				45	=>	'XXXXV',
				46	=>	'XXXXVI',
				47	=>	'XXXXVII',
				48	=>	'XXXXVIII',
				49	=>	'XXXXIX',
				50	=>	'L',
			);
	}
// convert ordinal to word
	function cardinalToOrdinalWord()
	{
		return array(
/****************************
 START TRANSLATION
****************************/
// Usually used for edition numbers in bibliographic styles that require words rather than arabic numerals.
// Any numbers not listed here will be returned without change.  e.g. here, 51 will be returned as 51 rather than 'fifty-first'.
// A maximum of 50 seems a reasonable number to go up to....
						'1'	=>	'Primero',
						'2'	=>	'Segundo',
						'3'	=>	'Tercero',
						'4'	=>	'Cuarto',
						'5'	=>	'Quinto',
						'6'	=>	'Sexto',
						'7'	=>	'Séptimo',
						'8'	=>	'Octavo',
						'9'	=>	'Noveno',
						'10'	=>	'Décimo',
						'11'	=>	'Undécimo',
						'12'	=>	'Duodécimo',
						'13'	=>	'Decimotercero',
						'14'	=>	'Decimocuarto',
						'15'	=>	'Decimoquinto',
						'16'	=>	'Decimosexto',
						'17'	=>	'Decimoséptimo',
						'18'	=>	'Decimooctavo',
						'19'	=>	'Decimonoveno',
						'20'	=>	'Vigésimo',
						'21'	=>	'Vigésimoprimero',
						'22'	=>	'Vigésimosegundo',
						'23'	=>	'Vigésimotercero',
						'24'	=>	'Vigésimocuarto',
						'25'	=>	'Vigésimoquinto',
						'26'	=>	'Vigésimosexto',
						'27'	=>	'Vigésimoséptimo',
						'28'	=>	'Vigésimooctavo',
						'29'	=>	'Vigésimonoveno',
						'30'	=>	'Trigésimo',
						'31'	=>	'Trigésimoprimero',
						'32'	=>	'Trigésimosegundo',
						'33'	=>	'Trigésimotercero',
						'34'	=>	'Trigésimocuarto',
						'35'	=>	'Trigésimoquinto',
						'36'	=>	'Trigésimosexto',
						'37'	=>	'Trigésimoséptimo',
						'38'	=>	'Trigésimooctavo',
						'39'	=>	'Trigésimonoveno',
						'40'	=>	'Cuadragésimo',
						'41'	=>	'Cuadragésimoprimero',
						'42'	=>	'Cuadragésimosegundo',
						'43'	=>	'Cuadragésimotercero',
						'44'	=>	'Cuadragésimoquinto',
						'45'	=>	'Cuadragésimoquinto',
						'46'	=>	'Cuadragésimosexto',
						'47'	=>	'Cuadragésimoséptimo',
						'48'	=>	'Cuadragésimooctavo',
						'49'	=>	'Cuadragésimonoveno',
						'50'	=>	'Quincuagésimo',
/****************************
 END TRANSLATION
****************************/
			);
	}
/**
* Format dates and times for localization (e.g. when viewing lists of resources in a multi-user wikindx 'Last edited by: xxx <date/time>)
*
* $timestamp comes in from the database in the format 'YYYY-MM-DD HH:MM:SS' e.g. 2013-01-31 15:54:55
*/
	public function dateFormat($timestamp)
	{
 		setlocale(LC_ALL, 'es_ES.utf8');
// If you're happy with $timestamp's current format, all you need to do is uncomment this line:
//		return $timestamp;
// Otherwise. . .
		$unixTime = strtotime($timestamp);
// You have two choices for setting the time.  The first attempts to translate day and month names but will not
// work if the chosen locale is not available on your system.
// See
// http://www.php.net/manual/en/function.setlocale.php
// and
// http://www.php.net/manual/en/function.strftime.php
//
// The second is locale independent
// See:
// http://www.php.net/manual/en/function.date.php
//
// For the first option, set $dateFormat = 1; otherwise set $dateFormat = 2;
/****************************
 START TRANSLATION
****************************/
		$dateFormat = 2;
		if($dateFormat == 1)
		{
			$format = '%c'; // e.g. Tue Feb 5 00:45:10 2009
			setlocale(LC_ALL, "es_ES.utf8"); // Spanish locale
			$timestamp = strftime($format, $unixTime);
		}
		else if($dateFormat == 2)
		{
			$config = FACTORY_CONFIG::getInstance();
			if(!isset($config->WIKINDX_TIMEZONE) || !$config->WIKINDX_TIMEZONE)
				date_default_timezone_set(date_default_timezone_get()); // default is UTC
			else
				date_default_timezone_set($config->WIKINDX_TIMEZONE);
			$format = "d M Y H:i:s e";
			$timestamp = date($format, $unixTime);
		}
		else
			$timestamp = '';
/****************************
 END TRANSLATION
****************************/
		return $timestamp;
	}
}
?>