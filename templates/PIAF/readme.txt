Author:
 Stéphane Aulery <lkppo@users.sourceforge.net>, 2016, 2017
 Mark Grimshaw <sirfragalot@users.sourceforge.net>, 2012, 2013, 2016, 2017

Illustration:

 favicon.ico is a public domain work of Supratim Nayak published at
 https://www.veryicon.com/icons/system/multipurpose-alphabet/

 Icons come from from FatCow, Free Icon Set (CC BY 3.0): www.fatcow.com.