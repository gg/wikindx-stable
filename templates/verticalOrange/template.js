/**********************************************************************************
WIKINDX: Bibliographic Management system.
Copyright (C)

This program is free software; you can redistribute it and/or modify it under the terms 
of the GNU General Public License as published by the Free Software Foundation; either 
version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; 
if not, write to the 
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

The WIKINDX Team 2011
sirfragalot@users.sourceforge.net

**********************************************************************************/

/**
* template.js
*
* This file provides the opportunity to alter the CSS display dependent upon certain browsers and to define various pop-up windows.
* template.js is loaded from header.tpl.
* initTemplate() is called in the onLoad function of the body tag in header.tpl.
* The minimum template.js should look like:

function initTemplate()
{
}
*/

var agt=navigator.userAgent.toLowerCase();
var is_nav  = ((agt.indexOf('mozilla')!=-1) && (agt.indexOf('spoofer')==-1)
                && (agt.indexOf('compatible') == -1) && (agt.indexOf('opera')==-1)
                && (agt.indexOf('webtv')==-1) && (agt.indexOf('hotjava')==-1));
var is_ie     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
var is_opera = (agt.indexOf("opera") != -1);
var is_win   = ((agt.indexOf("win")!=-1) || (agt.indexOf("16bit")!=-1));
var is_mac    = (agt.indexOf("mac")!=-1);
var is_safari = (agt.indexOf("safari") != -1);
var is_konqueror = (agt.indexOf("konqueror") != -1);
var is_gecko = (agt.indexOf("gecko") != -1);

/* ===== common JavaScript functions ===== */
/* ===== Code that should remain here although you can edit various parameters (window sizes, for example). ===== */
