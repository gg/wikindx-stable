{**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 *}

<table class="contentAbstract">
<tr>
	<td>
		<strong><span class="small">{$resourceSingle.abstract.title}</span></strong>
		{if isset($resourceSingle.abstract.editLink)}{$resourceSingle.abstract.editLink}&nbsp;&nbsp;{/if}{if isset($resourceSingle.abstract.deleteLink)}&nbsp;&nbsp;{$resourceSingle.abstract.deleteLink}{/if}
	</td>
</tr>

{if isset($resourceSingle.abstract.abstract)}
<tr class="alternate1">
	<td>
		{$resourceSingle.abstract.abstract}
		{if isset($multiUser)}
		<br><span class="hint">{$resourceSingle.abstract.userAdd}&nbsp;&nbsp;{$resourceSingle.abstract.userEdit}</span>
		{/if}
	</td>
</tr>
{/if}
</table>