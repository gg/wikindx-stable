{**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 *}

<!-- begin header template -->
{if $displayPopUp == false} {* Some wikindx pages are popups needing no titlebar. *}
<!-- TABLE for WIKINDX title and WIKINDX sourceforge link -->
<header>
<div id="title"><h1>{$headTitle}</h1></div>
<div id="logo">
<a href="{$wkx_link}" target="_blank"><img src="{$tplPath}/images/wikindx5logo-anim.gif" alt="WIKINDX SourceForge" title="WIKINDX SourceForge"></a>
</div>
</header>
{/if}
<!-- end header template -->