{**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 *}

<div class="contentNote">
<strong>
		<span class="small">{$resourceSingle.note.title}
		</span></strong>{if isset($resourceSingle.note.editLink)}&nbsp;&nbsp;{$resourceSingle.note.editLink}{/if}{if isset($resourceSingle.note.deleteLink)}&nbsp;&nbsp;{$resourceSingle.note.deleteLink}{/if}

{if isset($resourceSingle.note.note)}
	<div class="alternate1">

	{$resourceSingle.note.note}
		{if isset($multiUser)}
		<br><span class="hint">{$resourceSingle.note.userAdd}&nbsp;&nbsp;{$resourceSingle.note.userEdit}</span>
		{/if}

</div>
{/if}
</div>