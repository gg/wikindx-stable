{**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 *}
{if isset($pagingList)}
{if $pagingList != false}
<div class="contentPaging">
	{"&nbsp;&nbsp;|&nbsp;&nbsp;"|implode:$pagingList}
</div>
{/if}
{/if}