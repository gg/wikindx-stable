{**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 *}

<table class="contentNote">
<tr>
		<td><strong><span class="small">{$resourceSingle.note.title}</span></strong>{if isset($resourceSingle.note.editLink)}&nbsp;&nbsp;{$resourceSingle.note.editLink}{/if}{if isset($resourceSingle.note.deleteLink)}&nbsp;&nbsp;{$resourceSingle.note.deleteLink}{/if}</td>
</tr>

{if isset($resourceSingle.note.note)}
	<tr class="alternate1">
	<td>
	{$resourceSingle.note.note}
		{if isset($multiUser)}
		<br><span class="hint">{$resourceSingle.note.userAdd}&nbsp;&nbsp;{$resourceSingle.note.userEdit}</span>
		{/if}
	</td>
</tr>
{/if}
</table>
