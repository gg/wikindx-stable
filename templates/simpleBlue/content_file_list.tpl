{**
 * WIKINDX : Bibliographic Management system.
 * @link http://wikindx.sourceforge.net/ The WIKINDX SourceForge project
 * @author The WIKINDX Team
 * @copyright 2017 Mark Grimshaw <sirfragalot@users.sourceforge.net>
 * @license https://creativecommons.org/licenses/by-nc-sa/2.0/legalcode CC-BY-NC-SA 2.0
 *}

<table class="contentFileList">
{section loop=$fileList name=rows}
<tr class="{cycle values="alternate1,alternate2"}">
	<td>{if isset($fileList[rows])}{$fileList[rows]}{/if}</td>
	<td>{if isset($fileListIds[rows])}{$fileListIds[rows]}{/if}</td>
</tr>
{/section}
</table>